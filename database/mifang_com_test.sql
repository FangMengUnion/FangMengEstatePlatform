-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2021-01-05 09:01:30
-- 服务器版本： 5.5.62-log
-- PHP Version: 7.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mifang_com_test`
--

-- --------------------------------------------------------

--
-- 表的结构 `mf_admin`
--

CREATE TABLE `mf_admin` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `pid` int(11) NOT NULL COMMENT '父组织',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `mobile` varchar(16) NOT NULL COMMENT '手机号',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `avatar` varchar(100) NOT NULL DEFAULT '' COMMENT '头像',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(59) NOT NULL DEFAULT '' COMMENT 'Session标识',
  `status` varchar(30) NOT NULL DEFAULT 'normal' COMMENT '状态',
  `cop_id` int(11) NOT NULL COMMENT '所属公司',
  `loginip` varchar(64) NOT NULL,
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  `audit_uid` int(11) DEFAULT NULL COMMENT '审核人',
  `audit_ip` varchar(64) DEFAULT NULL COMMENT '审核ip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_admin`
--

INSERT INTO `mf_admin` (`id`, `pid`, `username`, `mobile`, `nickname`, `password`, `salt`, `avatar`, `email`, `loginfailure`, `logintime`, `createtime`, `updatetime`, `token`, `status`, `cop_id`, `loginip`, `audit_time`, `audit_uid`, `audit_ip`) VALUES
(1, 0, 'admin', '', 'Admin', '27da204f3bf4aeb94448dc37fc466d82', 'Uvn4Fg', '/assets/img/avatar.png', 'xundh@qq.com', 0, 1609807040, 1492186163, 1609807040, '6e988bc2-cae1-415f-b9e9-e9cc9ccb3032', 'normal', 1, '39.162.148.234', '0000-00-00 00:00:00', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_admin_log`
--

CREATE TABLE `mf_admin_log` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(1500) NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) DEFAULT NULL COMMENT '操作时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员日志表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_admin_log`
--

INSERT INTO `mf_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(1, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"9d337b231ee30f1c3c42c9d57cc3b8df\",\"username\":\"admin\",\"captcha\":\"e7kf\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082643),
(2, 1, 'admin', '/admin2020.php/newhouse/building/labels/index', '新房楼盘 楼盘标签 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"label_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"label_name\",\"keyField\":\"id\",\"searchField\":[\"label_name\"],\"label_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082769),
(3, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082775),
(4, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082777),
(5, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"cop_name\",\"keyField\":\"id\",\"searchField\":[\"cop_name\"],\"cop_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082800),
(6, 1, 'admin', '/admin2020.php/newhouse/department/items/index', '新房楼盘 售楼部 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082829),
(7, 1, 'admin', '/admin2020.php/newhouse/department/cameras/index', '新房楼盘 摄像机管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"description\",\"ASC\"]],\"showField\":\"description\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"description\"]}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082838),
(8, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"showField\":\"cop_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"cop_name\"]}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082838),
(9, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082847),
(10, 1, 'admin', '/admin2020.php/newhouse/customer/source/index', '新房楼盘 新房客户 客户来源分类', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"source\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"source\",\"keyField\":\"id\",\"searchField\":[\"source\"],\"source\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082867),
(11, 1, 'admin', '/admin2020.php/newhouse/customer/items/index', '新房楼盘 新房客户 新房客户 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"customer_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"customer_name\",\"keyField\":\"id\",\"searchField\":[\"customer_name\"],\"customer_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082880),
(12, 1, 'admin', '/admin2020.php/newhouse/customer/items/index', '新房楼盘 新房客户 新房客户 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"customer_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"customer_name\",\"keyField\":\"id\",\"searchField\":[\"customer_name\"],\"customer_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082886),
(13, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082887),
(14, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"100\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082916),
(15, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"decoration\"]}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082928),
(16, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082928),
(17, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340111\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340111\",\"searchField\":[\"areaName\"]}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082928),
(18, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37274\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37274\",\"searchField\":[\"building_name\"]}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082928),
(19, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082930),
(20, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082932),
(21, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082939),
(22, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082942),
(23, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"100\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082951),
(24, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '114.222.184.51', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 1608082960),
(25, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"67bec13fa5004bacb28bd5106922f178\",\"username\":\"admin\",\"captcha\":\"czk6\"}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083636),
(26, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083655),
(27, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083655),
(28, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"type_name\"]}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083675),
(29, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"3\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"3\",\"searchField\":[\"building_name\"]}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083675),
(30, 1, 'admin', '/admin2020.php/general.config/edit', '常规管理 系统配置 编辑', '{\"row\":{\"name\":\"\\u623f\\u76df\\u4e91\\u53f0\",\"beian\":\"\\u7696ICP\\u590715006137\\u53f7-4\",\"cdnurl\":\"\",\"version\":\"1.0.0\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\",\"second_show_fanghao\":\"0\",\"second_assistant\":\"1\"}}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083695),
(31, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083705),
(32, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '58.100.243.82', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3861.400 QQBrowser/10.7.4313.400', 1608083705),
(33, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"38596d030f5c0baa06632ac585d4e7e0\",\"username\":\"admin\",\"captcha\":\"xpuu\",\"keeplogin\":\"1\"}', '61.52.128.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608084143),
(34, 1, 'admin', '/admin2020.php/newhouse/building/follow/add?building_id=58&dialog=1', '新房楼盘 楼盘跟进 添加', '{\"building_id\":\"58\",\"dialog\":\"1\",\"row\":{\"building_id\":\"58\",\"state\":\"\\u5f85\\u552e\",\"comment\":\"111\"}}', '61.52.128.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608084163),
(35, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '61.52.128.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608084179),
(36, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '61.52.128.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608084179),
(37, 1, 'admin', '/admin2020.php/newhouse/building/items/edit/ids/58?dialog=1', '新房楼盘 楼盘信息 编辑', '{\"dialog\":\"1\",\"row\":{\"building_name\":\"\\u4e00\\u4e2a\\u5927\\u697c\\u76d8\",\"area\":\"\\u5b89\\u5fbd\\u7701\\/\\u5408\\u80a5\\u5e02\\/\\u80a5\\u4e1c\\u53bf\",\"lng\":\"117.451\",\"lat\":\"31.8988\",\"address\":\"\\u5b89\\u5fbd\\u7701\\u5408\\u80a5\\u5e02\\u80a5\\u4e1c\\u53bf\\u67d0\\u6761\\u8def\",\"is_top\":\"0\",\"work_begin\":\"08:30:00\",\"work_end\":\"18:30:00\",\"labels\":\"\",\"developer_adminid\":\"0\",\"contact_tel\":\"\",\"validate_begin\":\"2020-03-01\",\"validate_end\":\"2020-04-14\",\"agent_adminid\":\"0\",\"last_sell\":\"2020-04-14\",\"get_date\":\"2020-04-14\",\"property_year\":\"70\",\"volume_ratio\":\"2.2\",\"property_fee\":\"2\",\"greening_rate\":\"40\",\"parking_spaces\":\"2030\",\"house_quantity\":\"1940\",\"developer\":\"\\u5408\\u80a5\\u623f\\u5730\\u4ea7\\u5f00\\u53d1\\u6709\\u9650\\u516c\\u53f8\",\"property_company\":\"\\u7269\\u4e1a\\u516c\\u53f8\",\"scene_person\":\"\\u5f20\\u7ecf\\u7406\",\"scene_person_tel\":\"1888888888\",\"average_price\":\"11000\",\"reporting_policy\":\"\\u524d\\u4e09\\u540e\\u56db\",\"on_selling\":\"\\u76ee\\u524d\\u4e3b\\u529b\\u5728\\u552e\\u4e8c\\u671f\\u9ad8\\u5c4226#\\u300132#\\u300129#\\u697c\\uff0c\\u4e00\\u671f\\u51c6\\u73b0\\u623f\\u540c\\u6b65\\u5728\\u552e\\uff0c\\u6237\\u578b\\u9762\\u79ef91\\u33a1-107\\u33a1\\uff0c\\u5747\\u4ef711000\\u5143\\/\\u33a1\\u3002\",\"introduction\":\"123\",\"picture\":\"\\/uploads\\/20190622\\/8f28512e4d3d7fbdd65b0960d11c5b02.jpg\"},\"buildingTypesPolicy_1\":\"\",\"buildingTypesPrice_1\":\"\",\"buildingTypesPolicy_2\":\"\",\"buildingTypesPrice_2\":\"\",\"buildingTypesPolicy_3\":\"\",\"buildingTypesPrice_3\":\"\",\"buildingTypesPolicy_4\":\"\",\"buildingTypesPrice_4\":\"\",\"buildingTypesPolicy_5\":\"\",\"buildingTypesPrice_5\":\"\",\"buildingTypesPolicy_6\":\"\",\"buildingTypesPrice_6\":\"\",\"buildingTypesPolicy_7\":\"\",\"buildingTypesPrice_7\":\"\",\"picture\":{\"path-1\":\"\\/uploads\\/20190622\\/8f28512e4d3d7fbdd65b0960d11c5b02.jpg\",\"path-2\":\"\",\"path-3\":\"\",\"path-4\":\"\",\"path-5\":\"\",\"path-6\":\"\"},\"ids\":\"58\"}', '61.52.128.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608084195),
(38, 1, 'admin', '/admin2020.php/general/config/emailtest', '常规管理 系统配置', '{\"row\":{\"mail_type\":\"1\",\"mail_smtp_host\":\"smtp.163.com\",\"mail_smtp_port\":\"465\",\"mail_smtp_user\":\"mifunadmin@163.com\",\"mail_smtp_pass\":\"CYTXALUYFXSANFMI\",\"mail_verify_type\":\"2\",\"mail_from\":\"mifunadmin@163.com\"},\"receiver\":\"2628575493@qq.com\"}', '61.52.128.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608084292),
(39, 1, 'admin', '/admin2020.php/ajax/upload', '', '{\"name\":\"\\u5fae\\u4fe1\\u56fe\\u7247_20201215112656.png\"}', '61.52.128.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608084313),
(40, 1, 'admin', '/admin2020.php/assessment/propertynumtype/index', '评估管理 产权证类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"property_num_type\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"property_num_type\",\"keyField\":\"id\",\"searchField\":[\"property_num_type\"],\"property_num_type\":\"\"}', '220.178.61.203', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60', 1608084804),
(41, 0, 'Unknown', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"9f7aaa94cca0c4772063bd7b5f7ff194\",\"username\":\"admin\",\"captcha\":\"huwv\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084811),
(42, 1, 'admin', '/admin2020.php/assessment/shares/index', '评估管理 共有情况 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"description\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"description\",\"keyField\":\"id\",\"searchField\":[\"description\"],\"description\":\"\"}', '220.178.61.203', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60', 1608084824),
(43, 1, 'admin', '/admin2020.php/assessment/items/add?dialog=1', '评估管理 评估标的 添加', '{\"dialog\":\"1\",\"row\":{\"longitude\":\"117.282699\",\"latitude\":\"31.866842\",\"property_num_type\":\"2\",\"property_num\":\"\\u76962017\\u5408\\u4e0d\\u52a8\\u4ea7\\u6743\\u7b2c0111111\\u53f7\",\"address\":\"\\u5b89\\u5fbd\\u7701\\u5408\\u80a5\\u5e02\\u957f\\u4e30\\u53bf\\u6e05\\u9896\\u8def\",\"age\":\"2014\",\"right_limit\":\"\",\"obligee\":\"\\u5f20\\u5148\\u751f\",\"share_id\":\"3\",\"area\":\"\",\"area_addtional\":\"\",\"share_area\":\"\",\"floor_count\":\"\",\"location_floor\":\"\",\"right_typeid\":\"\",\"need_report\":\"0\",\"need_prereport\":\"1\"},\"document_from\":\"\",\"picture\":{\"path-1\":\"\",\"path-2\":\"\",\"path-4\":\"\"}}', '220.178.61.203', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.60', 1608084827),
(44, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"0d13bb93ad152ad379ee617b7fa7d70d\",\"username\":\"admin\",\"captcha\":\"x2n3\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084829),
(45, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084884),
(46, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084884),
(47, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"showField\":\"cop_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"cop_name\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084969),
(48, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"cop_name\",\"keyField\":\"id\",\"searchField\":[\"cop_name\"],\"cop_name\":\"\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084971),
(49, 1, 'admin', '/admin2020.php/newhouse/department/items/index', '新房楼盘 售楼部 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"name\",\"ASC\"]],\"showField\":\"name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"name\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084982),
(50, 1, 'admin', '/admin2020.php/newhouse/department/cameras/index', '新房楼盘 摄像机管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"description\",\"ASC\"]],\"showField\":\"description\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"description\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084997),
(51, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"showField\":\"cop_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"cop_name\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608084997),
(52, 1, 'admin', '/admin2020.php/newhouse/department/cameras/index', '新房楼盘 摄像机管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"description\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"description\",\"keyField\":\"id\",\"searchField\":[\"description\"],\"description\":\"\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085006),
(53, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"100\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&client_id=1)', 1608085131),
(54, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"decoration\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085160),
(55, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340111\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340111\",\"searchField\":[\"areaName\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085160),
(56, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085160),
(57, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37274\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37274\",\"searchField\":[\"building_name\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085160),
(58, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085170),
(59, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085172),
(60, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085176),
(61, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085178),
(62, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085188),
(63, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085188),
(64, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085188),
(65, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37268\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37268\",\"searchField\":[\"building_name\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085188),
(66, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&client_id=1)', 1608085219),
(67, 1, 'admin', '/admin2020.php/cert/loan/follow', '贷款业务 贷款跟进', '{\"item_id\":\"82b4095c539141cda727f063c94c4747\"}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&client_id=1)', 1608085311),
(68, 1, 'admin', '/admin2020.php/assessment/propertynumtype/index', '评估管理 产权证类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"property_num_type\",\"ASC\"]],\"showField\":\"property_num_type\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"property_num_type\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085428),
(69, 1, 'admin', '/admin2020.php/assessment/shares/index', '评估管理 共有情况 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"3\",\"orderBy\":[[\"description\",\"ASC\"]],\"showField\":\"description\",\"keyField\":\"id\",\"keyValue\":\"3\",\"searchField\":[\"description\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085428),
(70, 1, 'admin', '/admin2020.php/assessment/righttype/index', '评估管理 权利类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"righttype\",\"ASC\"]],\"showField\":\"righttype\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"righttype\"]}', '115.227.97.130', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 SocketLog(tabid=454&amp;client_id=1)', 1608085428),
(71, 0, 'Unknown', '/admin2020.php/index/login?url=%2Fadmin2020.php', '', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"659de7acdfc2a9ef14ce83052bf3b04d\",\"username\":\"admin\",\"captcha\":\"vthj\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608094919),
(72, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"cdcde17f775e284d4e0491b0c11db752\",\"username\":\"admin\",\"captcha\":\"urfh\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608094925),
(73, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608094974),
(74, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608094974),
(75, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"showField\":\"cop_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"cop_name\"]}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608095003),
(76, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343179\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608095099),
(77, 1, 'admin', '/admin2020.php/second/follow/add?item_id=343179&state=0&dialog=1', '二手房 二手房跟进 添加', '{\"item_id\":\"343179\",\"state\":\"0\",\"dialog\":\"1\",\"row\":{\"item_id\":\"343179\",\"state\":\"0\",\"comment\":\"\\u53cd\\u53cd\\u590d\\u590d\"}}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608095113),
(78, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343179\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608095113),
(79, 1, 'admin', '/admin2020.php/assessment/shares/index', '评估管理 共有情况 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"3\",\"orderBy\":[[\"description\",\"ASC\"]],\"showField\":\"description\",\"keyField\":\"id\",\"keyValue\":\"3\",\"searchField\":[\"description\"]}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099295),
(80, 1, 'admin', '/admin2020.php/assessment/propertynumtype/index', '评估管理 产权证类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"property_num_type\",\"ASC\"]],\"showField\":\"property_num_type\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"property_num_type\"]}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099295),
(81, 1, 'admin', '/admin2020.php/assessment/righttype/index', '评估管理 权利类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"righttype\",\"ASC\"]],\"showField\":\"righttype\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"righttype\"]}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099295),
(82, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"2\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099309),
(83, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"3\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099311),
(84, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"2\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099312),
(85, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"3\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099312),
(86, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"2\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099313),
(87, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"3\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099314),
(88, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"2\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099314),
(89, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"3\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099314),
(90, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"2\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099315),
(91, 1, 'admin', '/admin2020.php/assessment/prereport/verify', '评估管理 预评版本管理 编辑', '{\"ids\":\"34\",\"state\":\"3\"}', '58.59.17.174', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 1608099315),
(92, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"2dcf035474bcc4ba09256b2b4a9954fa\",\"username\":\"admin\",\"captcha\":\"jxpr\"}', '113.109.204.67', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 1608100812),
(93, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '113.109.204.67', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 1608100837),
(94, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '113.109.204.67', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 1608100837),
(95, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '113.109.204.67', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 1608100838),
(96, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"56\"}', '113.109.204.67', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 1608100994),
(97, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"91\"}', '113.109.204.67', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 1608101010),
(98, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"4c334db78e81eb4d3f8af71477543261\",\"username\":\"admin\",\"captcha\":\"nycp\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108332),
(99, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108389),
(100, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108389),
(101, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108405),
(102, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108408),
(103, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108412),
(104, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108415),
(105, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108417),
(106, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108420),
(107, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108422),
(108, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108426),
(109, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108429),
(110, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108430),
(111, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108433),
(112, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108435),
(113, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37282\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37282\",\"searchField\":[\"building_name\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108449);
INSERT INTO `mf_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(114, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"0\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"0\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108449),
(115, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108449),
(116, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108449),
(117, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108451),
(118, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108454),
(119, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108464),
(120, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108464),
(121, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108464),
(122, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340121\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340121\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108465),
(123, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108467),
(124, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108470),
(125, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108471),
(126, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108472),
(127, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108475),
(128, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340103\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340103\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108476),
(129, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108479),
(130, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340111\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340111\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108480),
(131, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108481),
(132, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108492),
(133, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340111\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340111\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108493),
(134, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108494),
(135, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108495),
(136, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108496),
(137, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340121\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340121\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108497),
(138, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108498),
(139, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108499),
(140, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108501),
(141, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"3\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108501),
(142, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"4\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108502),
(143, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"5\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108502),
(144, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"6\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108502),
(145, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"7\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108502),
(146, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"8\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108502),
(147, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"9\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108503),
(148, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"10\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108503),
(149, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"11\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108503),
(150, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"12\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108503),
(151, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"13\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108504),
(152, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"14\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108504),
(153, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"15\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108504),
(154, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"16\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108504),
(155, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"17\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108505),
(156, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"18\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108505),
(157, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"19\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108505),
(158, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108507),
(159, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340111\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340111\",\"searchField\":[\"areaName\"]}', '120.231.95.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608108508),
(160, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"6379b201d4e9e31945f82ac82edf9b0c\",\"username\":\"admin\",\"captcha\":\"pd6u\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126232),
(161, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126255),
(162, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126255),
(163, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126260),
(164, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126262),
(165, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126263),
(166, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126265),
(167, 1, 'admin', '/admin2020.php/second/items/checkExistsMobile', '二手房 出售房源', '{\"mobile\":\"111\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126303),
(168, 1, 'admin', '/admin2020.php/second/items/checkExistsDongShi', '二手房 出售房源', '{\"building_id\":\"37268\",\"dong\":\"1\",\"shi\":\"112\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126313),
(169, 1, 'admin', '/admin2020.php/second/items/add?dialog=1', '二手房 出售房源 添加', '{\"dialog\":\"1\",\"row\":{\"building_id\":\"37268\",\"areaCode\":\"340102\",\"address\":\"\\u5b89\\u5fbd\\u7701\\u5408\\u80a5\\u5e02\\u7476\\u6d77\\u533a\\u660e\\u7687\\u8def\",\"dong\":\"1\",\"shi\":\"112\",\"shi_count\":\"111\",\"ting_count\":\"1\",\"area\":\"111\",\"build_year\":\"1111\",\"unit_price\":\"10000.00\",\"total_price\":\"111\",\"floor\":\"1\",\"floor_total\":\"1\",\"decoration_id\":\"1\",\"type_id\":\"1\",\"customer_name\":\"\\u94a5\\u5319\",\"mobile\":\"111\",\"auction_price\":\"\",\"auction_time_start\":\"\",\"auction_time_end\":\"\",\"auction_url\":\"\"},\"auction_time\":\"\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126332),
(170, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126336),
(171, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126347),
(172, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"decoration\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126347),
(173, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37268\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37268\",\"searchField\":[\"building_name\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126347),
(174, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126347),
(175, 1, 'admin', '/admin2020.php/second/items/checkExistsMobile', '二手房 出售房源', '{\"mobile\":\"111123232432432\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126351),
(176, 1, 'admin', '/admin2020.php/second/items/edit/ids/343180?dialog=1', '二手房 出售房源 编辑', '{\"dialog\":\"1\",\"row\":{\"building_id\":\"37268\",\"areaCode\":\"340102\",\"address\":\"\\u5b89\\u5fbd\\u7701\\u5408\\u80a5\\u5e02\\u7476\\u6d77\\u533a\\u660e\\u7687\\u8def\",\"dong\":\"1\",\"shi\":\"112\",\"shi_count\":\"111\",\"ting_count\":\"1\",\"area\":\"111\",\"build_year\":\"1111\",\"unit_price\":\"10000\",\"total_price\":\"111\",\"floor\":\"1\",\"floor_total\":\"1\",\"decoration_id\":\"1\",\"type_id\":\"1\",\"customer_name\":\"\\u5148\\u751f\",\"mobile\":\"111123232432432\",\"auction_price\":\"0\",\"auction_time_start\":\"0000-00-00 00:00:00\",\"auction_time_end\":\"0000-00-00 00:00:00\",\"auction_url\":\"\"},\"auction_time\":\"0000-00-00 00:00:00 - 0000-00-00 00:00:00\",\"ids\":\"343180\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126351),
(177, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126354),
(178, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126360),
(179, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37268\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37268\",\"searchField\":[\"building_name\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126364),
(180, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126364),
(181, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"decoration\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126364),
(182, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '39.162.143.110', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608126364),
(183, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"c7e9684f2c8ac0c540d0024b762ea0f6\",\"username\":\"admin\",\"captcha\":\"dwpt\"}', '183.95.62.153', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 1608176238),
(184, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"517d3ee41ba8043da738217de21cd932\",\"username\":\"admin\",\"captcha\":\"ev6t\"}', '60.249.25.55', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608177739),
(185, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"bc6d63360302d15f95269ae1be3746af\",\"username\":\"admin\",\"captcha\":\"4ewp\"}', '125.62.6.101', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608216252),
(186, 1, 'admin', '/admin2020.php/index/login', '登录', '{\"__token__\":\"7f05877057fe75f1f30c23100f0c874e\",\"username\":\"admin\",\"captcha\":\"7smf\"}', '116.235.48.68', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608255694),
(187, 1, 'admin', '/admin2020.php/newhouse/department/items/index', '新房楼盘 售楼部 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"name\",\"ASC\"]],\"showField\":\"name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"name\"]}', '116.235.48.68', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608255710),
(188, 1, 'admin', '/admin2020.php/newhouse/department/cameras/index', '新房楼盘 摄像机管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"description\",\"ASC\"]],\"showField\":\"description\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"description\"]}', '116.235.48.68', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608255719),
(189, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"showField\":\"cop_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"cop_name\"]}', '116.235.48.68', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608255719),
(190, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"96b4adc054ddbf897217a391facc8653\",\"username\":\"admin\",\"captcha\":\"cpe6\",\"keeplogin\":\"1\"}', '112.32.59.61', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608383765),
(191, 1, 'admin', '/admin2020.php/general.config/edit', '常规管理 系统配置 编辑', '{\"row\":{\"name\":\"\\u623f\\u76df\\u4e91\\u53f0\",\"beian\":\"\\u7696ICP\\u590715006137\\u53f7-4\",\"cdnurl\":\"\",\"version\":\"1.0.0\",\"timezone\":\"Asia\\/Shanghai\",\"forbiddenip\":\"\",\"languages\":\"{&quot;backend&quot;:&quot;zh-cn&quot;,&quot;frontend&quot;:&quot;zh-cn&quot;}\",\"fixedpage\":\"dashboard\",\"second_show_fanghao\":\"1\",\"second_assistant\":\"1\"}}', '112.32.59.61', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608383851),
(192, 1, 'admin', '/admin2020.php/general/config/check', '常规管理 系统配置', '{\"row\":{\"name\":\"privacy_show_fanghao\"}}', '112.32.59.61', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608384000),
(193, 1, 'admin', '/admin2020.php/general.config/add', '常规管理 系统配置 添加', '{\"row\":{\"type\":\"radio\",\"group\":\"basic\",\"name\":\"privacy_show_fanghao\",\"title\":\"\\u79c1\\u623f\\u663e\\u793a\\u623f\\u53f7\",\"value\":\"privacy_show_fanghao\",\"content\":\"1|\\u663e\\u793a\\r\\n0|\\u9690\\u85cf\",\"tip\":\"\",\"rule\":\"\",\"extend\":\"\"}}', '112.32.59.61', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608384059),
(194, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343179\"}', '112.32.59.61', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608386230),
(195, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478749),
(196, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478759),
(197, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37282\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37282\",\"searchField\":[\"building_name\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478785),
(198, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"0\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"0\",\"searchField\":[\"areaName\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478785),
(199, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478785),
(200, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478785),
(201, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478791),
(202, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478808),
(203, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478847),
(204, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478851),
(205, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478853),
(206, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478856),
(207, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478863),
(208, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478866),
(209, 1, 'admin', '/admin2020.php/newhouse/department/items/index', '新房楼盘 售楼部 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478941),
(210, 1, 'admin', '/admin2020.php/newhouse/department/cameras/index', '新房楼盘 摄像机管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"description\",\"ASC\"]],\"showField\":\"description\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"description\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478958),
(211, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"showField\":\"cop_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"cop_name\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478958),
(212, 1, 'admin', '/admin2020.php/newhouse/department/cameras/index', '新房楼盘 摄像机管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"description\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"description\",\"keyField\":\"id\",\"searchField\":[\"description\"],\"description\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608478968),
(213, 1, 'admin', '/admin2020.php/auth/rule/multi/ids/156', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"156\",\"params\":\"ismenu=0\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479023),
(214, 1, 'admin', '/admin2020.php/index/index', '', '{\"action\":\"refreshmenu\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479023),
(215, 1, 'admin', '/admin2020.php/auth/rule/multi/ids/67', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"67\",\"params\":\"ismenu=0\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479048),
(216, 1, 'admin', '/admin2020.php/index/index', '', '{\"action\":\"refreshmenu\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479048),
(217, 1, 'admin', '/admin2020.php/auth/rule/multi/ids/67', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"67\",\"params\":\"ismenu=1\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479061),
(218, 1, 'admin', '/admin2020.php/index/index', '', '{\"action\":\"refreshmenu\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479061),
(219, 1, 'admin', '/admin2020.php/auth/rule/multi/ids/156', '权限管理 菜单规则', '{\"action\":\"\",\"ids\":\"156\",\"params\":\"ismenu=1\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479067),
(220, 1, 'admin', '/admin2020.php/index/index', '', '{\"action\":\"refreshmenu\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479067),
(221, 1, 'admin', '/admin2020.php/assessment/propertynumtype/index', '评估管理 产权证类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"property_num_type\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"property_num_type\",\"keyField\":\"id\",\"searchField\":[\"property_num_type\"],\"property_num_type\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479112),
(222, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479208),
(223, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479208),
(224, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479209),
(225, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479210),
(226, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479212),
(227, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479217),
(228, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"3\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479218),
(229, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"4\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479218);
INSERT INTO `mf_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(230, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"5\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479218),
(231, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"6\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479218),
(232, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"7\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479219),
(233, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"8\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479219),
(234, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"9\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479219),
(235, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"10\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479219),
(236, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"11\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479220),
(237, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"12\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479220),
(238, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"13\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479220),
(239, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"14\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479220),
(240, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"15\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479221),
(241, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"16\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479221),
(242, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"17\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479221),
(243, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"18\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479221),
(244, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"19\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479222),
(245, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"20\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479222),
(246, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479226),
(247, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340111\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340111\",\"searchField\":[\"areaName\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479228),
(248, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"20\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479229),
(249, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479235),
(250, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340103\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340103\",\"searchField\":[\"areaName\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479237),
(251, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"20\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479238),
(252, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479240),
(253, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479243),
(254, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340123\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340123\",\"searchField\":[\"areaName\"]}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479244),
(255, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"20\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479246),
(256, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608479252),
(257, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"95\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608481839),
(258, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"59\"}', '120.231.95.36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608481848),
(259, 0, 'Unknown', '/admin2020.php/index/login?url=%2Fadmin2020.php', '', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"8c1dbab65b0d977348b436a6d68e0ce0\",\"username\":\"admin\",\"captcha\":\"zzne\"}', '183.246.132.53', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608769714),
(260, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"6d93cc0f394080127b4c0207e0636c69\",\"username\":\"admin\",\"captcha\":\"ANVR\"}', '183.246.132.53', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608769723),
(261, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"97\"}', '183.246.132.53', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608769882),
(262, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"95\"}', '183.246.132.53', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608769892),
(263, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"74\"}', '183.246.132.53', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608769972),
(264, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"75\"}', '183.246.132.53', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1608769987),
(265, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"3feda7f70846331a91f161731941b670\",\"username\":\"admin\",\"captcha\":\"futu\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608773249),
(266, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608773345),
(267, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608773418),
(268, 0, 'Unknown', '/admin2020.php/index/login?url=%2Fadmin2020.php%2Fnewhouse%2Fbuilding%2Fpicturetype%3Fref%3Daddtabs', '', '{\"url\":\"\\/admin2020.php\\/newhouse\\/building\\/picturetype?ref=addtabs\",\"__token__\":\"2cf10fee7838d75159362a672142765a\",\"username\":\"admin\",\"captcha\":\"NAFP\"}', '120.208.20.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608774339),
(269, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php%2Fnewhouse%2Fbuilding%2Fpicturetype%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/admin2020.php\\/newhouse\\/building\\/picturetype?ref=addtabs\",\"__token__\":\"1ea7f9ab71b4dda44dea70dc6206da91\",\"username\":\"admin\",\"captcha\":\"MePP\"}', '120.208.20.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608774352),
(270, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.208.20.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608774413),
(271, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.208.20.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608774413),
(272, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.208.20.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608774418),
(273, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '120.208.20.213', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1608774419),
(274, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608774982),
(275, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608774982),
(276, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608774992),
(277, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608775010),
(278, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608775026),
(279, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608775026),
(280, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608775236),
(281, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608775248),
(282, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"100\"}', '120.208.19.222', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1608775256),
(283, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"51febee686009ddb39a82af551c463ba\",\"username\":\"admin\",\"captcha\":\"acic\"}', '125.120.79.147', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608801649),
(284, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"cace6da5331095ccebe1ea075a6575b3\",\"username\":\"admin\",\"captcha\":\"ubtl\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608804969),
(285, 1, 'admin', '/admin2020.php/newhouse/department/items/index', '新房楼盘 售楼部 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"name\",\"keyField\":\"id\",\"searchField\":[\"name\"],\"name\":\"\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805026),
(286, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805039),
(287, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805039),
(288, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805097),
(289, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805097),
(290, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805097),
(291, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805099),
(292, 1, 'admin', '/admin2020.php/rent/items/add?dialog=1', '租房 出租房源 添加', '{\"dialog\":\"1\",\"row\":{\"building_id\":\"37266\",\"areaCode\":\"450305\",\"address\":\"AABB\",\"dong\":\"1\\u680b\",\"shi\":\"1203\",\"shi_count\":\"2\",\"ting_count\":\"1\",\"area\":\"75\",\"build_year\":\"\",\"unit_price\":\"2300\",\"deposit\":\"4600\",\"floor\":\"12\",\"floor_total\":\"32\",\"decoration_id\":\"2\",\"type_id\":\"1\",\"customer_name\":\"\\u4f59\\u5148\\u751f\",\"mobile\":\"19966668888\"}}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805153),
(293, 1, 'admin', '/admin2020.php/auth/group/roletree', '权限管理 角色组', '{\"id\":\"17\",\"pid\":\"15\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805314),
(294, 1, 'admin', '/admin2020.php/auth/group/edit/ids/17?dialog=1', '权限管理 角色组 编辑', '{\"dialog\":\"1\",\"row\":{\"rules\":\"230,412,418,424,425,436,450,451,452,460,461,462,463,464,650,651,652,653,654,655,656,657,658,659,660,661,662,664,229,156,411,417,423,410,449\",\"pid\":\"15\",\"name\":\"\\u4e8c\\u624b\\u623f\\u7ecf\\u7eaa\\u4eba\",\"status\":\"normal\"},\"ids\":\"17\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805335),
(295, 1, 'admin', '/admin2020.php/auth/group/roletree', '权限管理 角色组', '{\"id\":\"2\",\"pid\":\"1\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805340),
(296, 1, 'admin', '/admin2020.php/user/user/edit/ids/13?dialog=1', '会员管理 会员管理 编辑', '{\"dialog\":\"1\",\"row\":{\"group_id\":\"1\",\"username\":\"admin1\",\"nickname\":\"admin1\",\"password\":\"\",\"email\":\"xundh2@qq.com\",\"mobile\":\"18305515155\",\"avatar\":\"\",\"level\":\"1\",\"birthday\":\"\",\"gender\":\"0\",\"money\":\"0.00\",\"score\":\"0\",\"successions\":\"2\",\"maxsuccessions\":\"10\",\"prevtime\":\"2020-12-24 17:20:03\",\"logintime\":\"2020-12-24 18:10:09\",\"loginip\":\"113.102.160.209\",\"loginfailure\":\"0\",\"joinip\":\"220.178.61.203\",\"jointime\":\"2020-05-18 15:46:04\",\"status\":\"normal\",\"bio\":\"\",\"applicationdescription\":\"\"},\"ids\":\"13\"}', '113.102.160.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36 Edg/87.0.664.66', 1608805431),
(297, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"32815221a63ccc68472c9de7189683dd\",\"username\":\"admin\",\"captcha\":\"j4d5\"}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886745),
(298, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886825),
(299, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343179\"}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886839),
(300, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37268\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37268\",\"searchField\":[\"building_name\"]}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886856),
(301, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886857),
(302, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"decoration\"]}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886857),
(303, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886857),
(304, 1, 'admin', '/admin2020.php/second/buildings/add?dialog=1', '二手房 楼盘管理 添加', '{\"dialog\":\"1\",\"row\":{\"building_name\":\"\\u5e0c\\u671b\\u5c0f\\u533a\",\"address\":\"\\u5b89\\u5fbd\\u7701\\u5408\\u80a5\\u5e02\\u5e90\\u9633\\u533a\\u7ea2\\u661f\\u8def90\\u53f7\",\"business\":\"\\u56db\\u724c\\u697c,\\u6850\\u57ce\\u8def,\\u900d\\u9065\\u6d25\",\"latitude\":\"31.866167\",\"longitude\":\"117.291035\",\"areaName\":\"\\u5b89\\u5fbd\\u7701\\/\\u829c\\u6e56\\u5e02\\/\\u5f0b\\u6c5f\\u533a\",\"areaCode\":\"340203\",\"alias_name1\":\"\",\"alias_name2\":\"\"}}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886957),
(305, 1, 'admin', '/admin2020.php/second/buildings/del/ids/37297', '二手房 楼盘管理 删除', '{\"action\":\"del\",\"ids\":\"37297\",\"params\":\"\"}', '221.230.166.133', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36', 1608886974),
(306, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"d4c81c1e7ef4ba291a0637b10c17acba\",\"username\":\"admin\",\"captcha\":\"fuun\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142679),
(307, 1, 'admin', '/admin2020.php/newhouse/building/labels/index', '新房楼盘 楼盘标签 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"label_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"label_name\",\"keyField\":\"id\",\"searchField\":[\"label_name\"],\"label_name\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142854),
(308, 1, 'admin', '/admin2020.php/newhouse/building/labels/index', '新房楼盘 楼盘标签 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"label_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"label_name\",\"keyField\":\"id\",\"searchField\":[\"label_name\"],\"label_name\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142904),
(309, 1, 'admin', '/admin2020.php/newhouse/building/labels/index', '新房楼盘 楼盘标签 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"label_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"label_name\",\"keyField\":\"id\",\"searchField\":[\"label_name\"],\"label_name\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142906),
(310, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142909),
(311, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142910),
(312, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142927),
(313, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142928),
(314, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"username\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"username\",\"keyField\":\"id\",\"searchField\":[\"username\"],\"username\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609142931),
(315, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343179\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609145591),
(316, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609145604),
(317, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609145616),
(318, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609145616),
(319, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609145618),
(320, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609145624),
(321, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"19820\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"19820\",\"searchField\":[\"building_name\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609148741),
(322, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340151\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340151\",\"searchField\":[\"areaName\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609148742),
(323, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"decoration\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609148742),
(324, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609148742),
(325, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"17\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"17\",\"searchField\":[\"building_name\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609151570),
(326, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340104\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340104\",\"searchField\":[\"areaName\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609151570),
(327, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"decoration\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609151570),
(328, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609151570),
(329, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '120.231.95.128', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36', 1609151584),
(330, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"6acc1fc420f0547939527d26b8ca1cc7\",\"username\":\"admin\",\"captcha\":\"q6tb\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207282),
(331, 1, 'admin', '/admin2020.php/newhouse/customer/source/index', '新房楼盘 新房客户 客户来源分类', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"source\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"source\",\"keyField\":\"id\",\"searchField\":[\"source\"],\"source\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207318),
(332, 1, 'admin', '/admin2020.php/newhouse/customer/items/add?dialog=1', '新房楼盘 新房客户 新房客户 添加', '{\"dialog\":\"1\",\"row\":{\"customer_name\":\"123\",\"customer_tel\":\"15138658889\",\"customer_tel2\":\"15138658888\",\"customer_tel3\":\"15138658888\",\"sex_radio\":\"\\u7537\",\"source_id\":\"4\",\"customer_from\":\"1\",\"mem\":\"22\",\"account_statement\":\"12\",\"credit_investigation\":\"1\",\"want_area\":\"\",\"want_floor_low\":\"\",\"want_floor_high\":\"\",\"want_acre_low\":\"\",\"want_acre_high\":\"\",\"want_price_low\":\"\",\"want_price_high\":\"\",\"payment_percentage\":\"\",\"property_quantity\":\"\",\"first_house\":\"\\u672a\\u77e5\",\"social_security\":\"\\u672a\\u77e5\",\"marriage_radio\":\"\\u672a\\u77e5\",\"building_id\":\"\",\"from_friend\":\"0\",\"friend_name\":\"\",\"friend_mobile\":\"\"}}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207327),
(333, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207339),
(334, 1, 'admin', '/admin2020.php/newhouse/report/items/add_ajax', '新房楼盘 报备审核 添加', '{\"customer_id\":\"1\",\"prepare_visit\":\"2020-12-25 10:02:22\",\"contents\":\"\",\"building_id\":\"58\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207344),
(335, 1, 'admin', '/admin2020.php/newhouse/customer/source/index', '新房楼盘 新房客户 客户来源分类', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"4\",\"orderBy\":[[\"source\",\"ASC\"]],\"showField\":\"source\",\"keyField\":\"id\",\"keyValue\":\"4\",\"searchField\":[\"source\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207349),
(336, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\",58\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\",58\",\"searchField\":[\"building_name\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207349),
(337, 1, 'admin', '/admin2020.php/newhouse/customer/items/edit/ids/1?dialog=1', '新房楼盘 新房客户 新房客户 编辑', '{\"dialog\":\"1\",\"row\":{\"customer_name\":\"123\",\"customer_tel\":\"15138658889\",\"customer_tel2\":\"15138658888\",\"customer_tel3\":\"15138658888\",\"sex_radio\":\"\\u7537\",\"source_id\":\"4\",\"customer_from\":\"1\",\"mem\":\"22\",\"account_statement\":\"12\",\"credit_investigation\":\"1\",\"want_area\":\"\\u5b89\\u5fbd\\u7701\\/\\u829c\\u6e56\\u5e02\\/\\u5f0b\\u6c5f\\u533a\",\"want_floor_low\":\"1\",\"want_floor_high\":\"4\",\"want_acre_low\":\"100\",\"want_acre_high\":\"150\",\"want_price_low\":\"150\",\"want_price_high\":\"150\",\"payment_percentage\":\"0\",\"property_quantity\":\"0\",\"first_house\":\"\\u672a\\u77e5\",\"social_security\":\"\\u672a\\u77e5\",\"marriage_radio\":\"\\u672a\\u77e5\",\"building_id\":\"58\",\"from_friend\":\"0\",\"friend_name\":\"\",\"friend_mobile\":\"\"},\"ids\":\"1\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207366),
(338, 1, 'admin', '/admin2020.php/newhouse/customer/source/index', '新房楼盘 新房客户 客户来源分类', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"4\",\"orderBy\":[[\"source\",\"ASC\"]],\"showField\":\"source\",\"keyField\":\"id\",\"keyValue\":\"4\",\"searchField\":[\"source\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207375),
(339, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"58\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"58\",\"searchField\":[\"building_name\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207375),
(340, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207435),
(341, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207435),
(342, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207436),
(343, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207444),
(344, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340121\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340121\",\"searchField\":[\"areaName\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207445);
INSERT INTO `mf_admin_log` (`id`, `admin_id`, `username`, `url`, `title`, `content`, `ip`, `useragent`, `createtime`) VALUES
(345, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207445),
(346, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207447),
(347, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207448),
(348, 1, 'admin', '/admin2020.php/second/items/checkExistsDongShi', '二手房 出售房源', '{\"building_id\":\"37268\",\"dong\":\"1\",\"shi\":\"11\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207452),
(349, 1, 'admin', '/admin2020.php/newhouse/department/items/index', '新房楼盘 售楼部 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"name\",\"ASC\"]],\"showField\":\"name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"name\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207593),
(350, 1, 'admin', '/admin2020.php/newhouse/department/cameras/index', '新房楼盘 摄像机管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"description\",\"ASC\"]],\"showField\":\"description\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"description\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207601),
(351, 1, 'admin', '/admin2020.php/auth/cops/index', '权限管理 企业管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"cop_name\",\"ASC\"]],\"showField\":\"cop_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"cop_name\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207601),
(352, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207606),
(353, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"type_name\",\"keyField\":\"id\",\"searchField\":[\"type_name\"],\"type_name\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207607),
(354, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207608),
(355, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"58\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"58\",\"searchField\":[\"building_name\"]}', '221.15.216.241', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609207617),
(356, 0, 'Unknown', '/admin2020.php/index/login?url=%2Fadmin2020.php', '', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"7f029d319e1b34d9ccf40b193dd40abf\",\"username\":\"admin\",\"captcha\":\"xsgq\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609379044),
(357, 0, 'Unknown', '/admin2020.php/index/login?url=%2Fadmin2020.php', '', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"2d03be2d933113e24b6819e7ca76ed00\",\"username\":\"admin\",\"captcha\":\"lwj6\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609379052),
(358, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"1532aabe937be45402e7053cc22ffe9b\",\"username\":\"admin\",\"captcha\":\"5sm3\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609379062),
(359, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"2\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609379132),
(360, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609379146),
(361, 1, 'admin', '/admin2020.php/rent/follow/add?item_id=1&state=1&dialog=1', '租房 租房跟进 添加', '{\"item_id\":\"1\",\"state\":\"1\",\"dialog\":\"1\",\"row\":{\"item_id\":\"1\",\"state\":\"0\",\"comment\":\"1515\"}}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609379172),
(362, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"1\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609379189),
(363, 1, 'admin', '/admin2020.php/newhouse/building/items/add?dialog=1', '新房楼盘 楼盘信息 添加', '{\"dialog\":\"1\",\"row\":{\"building_name\":\"\\u676d\\u5dde\\u9ad8\\u5fb7\\u7f6e\\u5730\\u5e7f\\u573a\",\"area\":\"\",\"lng\":\"\",\"lat\":\"\",\"address\":\"\",\"is_top\":\"0\",\"work_begin\":\"10:06:27\",\"work_end\":\"10:06:27\",\"labels\":\"\",\"developer_adminid\":\"\",\"contact_tel\":\"\",\"validate_begin\":\"2020-12-31\",\"validate_end\":\"2020-12-31\",\"agent_adminid\":\"\",\"last_sell\":\"2020-12-31\",\"get_date\":\"2020-12-31\",\"property_year\":\"\",\"volume_ratio\":\"\",\"property_fee\":\"\",\"greening_rate\":\"\",\"parking_spaces\":\"\",\"house_quantity\":\"\",\"developer\":\"\",\"property_company\":\"\",\"scene_person\":\"\",\"scene_person_tel\":\"\",\"average_price\":\"\",\"reporting_policy\":\"\",\"on_selling\":\"\",\"introduction\":\"\",\"picture\":\"\"}}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609380413),
(364, 1, 'admin', '/admin2020.php/rent/follow', '租房 租房跟进 查看', '{\"item_id\":\"2\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609380458),
(365, 1, 'admin', '/admin2020.php/ajax/upload', '', '{\"name\":\"\\u5458\\u5de5\\u8868 (2).xls\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609380574),
(366, 1, 'admin', '/admin2020.php/newhouse/department/items?addtabs=1', '新房楼盘 售楼部 查看', '{\"addtabs\":\"1\",\"file\":\"\\/uploads\\/20201231\\/aeab68f08e8ea51553cf8afd9aa08c0b.xls\"}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609380574),
(367, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php%2Fnewhouse%2Fdepartment%2Fitems%3Fref%3Daddtabs', '登录', '{\"url\":\"\\/admin2020.php\\/newhouse\\/department\\/items?ref=addtabs\",\"__token__\":\"d72143db2fe2871ee17b409fc4a1d6b3\",\"username\":\"admin\",\"captcha\":\"pgce\"}', '36.27.127.195', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609380747),
(368, 1, 'admin', '/admin2020.php/newhouse/customer/items/index', '新房楼盘 新房客户 新房客户 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"customer_name\",\"ASC\"]],\"showField\":\"customer_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"customer_name\"]}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609382447),
(369, 1, 'admin', '/admin2020.php/newhouse/building/items/index', '新房楼盘 楼盘信息 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"58\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"58\",\"searchField\":[\"building_name\"]}', '36.27.127.195', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609382447),
(370, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"0ad7d2532c3f162d9842f0333210cffd\",\"username\":\"admin\",\"captcha\":\"ic4f\"}', '115.193.231.151', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609683530),
(371, 1, 'admin', '/admin2020.php/newhouse/building/items/add?dialog=1', '新房楼盘 楼盘信息 添加', '{\"dialog\":\"1\",\"row\":{\"building_name\":\"\\u9ad8\\u5fb7\",\"area\":\"\",\"lng\":\"\",\"lat\":\"\",\"address\":\"\\u676d\\u5dde\",\"is_top\":\"0\",\"work_begin\":\"22:19:01\",\"work_end\":\"22:19:01\",\"labels\":\"\",\"developer_adminid\":\"\",\"contact_tel\":\"\",\"validate_begin\":\"2021-01-03\",\"validate_end\":\"2021-01-03\",\"agent_adminid\":\"\",\"last_sell\":\"2021-01-03\",\"get_date\":\"2021-01-03\",\"property_year\":\"\",\"volume_ratio\":\"\",\"property_fee\":\"\",\"greening_rate\":\"\",\"parking_spaces\":\"\",\"house_quantity\":\"\",\"developer\":\"\",\"property_company\":\"\",\"scene_person\":\"\",\"scene_person_tel\":\"\",\"average_price\":\"\",\"reporting_policy\":\"\",\"on_selling\":\"\",\"introduction\":\"\",\"picture\":\"\"}}', '115.193.231.151', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609683567),
(374, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '115.193.231.151', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609683580),
(375, 1, 'admin', '/admin2020.php/auth/admin/selectlist', '权限管理 管理员管理', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"0\",\"orderBy\":[[\"username\",\"ASC\"]],\"showField\":\"username\",\"keyField\":\"id\",\"keyValue\":\"0\",\"searchField\":[\"username\"]}', '115.193.231.151', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609683580),
(376, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"340b083dc823215e8668cef863842a21\",\"username\":\"admin\",\"captcha\":\"nn4y\"}', '125.122.37.230', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609723616),
(377, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"5049044cfab7cb6bed27c1694f57b06b\",\"username\":\"admin\",\"captcha\":\"5jpi\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609746385),
(378, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747042),
(379, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747174),
(380, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747174),
(381, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747189),
(382, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747194),
(383, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747196),
(384, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747201),
(385, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747208),
(386, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\\u4e2d\\u592e\\u516c\\u56ed\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\",\"alias_name1\",\"alias_name2\",\"building_name_en\"],\"building_name,alias_name1,alias_name2,building_name_en\":\"\\u4e2d\\u592e\\u516c\\u56ed\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747210),
(387, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747211),
(388, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"2\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747214),
(389, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"3\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747214),
(390, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"4\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747215),
(391, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"5\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747215),
(392, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"6\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747216),
(393, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"7\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747216),
(394, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"8\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747216),
(395, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"9\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747216),
(396, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"10\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747217),
(397, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"11\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747217),
(398, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"12\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747217),
(399, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"13\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747217),
(400, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"14\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747218),
(401, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"15\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747218),
(402, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"16\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747218),
(403, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"17\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747218),
(404, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"18\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747219),
(405, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"19\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747219),
(406, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"20\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747219),
(407, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"21\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747219),
(408, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"22\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747219),
(409, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"23\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747220),
(410, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"24\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747220),
(411, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"25\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747220),
(412, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"26\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747220),
(413, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"27\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747221),
(414, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"28\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747221),
(415, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"29\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747221),
(416, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"30\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747226),
(417, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"31\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747226),
(418, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"32\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747226),
(419, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"33\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747226),
(420, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"34\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747227),
(421, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"35\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747227),
(422, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"36\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747227),
(423, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"37\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747227),
(424, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"37\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747233),
(425, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\\u8c2f\\u57ce\\u533a\"],\"pageNumber\":\"37\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\\u8c2f\\u57ce\\u533a\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747234),
(426, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747271),
(427, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747274),
(428, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747276),
(429, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"+\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"+\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747283),
(430, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747303),
(431, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"decoration\"]}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747303),
(432, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747303),
(433, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37268\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37268\",\"searchField\":[\"building_name\"]}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747303),
(434, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747306),
(435, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\\u4e07\\u79d1\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\\u4e07\\u79d1\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747319),
(436, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"building_name\",\"keyField\":\"id\",\"searchField\":[\"building_name\"],\"building_name\":\"\"}', '58.243.17.190', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36', 1609747323),
(437, 0, 'Unknown', '/admin2020.php/index/login?url=%2Fadmin2020.php', '', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"dca9a3d705e4edfbaf99799ae5ba4d7c\",\"username\":\"admin\",\"captcha\":\"kvcw\"}', '39.162.129.217', 'Mozilla/5.0 (Linux; Android 10; GLK-AL00; HMSCore 5.0.5.300) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 HuaweiBrowser/11.0.5.304 Mobile Safari/537.36', 1609759256),
(438, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"04e277f315b11dd42473ec413ff0bdb2\",\"username\":\"admin\",\"captcha\":\"qyea\"}', '39.162.129.217', 'Mozilla/5.0 (Linux; Android 10; GLK-AL00; HMSCore 5.0.5.300) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 HuaweiBrowser/11.0.5.304 Mobile Safari/537.36', 1609759273),
(439, 1, 'admin', '/admin2020.php/second/buildings/index', '二手房 楼盘管理 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"37268\",\"orderBy\":[[\"building_name\",\"ASC\"]],\"showField\":\"building_name\",\"keyField\":\"id\",\"keyValue\":\"37268\",\"searchField\":[\"building_name\"]}', '39.162.129.217', 'Mozilla/5.0 (Linux; Android 10; GLK-AL00; HMSCore 5.0.5.300) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 HuaweiBrowser/11.0.5.304 Mobile Safari/537.36', 1609759322),
(440, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"searchTable\":\"tbl\",\"searchKey\":\"areaCode\",\"searchValue\":\"340102\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"keyValue\":\"340102\",\"searchField\":[\"areaName\"]}', '39.162.129.217', 'Mozilla/5.0 (Linux; Android 10; GLK-AL00; HMSCore 5.0.5.300) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 HuaweiBrowser/11.0.5.304 Mobile Safari/537.36', 1609759322),
(441, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"decoration\"]}', '39.162.129.217', 'Mozilla/5.0 (Linux; Android 10; GLK-AL00; HMSCore 5.0.5.300) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 HuaweiBrowser/11.0.5.304 Mobile Safari/537.36', 1609759322),
(442, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '39.162.129.217', 'Mozilla/5.0 (Linux; Android 10; GLK-AL00; HMSCore 5.0.5.300) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 HuaweiBrowser/11.0.5.304 Mobile Safari/537.36', 1609759322),
(443, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"56\"}', '39.162.129.217', 'Mozilla/5.0 (Linux; Android 10; GLK-AL00; HMSCore 5.0.5.300) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 HuaweiBrowser/11.0.5.304 Mobile Safari/537.36', 1609759337),
(444, 1, 'admin', '/admin2020.php/index/login?url=%2Fadmin2020.php', '登录', '{\"url\":\"\\/admin2020.php\",\"__token__\":\"5811c938839277353d3a483543090ae6\",\"username\":\"admin\",\"captcha\":\"w7ai\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807040),
(445, 1, 'admin', '/admin2020.php/user/user/edit/ids/17?dialog=1', '会员管理 会员管理 编辑', '{\"dialog\":\"1\",\"row\":{\"group_id\":\"2\",\"username\":\"vivi\",\"nickname\":\"vivi\",\"password\":\"\",\"email\":\"vivi@163.com\",\"mobile\":\"13135632689\",\"avatar\":\"\",\"level\":\"1\",\"birthday\":\"\",\"gender\":\"0\",\"money\":\"0.00\",\"score\":\"0\",\"successions\":\"1\",\"maxsuccessions\":\"1\",\"prevtime\":\"2021-01-04 19:14:32\",\"logintime\":\"2021-01-04 19:14:32\",\"loginip\":\"39.162.129.217\",\"loginfailure\":\"0\",\"joinip\":\"39.162.129.217\",\"jointime\":\"2021-01-04 19:14:32\",\"status\":\"normal\",\"bio\":\"\",\"applicationdescription\":\"\"},\"ids\":\"17\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807124),
(446, 1, 'admin', '/admin2020.php/user/user/multi/ids/17', '会员管理 会员管理 批量更新', '{\"ids\":\"17\",\"params\":\"status=normal\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807163),
(447, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343179\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807303),
(448, 1, 'admin', '/admin2020.php/second/items/del/ids/98,97,96,95,94,93,92,91,89,88,87,85,84,83,82', '二手房 出售房源 删除', '{\"action\":\"del\",\"ids\":\"98,97,96,95,94,93,92,91,89,88,87,85,84,83,82\",\"params\":\"\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807385),
(449, 1, 'admin', '/admin2020.php/second/items/del/ids/81,80,79,78', '二手房 出售房源 删除', '{\"action\":\"del\",\"ids\":\"81,80,79,78\",\"params\":\"\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807393),
(450, 1, 'admin', '/admin2020.php/second/items/del/ids/77,76,75,73,72,70,67,66,64,62,61', '二手房 出售房源 删除', '{\"action\":\"del\",\"ids\":\"77,76,75,73,72,70,67,66,64,62,61\",\"params\":\"\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807404),
(451, 1, 'admin', '/admin2020.php/second/items/del/ids/74,71,69,68,60,59,58,57', '二手房 出售房源 删除', '{\"action\":\"del\",\"ids\":\"74,71,69,68,60,59,58,57\",\"params\":\"\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807411),
(452, 1, 'admin', '/admin2020.php/second/follow', '二手房 二手房跟进 查看', '{\"item_id\":\"343180\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807488),
(453, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807496),
(454, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807496),
(455, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"decoration\",\"keyField\":\"id\",\"searchField\":[\"decoration\"],\"decoration\":\"\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807505),
(456, 1, 'admin', '/admin2020.php/area/items/index', '', '{\"q_word\":[\"\"],\"pageNumber\":\"1\",\"pageSize\":\"10\",\"andOr\":\"AND\",\"orderBy\":[[\"areaName\",\"ASC\"]],\"searchTable\":\"tbl\",\"showField\":\"areaName\",\"keyField\":\"areaCode\",\"searchField\":[\"areaName\"],\"areaName\":\"\"}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807510),
(457, 1, 'admin', '/admin2020.php/second/decoration/index', '二手房 装修设置 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"2\",\"orderBy\":[[\"decoration\",\"ASC\"]],\"showField\":\"decoration\",\"keyField\":\"id\",\"keyValue\":\"2\",\"searchField\":[\"decoration\"]}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807645),
(458, 1, 'admin', '/admin2020.php/newhouse/building/types/index', '新房楼盘 房屋类型 查看', '{\"searchTable\":\"tbl\",\"searchKey\":\"id\",\"searchValue\":\"1\",\"orderBy\":[[\"type_name\",\"ASC\"]],\"showField\":\"type_name\",\"keyField\":\"id\",\"keyValue\":\"1\",\"searchField\":[\"type_name\"]}', '39.162.148.234', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0', 1609807645);

-- --------------------------------------------------------

--
-- 表的结构 `mf_area_items`
--

CREATE TABLE `mf_area_items` (
  `areaId` int(20) NOT NULL COMMENT '地区Id',
  `areaCode` varchar(50) NOT NULL COMMENT '地区编码',
  `areaName` varchar(20) NOT NULL COMMENT '地区名',
  `level` tinyint(4) DEFAULT '-1' COMMENT '地区级别（1:省份province,2:市city,3:区县district,4:街道street）',
  `cityCode` varchar(50) DEFAULT NULL COMMENT '城市编码',
  `center` varchar(50) DEFAULT NULL COMMENT '城市中心点（即：经纬度坐标）',
  `parentId` int(20) DEFAULT '-1' COMMENT '地区父节点'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地区码表';

--
-- 转存表中的数据 `mf_area_items`
--

INSERT INTO `mf_area_items` (`areaId`, `areaCode`, `areaName`, `level`, `cityCode`, `center`, `parentId`) VALUES
(1, '110000', '北京市', 1, '010', '116.407394,39.904211', -1),
(2, '110100', '北京城区', 2, '010', '116.407394,39.904211', 1),
(3, '110101', '东城区', 3, '010', '116.41649,39.928341', 2),
(4, '110102', '西城区', 3, '010', '116.365873,39.912235', 2),
(5, '110105', '朝阳区', 3, '010', '116.443205,39.921506', 2),
(6, '110106', '丰台区', 3, '010', '116.287039,39.858421', 2),
(7, '110107', '石景山区', 3, '010', '116.222933,39.906611', 2),
(8, '110108', '海淀区', 3, '010', '116.298262,39.95993', 2),
(9, '110109', '门头沟区', 3, '010', '116.101719,39.940338', 2),
(10, '110111', '房山区', 3, '010', '116.143486,39.748823', 2),
(11, '110112', '通州区', 3, '010', '116.656434,39.909946', 2),
(12, '110113', '顺义区', 3, '010', '116.654642,40.130211', 2),
(13, '110114', '昌平区', 3, '010', '116.231254,40.220804', 2),
(14, '110115', '大兴区', 3, '010', '116.341483,39.726917', 2),
(15, '110116', '怀柔区', 3, '010', '116.631931,40.316053', 2),
(16, '110117', '平谷区', 3, '010', '117.121351,40.140595', 2),
(17, '110118', '密云区', 3, '010', '116.843047,40.376894', 2),
(18, '110119', '延庆区', 3, '010', '115.974981,40.456591', 2),
(19, '120000', '天津市', 1, '022', '117.200983,39.084158', -1),
(20, '120100', '天津城区', 2, '022', '117.200983,39.084158', 19),
(21, '120101', '和平区', 3, '022', '117.214699,39.117196', 20),
(22, '120102', '河东区', 3, '022', '117.251584,39.128294', 20),
(23, '120103', '河西区', 3, '022', '117.223371,39.109563', 20),
(24, '120104', '南开区', 3, '022', '117.150738,39.138205', 20),
(25, '120105', '河北区', 3, '022', '117.196648,39.147869', 20),
(26, '120106', '红桥区', 3, '022', '117.151533,39.167345', 20),
(27, '120110', '东丽区', 3, '022', '117.31362,39.086802', 20),
(28, '120111', '西青区', 3, '022', '117.008826,39.141152', 20),
(29, '120112', '津南区', 3, '022', '117.35726,38.937928', 20),
(30, '120113', '北辰区', 3, '022', '117.135488,39.224791', 20),
(31, '120114', '武清区', 3, '022', '117.044387,39.384119', 20),
(32, '120115', '宝坻区', 3, '022', '117.309874,39.717564', 20),
(33, '120116', '滨海新区', 3, '022', '117.698407,39.01727', 20),
(34, '120117', '宁河区', 3, '022', '117.826724,39.330087', 20),
(35, '120118', '静海区', 3, '022', '116.974232,38.94745', 20),
(36, '120119', '蓟州区', 3, '022', '117.408296,40.045851', 20),
(37, '130000', '河北省', 1, '[]', '114.530235,38.037433', -1),
(38, '130100', '石家庄市', 2, '0311', '114.514793,38.042228', 37),
(39, '130102', '长安区', 3, '0311', '114.539395,38.036347', 38),
(40, '130104', '桥西区', 3, '0311', '114.461088,38.004193', 38),
(41, '130105', '新华区', 3, '0311', '114.463377,38.05095', 38),
(42, '130107', '井陉矿区', 3, '0311', '114.062062,38.065151', 38),
(43, '130108', '裕华区', 3, '0311', '114.531202,38.00643', 38),
(44, '130109', '藁城区', 3, '0311', '114.847023,38.021453', 38),
(45, '130110', '鹿泉区', 3, '0311', '114.313654,38.085953', 38),
(46, '130111', '栾城区', 3, '0311', '114.648318,37.900199', 38),
(47, '130121', '井陉县', 3, '0311', '114.145242,38.032118', 38),
(48, '130123', '正定县', 3, '0311', '114.570941,38.146444', 38),
(49, '130125', '行唐县', 3, '0311', '114.552714,38.438377', 38),
(50, '130126', '灵寿县', 3, '0311', '114.382614,38.308665', 38),
(51, '130127', '高邑县', 3, '0311', '114.611121,37.615534', 38),
(52, '130128', '深泽县', 3, '0311', '115.20092,38.184033', 38),
(53, '130129', '赞皇县', 3, '0311', '114.386111,37.665663', 38),
(54, '130130', '无极县', 3, '0311', '114.97634,38.179192', 38),
(55, '130131', '平山县', 3, '0311', '114.195918,38.247888', 38),
(56, '130132', '元氏县', 3, '0311', '114.525409,37.766513', 38),
(57, '130133', '赵县', 3, '0311', '114.776297,37.756578', 38),
(58, '130181', '辛集市', 3, '0311', '115.217658,37.943121', 38),
(59, '130183', '晋州市', 3, '0311', '115.044213,38.033671', 38),
(60, '130184', '新乐市', 3, '0311', '114.683776,38.343319', 38),
(61, '130200', '唐山市', 2, '0315', '118.180193,39.630867', 37),
(62, '130202', '路南区', 3, '0315', '118.154354,39.625058', 61),
(63, '130203', '路北区', 3, '0315', '118.200692,39.624437', 61),
(64, '130204', '古冶区', 3, '0315', '118.447635,39.733578', 61),
(65, '130205', '开平区', 3, '0315', '118.261841,39.671001', 61),
(66, '130207', '丰南区', 3, '0315', '118.085169,39.576031', 61),
(67, '130208', '丰润区', 3, '0315', '118.162215,39.832582', 61),
(68, '130209', '曹妃甸区', 3, '0315', '118.460379,39.27307', 61),
(69, '130223', '滦县', 3, '0315', '118.703598,39.740593', 61),
(70, '130224', '滦南县', 3, '0315', '118.682379,39.518996', 61),
(71, '130225', '乐亭县', 3, '0315', '118.912571,39.425608', 61),
(72, '130227', '迁西县', 3, '0315', '118.314715,40.1415', 61),
(73, '130229', '玉田县', 3, '0315', '117.738658,39.900401', 61),
(74, '130281', '遵化市', 3, '0315', '117.965892,40.189201', 61),
(75, '130283', '迁安市', 3, '0315', '118.701144,39.999174', 61),
(76, '130300', '秦皇岛市', 2, '0335', '119.518197,39.888701', 37),
(77, '130302', '海港区', 3, '0335', '119.564962,39.94756', 76),
(78, '130303', '山海关区', 3, '0335', '119.775799,39.978848', 76),
(79, '130304', '北戴河区', 3, '0335', '119.484522,39.834596', 76),
(80, '130306', '抚宁区', 3, '0335', '119.244847,39.876253', 76),
(81, '130321', '青龙满族自治县', 3, '0335', '118.949684,40.407578', 76),
(82, '130322', '昌黎县', 3, '0335', '119.199551,39.700911', 76),
(83, '130324', '卢龙县', 3, '0335', '118.892986,39.891946', 76),
(84, '130400', '邯郸市', 2, '0310', '114.538959,36.625594', 37),
(85, '130402', '邯山区', 3, '0310', '114.531002,36.594313', 84),
(86, '130403', '丛台区', 3, '0310', '114.492896,36.636409', 84),
(87, '130404', '复兴区', 3, '0310', '114.462061,36.639033', 84),
(88, '130406', '峰峰矿区', 3, '0310', '114.212802,36.419739', 84),
(89, '130423', '临漳县', 3, '0310', '114.619536,36.335025', 84),
(90, '130424', '成安县', 3, '0310', '114.670032,36.444317', 84),
(91, '130425', '大名县', 3, '0310', '115.147814,36.285616', 84),
(92, '130426', '涉县', 3, '0310', '113.6914,36.584994', 84),
(93, '130427', '磁县', 3, '0310', '114.373946,36.374011', 84),
(94, '130407', '肥乡区', 3, '0310', '114.800166,36.548131', 84),
(95, '130408', '永年区', 3, '0310', '114.543832,36.743966', 84),
(96, '130430', '邱县', 3, '0310', '115.200589,36.811148', 84),
(97, '130431', '鸡泽县', 3, '0310', '114.889376,36.91034', 84),
(98, '130432', '广平县', 3, '0310', '114.948606,36.483484', 84),
(99, '130433', '馆陶县', 3, '0310', '115.282467,36.547556', 84),
(100, '130434', '魏县', 3, '0310', '114.93892,36.359868', 84),
(101, '130435', '曲周县', 3, '0310', '114.957504,36.76607', 84),
(102, '130481', '武安市', 3, '0310', '114.203697,36.696506', 84),
(103, '130500', '邢台市', 2, '0319', '114.504677,37.070834', 37),
(104, '130502', '桥东区', 3, '0319', '114.507058,37.071287', 103),
(105, '130503', '桥西区', 3, '0319', '114.468601,37.059827', 103),
(106, '130521', '邢台县', 3, '0319', '114.561132,37.05073', 103),
(107, '130522', '临城县', 3, '0319', '114.498761,37.444498', 103),
(108, '130523', '内丘县', 3, '0319', '114.512128,37.286669', 103),
(109, '130524', '柏乡县', 3, '0319', '114.693425,37.482422', 103),
(110, '130525', '隆尧县', 3, '0319', '114.770419,37.350172', 103),
(111, '130526', '任县', 3, '0319', '114.671936,37.120982', 103),
(112, '130527', '南和县', 3, '0319', '114.683863,37.005017', 103),
(113, '130528', '宁晋县', 3, '0319', '114.93992,37.624564', 103),
(114, '130529', '巨鹿县', 3, '0319', '115.037477,37.221112', 103),
(115, '130530', '新河县', 3, '0319', '115.250907,37.520862', 103),
(116, '130531', '广宗县', 3, '0319', '115.142626,37.074661', 103),
(117, '130532', '平乡县', 3, '0319', '115.030075,37.063148', 103),
(118, '130533', '威县', 3, '0319', '115.266703,36.975478', 103),
(119, '130534', '清河县', 3, '0319', '115.667208,37.039991', 103),
(120, '130535', '临西县', 3, '0319', '115.501048,36.870811', 103),
(121, '130581', '南宫市', 3, '0319', '115.408747,37.359264', 103),
(122, '130582', '沙河市', 3, '0319', '114.503339,36.854929', 103),
(123, '130600', '保定市', 2, '0312', '115.464589,38.874434', 37),
(124, '130602', '竞秀区', 3, '0312', '115.45877,38.877449', 123),
(125, '130606', '莲池区', 3, '0312', '115.497097,38.883582', 123),
(126, '130607', '满城区', 3, '0312', '115.322334,38.949119', 123),
(127, '130608', '清苑区', 3, '0312', '115.489959,38.765148', 123),
(128, '130609', '徐水区', 3, '0312', '115.655774,39.018736', 123),
(129, '130623', '涞水县', 3, '0312', '115.713904,39.394316', 123),
(130, '130624', '阜平县', 3, '0312', '114.195104,38.849152', 123),
(131, '130626', '定兴县', 3, '0312', '115.808296,39.263145', 123),
(132, '130627', '唐县', 3, '0312', '114.982972,38.748203', 123),
(133, '130628', '高阳县', 3, '0312', '115.778965,38.700088', 123),
(134, '130629', '容城县', 3, '0312', '115.861657,39.042784', 123),
(135, '130630', '涞源县', 3, '0312', '114.694283,39.360247', 123),
(136, '130631', '望都县', 3, '0312', '115.155128,38.695842', 123),
(137, '130632', '安新县', 3, '0312', '115.935603,38.935369', 123),
(138, '130633', '易县', 3, '0312', '115.497457,39.349393', 123),
(139, '130634', '曲阳县', 3, '0312', '114.745008,38.622248', 123),
(140, '130635', '蠡县', 3, '0312', '115.583854,38.488055', 123),
(141, '130636', '顺平县', 3, '0312', '115.13547,38.837487', 123),
(142, '130637', '博野县', 3, '0312', '115.46438,38.457364', 123),
(143, '130638', '雄县', 3, '0312', '116.10865,38.99455', 123),
(144, '130681', '涿州市', 3, '0312', '115.974422,39.485282', 123),
(145, '130682', '定州市', 3, '0312', '114.990392,38.516461', 123),
(146, '130683', '安国市', 3, '0312', '115.326646,38.418439', 123),
(147, '130684', '高碑店市', 3, '0312', '115.873886,39.326839', 123),
(148, '130700', '张家口市', 2, '0313', '114.886252,40.768493', 37),
(149, '130702', '桥东区', 3, '0313', '114.894189,40.788434', 148),
(150, '130703', '桥西区', 3, '0313', '114.869657,40.819581', 148),
(151, '130705', '宣化区', 3, '0313', '115.099494,40.608793', 148),
(152, '130706', '下花园区', 3, '0313', '115.287352,40.502652', 148),
(153, '130708', '万全区', 3, '0313', '114.740557,40.766965', 148),
(154, '130709', '崇礼区', 3, '0313', '115.282668,40.974675', 148),
(155, '130722', '张北县', 3, '0313', '114.720077,41.158596', 148),
(156, '130723', '康保县', 3, '0313', '114.600404,41.852368', 148),
(157, '130724', '沽源县', 3, '0313', '115.688692,41.669668', 148),
(158, '130725', '尚义县', 3, '0313', '113.969618,41.076226', 148),
(159, '130726', '蔚县', 3, '0313', '114.588903,39.840842', 148),
(160, '130727', '阳原县', 3, '0313', '114.150348,40.104663', 148),
(161, '130728', '怀安县', 3, '0313', '114.385791,40.674193', 148),
(162, '130730', '怀来县', 3, '0313', '115.517861,40.415343', 148),
(163, '130731', '涿鹿县', 3, '0313', '115.205345,40.379562', 148),
(164, '130732', '赤城县', 3, '0313', '115.831498,40.912921', 148),
(165, '130800', '承德市', 2, '0314', '117.962749,40.952942', 37),
(166, '130802', '双桥区', 3, '0314', '117.943466,40.974643', 165),
(167, '130803', '双滦区', 3, '0314', '117.799888,40.959236', 165),
(168, '130804', '鹰手营子矿区', 3, '0314', '117.659499,40.546361', 165),
(169, '130821', '承德县', 3, '0314', '118.173824,40.768238', 165),
(170, '130822', '兴隆县', 3, '0314', '117.500558,40.417358', 165),
(171, '130881', '平泉市', 3, '0314', '118.701951,41.018405', 165),
(172, '130824', '滦平县', 3, '0314', '117.332801,40.941482', 165),
(173, '130825', '隆化县', 3, '0314', '117.738937,41.313791', 165),
(174, '130826', '丰宁满族自治县', 3, '0314', '116.646051,41.209069', 165),
(175, '130827', '宽城满族自治县', 3, '0314', '118.485313,40.611391', 165),
(176, '130828', '围场满族蒙古族自治县', 3, '0314', '117.760159,41.938529', 165),
(177, '130900', '沧州市', 2, '0317', '116.838834,38.304477', 37),
(178, '130902', '新华区', 3, '0317', '116.866284,38.314416', 177),
(179, '130903', '运河区', 3, '0317', '116.843673,38.283749', 177),
(180, '130921', '沧县', 3, '0317', '117.007478,38.219856', 177),
(181, '130922', '青县', 3, '0317', '116.804305,38.583021', 177),
(182, '130923', '东光县', 3, '0317', '116.537067,37.888248', 177),
(183, '130924', '海兴县', 3, '0317', '117.497651,38.143169', 177),
(184, '130925', '盐山县', 3, '0317', '117.230602,38.058087', 177),
(185, '130926', '肃宁县', 3, '0317', '115.829758,38.422801', 177),
(186, '130927', '南皮县', 3, '0317', '116.708347,38.038421', 177),
(187, '130928', '吴桥县', 3, '0317', '116.391508,37.627661', 177),
(188, '130929', '献县', 3, '0317', '116.122725,38.190185', 177),
(189, '130930', '孟村回族自治县', 3, '0317', '117.104298,38.053409', 177),
(190, '130981', '泊头市', 3, '0317', '116.578367,38.083437', 177),
(191, '130982', '任丘市', 3, '0317', '116.082917,38.683591', 177),
(192, '130983', '黄骅市', 3, '0317', '117.329949,38.371402', 177),
(193, '130984', '河间市', 3, '0317', '116.099517,38.446624', 177),
(194, '131000', '廊坊市', 2, '0316', '116.683752,39.538047', 37),
(195, '131002', '安次区', 3, '0316', '116.694544,39.502569', 194),
(196, '131003', '广阳区', 3, '0316', '116.71069,39.522786', 194),
(197, '131022', '固安县', 3, '0316', '116.298657,39.438214', 194),
(198, '131023', '永清县', 3, '0316', '116.50568,39.330689', 194),
(199, '131024', '香河县', 3, '0316', '117.006093,39.761424', 194),
(200, '131025', '大城县', 3, '0316', '116.653793,38.705449', 194),
(201, '131026', '文安县', 3, '0316', '116.457898,38.87292', 194),
(202, '131028', '大厂回族自治县', 3, '0316', '116.989574,39.886547', 194),
(203, '131081', '霸州市', 3, '0316', '116.391484,39.125744', 194),
(204, '131082', '三河市', 3, '0316', '117.078294,39.982718', 194),
(205, '131100', '衡水市', 2, '0318', '115.670177,37.73892', 37),
(206, '131102', '桃城区', 3, '0318', '115.67545,37.735465', 205),
(207, '131103', '冀州区', 3, '0318', '115.579308,37.550856', 205),
(208, '131121', '枣强县', 3, '0318', '115.724259,37.513417', 205),
(209, '131122', '武邑县', 3, '0318', '115.887531,37.801665', 205),
(210, '131123', '武强县', 3, '0318', '115.982461,38.041368', 205),
(211, '131124', '饶阳县', 3, '0318', '115.725833,38.235892', 205),
(212, '131125', '安平县', 3, '0318', '115.519278,38.234501', 205),
(213, '131126', '故城县', 3, '0318', '115.965874,37.347409', 205),
(214, '131127', '景县', 3, '0318', '116.270648,37.69229', 205),
(215, '131128', '阜城县', 3, '0318', '116.175262,37.862505', 205),
(216, '131182', '深州市', 3, '0318', '115.559574,38.001535', 205),
(217, '140000', '山西省', 1, '[]', '112.562678,37.873499', -1),
(218, '140100', '太原市', 2, '0351', '112.548879,37.87059', 217),
(219, '140105', '小店区', 3, '0351', '112.565659,37.736525', 218),
(220, '140106', '迎泽区', 3, '0351', '112.5634,37.863451', 218),
(221, '140107', '杏花岭区', 3, '0351', '112.570604,37.893955', 218),
(222, '140108', '尖草坪区', 3, '0351', '112.486691,37.940387', 218),
(223, '140109', '万柏林区', 3, '0351', '112.515937,37.85958', 218),
(224, '140110', '晋源区', 3, '0351', '112.47794,37.715193', 218),
(225, '140121', '清徐县', 3, '0351', '112.358667,37.607443', 218),
(226, '140122', '阳曲县', 3, '0351', '112.672952,38.058488', 218),
(227, '140123', '娄烦县', 3, '0351', '111.797083,38.067932', 218),
(228, '140181', '古交市', 3, '0351', '112.175853,37.907129', 218),
(229, '140200', '大同市', 2, '0352', '113.300129,40.076763', 217),
(230, '140202', '城区', 3, '0352', '113.298026,40.075666', 229),
(231, '140203', '矿区', 3, '0352', '113.177206,40.036858', 229),
(232, '140211', '南郊区', 3, '0352', '113.149693,40.005404', 229),
(233, '140212', '新荣区', 3, '0352', '113.140004,40.255866', 229),
(234, '140221', '阳高县', 3, '0352', '113.748944,40.361059', 229),
(235, '140222', '天镇县', 3, '0352', '114.090867,40.420237', 229),
(236, '140223', '广灵县', 3, '0352', '114.282758,39.760281', 229),
(237, '140224', '灵丘县', 3, '0352', '114.23435,39.442406', 229),
(238, '140225', '浑源县', 3, '0352', '113.699475,39.693406', 229),
(239, '140226', '左云县', 3, '0352', '112.703008,40.013442', 229),
(240, '140227', '大同县', 3, '0352', '113.61244,40.040294', 229),
(241, '140300', '阳泉市', 2, '0353', '113.580519,37.856971', 217),
(242, '140302', '城区', 3, '0353', '113.600669,37.847436', 241),
(243, '140303', '矿区', 3, '0353', '113.555279,37.868494', 241),
(244, '140311', '郊区', 3, '0353', '113.594163,37.944679', 241),
(245, '140321', '平定县', 3, '0353', '113.630107,37.804988', 241),
(246, '140322', '盂县', 3, '0353', '113.41233,38.085619', 241),
(247, '140400', '长治市', 2, '0355', '113.116404,36.195409', 217),
(248, '140402', '城区', 3, '0355', '113.123088,36.20353', 247),
(249, '140411', '郊区', 3, '0355', '113.101211,36.218388', 247),
(250, '140421', '长治县', 3, '0355', '113.051407,36.052858', 247),
(251, '140423', '襄垣县', 3, '0355', '113.051491,36.535817', 247),
(252, '140424', '屯留县', 3, '0355', '112.891998,36.315663', 247),
(253, '140425', '平顺县', 3, '0355', '113.435961,36.200179', 247),
(254, '140426', '黎城县', 3, '0355', '113.387155,36.502328', 247),
(255, '140427', '壶关县', 3, '0355', '113.207049,36.115448', 247),
(256, '140428', '长子县', 3, '0355', '112.8779,36.122334', 247),
(257, '140429', '武乡县', 3, '0355', '112.864561,36.837625', 247),
(258, '140430', '沁县', 3, '0355', '112.699226,36.756063', 247),
(259, '140431', '沁源县', 3, '0355', '112.337446,36.5002', 247),
(260, '140481', '潞城市', 3, '0355', '113.228852,36.334104', 247),
(261, '140500', '晋城市', 2, '0356', '112.851486,35.490684', 217),
(262, '140502', '城区', 3, '0356', '112.853555,35.501571', 261),
(263, '140521', '沁水县', 3, '0356', '112.186738,35.690141', 261),
(264, '140522', '阳城县', 3, '0356', '112.414738,35.486029', 261),
(265, '140524', '陵川县', 3, '0356', '113.280688,35.775685', 261),
(266, '140525', '泽州县', 3, '0356', '112.899137,35.617221', 261),
(267, '140581', '高平市', 3, '0356', '112.92392,35.797997', 261),
(268, '140600', '朔州市', 2, '0349', '112.432991,39.331855', 217),
(269, '140602', '朔城区', 3, '0349', '112.432312,39.319519', 268),
(270, '140603', '平鲁区', 3, '0349', '112.28833,39.512155', 268),
(271, '140621', '山阴县', 3, '0349', '112.816413,39.527893', 268),
(272, '140622', '应县', 3, '0349', '113.191098,39.554247', 268),
(273, '140623', '右玉县', 3, '0349', '112.466989,39.989063', 268),
(274, '140624', '怀仁县', 3, '0349', '113.131717,39.821627', 268),
(275, '140700', '晋中市', 2, '0354', '112.752652,37.687357', 217),
(276, '140702', '榆次区', 3, '0354', '112.708224,37.697794', 275),
(277, '140721', '榆社县', 3, '0354', '112.975209,37.070916', 275),
(278, '140722', '左权县', 3, '0354', '113.379403,37.082943', 275),
(279, '140723', '和顺县', 3, '0354', '113.570415,37.32957', 275),
(280, '140724', '昔阳县', 3, '0354', '113.706977,37.61253', 275),
(281, '140725', '寿阳县', 3, '0354', '113.176373,37.895191', 275),
(282, '140726', '太谷县', 3, '0354', '112.551305,37.421307', 275),
(283, '140727', '祁县', 3, '0354', '112.335542,37.357869', 275),
(284, '140728', '平遥县', 3, '0354', '112.176136,37.189421', 275),
(285, '140729', '灵石县', 3, '0354', '111.77864,36.847927', 275),
(286, '140781', '介休市', 3, '0354', '111.916711,37.026944', 275),
(287, '140800', '运城市', 2, '0359', '111.00746,35.026516', 217),
(288, '140802', '盐湖区', 3, '0359', '110.998272,35.015101', 287),
(289, '140821', '临猗县', 3, '0359', '110.774547,35.144277', 287),
(290, '140822', '万荣县', 3, '0359', '110.838024,35.415253', 287),
(291, '140823', '闻喜县', 3, '0359', '111.22472,35.356644', 287),
(292, '140824', '稷山县', 3, '0359', '110.983333,35.604025', 287),
(293, '140825', '新绛县', 3, '0359', '111.224734,35.616251', 287),
(294, '140826', '绛县', 3, '0359', '111.568236,35.49119', 287),
(295, '140827', '垣曲县', 3, '0359', '111.670108,35.297369', 287),
(296, '140828', '夏县', 3, '0359', '111.220456,35.141363', 287),
(297, '140829', '平陆县', 3, '0359', '111.194133,34.82926', 287),
(298, '140830', '芮城县', 3, '0359', '110.694369,34.693579', 287),
(299, '140881', '永济市', 3, '0359', '110.447543,34.8671', 287),
(300, '140882', '河津市', 3, '0359', '110.712063,35.596383', 287),
(301, '140900', '忻州市', 2, '0350', '112.734174,38.416663', 217),
(302, '140902', '忻府区', 3, '0350', '112.746046,38.404242', 301),
(303, '140921', '定襄县', 3, '0350', '112.957237,38.473506', 301),
(304, '140922', '五台县', 3, '0350', '113.255309,38.728315', 301),
(305, '140923', '代县', 3, '0350', '112.960282,39.066917', 301),
(306, '140924', '繁峙县', 3, '0350', '113.265563,39.188811', 301),
(307, '140925', '宁武县', 3, '0350', '112.304722,39.001524', 301),
(308, '140926', '静乐县', 3, '0350', '111.939498,38.359306', 301),
(309, '140927', '神池县', 3, '0350', '112.211296,39.090552', 301),
(310, '140928', '五寨县', 3, '0350', '111.846904,38.910726', 301),
(311, '140929', '岢岚县', 3, '0350', '111.57285,38.70418', 301),
(312, '140930', '河曲县', 3, '0350', '111.138472,39.384482', 301),
(313, '140931', '保德县', 3, '0350', '111.086564,39.022487', 301),
(314, '140932', '偏关县', 3, '0350', '111.508831,39.436306', 301),
(315, '140981', '原平市', 3, '0350', '112.711058,38.731402', 301),
(316, '141000', '临汾市', 2, '0357', '111.518975,36.088005', 217),
(317, '141002', '尧都区', 3, '0357', '111.579554,36.07884', 316),
(318, '141021', '曲沃县', 3, '0357', '111.47586,35.641086', 316),
(319, '141022', '翼城县', 3, '0357', '111.718951,35.738576', 316),
(320, '141023', '襄汾县', 3, '0357', '111.441725,35.876293', 316),
(321, '141024', '洪洞县', 3, '0357', '111.674965,36.253747', 316),
(322, '141025', '古县', 3, '0357', '111.920465,36.266914', 316),
(323, '141026', '安泽县', 3, '0357', '112.250144,36.147787', 316),
(324, '141027', '浮山县', 3, '0357', '111.848883,35.968124', 316),
(325, '141028', '吉县', 3, '0357', '110.681763,36.098188', 316),
(326, '141029', '乡宁县', 3, '0357', '110.847021,35.970389', 316),
(327, '141030', '大宁县', 3, '0357', '110.75291,36.465102', 316),
(328, '141031', '隰县', 3, '0357', '110.940637,36.69333', 316),
(329, '141032', '永和县', 3, '0357', '110.632006,36.759507', 316),
(330, '141033', '蒲县', 3, '0357', '111.096439,36.411826', 316),
(331, '141034', '汾西县', 3, '0357', '111.56395,36.652854', 316),
(332, '141081', '侯马市', 3, '0357', '111.372002,35.619105', 316),
(333, '141082', '霍州市', 3, '0357', '111.755398,36.56893', 316),
(334, '141100', '吕梁市', 2, '0358', '111.144699,37.519126', 217),
(335, '141102', '离石区', 3, '0358', '111.150695,37.51786', 334),
(336, '141121', '文水县', 3, '0358', '112.028866,37.438101', 334),
(337, '141122', '交城县', 3, '0358', '112.156064,37.551963', 334),
(338, '141123', '兴县', 3, '0358', '111.127667,38.462389', 334),
(339, '141124', '临县', 3, '0358', '110.992093,37.950758', 334),
(340, '141125', '柳林县', 3, '0358', '110.889007,37.429772', 334),
(341, '141126', '石楼县', 3, '0358', '110.834634,36.99857', 334),
(342, '141127', '岚县', 3, '0358', '111.671917,38.279299', 334),
(343, '141128', '方山县', 3, '0358', '111.244098,37.894631', 334),
(344, '141129', '中阳县', 3, '0358', '111.179657,37.357058', 334),
(345, '141130', '交口县', 3, '0358', '111.181151,36.982186', 334),
(346, '141181', '孝义市', 3, '0358', '111.778818,37.146294', 334),
(347, '141182', '汾阳市', 3, '0358', '111.770477,37.261756', 334),
(348, '150000', '内蒙古自治区', 1, '[]', '111.76629,40.81739', -1),
(349, '150100', '呼和浩特市', 2, '0471', '111.749995,40.842356', 348),
(350, '150102', '新城区', 3, '0471', '111.665544,40.858289', 349),
(351, '150103', '回民区', 3, '0471', '111.623692,40.808608', 349),
(352, '150104', '玉泉区', 3, '0471', '111.673881,40.753655', 349),
(353, '150105', '赛罕区', 3, '0471', '111.701355,40.792667', 349),
(354, '150121', '土默特左旗', 3, '0471', '111.163902,40.729572', 349),
(355, '150122', '托克托县', 3, '0471', '111.194312,40.277431', 349),
(356, '150123', '和林格尔县', 3, '0471', '111.821843,40.378787', 349),
(357, '150124', '清水河县', 3, '0471', '111.647609,39.921095', 349),
(358, '150125', '武川县', 3, '0471', '111.451303,41.096471', 349),
(359, '150200', '包头市', 2, '0472', '109.953504,40.621157', 348),
(360, '150202', '东河区', 3, '0472', '110.044106,40.576319', 359),
(361, '150203', '昆都仑区', 3, '0472', '109.837707,40.642578', 359),
(362, '150204', '青山区', 3, '0472', '109.901572,40.643246', 359),
(363, '150205', '石拐区', 3, '0472', '110.060254,40.681748', 359),
(364, '150206', '白云鄂博矿区', 3, '0472', '109.973803,41.769511', 359),
(365, '150207', '九原区', 3, '0472', '109.967449,40.610561', 359),
(366, '150221', '土默特右旗', 3, '0472', '110.524262,40.569426', 359),
(367, '150222', '固阳县', 3, '0472', '110.060514,41.034105', 359),
(368, '150223', '达尔罕茂明安联合旗', 3, '0472', '110.432626,41.698992', 359),
(369, '150300', '乌海市', 2, '0473', '106.794216,39.655248', 348),
(370, '150302', '海勃湾区', 3, '0473', '106.822778,39.691156', 369),
(371, '150303', '海南区', 3, '0473', '106.891424,39.441364', 369),
(372, '150304', '乌达区', 3, '0473', '106.726099,39.505925', 369),
(373, '150400', '赤峰市', 2, '0476', '118.88694,42.257843', 348),
(374, '150402', '红山区', 3, '0476', '118.953854,42.296588', 373),
(375, '150403', '元宝山区', 3, '0476', '119.288611,42.038902', 373),
(376, '150404', '松山区', 3, '0476', '118.916208,42.299798', 373),
(377, '150421', '阿鲁科尔沁旗', 3, '0476', '120.0657,43.872298', 373),
(378, '150422', '巴林左旗', 3, '0476', '119.362931,43.960889', 373),
(379, '150423', '巴林右旗', 3, '0476', '118.66518,43.534414', 373),
(380, '150424', '林西县', 3, '0476', '118.05545,43.61812', 373),
(381, '150425', '克什克腾旗', 3, '0476', '117.545797,43.264988', 373),
(382, '150426', '翁牛特旗', 3, '0476', '119.00658,42.936188', 373),
(383, '150428', '喀喇沁旗', 3, '0476', '118.701937,41.927363', 373),
(384, '150429', '宁城县', 3, '0476', '119.318876,41.601375', 373),
(385, '150430', '敖汉旗', 3, '0476', '119.921603,42.290781', 373),
(386, '150500', '通辽市', 2, '0475', '122.243444,43.652889', 348),
(387, '150502', '科尔沁区', 3, '0475', '122.255671,43.623078', 386),
(388, '150521', '科尔沁左翼中旗', 3, '0475', '123.312264,44.126625', 386),
(389, '150522', '科尔沁左翼后旗', 3, '0475', '122.35677,42.935105', 386),
(390, '150523', '开鲁县', 3, '0475', '121.319308,43.601244', 386),
(391, '150524', '库伦旗', 3, '0475', '121.8107,42.735656', 386),
(392, '150525', '奈曼旗', 3, '0475', '120.658282,42.867226', 386),
(393, '150526', '扎鲁特旗', 3, '0475', '120.911676,44.556389', 386),
(394, '150581', '霍林郭勒市', 3, '0475', '119.68187,45.533962', 386),
(395, '150600', '鄂尔多斯市', 2, '0477', '109.781327,39.608266', 348),
(396, '150602', '东胜区', 3, '0477', '109.963333,39.822593', 395),
(397, '150603', '康巴什区', 3, '0477', '109.790076,39.607472', 395),
(398, '150621', '达拉特旗', 3, '0477', '110.033833,40.412438', 395),
(399, '150622', '准格尔旗', 3, '0477', '111.240171,39.864361', 395),
(400, '150623', '鄂托克前旗', 3, '0477', '107.477514,38.182362', 395),
(401, '150624', '鄂托克旗', 3, '0477', '107.97616,39.08965', 395),
(402, '150625', '杭锦旗', 3, '0477', '108.736208,39.833309', 395),
(403, '150626', '乌审旗', 3, '0477', '108.817607,38.604136', 395),
(404, '150627', '伊金霍洛旗', 3, '0477', '109.74774,39.564659', 395),
(405, '150700', '呼伦贝尔市', 2, '0470', '119.765558,49.211576', 348),
(406, '150702', '海拉尔区', 3, '0470', '119.736176,49.212188', 405),
(407, '150703', '扎赉诺尔区', 3, '0470', '117.670248,49.510375', 405),
(408, '150721', '阿荣旗', 3, '0470', '123.459049,48.126584', 405),
(409, '150722', '莫力达瓦达斡尔族自治旗', 3, '0470', '124.519023,48.477728', 405),
(410, '150723', '鄂伦春自治旗', 3, '0470', '123.726201,50.591842', 405),
(411, '150724', '鄂温克族自治旗', 3, '0470', '119.755239,49.146592', 405),
(412, '150725', '陈巴尔虎旗', 3, '0470', '119.424026,49.328916', 405),
(413, '150726', '新巴尔虎左旗', 3, '0470', '118.269819,48.218241', 405),
(414, '150727', '新巴尔虎右旗', 3, '0470', '116.82369,48.672101', 405),
(415, '150781', '满洲里市', 3, '0470', '117.378529,49.597841', 405),
(416, '150782', '牙克石市', 3, '0470', '120.711775,49.285629', 405),
(417, '150783', '扎兰屯市', 3, '0470', '122.737467,48.013733', 405),
(418, '150784', '额尔古纳市', 3, '0470', '120.180506,50.243102', 405),
(419, '150785', '根河市', 3, '0470', '121.520388,50.780344', 405),
(420, '150800', '巴彦淖尔市', 2, '0478', '107.387657,40.743213', 348),
(421, '150802', '临河区', 3, '0478', '107.363918,40.751187', 420),
(422, '150821', '五原县', 3, '0478', '108.267561,41.088421', 420),
(423, '150822', '磴口县', 3, '0478', '107.008248,40.330523', 420),
(424, '150823', '乌拉特前旗', 3, '0478', '108.652114,40.737018', 420),
(425, '150824', '乌拉特中旗', 3, '0478', '108.513645,41.587732', 420),
(426, '150825', '乌拉特后旗', 3, '0478', '107.074621,41.084282', 420),
(427, '150826', '杭锦后旗', 3, '0478', '107.151245,40.88602', 420),
(428, '150900', '乌兰察布市', 2, '0474', '113.132584,40.994785', 348),
(429, '150902', '集宁区', 3, '0474', '113.116453,41.034134', 428),
(430, '150921', '卓资县', 3, '0474', '112.577528,40.894691', 428),
(431, '150922', '化德县', 3, '0474', '114.010437,41.90456', 428),
(432, '150923', '商都县', 3, '0474', '113.577816,41.562113', 428),
(433, '150924', '兴和县', 3, '0474', '113.834173,40.872301', 428),
(434, '150925', '凉城县', 3, '0474', '112.503971,40.531555', 428),
(435, '150926', '察哈尔右翼前旗', 3, '0474', '113.214733,40.785631', 428),
(436, '150927', '察哈尔右翼中旗', 3, '0474', '112.635577,41.277462', 428),
(437, '150928', '察哈尔右翼后旗', 3, '0474', '113.191035,41.436069', 428),
(438, '150929', '四子王旗', 3, '0474', '111.706617,41.533462', 428),
(439, '150981', '丰镇市', 3, '0474', '113.109892,40.436983', 428),
(440, '152200', '兴安盟', 2, '0482', '122.037657,46.082462', 348),
(441, '152201', '乌兰浩特市', 3, '0482', '122.093123,46.072731', 440),
(442, '152202', '阿尔山市', 3, '0482', '119.943575,47.17744', 440),
(443, '152221', '科尔沁右翼前旗', 3, '0482', '121.952621,46.079833', 440),
(444, '152222', '科尔沁右翼中旗', 3, '0482', '121.47653,45.060837', 440),
(445, '152223', '扎赉特旗', 3, '0482', '122.899656,46.723237', 440),
(446, '152224', '突泉县', 3, '0482', '121.593799,45.38193', 440),
(447, '152500', '锡林郭勒盟', 2, '0479', '116.048222,43.933454', 348),
(448, '152501', '二连浩特市', 3, '0479', '111.951002,43.6437', 447),
(449, '152502', '锡林浩特市', 3, '0479', '116.086029,43.933403', 447),
(450, '152522', '阿巴嘎旗', 3, '0479', '114.950248,44.022995', 447),
(451, '152523', '苏尼特左旗', 3, '0479', '113.667248,43.85988', 447),
(452, '152524', '苏尼特右旗', 3, '0479', '112.641783,42.742892', 447),
(453, '152525', '东乌珠穆沁旗', 3, '0479', '116.974494,45.498221', 447),
(454, '152526', '西乌珠穆沁旗', 3, '0479', '117.608911,44.587882', 447),
(455, '152527', '太仆寺旗', 3, '0479', '115.282986,41.877135', 447),
(456, '152528', '镶黄旗', 3, '0479', '113.847287,42.232371', 447),
(457, '152529', '正镶白旗', 3, '0479', '115.029848,42.28747', 447),
(458, '152530', '正蓝旗', 3, '0479', '115.99247,42.241638', 447),
(459, '152531', '多伦县', 3, '0479', '116.485555,42.203591', 447),
(460, '152900', '阿拉善盟', 2, '0483', '105.728957,38.851921', 348),
(461, '152921', '阿拉善左旗', 3, '0483', '105.666275,38.833389', 460),
(462, '152922', '阿拉善右旗', 3, '0483', '101.666917,39.216185', 460),
(463, '152923', '额济纳旗', 3, '0483', '101.055731,41.95455', 460),
(464, '210000', '辽宁省', 1, '[]', '123.431382,41.836175', -1),
(465, '210100', '沈阳市', 2, '024', '123.465035,41.677284', 464),
(466, '210102', '和平区', 3, '024', '123.420368,41.789833', 465),
(467, '210103', '沈河区', 3, '024', '123.458691,41.796177', 465),
(468, '210104', '大东区', 3, '024', '123.469948,41.805137', 465),
(469, '210105', '皇姑区', 3, '024', '123.442378,41.824516', 465),
(470, '210106', '铁西区', 3, '024', '123.333968,41.820807', 465),
(471, '210111', '苏家屯区', 3, '024', '123.344062,41.664757', 465),
(472, '210112', '浑南区', 3, '024', '123.449714,41.714914', 465),
(473, '210113', '沈北新区', 3, '024', '123.583196,41.912487', 465),
(474, '210114', '于洪区', 3, '024', '123.308119,41.793721', 465),
(475, '210115', '辽中区', 3, '024', '122.765409,41.516826', 465),
(476, '210123', '康平县', 3, '024', '123.343699,42.72793', 465),
(477, '210124', '法库县', 3, '024', '123.440294,42.50108', 465),
(478, '210181', '新民市', 3, '024', '122.836723,41.985186', 465),
(479, '210200', '大连市', 2, '0411', '121.614848,38.914086', 464),
(480, '210202', '中山区', 3, '0411', '121.644926,38.918574', 479),
(481, '210203', '西岗区', 3, '0411', '121.612324,38.914687', 479),
(482, '210204', '沙河口区', 3, '0411', '121.594297,38.904788', 479),
(483, '210211', '甘井子区', 3, '0411', '121.525466,38.953343', 479),
(484, '210212', '旅顺口区', 3, '0411', '121.261953,38.851705', 479),
(485, '210213', '金州区', 3, '0411', '121.782655,39.050001', 479),
(486, '210214', '普兰店区', 3, '0411', '121.938269,39.392095', 479),
(487, '210224', '长海县', 3, '0411', '122.588494,39.272728', 479),
(488, '210281', '瓦房店市', 3, '0411', '121.979543,39.626897', 479),
(489, '210283', '庄河市', 3, '0411', '122.967424,39.680843', 479),
(490, '210300', '鞍山市', 2, '0412', '122.994329,41.108647', 464),
(491, '210302', '铁东区', 3, '0412', '122.991052,41.089933', 490),
(492, '210303', '铁西区', 3, '0412', '122.969629,41.119884', 490),
(493, '210304', '立山区', 3, '0412', '123.029091,41.150401', 490),
(494, '210311', '千山区', 3, '0412', '122.944751,41.068901', 490),
(495, '210321', '台安县', 3, '0412', '122.436196,41.412767', 490),
(496, '210323', '岫岩满族自治县', 3, '0412', '123.280935,40.29088', 490),
(497, '210381', '海城市', 3, '0412', '122.685217,40.882377', 490),
(498, '210400', '抚顺市', 2, '0413', '123.957208,41.880872', 464),
(499, '210402', '新抚区', 3, '0413', '123.912872,41.862026', 498),
(500, '210403', '东洲区', 3, '0413', '124.038685,41.853191', 498),
(501, '210404', '望花区', 3, '0413', '123.784225,41.853641', 498),
(502, '210411', '顺城区', 3, '0413', '123.945075,41.883235', 498),
(503, '210421', '抚顺县', 3, '0413', '124.097978,41.922644', 498),
(504, '210422', '新宾满族自治县', 3, '0413', '125.039978,41.734256', 498),
(505, '210423', '清原满族自治县', 3, '0413', '124.924083,42.100538', 498),
(506, '210500', '本溪市', 2, '0414', '123.685142,41.486981', 464),
(507, '210502', '平山区', 3, '0414', '123.769088,41.299587', 506),
(508, '210503', '溪湖区', 3, '0414', '123.767646,41.329219', 506),
(509, '210504', '明山区', 3, '0414', '123.817214,41.308719', 506),
(510, '210505', '南芬区', 3, '0414', '123.744802,41.100445', 506),
(511, '210521', '本溪满族自治县', 3, '0414', '124.120635,41.302009', 506),
(512, '210522', '桓仁满族自治县', 3, '0414', '125.361007,41.267127', 506),
(513, '210600', '丹东市', 2, '0415', '124.35445,40.000787', 464),
(514, '210602', '元宝区', 3, '0415', '124.395661,40.136434', 513),
(515, '210603', '振兴区', 3, '0415', '124.383237,40.129944', 513),
(516, '210604', '振安区', 3, '0415', '124.470034,40.201553', 513),
(517, '210624', '宽甸满族自治县', 3, '0415', '124.783659,40.731316', 513),
(518, '210681', '东港市', 3, '0415', '124.152705,39.863008', 513),
(519, '210682', '凤城市', 3, '0415', '124.066919,40.452297', 513),
(520, '210700', '锦州市', 2, '0416', '121.126846,41.095685', 464),
(521, '210702', '古塔区', 3, '0416', '121.128279,41.117245', 520),
(522, '210703', '凌河区', 3, '0416', '121.150877,41.114989', 520),
(523, '210711', '太和区', 3, '0416', '121.103892,41.109147', 520),
(524, '210726', '黑山县', 3, '0416', '122.126292,41.653593', 520),
(525, '210727', '义县', 3, '0416', '121.23908,41.533086', 520),
(526, '210781', '凌海市', 3, '0416', '121.35549,41.160567', 520),
(527, '210782', '北镇市', 3, '0416', '121.777395,41.58844', 520),
(528, '210800', '营口市', 2, '0417', '122.219458,40.625364', 464),
(529, '210802', '站前区', 3, '0417', '122.259033,40.672563', 528),
(530, '210803', '西市区', 3, '0417', '122.206419,40.666213', 528),
(531, '210804', '鲅鱼圈区', 3, '0417', '122.121521,40.226661', 528),
(532, '210811', '老边区', 3, '0417', '122.380087,40.680191', 528),
(533, '210881', '盖州市', 3, '0417', '122.349012,40.40074', 528),
(534, '210882', '大石桥市', 3, '0417', '122.509006,40.644482', 528),
(535, '210900', '阜新市', 2, '0418', '121.670273,42.021602', 464),
(536, '210902', '海州区', 3, '0418', '121.657638,42.011162', 535),
(537, '210903', '新邱区', 3, '0418', '121.792535,42.087632', 535),
(538, '210904', '太平区', 3, '0418', '121.678604,42.010669', 535),
(539, '210905', '清河门区', 3, '0418', '121.416105,41.7831', 535),
(540, '210911', '细河区', 3, '0418', '121.68054,42.025494', 535),
(541, '210921', '阜新蒙古族自治县', 3, '0418', '121.757901,42.065175', 535),
(542, '210922', '彰武县', 3, '0418', '122.538793,42.386543', 535),
(543, '211000', '辽阳市', 2, '0419', '123.236974,41.267794', 464),
(544, '211002', '白塔区', 3, '0419', '123.174325,41.270347', 543),
(545, '211003', '文圣区', 3, '0419', '123.231408,41.283754', 543),
(546, '211004', '宏伟区', 3, '0419', '123.196672,41.217649', 543),
(547, '211005', '弓长岭区', 3, '0419', '123.419803,41.151847', 543),
(548, '211011', '太子河区', 3, '0419', '123.18144,41.295023', 543),
(549, '211021', '辽阳县', 3, '0419', '123.105694,41.205329', 543),
(550, '211081', '灯塔市', 3, '0419', '123.339312,41.426372', 543),
(551, '211100', '盘锦市', 2, '0427', '122.170584,40.719847', 464),
(552, '211102', '双台子区', 3, '0427', '122.039787,41.19965', 551),
(553, '211103', '兴隆台区', 3, '0427', '122.070769,41.119898', 551),
(554, '211104', '大洼区', 3, '0427', '122.082574,41.002279', 551),
(555, '211122', '盘山县', 3, '0427', '121.996411,41.242639', 551),
(556, '211200', '铁岭市', 2, '0410', '123.726035,42.223828', 464),
(557, '211202', '银州区', 3, '0410', '123.842305,42.286129', 556),
(558, '211204', '清河区', 3, '0410', '124.159191,42.546565', 556),
(559, '211221', '铁岭县', 3, '0410', '123.728933,42.223395', 556),
(560, '211223', '西丰县', 3, '0410', '124.727392,42.73803', 556),
(561, '211224', '昌图县', 3, '0410', '124.111099,42.785791', 556),
(562, '211281', '调兵山市', 3, '0410', '123.567117,42.467521', 556),
(563, '211282', '开原市', 3, '0410', '124.038268,42.546307', 556),
(564, '211300', '朝阳市', 2, '0421', '120.450879,41.573762', 464),
(565, '211302', '双塔区', 3, '0421', '120.453744,41.565627', 564),
(566, '211303', '龙城区', 3, '0421', '120.413376,41.576749', 564),
(567, '211321', '朝阳县', 3, '0421', '120.389754,41.497825', 564),
(568, '211322', '建平县', 3, '0421', '119.64328,41.403128', 564),
(569, '211324', '喀喇沁左翼蒙古族自治县', 3, '0421', '119.741223,41.12815', 564),
(570, '211381', '北票市', 3, '0421', '120.77073,41.800683', 564),
(571, '211382', '凌源市', 3, '0421', '119.401574,41.245445', 564),
(572, '211400', '葫芦岛市', 2, '0429', '120.836939,40.71104', 464),
(573, '211402', '连山区', 3, '0429', '120.869231,40.774461', 572),
(574, '211403', '龙港区', 3, '0429', '120.893786,40.735519', 572),
(575, '211404', '南票区', 3, '0429', '120.749727,41.107107', 572),
(576, '211421', '绥中县', 3, '0429', '120.344311,40.32558', 572),
(577, '211422', '建昌县', 3, '0429', '119.837124,40.824367', 572),
(578, '211481', '兴城市', 3, '0429', '120.756479,40.609731', 572),
(579, '220000', '吉林省', 1, '[]', '125.32568,43.897016', -1),
(580, '220100', '长春市', 2, '0431', '125.323513,43.817251', 579),
(581, '220102', '南关区', 3, '0431', '125.350173,43.863989', 580),
(582, '220103', '宽城区', 3, '0431', '125.326581,43.943612', 580),
(583, '220104', '朝阳区', 3, '0431', '125.288254,43.833762', 580),
(584, '220105', '二道区', 3, '0431', '125.374327,43.865577', 580),
(585, '220106', '绿园区', 3, '0431', '125.256135,43.880975', 580),
(586, '220112', '双阳区', 3, '0431', '125.664662,43.525311', 580),
(587, '220113', '九台区', 3, '0431', '125.839573,44.151742', 580),
(588, '220122', '农安县', 3, '0431', '125.184887,44.432763', 580),
(589, '220182', '榆树市', 3, '0431', '126.533187,44.840318', 580),
(590, '220183', '德惠市', 3, '0431', '125.728755,44.522056', 580),
(591, '220200', '吉林市', 2, '0432', '126.549572,43.837883', 579),
(592, '220202', '昌邑区', 3, '0432', '126.574709,43.881818', 591),
(593, '220203', '龙潭区', 3, '0432', '126.562197,43.910802', 591),
(594, '220204', '船营区', 3, '0432', '126.540966,43.833445', 591),
(595, '220211', '丰满区', 3, '0432', '126.562274,43.821601', 591),
(596, '220221', '永吉县', 3, '0432', '126.497741,43.672582', 591),
(597, '220281', '蛟河市', 3, '0432', '127.344229,43.724007', 591),
(598, '220282', '桦甸市', 3, '0432', '126.746309,42.972096', 591),
(599, '220283', '舒兰市', 3, '0432', '126.965607,44.406105', 591),
(600, '220284', '磐石市', 3, '0432', '126.060427,42.946285', 591),
(601, '220300', '四平市', 2, '0434', '124.350398,43.166419', 579),
(602, '220302', '铁西区', 3, '0434', '124.345722,43.146155', 601),
(603, '220303', '铁东区', 3, '0434', '124.409591,43.162105', 601),
(604, '220322', '梨树县', 3, '0434', '124.33539,43.30706', 601),
(605, '220323', '伊通满族自治县', 3, '0434', '125.305393,43.345754', 601),
(606, '220381', '公主岭市', 3, '0434', '124.822929,43.504676', 601),
(607, '220382', '双辽市', 3, '0434', '123.502723,43.518302', 601),
(608, '220400', '辽源市', 2, '0437', '125.14366,42.887766', 579),
(609, '220402', '龙山区', 3, '0437', '125.136627,42.90158', 608),
(610, '220403', '西安区', 3, '0437', '125.149281,42.927324', 608),
(611, '220421', '东丰县', 3, '0437', '125.531021,42.677371', 608),
(612, '220422', '东辽县', 3, '0437', '124.991424,42.92625', 608),
(613, '220500', '通化市', 2, '0435', '125.939697,41.728401', 579),
(614, '220502', '东昌区', 3, '0435', '125.927101,41.702859', 613),
(615, '220503', '二道江区', 3, '0435', '126.042678,41.774044', 613),
(616, '220521', '通化县', 3, '0435', '125.759259,41.679808', 613),
(617, '220523', '辉南县', 3, '0435', '126.046783,42.684921', 613),
(618, '220524', '柳河县', 3, '0435', '125.744735,42.284605', 613),
(619, '220581', '梅河口市', 3, '0435', '125.710859,42.539253', 613),
(620, '220582', '集安市', 3, '0435', '126.19403,41.125307', 613),
(621, '220600', '白山市', 2, '0439', '126.41473,41.943972', 579),
(622, '220602', '浑江区', 3, '0439', '126.416093,41.945409', 621),
(623, '220605', '江源区', 3, '0439', '126.591178,42.056747', 621),
(624, '220621', '抚松县', 3, '0439', '127.449763,42.221207', 621),
(625, '220622', '靖宇县', 3, '0439', '126.813583,42.388896', 621),
(626, '220623', '长白朝鲜族自治县', 3, '0439', '128.200789,41.420018', 621),
(627, '220681', '临江市', 3, '0439', '126.918087,41.811979', 621),
(628, '220700', '松原市', 2, '0438', '124.825042,45.141548', 579),
(629, '220702', '宁江区', 3, '0438', '124.86562,45.209915', 628),
(630, '220721', '前郭尔罗斯蒙古族自治县', 3, '0438', '124.823417,45.118061', 628),
(631, '220722', '长岭县', 3, '0438', '123.967483,44.275895', 628),
(632, '220723', '乾安县', 3, '0438', '124.041139,45.003773', 628),
(633, '220781', '扶余市', 3, '0438', '126.049803,44.9892', 628),
(634, '220800', '白城市', 2, '0436', '122.838714,45.619884', 579),
(635, '220802', '洮北区', 3, '0436', '122.851029,45.621716', 634),
(636, '220821', '镇赉县', 3, '0436', '123.199607,45.84835', 634),
(637, '220822', '通榆县', 3, '0436', '123.088238,44.81291', 634),
(638, '220881', '洮南市', 3, '0436', '122.798579,45.356807', 634),
(639, '220882', '大安市', 3, '0436', '124.292626,45.506996', 634),
(640, '222400', '延边朝鲜族自治州', 2, '1433', '129.471868,42.909408', 579),
(641, '222401', '延吉市', 3, '1433', '129.508804,42.89125', 640),
(642, '222402', '图们市', 3, '1433', '129.84371,42.968044', 640),
(643, '222403', '敦化市', 3, '1433', '128.232131,43.372642', 640),
(644, '222404', '珲春市', 3, '1433', '130.366036,42.862821', 640),
(645, '222405', '龙井市', 3, '1433', '129.427066,42.76631', 640),
(646, '222406', '和龙市', 3, '1433', '129.010106,42.546675', 640),
(647, '222424', '汪清县', 3, '1433', '129.771607,43.312522', 640),
(648, '222426', '安图县', 3, '1433', '128.899772,43.11195', 640),
(649, '230000', '黑龙江省', 1, '[]', '126.661665,45.742366', -1),
(650, '230100', '哈尔滨市', 2, '0451', '126.534967,45.803775', 649),
(651, '230102', '道里区', 3, '0451', '126.616973,45.75577', 650),
(652, '230103', '南岗区', 3, '0451', '126.668784,45.760174', 650),
(653, '230104', '道外区', 3, '0451', '126.64939,45.792057', 650),
(654, '230108', '平房区', 3, '0451', '126.637611,45.597911', 650),
(655, '230109', '松北区', 3, '0451', '126.516914,45.794504', 650),
(656, '230110', '香坊区', 3, '0451', '126.662593,45.707716', 650),
(657, '230111', '呼兰区', 3, '0451', '126.587905,45.889457', 650),
(658, '230112', '阿城区', 3, '0451', '126.958098,45.548669', 650),
(659, '230113', '双城区', 3, '0451', '126.312624,45.383218', 650),
(660, '230123', '依兰县', 3, '0451', '129.567877,46.325419', 650),
(661, '230124', '方正县', 3, '0451', '128.829536,45.851694', 650),
(662, '230125', '宾县', 3, '0451', '127.466634,45.745917', 650),
(663, '230126', '巴彦县', 3, '0451', '127.403781,46.086549', 650),
(664, '230127', '木兰县', 3, '0451', '128.043466,45.950582', 650),
(665, '230128', '通河县', 3, '0451', '128.746124,45.990205', 650),
(666, '230129', '延寿县', 3, '0451', '128.331643,45.451897', 650),
(667, '230183', '尚志市', 3, '0451', '128.009894,45.209586', 650),
(668, '230184', '五常市', 3, '0451', '127.167618,44.931991', 650),
(669, '230200', '齐齐哈尔市', 2, '0452', '123.918186,47.354348', 649),
(670, '230202', '龙沙区', 3, '0452', '123.957531,47.317308', 669),
(671, '230203', '建华区', 3, '0452', '123.955464,47.354364', 669),
(672, '230204', '铁锋区', 3, '0452', '123.978293,47.340517', 669),
(673, '230205', '昂昂溪区', 3, '0452', '123.8224,47.15516', 669),
(674, '230206', '富拉尔基区', 3, '0452', '123.629189,47.208843', 669),
(675, '230207', '碾子山区', 3, '0452', '122.887775,47.516872', 669),
(676, '230208', '梅里斯达斡尔族区', 3, '0452', '123.75291,47.309537', 669),
(677, '230221', '龙江县', 3, '0452', '123.205323,47.338665', 669),
(678, '230223', '依安县', 3, '0452', '125.306278,47.893548', 669),
(679, '230224', '泰来县', 3, '0452', '123.416631,46.393694', 669),
(680, '230225', '甘南县', 3, '0452', '123.507429,47.922405', 669),
(681, '230227', '富裕县', 3, '0452', '124.473793,47.774347', 669),
(682, '230229', '克山县', 3, '0452', '125.875705,48.037031', 669),
(683, '230230', '克东县', 3, '0452', '126.24872,48.04206', 669),
(684, '230231', '拜泉县', 3, '0452', '126.100213,47.595851', 669),
(685, '230281', '讷河市', 3, '0452', '124.88287,48.466592', 669),
(686, '230300', '鸡西市', 2, '0467', '130.969333,45.295075', 649),
(687, '230302', '鸡冠区', 3, '0467', '130.981185,45.304355', 686),
(688, '230303', '恒山区', 3, '0467', '130.904963,45.210668', 686),
(689, '230304', '滴道区', 3, '0467', '130.843613,45.348763', 686),
(690, '230305', '梨树区', 3, '0467', '130.69699,45.092046', 686),
(691, '230306', '城子河区', 3, '0467', '131.011304,45.33697', 686),
(692, '230307', '麻山区', 3, '0467', '130.478187,45.212088', 686),
(693, '230321', '鸡东县', 3, '0467', '131.124079,45.260412', 686),
(694, '230381', '虎林市', 3, '0467', '132.93721,45.762685', 686),
(695, '230382', '密山市', 3, '0467', '131.846635,45.529774', 686),
(696, '230400', '鹤岗市', 2, '0468', '130.297943,47.350189', 649),
(697, '230402', '向阳区', 3, '0468', '130.294235,47.342468', 696),
(698, '230403', '工农区', 3, '0468', '130.274684,47.31878', 696),
(699, '230404', '南山区', 3, '0468', '130.286788,47.315174', 696),
(700, '230405', '兴安区', 3, '0468', '130.239245,47.252849', 696),
(701, '230406', '东山区', 3, '0468', '130.317002,47.338537', 696),
(702, '230407', '兴山区', 3, '0468', '130.303481,47.357702', 696),
(703, '230421', '萝北县', 3, '0468', '130.85155,47.576444', 696),
(704, '230422', '绥滨县', 3, '0468', '131.852759,47.289115', 696),
(705, '230500', '双鸭山市', 2, '0469', '131.141195,46.676418', 649),
(706, '230502', '尖山区', 3, '0469', '131.158415,46.64635', 705),
(707, '230503', '岭东区', 3, '0469', '131.164723,46.592721', 705),
(708, '230505', '四方台区', 3, '0469', '131.337592,46.597264', 705),
(709, '230506', '宝山区', 3, '0469', '131.401589,46.577167', 705),
(710, '230521', '集贤县', 3, '0469', '131.141311,46.728412', 705),
(711, '230522', '友谊县', 3, '0469', '131.808063,46.767299', 705),
(712, '230523', '宝清县', 3, '0469', '132.196853,46.327457', 705),
(713, '230524', '饶河县', 3, '0469', '134.013872,46.798163', 705),
(714, '230600', '大庆市', 2, '0459', '125.103784,46.589309', 649),
(715, '230602', '萨尔图区', 3, '0459', '125.135591,46.629092', 714),
(716, '230603', '龙凤区', 3, '0459', '125.135326,46.562247', 714),
(717, '230604', '让胡路区', 3, '0459', '124.870596,46.652357', 714),
(718, '230605', '红岗区', 3, '0459', '124.891039,46.398418', 714),
(719, '230606', '大同区', 3, '0459', '124.812364,46.039827', 714),
(720, '230621', '肇州县', 3, '0459', '125.268643,45.699066', 714),
(721, '230622', '肇源县', 3, '0459', '125.078223,45.51932', 714),
(722, '230623', '林甸县', 3, '0459', '124.863603,47.171717', 714),
(723, '230624', '杜尔伯特蒙古族自治县', 3, '0459', '124.442572,46.862817', 714),
(724, '230700', '伊春市', 2, '0458', '128.841125,47.727535', 649),
(725, '230702', '伊春区', 3, '0458', '128.907257,47.728237', 724),
(726, '230703', '南岔区', 3, '0458', '129.283467,47.138034', 724),
(727, '230704', '友好区', 3, '0458', '128.836291,47.841032', 724),
(728, '230705', '西林区', 3, '0458', '129.312851,47.480735', 724),
(729, '230706', '翠峦区', 3, '0458', '128.669754,47.726394', 724),
(730, '230707', '新青区', 3, '0458', '129.533599,48.290455', 724),
(731, '230708', '美溪区', 3, '0458', '129.129314,47.63509', 724),
(732, '230709', '金山屯区', 3, '0458', '129.429117,47.413074', 724),
(733, '230710', '五营区', 3, '0458', '129.245343,48.10791', 724),
(734, '230711', '乌马河区', 3, '0458', '128.799477,47.727687', 724),
(735, '230712', '汤旺河区', 3, '0458', '129.571108,48.454651', 724),
(736, '230713', '带岭区', 3, '0458', '129.020888,47.028379', 724),
(737, '230714', '乌伊岭区', 3, '0458', '129.43792,48.590322', 724),
(738, '230715', '红星区', 3, '0458', '129.390983,48.239431', 724),
(739, '230716', '上甘岭区', 3, '0458', '129.02426,47.974707', 724),
(740, '230722', '嘉荫县', 3, '0458', '130.403134,48.888972', 724),
(741, '230781', '铁力市', 3, '0458', '128.032424,46.986633', 724),
(742, '230800', '佳木斯市', 2, '0454', '130.318878,46.799777', 649),
(743, '230803', '向阳区', 3, '0454', '130.365346,46.80779', 742),
(744, '230804', '前进区', 3, '0454', '130.375062,46.814102', 742),
(745, '230805', '东风区', 3, '0454', '130.403664,46.822571', 742),
(746, '230811', '郊区', 3, '0454', '130.327194,46.810085', 742),
(747, '230822', '桦南县', 3, '0454', '130.553343,46.239184', 742),
(748, '230826', '桦川县', 3, '0454', '130.71908,47.023001', 742),
(749, '230828', '汤原县', 3, '0454', '129.905072,46.730706', 742),
(750, '230881', '同江市', 3, '0454', '132.510919,47.642707', 742),
(751, '230882', '富锦市', 3, '0454', '132.037686,47.250107', 742),
(752, '230883', '抚远市', 3, '0454', '134.307884,48.364687', 742),
(753, '230900', '七台河市', 2, '0464', '131.003082,45.771396', 649),
(754, '230902', '新兴区', 3, '0464', '130.932143,45.81593', 753),
(755, '230903', '桃山区', 3, '0464', '131.020202,45.765705', 753),
(756, '230904', '茄子河区', 3, '0464', '131.068075,45.785215', 753),
(757, '230921', '勃利县', 3, '0464', '130.59217,45.755063', 753),
(758, '231000', '牡丹江市', 2, '0453', '129.633168,44.551653', 649),
(759, '231002', '东安区', 3, '0453', '129.626641,44.58136', 758),
(760, '231003', '阳明区', 3, '0453', '129.635615,44.596104', 758),
(761, '231004', '爱民区', 3, '0453', '129.591537,44.596042', 758),
(762, '231005', '西安区', 3, '0453', '129.616058,44.577625', 758),
(763, '231025', '林口县', 3, '0453', '130.284033,45.278046', 758),
(764, '231081', '绥芬河市', 3, '0453', '131.152545,44.412308', 758),
(765, '231083', '海林市', 3, '0453', '129.380481,44.594213', 758),
(766, '231084', '宁安市', 3, '0453', '129.482851,44.34072', 758),
(767, '231085', '穆棱市', 3, '0453', '130.524436,44.918813', 758),
(768, '231086', '东宁市', 3, '0453', '131.122915,44.087585', 758),
(769, '231100', '黑河市', 2, '0456', '127.528293,50.245129', 649),
(770, '231102', '爱辉区', 3, '0456', '127.50045,50.252105', 769),
(771, '231121', '嫩江县', 3, '0456', '125.221192,49.185766', 769),
(772, '231123', '逊克县', 3, '0456', '128.478749,49.564252', 769),
(773, '231124', '孙吴县', 3, '0456', '127.336303,49.425647', 769),
(774, '231181', '北安市', 3, '0456', '126.490864,48.241365', 769),
(775, '231182', '五大连池市', 3, '0456', '126.205516,48.517257', 769),
(776, '231200', '绥化市', 2, '0455', '126.968887,46.653845', 649),
(777, '231202', '北林区', 3, '0455', '126.985504,46.6375', 776),
(778, '231221', '望奎县', 3, '0455', '126.486075,46.832719', 776),
(779, '231222', '兰西县', 3, '0455', '126.288117,46.25245', 776),
(780, '231223', '青冈县', 3, '0455', '126.099195,46.70391', 776),
(781, '231224', '庆安县', 3, '0455', '127.507824,46.880102', 776),
(782, '231225', '明水县', 3, '0455', '125.906301,47.173426', 776),
(783, '231226', '绥棱县', 3, '0455', '127.114832,47.236015', 776),
(784, '231281', '安达市', 3, '0455', '125.346156,46.419633', 776),
(785, '231282', '肇东市', 3, '0455', '125.961814,46.051126', 776),
(786, '231283', '海伦市', 3, '0455', '126.930106,47.45117', 776),
(787, '232700', '大兴安岭地区', 2, '0457', '124.711526,52.335262', 649),
(788, '232701', '加格达奇区', 3, '0457', '124.139595,50.408735', 787),
(789, '232721', '呼玛县', 3, '0457', '126.652396,51.726091', 787),
(790, '232722', '塔河县', 3, '0457', '124.709996,52.334456', 787),
(791, '232723', '漠河县', 3, '0457', '122.538591,52.972272', 787),
(792, '310000', '上海市', 1, '021', '121.473662,31.230372', -1),
(793, '310100', '上海城区', 2, '021', '121.473662,31.230372', 792),
(794, '310101', '黄浦区', 3, '021', '121.484428,31.231739', 793),
(795, '310104', '徐汇区', 3, '021', '121.436128,31.188464', 793),
(796, '310105', '长宁区', 3, '021', '121.424622,31.220372', 793),
(797, '310106', '静安区', 3, '021', '121.447453,31.227906', 793),
(798, '310107', '普陀区', 3, '021', '121.395514,31.249603', 793),
(799, '310109', '虹口区', 3, '021', '121.505133,31.2646', 793),
(800, '310110', '杨浦区', 3, '021', '121.525727,31.259822', 793),
(801, '310112', '闵行区', 3, '021', '121.380831,31.1129', 793),
(802, '310113', '宝山区', 3, '021', '121.489612,31.405457', 793),
(803, '310114', '嘉定区', 3, '021', '121.265374,31.375869', 793),
(804, '310115', '浦东新区', 3, '021', '121.544379,31.221517', 793),
(805, '310116', '金山区', 3, '021', '121.342455,30.741798', 793),
(806, '310117', '松江区', 3, '021', '121.227747,31.032243', 793),
(807, '310118', '青浦区', 3, '021', '121.124178,31.150681', 793);
INSERT INTO `mf_area_items` (`areaId`, `areaCode`, `areaName`, `level`, `cityCode`, `center`, `parentId`) VALUES
(808, '310120', '奉贤区', 3, '021', '121.474055,30.917766', 793),
(809, '310151', '崇明区', 3, '021', '121.397421,31.623728', 793),
(810, '320000', '江苏省', 1, '[]', '118.762765,32.060875', -1),
(811, '320100', '南京市', 2, '025', '118.796682,32.05957', 810),
(812, '320102', '玄武区', 3, '025', '118.797757,32.048498', 811),
(813, '320104', '秦淮区', 3, '025', '118.79476,32.039113', 811),
(814, '320105', '建邺区', 3, '025', '118.731793,32.003731', 811),
(815, '320106', '鼓楼区', 3, '025', '118.770182,32.066601', 811),
(816, '320111', '浦口区', 3, '025', '118.628003,32.058903', 811),
(817, '320113', '栖霞区', 3, '025', '118.909153,32.096388', 811),
(818, '320114', '雨花台区', 3, '025', '118.779051,31.99126', 811),
(819, '320115', '江宁区', 3, '025', '118.840015,31.952612', 811),
(820, '320116', '六合区', 3, '025', '118.822132,32.323584', 811),
(821, '320117', '溧水区', 3, '025', '119.028288,31.651099', 811),
(822, '320118', '高淳区', 3, '025', '118.89222,31.327586', 811),
(823, '320200', '无锡市', 2, '0510', '120.31191,31.491169', 810),
(824, '320205', '锡山区', 3, '0510', '120.357858,31.589715', 823),
(825, '320206', '惠山区', 3, '0510', '120.298433,31.680335', 823),
(826, '320211', '滨湖区', 3, '0510', '120.283811,31.527276', 823),
(827, '320213', '梁溪区', 3, '0510', '120.303108,31.566155', 823),
(828, '320214', '新吴区', 3, '0510', '120.352782,31.550966', 823),
(829, '320281', '江阴市', 3, '0510', '120.286129,31.921345', 823),
(830, '320282', '宜兴市', 3, '0510', '119.823308,31.340637', 823),
(831, '320300', '徐州市', 2, '0516', '117.284124,34.205768', 810),
(832, '320302', '鼓楼区', 3, '0516', '117.185576,34.288646', 831),
(833, '320303', '云龙区', 3, '0516', '117.251076,34.253164', 831),
(834, '320305', '贾汪区', 3, '0516', '117.464958,34.436936', 831),
(835, '320311', '泉山区', 3, '0516', '117.194469,34.225522', 831),
(836, '320312', '铜山区', 3, '0516', '117.169461,34.180779', 831),
(837, '320321', '丰县', 3, '0516', '116.59539,34.693906', 831),
(838, '320322', '沛县', 3, '0516', '116.936353,34.760761', 831),
(839, '320324', '睢宁县', 3, '0516', '117.941563,33.912597', 831),
(840, '320381', '新沂市', 3, '0516', '118.354537,34.36958', 831),
(841, '320382', '邳州市', 3, '0516', '118.012531,34.338888', 831),
(842, '320400', '常州市', 2, '0519', '119.974061,31.811226', 810),
(843, '320402', '天宁区', 3, '0519', '119.999219,31.792787', 842),
(844, '320404', '钟楼区', 3, '0519', '119.902369,31.802089', 842),
(845, '320411', '新北区', 3, '0519', '119.971697,31.830427', 842),
(846, '320412', '武进区', 3, '0519', '119.942437,31.701187', 842),
(847, '320413', '金坛区', 3, '0519', '119.597811,31.723219', 842),
(848, '320481', '溧阳市', 3, '0519', '119.48421,31.416911', 842),
(849, '320500', '苏州市', 2, '0512', '120.585728,31.2974', 810),
(850, '320505', '虎丘区', 3, '0512', '120.434238,31.329601', 849),
(851, '320506', '吴中区', 3, '0512', '120.632308,31.263183', 849),
(852, '320507', '相城区', 3, '0512', '120.642626,31.369089', 849),
(853, '320508', '姑苏区', 3, '0512', '120.617369,31.33565', 849),
(854, '320509', '吴江区', 3, '0512', '120.645157,31.138677', 849),
(855, '320581', '常熟市', 3, '0512', '120.752481,31.654375', 849),
(856, '320582', '张家港市', 3, '0512', '120.555982,31.875571', 849),
(857, '320583', '昆山市', 3, '0512', '120.980736,31.385597', 849),
(858, '320585', '太仓市', 3, '0512', '121.13055,31.457735', 849),
(859, '320600', '南通市', 2, '0513', '120.894676,31.981143', 810),
(860, '320602', '崇川区', 3, '0513', '120.857434,32.009875', 859),
(861, '320611', '港闸区', 3, '0513', '120.818526,32.032441', 859),
(862, '320612', '通州区', 3, '0513', '121.073828,32.06568', 859),
(863, '320621', '海安县', 3, '0513', '120.467343,32.533572', 859),
(864, '320623', '如东县', 3, '0513', '121.185201,32.331765', 859),
(865, '320681', '启东市', 3, '0513', '121.655432,31.793278', 859),
(866, '320682', '如皋市', 3, '0513', '120.573803,32.371562', 859),
(867, '320684', '海门市', 3, '0513', '121.18181,31.869483', 859),
(868, '320700', '连云港市', 2, '0518', '119.221611,34.596653', 810),
(869, '320703', '连云区', 3, '0518', '119.338788,34.760249', 868),
(870, '320706', '海州区', 3, '0518', '119.163509,34.572274', 868),
(871, '320707', '赣榆区', 3, '0518', '119.17333,34.841348', 868),
(872, '320722', '东海县', 3, '0518', '118.752842,34.542308', 868),
(873, '320723', '灌云县', 3, '0518', '119.239381,34.284381', 868),
(874, '320724', '灌南县', 3, '0518', '119.315651,34.087134', 868),
(875, '320800', '淮安市', 2, '0517', '119.113185,33.551052', 810),
(876, '320812', '清江浦区', 3, '0517', '119.026718,33.552627', 875),
(877, '320803', '淮安区', 3, '0517', '119.141099,33.502868', 875),
(878, '320804', '淮阴区', 3, '0517', '119.034725,33.631892', 875),
(879, '320813', '洪泽区', 3, '0517', '118.873241,33.294214', 875),
(880, '320826', '涟水县', 3, '0517', '119.260227,33.781331', 875),
(881, '320830', '盱眙县', 3, '0517', '118.54436,33.011971', 875),
(882, '320831', '金湖县', 3, '0517', '119.020584,33.025433', 875),
(883, '320900', '盐城市', 2, '0515', '120.163107,33.347708', 810),
(884, '320902', '亭湖区', 3, '0515', '120.197358,33.390536', 883),
(885, '320903', '盐都区', 3, '0515', '120.153712,33.338283', 883),
(886, '320904', '大丰区', 3, '0515', '120.50085,33.200333', 883),
(887, '320921', '响水县', 3, '0515', '119.578364,34.199479', 883),
(888, '320922', '滨海县', 3, '0515', '119.82083,33.990334', 883),
(889, '320923', '阜宁县', 3, '0515', '119.802527,33.759325', 883),
(890, '320924', '射阳县', 3, '0515', '120.229986,33.758361', 883),
(891, '320925', '建湖县', 3, '0515', '119.7886,33.439067', 883),
(892, '320981', '东台市', 3, '0515', '120.320328,32.868426', 883),
(893, '321000', '扬州市', 2, '0514', '119.412939,32.394209', 810),
(894, '321002', '广陵区', 3, '0514', '119.431849,32.39472', 893),
(895, '321003', '邗江区', 3, '0514', '119.397994,32.377655', 893),
(896, '321012', '江都区', 3, '0514', '119.569989,32.434672', 893),
(897, '321023', '宝应县', 3, '0514', '119.360729,33.240391', 893),
(898, '321081', '仪征市', 3, '0514', '119.184766,32.272258', 893),
(899, '321084', '高邮市', 3, '0514', '119.459161,32.781659', 893),
(900, '321100', '镇江市', 2, '0511', '119.425836,32.187849', 810),
(901, '321102', '京口区', 3, '0511', '119.47016,32.19828', 900),
(902, '321111', '润州区', 3, '0511', '119.411959,32.195264', 900),
(903, '321112', '丹徒区', 3, '0511', '119.433853,32.131962', 900),
(904, '321181', '丹阳市', 3, '0511', '119.606439,32.010214', 900),
(905, '321182', '扬中市', 3, '0511', '119.797634,32.23483', 900),
(906, '321183', '句容市', 3, '0511', '119.168695,31.944998', 900),
(907, '321200', '泰州市', 2, '0523', '119.922933,32.455536', 810),
(908, '321202', '海陵区', 3, '0523', '119.919424,32.491016', 907),
(909, '321203', '高港区', 3, '0523', '119.881717,32.318821', 907),
(910, '321204', '姜堰区', 3, '0523', '120.127934,32.509155', 907),
(911, '321281', '兴化市', 3, '0523', '119.852541,32.910459', 907),
(912, '321282', '靖江市', 3, '0523', '120.277138,31.982751', 907),
(913, '321283', '泰兴市', 3, '0523', '120.051743,32.171853', 907),
(914, '321300', '宿迁市', 2, '0527', '118.275198,33.963232', 810),
(915, '321302', '宿城区', 3, '0527', '118.242533,33.963029', 914),
(916, '321311', '宿豫区', 3, '0527', '118.330781,33.946822', 914),
(917, '321322', '沭阳县', 3, '0527', '118.804784,34.111022', 914),
(918, '321323', '泗阳县', 3, '0527', '118.703424,33.722478', 914),
(919, '321324', '泗洪县', 3, '0527', '118.223591,33.476051', 914),
(920, '330000', '浙江省', 1, '[]', '120.152585,30.266597', -1),
(921, '330100', '杭州市', 2, '0571', '120.209789,30.24692', 920),
(922, '330102', '上城区', 3, '0571', '120.169312,30.242404', 921),
(923, '330103', '下城区', 3, '0571', '120.180891,30.281677', 921),
(924, '330104', '江干区', 3, '0571', '120.205001,30.257012', 921),
(925, '330105', '拱墅区', 3, '0571', '120.141406,30.319037', 921),
(926, '330106', '西湖区', 3, '0571', '120.130194,30.259463', 921),
(927, '330108', '滨江区', 3, '0571', '120.211623,30.208847', 921),
(928, '330109', '萧山区', 3, '0571', '120.264253,30.183806', 921),
(929, '330110', '余杭区', 3, '0571', '120.299401,30.419045', 921),
(930, '330111', '富阳区', 3, '0571', '119.960076,30.048692', 921),
(931, '330122', '桐庐县', 3, '0571', '119.691467,29.79299', 921),
(932, '330127', '淳安县', 3, '0571', '119.042037,29.608886', 921),
(933, '330182', '建德市', 3, '0571', '119.281231,29.474759', 921),
(934, '330185', '临安市', 3, '0571', '119.724734,30.233873', 921),
(935, '330200', '宁波市', 2, '0574', '121.622485,29.859971', 920),
(936, '330203', '海曙区', 3, '0574', '121.550752,29.874903', 935),
(937, '330205', '江北区', 3, '0574', '121.555081,29.886781', 935),
(938, '330206', '北仑区', 3, '0574', '121.844172,29.899778', 935),
(939, '330211', '镇海区', 3, '0574', '121.596496,29.965203', 935),
(940, '330212', '鄞州区', 3, '0574', '121.546603,29.816511', 935),
(941, '330225', '象山县', 3, '0574', '121.869339,29.476705', 935),
(942, '330226', '宁海县', 3, '0574', '121.429477,29.287939', 935),
(943, '330281', '余姚市', 3, '0574', '121.154629,30.037106', 935),
(944, '330282', '慈溪市', 3, '0574', '121.266561,30.170261', 935),
(945, '330213', '奉化区', 3, '0574', '121.406997,29.655144', 935),
(946, '330300', '温州市', 2, '0577', '120.699361,27.993828', 920),
(947, '330302', '鹿城区', 3, '0577', '120.655271,28.015737', 946),
(948, '330303', '龙湾区', 3, '0577', '120.811213,27.932747', 946),
(949, '330304', '瓯海区', 3, '0577', '120.61491,27.966844', 946),
(950, '330305', '洞头区', 3, '0577', '121.157249,27.836154', 946),
(951, '330324', '永嘉县', 3, '0577', '120.692025,28.153607', 946),
(952, '330326', '平阳县', 3, '0577', '120.565793,27.661918', 946),
(953, '330327', '苍南县', 3, '0577', '120.427619,27.519773', 946),
(954, '330328', '文成县', 3, '0577', '120.091498,27.786996', 946),
(955, '330329', '泰顺县', 3, '0577', '119.717649,27.556884', 946),
(956, '330381', '瑞安市', 3, '0577', '120.655148,27.778657', 946),
(957, '330382', '乐清市', 3, '0577', '120.983906,28.113725', 946),
(958, '330400', '嘉兴市', 2, '0573', '120.75547,30.746191', 920),
(959, '330402', '南湖区', 3, '0573', '120.783024,30.747842', 958),
(960, '330411', '秀洲区', 3, '0573', '120.710082,30.765188', 958),
(961, '330421', '嘉善县', 3, '0573', '120.926028,30.830864', 958),
(962, '330424', '海盐县', 3, '0573', '120.946263,30.526435', 958),
(963, '330481', '海宁市', 3, '0573', '120.680239,30.511539', 958),
(964, '330482', '平湖市', 3, '0573', '121.015142,30.677233', 958),
(965, '330483', '桐乡市', 3, '0573', '120.565098,30.630173', 958),
(966, '330500', '湖州市', 2, '0572', '120.086809,30.89441', 920),
(967, '330502', '吴兴区', 3, '0572', '120.185838,30.857151', 966),
(968, '330503', '南浔区', 3, '0572', '120.418513,30.849689', 966),
(969, '330521', '德清县', 3, '0572', '119.9774,30.54251', 966),
(970, '330522', '长兴县', 3, '0572', '119.910952,31.026665', 966),
(971, '330523', '安吉县', 3, '0572', '119.680353,30.638674', 966),
(972, '330600', '绍兴市', 2, '0575', '120.580364,30.030192', 920),
(973, '330602', '越城区', 3, '0575', '120.582633,29.988244', 972),
(974, '330603', '柯桥区', 3, '0575', '120.495085,30.081929', 972),
(975, '330604', '上虞区', 3, '0575', '120.868122,30.03312', 972),
(976, '330624', '新昌县', 3, '0575', '120.903866,29.499831', 972),
(977, '330681', '诸暨市', 3, '0575', '120.246863,29.708692', 972),
(978, '330683', '嵊州市', 3, '0575', '120.831025,29.56141', 972),
(979, '330700', '金华市', 2, '0579', '119.647229,29.079208', 920),
(980, '330702', '婺城区', 3, '0579', '119.571728,29.0872', 979),
(981, '330703', '金东区', 3, '0579', '119.69278,29.099723', 979),
(982, '330723', '武义县', 3, '0579', '119.816562,28.89267', 979),
(983, '330726', '浦江县', 3, '0579', '119.892222,29.452476', 979),
(984, '330727', '磐安县', 3, '0579', '120.450005,29.054548', 979),
(985, '330781', '兰溪市', 3, '0579', '119.460472,29.2084', 979),
(986, '330782', '义乌市', 3, '0579', '120.075106,29.306775', 979),
(987, '330783', '东阳市', 3, '0579', '120.241566,29.289648', 979),
(988, '330784', '永康市', 3, '0579', '120.047651,28.888555', 979),
(989, '330800', '衢州市', 2, '0570', '118.859457,28.970079', 920),
(990, '330802', '柯城区', 3, '0570', '118.871516,28.96862', 989),
(991, '330803', '衢江区', 3, '0570', '118.95946,28.97978', 989),
(992, '330822', '常山县', 3, '0570', '118.511235,28.901462', 989),
(993, '330824', '开化县', 3, '0570', '118.415495,29.137336', 989),
(994, '330825', '龙游县', 3, '0570', '119.172189,29.028439', 989),
(995, '330881', '江山市', 3, '0570', '118.626991,28.737331', 989),
(996, '330900', '舟山市', 2, '0580', '122.207106,29.985553', 920),
(997, '330902', '定海区', 3, '0580', '122.106773,30.019858', 996),
(998, '330903', '普陀区', 3, '0580', '122.323867,29.97176', 996),
(999, '330921', '岱山县', 3, '0580', '122.226237,30.264139', 996),
(1000, '330922', '嵊泗县', 3, '0580', '122.451382,30.725686', 996),
(1001, '331000', '台州市', 2, '0576', '121.42076,28.65638', 920),
(1002, '331002', '椒江区', 3, '0576', '121.442978,28.672981', 1001),
(1003, '331003', '黄岩区', 3, '0576', '121.261972,28.650083', 1001),
(1004, '331004', '路桥区', 3, '0576', '121.365123,28.582654', 1001),
(1005, '331021', '玉环市', 3, '0576', '121.231805,28.135929', 1001),
(1006, '331022', '三门县', 3, '0576', '121.395711,29.104789', 1001),
(1007, '331023', '天台县', 3, '0576', '121.006595,29.144064', 1001),
(1008, '331024', '仙居县', 3, '0576', '120.728801,28.846966', 1001),
(1009, '331081', '温岭市', 3, '0576', '121.385604,28.372506', 1001),
(1010, '331082', '临海市', 3, '0576', '121.144556,28.858881', 1001),
(1011, '331100', '丽水市', 2, '0578', '119.922796,28.46763', 920),
(1012, '331102', '莲都区', 3, '0578', '119.912626,28.445928', 1011),
(1013, '331121', '青田县', 3, '0578', '120.289478,28.139837', 1011),
(1014, '331122', '缙云县', 3, '0578', '120.091572,28.659283', 1011),
(1015, '331123', '遂昌县', 3, '0578', '119.276103,28.592148', 1011),
(1016, '331124', '松阳县', 3, '0578', '119.481511,28.448803', 1011),
(1017, '331125', '云和县', 3, '0578', '119.573397,28.11579', 1011),
(1018, '331126', '庆元县', 3, '0578', '119.06259,27.61922', 1011),
(1019, '331127', '景宁畲族自治县', 3, '0578', '119.635739,27.9733', 1011),
(1020, '331181', '龙泉市', 3, '0578', '119.141473,28.074649', 1011),
(1021, '340000', '安徽省', 1, '[]', '117.329949,31.733806', -1),
(1022, '340100', '合肥市', 2, '0551', '117.227219,31.820591', 1021),
(1023, '340102', '瑶海区', 3, '0551', '117.309546,31.857917', 1022),
(1024, '340103', '庐阳区', 3, '0551', '117.264786,31.878589', 1022),
(1025, '340104', '蜀山区', 3, '0551', '117.260521,31.85124', 1022),
(1026, '340111', '包河区', 3, '0551', '117.309519,31.793859', 1022),
(1027, '340121', '长丰县', 3, '0551', '117.167564,32.478018', 1022),
(1028, '340122', '肥东县', 3, '0551', '117.469382,31.88794', 1022),
(1029, '340123', '肥西县', 3, '0551', '117.157981,31.706809', 1022),
(1030, '340124', '庐江县', 3, '0551', '117.2882,31.256524', 1022),
(1031, '340181', '巢湖市', 3, '0551', '117.890354,31.624522', 1022),
(1032, '340200', '芜湖市', 2, '0553', '118.432941,31.352859', 1021),
(1033, '340202', '镜湖区', 3, '0553', '118.385009,31.340728', 1032),
(1034, '340203', '弋江区', 3, '0553', '118.372655,31.311756', 1032),
(1035, '340207', '鸠江区', 3, '0553', '118.391734,31.369373', 1032),
(1036, '340208', '三山区', 3, '0553', '118.268101,31.219568', 1032),
(1037, '340221', '芜湖县', 3, '0553', '118.576124,31.134809', 1032),
(1038, '340222', '繁昌县', 3, '0553', '118.198703,31.101782', 1032),
(1039, '340223', '南陵县', 3, '0553', '118.334359,30.914922', 1032),
(1040, '340225', '无为县', 3, '0553', '117.902366,31.303167', 1032),
(1041, '340300', '蚌埠市', 2, '0552', '117.388512,32.91663', 1021),
(1042, '340302', '龙子湖区', 3, '0552', '117.379778,32.950611', 1041),
(1043, '340303', '蚌山区', 3, '0552', '117.373595,32.917048', 1041),
(1044, '340304', '禹会区', 3, '0552', '117.342155,32.929799', 1041),
(1045, '340311', '淮上区', 3, '0552', '117.35933,32.965435', 1041),
(1046, '340321', '怀远县', 3, '0552', '117.205237,32.970031', 1041),
(1047, '340322', '五河县', 3, '0552', '117.879486,33.127823', 1041),
(1048, '340323', '固镇县', 3, '0552', '117.316913,33.31688', 1041),
(1049, '340400', '淮南市', 2, '0554', '117.018399,32.587117', 1021),
(1050, '340402', '大通区', 3, '0554', '117.053314,32.631519', 1049),
(1051, '340403', '田家庵区', 3, '0554', '117.017349,32.647277', 1049),
(1052, '340404', '谢家集区', 3, '0554', '116.859188,32.600037', 1049),
(1053, '340405', '八公山区', 3, '0554', '116.83349,32.631379', 1049),
(1054, '340406', '潘集区', 3, '0554', '116.834715,32.77208', 1049),
(1055, '340421', '凤台县', 3, '0554', '116.71105,32.709444', 1049),
(1056, '340422', '寿县', 3, '0554', '116.798232,32.545109', 1049),
(1057, '340500', '马鞍山市', 2, '0555', '118.507011,31.67044', 1021),
(1058, '340503', '花山区', 3, '0555', '118.492565,31.71971', 1057),
(1059, '340504', '雨山区', 3, '0555', '118.498578,31.682132', 1057),
(1060, '340506', '博望区', 3, '0555', '118.844538,31.558471', 1057),
(1061, '340521', '当涂县', 3, '0555', '118.497972,31.571213', 1057),
(1062, '340522', '含山县', 3, '0555', '118.101421,31.735598', 1057),
(1063, '340523', '和县', 3, '0555', '118.353667,31.742293', 1057),
(1064, '340600', '淮北市', 2, '0561', '116.798265,33.955844', 1021),
(1065, '340602', '杜集区', 3, '0561', '116.828133,33.991451', 1064),
(1066, '340603', '相山区', 3, '0561', '116.794344,33.959892', 1064),
(1067, '340604', '烈山区', 3, '0561', '116.813042,33.895139', 1064),
(1068, '340621', '濉溪县', 3, '0561', '116.766298,33.915477', 1064),
(1069, '340700', '铜陵市', 2, '0562', '117.81154,30.945515', 1021),
(1070, '340705', '铜官区', 3, '0562', '117.85616,30.936272', 1069),
(1071, '340706', '义安区', 3, '0562', '117.791544,30.952823', 1069),
(1072, '340711', '郊区', 3, '0562', '117.768026,30.821069', 1069),
(1073, '340722', '枞阳县', 3, '0562', '117.250594,30.706018', 1069),
(1074, '340800', '安庆市', 2, '0556', '117.115101,30.531919', 1021),
(1075, '340802', '迎江区', 3, '0556', '117.09115,30.511548', 1074),
(1076, '340803', '大观区', 3, '0556', '117.013469,30.553697', 1074),
(1077, '340811', '宜秀区', 3, '0556', '116.987542,30.613332', 1074),
(1078, '340822', '怀宁县', 3, '0556', '116.829475,30.733824', 1074),
(1079, '340824', '潜山县', 3, '0556', '116.581371,30.631136', 1074),
(1080, '340825', '太湖县', 3, '0556', '116.308795,30.45422', 1074),
(1081, '340826', '宿松县', 3, '0556', '116.129105,30.153746', 1074),
(1082, '340827', '望江县', 3, '0556', '116.706498,30.128002', 1074),
(1083, '340828', '岳西县', 3, '0556', '116.359692,30.849762', 1074),
(1084, '340881', '桐城市', 3, '0556', '116.936748,31.035848', 1074),
(1085, '341000', '黄山市', 2, '0559', '118.338272,29.715185', 1021),
(1086, '341002', '屯溪区', 3, '0559', '118.315329,29.696108', 1085),
(1087, '341003', '黄山区', 3, '0559', '118.141567,30.272942', 1085),
(1088, '341004', '徽州区', 3, '0559', '118.336743,29.827271', 1085),
(1089, '341021', '歙县', 3, '0559', '118.415345,29.861379', 1085),
(1090, '341022', '休宁县', 3, '0559', '118.193618,29.784124', 1085),
(1091, '341023', '黟县', 3, '0559', '117.938373,29.924805', 1085),
(1092, '341024', '祁门县', 3, '0559', '117.717396,29.854055', 1085),
(1093, '341100', '滁州市', 2, '0550', '118.327944,32.255636', 1021),
(1094, '341102', '琅琊区', 3, '0550', '118.305961,32.294631', 1093),
(1095, '341103', '南谯区', 3, '0550', '118.41697,32.200197', 1093),
(1096, '341122', '来安县', 3, '0550', '118.435718,32.452199', 1093),
(1097, '341124', '全椒县', 3, '0550', '118.274149,32.08593', 1093),
(1098, '341125', '定远县', 3, '0550', '117.698562,32.530981', 1093),
(1099, '341126', '凤阳县', 3, '0550', '117.531622,32.874735', 1093),
(1100, '341181', '天长市', 3, '0550', '119.004816,32.667571', 1093),
(1101, '341182', '明光市', 3, '0550', '118.018193,32.78196', 1093),
(1102, '341200', '阜阳市', 2, '1558', '115.814504,32.890479', 1021),
(1103, '341202', '颍州区', 3, '1558', '115.806942,32.883468', 1102),
(1104, '341203', '颍东区', 3, '1558', '115.856762,32.912477', 1102),
(1105, '341204', '颍泉区', 3, '1558', '115.80835,32.925211', 1102),
(1106, '341221', '临泉县', 3, '1558', '115.263115,33.039715', 1102),
(1107, '341222', '太和县', 3, '1558', '115.621941,33.160327', 1102),
(1108, '341225', '阜南县', 3, '1558', '115.595643,32.658297', 1102),
(1109, '341226', '颍上县', 3, '1558', '116.256772,32.653211', 1102),
(1110, '341282', '界首市', 3, '1558', '115.374821,33.258244', 1102),
(1111, '341300', '宿州市', 2, '0557', '116.964195,33.647309', 1021),
(1112, '341302', '埇桥区', 3, '0557', '116.977203,33.64059', 1111),
(1113, '341321', '砀山县', 3, '0557', '116.367095,34.442561', 1111),
(1114, '341322', '萧县', 3, '0557', '116.947349,34.188732', 1111),
(1115, '341323', '灵璧县', 3, '0557', '117.549395,33.554604', 1111),
(1116, '341324', '泗县', 3, '0557', '117.910629,33.482982', 1111),
(1117, '341500', '六安市', 2, '0564', '116.520139,31.735456', 1021),
(1118, '341502', '金安区', 3, '0564', '116.539173,31.750119', 1117),
(1119, '341503', '裕安区', 3, '0564', '116.479829,31.738183', 1117),
(1120, '341504', '叶集区', 3, '0564', '115.925271,31.863693', 1117),
(1121, '341522', '霍邱县', 3, '0564', '116.277911,32.353038', 1117),
(1122, '341523', '舒城县', 3, '0564', '116.948736,31.462234', 1117),
(1123, '341524', '金寨县', 3, '0564', '115.934366,31.72717', 1117),
(1124, '341525', '霍山县', 3, '0564', '116.351892,31.410561', 1117),
(1125, '341600', '亳州市', 2, '0558', '115.77867,33.844592', 1021),
(1126, '341602', '谯城区', 3, '0558', '115.779025,33.876235', 1125),
(1127, '341621', '涡阳县', 3, '0558', '116.215665,33.492921', 1125),
(1128, '341622', '蒙城县', 3, '0558', '116.564247,33.26583', 1125),
(1129, '341623', '利辛县', 3, '0558', '116.208564,33.144515', 1125),
(1130, '341700', '池州市', 2, '0566', '117.491592,30.664779', 1021),
(1131, '341702', '贵池区', 3, '0566', '117.567264,30.687219', 1130),
(1132, '341721', '东至县', 3, '0566', '117.027618,30.111163', 1130),
(1133, '341722', '石台县', 3, '0566', '117.486306,30.210313', 1130),
(1134, '341723', '青阳县', 3, '0566', '117.84743,30.63923', 1130),
(1135, '341800', '宣城市', 2, '0563', '118.75868,30.940195', 1021),
(1136, '341802', '宣州区', 3, '0563', '118.785561,30.944076', 1135),
(1137, '341821', '郎溪县', 3, '0563', '119.179656,31.126412', 1135),
(1138, '341822', '广德县', 3, '0563', '119.420935,30.877555', 1135),
(1139, '341823', '泾县', 3, '0563', '118.419859,30.688634', 1135),
(1140, '341824', '绩溪县', 3, '0563', '118.578519,30.067533', 1135),
(1141, '341825', '旌德县', 3, '0563', '118.549861,30.298142', 1135),
(1142, '341881', '宁国市', 3, '0563', '118.983171,30.633882', 1135),
(1143, '350000', '福建省', 1, '[]', '119.295143,26.100779', -1),
(1144, '350100', '福州市', 2, '0591', '119.296389,26.074268', 1143),
(1145, '350102', '鼓楼区', 3, '0591', '119.303917,26.081983', 1144),
(1146, '350103', '台江区', 3, '0591', '119.314041,26.052843', 1144),
(1147, '350104', '仓山区', 3, '0591', '119.273545,26.046743', 1144),
(1148, '350105', '马尾区', 3, '0591', '119.455588,25.9895', 1144),
(1149, '350111', '晋安区', 3, '0591', '119.328521,26.082107', 1144),
(1150, '350121', '闽侯县', 3, '0591', '119.131724,26.150047', 1144),
(1151, '350122', '连江县', 3, '0591', '119.539704,26.197364', 1144),
(1152, '350123', '罗源县', 3, '0591', '119.549776,26.489558', 1144),
(1153, '350124', '闽清县', 3, '0591', '118.863361,26.221197', 1144),
(1154, '350125', '永泰县', 3, '0591', '118.932592,25.866694', 1144),
(1155, '350128', '平潭县', 3, '0591', '119.790168,25.49872', 1144),
(1156, '350181', '福清市', 3, '0591', '119.384201,25.72071', 1144),
(1157, '350182', '长乐市', 3, '0591', '119.523266,25.962888', 1144),
(1158, '350200', '厦门市', 2, '0592', '118.089204,24.479664', 1143),
(1159, '350203', '思明区', 3, '0592', '118.082649,24.445484', 1158),
(1160, '350205', '海沧区', 3, '0592', '118.032984,24.484685', 1158),
(1161, '350206', '湖里区', 3, '0592', '118.146768,24.512904', 1158),
(1162, '350211', '集美区', 3, '0592', '118.097337,24.575969', 1158),
(1163, '350212', '同安区', 3, '0592', '118.152041,24.723234', 1158),
(1164, '350213', '翔安区', 3, '0592', '118.248034,24.618543', 1158),
(1165, '350300', '莆田市', 2, '0594', '119.007777,25.454084', 1143),
(1166, '350302', '城厢区', 3, '0594', '118.993884,25.419319', 1165),
(1167, '350303', '涵江区', 3, '0594', '119.116289,25.45872', 1165),
(1168, '350304', '荔城区', 3, '0594', '119.015061,25.431941', 1165),
(1169, '350305', '秀屿区', 3, '0594', '119.105494,25.31836', 1165),
(1170, '350322', '仙游县', 3, '0594', '118.691637,25.362093', 1165),
(1171, '350400', '三明市', 2, '0598', '117.638678,26.263406', 1143),
(1172, '350402', '梅列区', 3, '0598', '117.645855,26.271711', 1171),
(1173, '350403', '三元区', 3, '0598', '117.608044,26.234019', 1171),
(1174, '350421', '明溪县', 3, '0598', '117.202226,26.355856', 1171),
(1175, '350423', '清流县', 3, '0598', '116.816909,26.177796', 1171),
(1176, '350424', '宁化县', 3, '0598', '116.654365,26.261754', 1171),
(1177, '350425', '大田县', 3, '0598', '117.847115,25.692699', 1171),
(1178, '350426', '尤溪县', 3, '0598', '118.190467,26.170171', 1171),
(1179, '350427', '沙县', 3, '0598', '117.792396,26.397199', 1171),
(1180, '350428', '将乐县', 3, '0598', '117.471372,26.728952', 1171),
(1181, '350429', '泰宁县', 3, '0598', '117.17574,26.900259', 1171),
(1182, '350430', '建宁县', 3, '0598', '116.848443,26.833588', 1171),
(1183, '350481', '永安市', 3, '0598', '117.365052,25.941937', 1171),
(1184, '350500', '泉州市', 2, '0595', '118.675676,24.874132', 1143),
(1185, '350502', '鲤城区', 3, '0595', '118.587097,24.907424', 1184),
(1186, '350503', '丰泽区', 3, '0595', '118.613172,24.891173', 1184),
(1187, '350504', '洛江区', 3, '0595', '118.671193,24.939796', 1184),
(1188, '350505', '泉港区', 3, '0595', '118.916309,25.119815', 1184),
(1189, '350521', '惠安县', 3, '0595', '118.796607,25.030801', 1184),
(1190, '350524', '安溪县', 3, '0595', '118.186288,25.055954', 1184),
(1191, '350525', '永春县', 3, '0595', '118.294048,25.321565', 1184),
(1192, '350526', '德化县', 3, '0595', '118.241094,25.491493', 1184),
(1193, '350527', '金门县', 3, '0595', '118.323221,24.436417', 1184),
(1194, '350581', '石狮市', 3, '0595', '118.648066,24.732204', 1184),
(1195, '350582', '晋江市', 3, '0595', '118.551682,24.781636', 1184),
(1196, '350583', '南安市', 3, '0595', '118.386279,24.960385', 1184),
(1197, '350600', '漳州市', 2, '0596', '117.647093,24.513025', 1143),
(1198, '350602', '芗城区', 3, '0596', '117.653968,24.510787', 1197),
(1199, '350603', '龙文区', 3, '0596', '117.709754,24.503113', 1197),
(1200, '350622', '云霄县', 3, '0596', '117.339573,23.957936', 1197),
(1201, '350623', '漳浦县', 3, '0596', '117.613808,24.117102', 1197),
(1202, '350624', '诏安县', 3, '0596', '117.175184,23.711579', 1197),
(1203, '350625', '长泰县', 3, '0596', '117.759153,24.625449', 1197),
(1204, '350626', '东山县', 3, '0596', '117.430061,23.701262', 1197),
(1205, '350627', '南靖县', 3, '0596', '117.35732,24.514654', 1197),
(1206, '350628', '平和县', 3, '0596', '117.315017,24.363508', 1197),
(1207, '350629', '华安县', 3, '0596', '117.534103,25.004425', 1197),
(1208, '350681', '龙海市', 3, '0596', '117.818197,24.446706', 1197),
(1209, '350700', '南平市', 2, '0599', '118.17771,26.641774', 1143),
(1210, '350702', '延平区', 3, '0599', '118.182036,26.637438', 1209),
(1211, '350703', '建阳区', 3, '0599', '118.120464,27.331876', 1209),
(1212, '350721', '顺昌县', 3, '0599', '117.810357,26.793288', 1209),
(1213, '350722', '浦城县', 3, '0599', '118.541256,27.917263', 1209),
(1214, '350723', '光泽县', 3, '0599', '117.334106,27.540987', 1209),
(1215, '350724', '松溪县', 3, '0599', '118.785468,27.526232', 1209),
(1216, '350725', '政和县', 3, '0599', '118.857642,27.366104', 1209),
(1217, '350781', '邵武市', 3, '0599', '117.492533,27.340326', 1209),
(1218, '350782', '武夷山市', 3, '0599', '118.035309,27.756647', 1209),
(1219, '350783', '建瓯市', 3, '0599', '118.304966,27.022774', 1209),
(1220, '350800', '龙岩市', 2, '0597', '117.017295,25.075119', 1143),
(1221, '350802', '新罗区', 3, '0597', '117.037155,25.098312', 1220),
(1222, '350803', '永定区', 3, '0597', '116.732091,24.723961', 1220),
(1223, '350821', '长汀县', 3, '0597', '116.357581,25.833531', 1220),
(1224, '350823', '上杭县', 3, '0597', '116.420098,25.049518', 1220),
(1225, '350824', '武平县', 3, '0597', '116.100414,25.095386', 1220),
(1226, '350825', '连城县', 3, '0597', '116.754472,25.710538', 1220),
(1227, '350881', '漳平市', 3, '0597', '117.419998,25.290184', 1220),
(1228, '350900', '宁德市', 2, '0593', '119.547932,26.665617', 1143),
(1229, '350902', '蕉城区', 3, '0593', '119.526299,26.66061', 1228),
(1230, '350921', '霞浦县', 3, '0593', '120.005146,26.885703', 1228),
(1231, '350922', '古田县', 3, '0593', '118.746284,26.577837', 1228),
(1232, '350923', '屏南县', 3, '0593', '118.985895,26.908276', 1228),
(1233, '350924', '寿宁县', 3, '0593', '119.514986,27.454479', 1228),
(1234, '350925', '周宁县', 3, '0593', '119.339025,27.104591', 1228),
(1235, '350926', '柘荣县', 3, '0593', '119.900609,27.233933', 1228),
(1236, '350981', '福安市', 3, '0593', '119.64785,27.08834', 1228),
(1237, '350982', '福鼎市', 3, '0593', '120.216977,27.324479', 1228),
(1238, '360000', '江西省', 1, '[]', '115.81635,28.63666', -1),
(1239, '360100', '南昌市', 2, '0791', '115.858198,28.682892', 1238),
(1240, '360102', '东湖区', 3, '0791', '115.903526,28.698731', 1239),
(1241, '360103', '西湖区', 3, '0791', '115.877233,28.657595', 1239),
(1242, '360104', '青云谱区', 3, '0791', '115.925749,28.621169', 1239),
(1243, '360105', '湾里区', 3, '0791', '115.730847,28.714796', 1239),
(1244, '360111', '青山湖区', 3, '0791', '115.962144,28.682984', 1239),
(1245, '360112', '新建区', 3, '0791', '115.815277,28.692864', 1239),
(1246, '360121', '南昌县', 3, '0791', '115.933742,28.558296', 1239),
(1247, '360123', '安义县', 3, '0791', '115.548658,28.846', 1239),
(1248, '360124', '进贤县', 3, '0791', '116.241288,28.377343', 1239),
(1249, '360200', '景德镇市', 2, '0798', '117.178222,29.268945', 1238),
(1250, '360202', '昌江区', 3, '0798', '117.18363,29.273565', 1249),
(1251, '360203', '珠山区', 3, '0798', '117.202919,29.299938', 1249),
(1252, '360222', '浮梁县', 3, '0798', '117.215066,29.352253', 1249),
(1253, '360281', '乐平市', 3, '0798', '117.151796,28.97844', 1249),
(1254, '360300', '萍乡市', 2, '0799', '113.887083,27.658373', 1238),
(1255, '360302', '安源区', 3, '0799', '113.870704,27.61511', 1254),
(1256, '360313', '湘东区', 3, '0799', '113.733047,27.640075', 1254),
(1257, '360321', '莲花县', 3, '0799', '113.961488,27.127664', 1254),
(1258, '360322', '上栗县', 3, '0799', '113.795311,27.880301', 1254),
(1259, '360323', '芦溪县', 3, '0799', '114.029827,27.630806', 1254),
(1260, '360400', '九江市', 2, '0792', '115.952914,29.662117', 1238),
(1261, '360402', '濂溪区', 3, '0792', '115.992842,29.668064', 1260),
(1262, '360403', '浔阳区', 3, '0792', '115.990301,29.727593', 1260),
(1263, '360421', '九江县', 3, '0792', '115.911323,29.608431', 1260),
(1264, '360423', '武宁县', 3, '0792', '115.092757,29.246591', 1260),
(1265, '360424', '修水县', 3, '0792', '114.546836,29.025726', 1260),
(1266, '360425', '永修县', 3, '0792', '115.831956,29.011871', 1260),
(1267, '360426', '德安县', 3, '0792', '115.767447,29.298696', 1260),
(1268, '360483', '庐山市', 3, '0792', '116.04506,29.448128', 1260),
(1269, '360428', '都昌县', 3, '0792', '116.203979,29.273239', 1260),
(1270, '360429', '湖口县', 3, '0792', '116.251947,29.731101', 1260),
(1271, '360430', '彭泽县', 3, '0792', '116.56438,29.876991', 1260),
(1272, '360481', '瑞昌市', 3, '0792', '115.681335,29.675834', 1260),
(1273, '360482', '共青城市', 3, '0792', '115.808844,29.248316', 1260),
(1274, '360500', '新余市', 2, '0790', '114.917346,27.817808', 1238),
(1275, '360502', '渝水区', 3, '0790', '114.944549,27.800148', 1274),
(1276, '360521', '分宜县', 3, '0790', '114.692049,27.814757', 1274),
(1277, '360600', '鹰潭市', 2, '0701', '117.042173,28.272537', 1238),
(1278, '360602', '月湖区', 3, '0701', '117.102475,28.267018', 1277),
(1279, '360622', '余江县', 3, '0701', '116.85926,28.198652', 1277),
(1280, '360681', '贵溪市', 3, '0701', '117.245497,28.292519', 1277),
(1281, '360700', '赣州市', 2, '0797', '114.933546,25.830694', 1238),
(1282, '360702', '章贡区', 3, '0797', '114.921171,25.817816', 1281),
(1283, '360703', '南康区', 3, '0797', '114.765412,25.66145', 1281),
(1284, '360704', '赣县区', 3, '0797', '115.011561,25.86069', 1281),
(1285, '360722', '信丰县', 3, '0797', '114.922922,25.386379', 1281),
(1286, '360723', '大余县', 3, '0797', '114.362112,25.401313', 1281),
(1287, '360724', '上犹县', 3, '0797', '114.551138,25.785172', 1281),
(1288, '360725', '崇义县', 3, '0797', '114.308267,25.681784', 1281),
(1289, '360726', '安远县', 3, '0797', '115.393922,25.136927', 1281),
(1290, '360727', '龙南县', 3, '0797', '114.789873,24.911069', 1281),
(1291, '360728', '定南县', 3, '0797', '115.027845,24.78441', 1281),
(1292, '360729', '全南县', 3, '0797', '114.530125,24.742403', 1281),
(1293, '360730', '宁都县', 3, '0797', '116.009472,26.470116', 1281),
(1294, '360731', '于都县', 3, '0797', '115.415508,25.952068', 1281),
(1295, '360732', '兴国县', 3, '0797', '115.363189,26.337937', 1281),
(1296, '360733', '会昌县', 3, '0797', '115.786056,25.600272', 1281),
(1297, '360734', '寻乌县', 3, '0797', '115.637933,24.969167', 1281),
(1298, '360735', '石城县', 3, '0797', '116.346995,26.314775', 1281),
(1299, '360781', '瑞金市', 3, '0797', '116.027134,25.885555', 1281),
(1300, '360800', '吉安市', 2, '0796', '114.966567,27.090763', 1238),
(1301, '360802', '吉州区', 3, '0796', '114.994763,27.143801', 1300),
(1302, '360803', '青原区', 3, '0796', '115.014811,27.081977', 1300),
(1303, '360821', '吉安县', 3, '0796', '114.907875,27.039787', 1300),
(1304, '360822', '吉水县', 3, '0796', '115.135507,27.229632', 1300),
(1305, '360823', '峡江县', 3, '0796', '115.316566,27.582901', 1300),
(1306, '360824', '新干县', 3, '0796', '115.387052,27.740191', 1300),
(1307, '360825', '永丰县', 3, '0796', '115.421344,27.316939', 1300),
(1308, '360826', '泰和县', 3, '0796', '114.92299,26.801628', 1300),
(1309, '360827', '遂川县', 3, '0796', '114.520537,26.313737', 1300),
(1310, '360828', '万安县', 3, '0796', '114.759364,26.456553', 1300),
(1311, '360829', '安福县', 3, '0796', '114.619893,27.392873', 1300),
(1312, '360830', '永新县', 3, '0796', '114.243072,26.944962', 1300),
(1313, '360881', '井冈山市', 3, '0796', '114.289228,26.748081', 1300),
(1314, '360900', '宜春市', 2, '0795', '114.416785,27.815743', 1238),
(1315, '360902', '袁州区', 3, '0795', '114.427858,27.797091', 1314),
(1316, '360921', '奉新县', 3, '0795', '115.400491,28.688423', 1314),
(1317, '360922', '万载县', 3, '0795', '114.444854,28.105689', 1314),
(1318, '360923', '上高县', 3, '0795', '114.947683,28.238061', 1314),
(1319, '360924', '宜丰县', 3, '0795', '114.802852,28.394565', 1314),
(1320, '360925', '靖安县', 3, '0795', '115.362628,28.861478', 1314),
(1321, '360926', '铜鼓县', 3, '0795', '114.371172,28.520769', 1314),
(1322, '360981', '丰城市', 3, '0795', '115.771093,28.159141', 1314),
(1323, '360982', '樟树市', 3, '0795', '115.546152,28.055853', 1314),
(1324, '360983', '高安市', 3, '0795', '115.360619,28.441152', 1314),
(1325, '361000', '抚州市', 2, '0794', '116.358181,27.949217', 1238),
(1326, '361002', '临川区', 3, '0794', '116.312166,27.934572', 1325),
(1327, '361021', '南城县', 3, '0794', '116.63704,27.569678', 1325),
(1328, '361022', '黎川县', 3, '0794', '116.907681,27.282333', 1325),
(1329, '361023', '南丰县', 3, '0794', '116.525725,27.218444', 1325),
(1330, '361024', '崇仁县', 3, '0794', '116.07626,27.754466', 1325),
(1331, '361025', '乐安县', 3, '0794', '115.83048,27.428765', 1325),
(1332, '361026', '宜黄县', 3, '0794', '116.236201,27.554886', 1325),
(1333, '361027', '金溪县', 3, '0794', '116.755058,27.918959', 1325),
(1334, '361028', '资溪县', 3, '0794', '117.060263,27.706101', 1325),
(1335, '361003', '东乡区', 3, '0794', '116.603559,28.247696', 1325),
(1336, '361030', '广昌县', 3, '0794', '116.335686,26.843684', 1325),
(1337, '361100', '上饶市', 2, '0793', '117.943433,28.454863', 1238),
(1338, '361102', '信州区', 3, '0793', '117.966268,28.431006', 1337),
(1339, '361103', '广丰区', 3, '0793', '118.19124,28.436285', 1337),
(1340, '361121', '上饶县', 3, '0793', '117.907849,28.448982', 1337),
(1341, '361123', '玉山县', 3, '0793', '118.244769,28.682309', 1337),
(1342, '361124', '铅山县', 3, '0793', '117.709659,28.315664', 1337),
(1343, '361125', '横峰县', 3, '0793', '117.596452,28.407117', 1337),
(1344, '361126', '弋阳县', 3, '0793', '117.449588,28.378044', 1337),
(1345, '361127', '余干县', 3, '0793', '116.695646,28.702302', 1337),
(1346, '361128', '鄱阳县', 3, '0793', '116.70359,29.004847', 1337),
(1347, '361129', '万年县', 3, '0793', '117.058445,28.694582', 1337),
(1348, '361130', '婺源县', 3, '0793', '117.861797,29.248085', 1337),
(1349, '361181', '德兴市', 3, '0793', '117.578713,28.946464', 1337),
(1350, '370000', '山东省', 1, '[]', '117.019915,36.671156', -1),
(1351, '370100', '济南市', 2, '0531', '117.120098,36.6512', 1350),
(1352, '370102', '历下区', 3, '0531', '117.076441,36.666465', 1351),
(1353, '370103', '市中区', 3, '0531', '116.997845,36.651335', 1351),
(1354, '370104', '槐荫区', 3, '0531', '116.901224,36.651441', 1351),
(1355, '370105', '天桥区', 3, '0531', '116.987153,36.678589', 1351),
(1356, '370112', '历城区', 3, '0531', '117.06523,36.680259', 1351),
(1357, '370113', '长清区', 3, '0531', '116.751843,36.55371', 1351),
(1358, '370124', '平阴县', 3, '0531', '116.456006,36.289251', 1351),
(1359, '370125', '济阳县', 3, '0531', '117.173524,36.978537', 1351),
(1360, '370126', '商河县', 3, '0531', '117.157232,37.309041', 1351),
(1361, '370114', '章丘区', 3, '0531', '117.526228,36.681258', 1351),
(1362, '370200', '青岛市', 2, '0532', '120.382621,36.067131', 1350),
(1363, '370202', '市南区', 3, '0532', '120.412392,36.075651', 1362),
(1364, '370203', '市北区', 3, '0532', '120.374701,36.0876', 1362),
(1365, '370211', '黄岛区', 3, '0532', '120.198055,35.960933', 1362),
(1366, '370212', '崂山区', 3, '0532', '120.468956,36.107538', 1362),
(1367, '370213', '李沧区', 3, '0532', '120.432922,36.145519', 1362),
(1368, '370214', '城阳区', 3, '0532', '120.396256,36.307559', 1362),
(1369, '370281', '胶州市', 3, '0532', '120.033382,36.26468', 1362),
(1370, '370282', '即墨市', 3, '0532', '120.447158,36.389408', 1362),
(1371, '370283', '平度市', 3, '0532', '119.98842,36.776357', 1362),
(1372, '370285', '莱西市', 3, '0532', '120.51769,36.889084', 1362),
(1373, '370300', '淄博市', 2, '0533', '118.055019,36.813546', 1350),
(1374, '370302', '淄川区', 3, '0533', '117.966723,36.643452', 1373),
(1375, '370303', '张店区', 3, '0533', '118.017938,36.806669', 1373),
(1376, '370304', '博山区', 3, '0533', '117.861851,36.494701', 1373),
(1377, '370305', '临淄区', 3, '0533', '118.309118,36.826981', 1373),
(1378, '370306', '周村区', 3, '0533', '117.869886,36.803072', 1373),
(1379, '370321', '桓台县', 3, '0533', '118.097922,36.959804', 1373),
(1380, '370322', '高青县', 3, '0533', '117.826924,37.170979', 1373),
(1381, '370323', '沂源县', 3, '0533', '118.170855,36.185038', 1373),
(1382, '370400', '枣庄市', 2, '0632', '117.323725,34.810488', 1350),
(1383, '370402', '市中区', 3, '0632', '117.556139,34.863554', 1382),
(1384, '370403', '薛城区', 3, '0632', '117.263164,34.795062', 1382),
(1385, '370404', '峄城区', 3, '0632', '117.590816,34.773263', 1382),
(1386, '370405', '台儿庄区', 3, '0632', '117.734414,34.56244', 1382),
(1387, '370406', '山亭区', 3, '0632', '117.461517,35.099528', 1382),
(1388, '370481', '滕州市', 3, '0632', '117.165824,35.114155', 1382),
(1389, '370500', '东营市', 2, '0546', '118.674614,37.433963', 1350),
(1390, '370502', '东营区', 3, '0546', '118.582184,37.448964', 1389),
(1391, '370503', '河口区', 3, '0546', '118.525543,37.886162', 1389),
(1392, '370505', '垦利区', 3, '0546', '118.575228,37.573054', 1389),
(1393, '370522', '利津县', 3, '0546', '118.255287,37.490328', 1389),
(1394, '370523', '广饶县', 3, '0546', '118.407107,37.053555', 1389),
(1395, '370600', '烟台市', 2, '0535', '121.447852,37.464539', 1350),
(1396, '370602', '芝罘区', 3, '0535', '121.400445,37.541475', 1395),
(1397, '370611', '福山区', 3, '0535', '121.267741,37.498246', 1395),
(1398, '370612', '牟平区', 3, '0535', '121.600455,37.387061', 1395),
(1399, '370613', '莱山区', 3, '0535', '121.445301,37.511291', 1395),
(1400, '370634', '长岛县', 3, '0535', '120.73658,37.921368', 1395),
(1401, '370681', '龙口市', 3, '0535', '120.477813,37.646107', 1395),
(1402, '370682', '莱阳市', 3, '0535', '120.711672,36.978941', 1395),
(1403, '370683', '莱州市', 3, '0535', '119.942274,37.177129', 1395),
(1404, '370684', '蓬莱市', 3, '0535', '120.758848,37.810661', 1395),
(1405, '370685', '招远市', 3, '0535', '120.434071,37.355469', 1395),
(1406, '370686', '栖霞市', 3, '0535', '120.849675,37.335123', 1395),
(1407, '370687', '海阳市', 3, '0535', '121.173793,36.688', 1395),
(1408, '370700', '潍坊市', 2, '0536', '119.161748,36.706962', 1350),
(1409, '370702', '潍城区', 3, '0536', '119.024835,36.7281', 1408),
(1410, '370703', '寒亭区', 3, '0536', '119.211157,36.755623', 1408),
(1411, '370704', '坊子区', 3, '0536', '119.166485,36.654448', 1408),
(1412, '370705', '奎文区', 3, '0536', '119.132482,36.70759', 1408),
(1413, '370724', '临朐县', 3, '0536', '118.542982,36.512506', 1408),
(1414, '370725', '昌乐县', 3, '0536', '118.829992,36.706964', 1408),
(1415, '370781', '青州市', 3, '0536', '118.479654,36.684789', 1408),
(1416, '370782', '诸城市', 3, '0536', '119.410103,35.995654', 1408),
(1417, '370783', '寿光市', 3, '0536', '118.790739,36.85576', 1408),
(1418, '370784', '安丘市', 3, '0536', '119.218978,36.478493', 1408),
(1419, '370785', '高密市', 3, '0536', '119.755597,36.382594', 1408),
(1420, '370786', '昌邑市', 3, '0536', '119.403069,36.843319', 1408),
(1421, '370800', '济宁市', 2, '0537', '116.587282,35.414982', 1350),
(1422, '370811', '任城区', 3, '0537', '116.606103,35.444028', 1421),
(1423, '370812', '兖州区', 3, '0537', '116.783833,35.553144', 1421),
(1424, '370826', '微山县', 3, '0537', '117.128827,34.806554', 1421),
(1425, '370827', '鱼台县', 3, '0537', '116.650608,35.012749', 1421),
(1426, '370828', '金乡县', 3, '0537', '116.311532,35.066619', 1421),
(1427, '370829', '嘉祥县', 3, '0537', '116.342449,35.408824', 1421),
(1428, '370830', '汶上县', 3, '0537', '116.49708,35.712298', 1421),
(1429, '370831', '泗水县', 3, '0537', '117.251195,35.664323', 1421),
(1430, '370832', '梁山县', 3, '0537', '116.096044,35.802306', 1421),
(1431, '370881', '曲阜市', 3, '0537', '116.986526,35.581108', 1421),
(1432, '370883', '邹城市', 3, '0537', '117.007453,35.40268', 1421),
(1433, '370900', '泰安市', 2, '0538', '117.087614,36.200252', 1350),
(1434, '370902', '泰山区', 3, '0538', '117.135354,36.192083', 1433),
(1435, '370911', '岱岳区', 3, '0538', '117.041581,36.187989', 1433),
(1436, '370921', '宁阳县', 3, '0538', '116.805796,35.758786', 1433),
(1437, '370923', '东平县', 3, '0538', '116.470304,35.937102', 1433),
(1438, '370982', '新泰市', 3, '0538', '117.767952,35.909032', 1433),
(1439, '370983', '肥城市', 3, '0538', '116.768358,36.182571', 1433),
(1440, '371000', '威海市', 2, '0631', '122.120282,37.513412', 1350),
(1441, '371002', '环翠区', 3, '0631', '122.123443,37.50199', 1440),
(1442, '371003', '文登区', 3, '0631', '122.05767,37.193735', 1440),
(1443, '371082', '荣成市', 3, '0631', '122.486657,37.16516', 1440),
(1444, '371083', '乳山市', 3, '0631', '121.539764,36.919816', 1440),
(1445, '371100', '日照市', 2, '0633', '119.526925,35.416734', 1350),
(1446, '371102', '东港区', 3, '0633', '119.462267,35.42548', 1445),
(1447, '371103', '岚山区', 3, '0633', '119.318928,35.121884', 1445),
(1448, '371121', '五莲县', 3, '0633', '119.213619,35.760228', 1445),
(1449, '371122', '莒县', 3, '0633', '118.837063,35.579868', 1445),
(1450, '371200', '莱芜市', 2, '0634', '117.676723,36.213813', 1350),
(1451, '371202', '莱城区', 3, '0634', '117.659884,36.203179', 1450),
(1452, '371203', '钢城区', 3, '0634', '117.811354,36.058572', 1450),
(1453, '371300', '临沂市', 2, '0539', '118.356414,35.104673', 1350),
(1454, '371302', '兰山区', 3, '0539', '118.347842,35.051804', 1453),
(1455, '371311', '罗庄区', 3, '0539', '118.284786,34.996741', 1453),
(1456, '371312', '河东区', 3, '0539', '118.402893,35.089916', 1453),
(1457, '371321', '沂南县', 3, '0539', '118.465221,35.550217', 1453),
(1458, '371322', '郯城县', 3, '0539', '118.367215,34.613586', 1453),
(1459, '371323', '沂水县', 3, '0539', '118.627917,35.79045', 1453),
(1460, '371324', '兰陵县', 3, '0539', '118.07065,34.857149', 1453),
(1461, '371325', '费县', 3, '0539', '117.977325,35.26596', 1453),
(1462, '371326', '平邑县', 3, '0539', '117.640352,35.505943', 1453),
(1463, '371327', '莒南县', 3, '0539', '118.835163,35.174846', 1453),
(1464, '371328', '蒙阴县', 3, '0539', '117.953621,35.719396', 1453),
(1465, '371329', '临沭县', 3, '0539', '118.650781,34.919851', 1453),
(1466, '371400', '德州市', 2, '0534', '116.359381,37.436657', 1350),
(1467, '371402', '德城区', 3, '0534', '116.29947,37.450804', 1466),
(1468, '371403', '陵城区', 3, '0534', '116.576092,37.335794', 1466),
(1469, '371422', '宁津县', 3, '0534', '116.800306,37.652189', 1466),
(1470, '371423', '庆云县', 3, '0534', '117.385256,37.775349', 1466),
(1471, '371424', '临邑县', 3, '0534', '116.866799,37.189797', 1466),
(1472, '371425', '齐河县', 3, '0534', '116.762893,36.784158', 1466),
(1473, '371426', '平原县', 3, '0534', '116.434032,37.165323', 1466),
(1474, '371427', '夏津县', 3, '0534', '116.001726,36.948371', 1466),
(1475, '371428', '武城县', 3, '0534', '116.069302,37.213311', 1466),
(1476, '371481', '乐陵市', 3, '0534', '117.231934,37.729907', 1466),
(1477, '371482', '禹城市', 3, '0534', '116.638327,36.933812', 1466),
(1478, '371500', '聊城市', 2, '0635', '115.985389,36.456684', 1350),
(1479, '371502', '东昌府区', 3, '0635', '115.988349,36.434669', 1478),
(1480, '371521', '阳谷县', 3, '0635', '115.79182,36.114392', 1478),
(1481, '371522', '莘县', 3, '0635', '115.671191,36.233598', 1478),
(1482, '371523', '茌平县', 3, '0635', '116.25527,36.580688', 1478),
(1483, '371524', '东阿县', 3, '0635', '116.247579,36.334917', 1478),
(1484, '371525', '冠县', 3, '0635', '115.442739,36.484009', 1478),
(1485, '371526', '高唐县', 3, '0635', '116.23016,36.846762', 1478),
(1486, '371581', '临清市', 3, '0635', '115.704881,36.838277', 1478),
(1487, '371600', '滨州市', 2, '0543', '117.970699,37.38198', 1350),
(1488, '371602', '滨城区', 3, '0543', '118.019326,37.430724', 1487),
(1489, '371603', '沾化区', 3, '0543', '118.098902,37.69926', 1487),
(1490, '371621', '惠民县', 3, '0543', '117.509921,37.489877', 1487),
(1491, '371622', '阳信县', 3, '0543', '117.603339,37.632433', 1487),
(1492, '371623', '无棣县', 3, '0543', '117.625696,37.77026', 1487),
(1493, '371625', '博兴县', 3, '0543', '118.110709,37.15457', 1487),
(1494, '371626', '邹平县', 3, '0543', '117.743109,36.862989', 1487),
(1495, '371700', '菏泽市', 2, '0530', '115.480656,35.23375', 1350),
(1496, '371702', '牡丹区', 3, '0530', '115.417826,35.252512', 1495),
(1497, '371703', '定陶区', 3, '0530', '115.57302,35.070995', 1495),
(1498, '371721', '曹县', 3, '0530', '115.542328,34.825508', 1495),
(1499, '371722', '单县', 3, '0530', '116.107428,34.778808', 1495),
(1500, '371723', '成武县', 3, '0530', '115.889764,34.952459', 1495),
(1501, '371724', '巨野县', 3, '0530', '116.062394,35.388925', 1495),
(1502, '371725', '郓城县', 3, '0530', '115.9389,35.575135', 1495),
(1503, '371726', '鄄城县', 3, '0530', '115.510192,35.563408', 1495),
(1504, '371728', '东明县', 3, '0530', '115.107404,35.276162', 1495),
(1505, '410000', '河南省', 1, '[]', '113.753394,34.765869', -1),
(1506, '410100', '郑州市', 2, '0371', '113.625328,34.746611', 1505),
(1507, '410102', '中原区', 3, '0371', '113.613337,34.748256', 1506),
(1508, '410103', '二七区', 3, '0371', '113.640211,34.724114', 1506),
(1509, '410104', '管城回族区', 3, '0371', '113.6775,34.75429', 1506),
(1510, '410105', '金水区', 3, '0371', '113.660617,34.800004', 1506),
(1511, '410106', '上街区', 3, '0371', '113.30893,34.802752', 1506),
(1512, '410108', '惠济区', 3, '0371', '113.6169,34.867457', 1506),
(1513, '410122', '中牟县', 3, '0371', '113.976253,34.718936', 1506),
(1514, '410181', '巩义市', 3, '0371', '113.022406,34.7481', 1506),
(1515, '410182', '荥阳市', 3, '0371', '113.38324,34.786948', 1506),
(1516, '410183', '新密市', 3, '0371', '113.391087,34.539376', 1506),
(1517, '410184', '新郑市', 3, '0371', '113.740662,34.395949', 1506),
(1518, '410185', '登封市', 3, '0371', '113.050581,34.454443', 1506),
(1519, '410200', '开封市', 2, '0378', '114.307677,34.797966', 1505),
(1520, '410202', '龙亭区', 3, '0378', '114.356076,34.815565', 1519),
(1521, '410203', '顺河回族区', 3, '0378', '114.364875,34.800458', 1519),
(1522, '410204', '鼓楼区', 3, '0378', '114.348306,34.78856', 1519),
(1523, '410205', '禹王台区', 3, '0378', '114.34817,34.777104', 1519),
(1524, '410212', '祥符区', 3, '0378', '114.441285,34.756916', 1519),
(1525, '410221', '杞县', 3, '0378', '114.783139,34.549174', 1519),
(1526, '410222', '通许县', 3, '0378', '114.467467,34.480433', 1519),
(1527, '410223', '尉氏县', 3, '0378', '114.193081,34.411494', 1519),
(1528, '410225', '兰考县', 3, '0378', '114.821348,34.822211', 1519),
(1529, '410300', '洛阳市', 2, '0379', '112.453926,34.620202', 1505),
(1530, '410302', '老城区', 3, '0379', '112.469766,34.6842', 1529),
(1531, '410303', '西工区', 3, '0379', '112.427914,34.660378', 1529),
(1532, '410304', '瀍河回族区', 3, '0379', '112.500131,34.679773', 1529),
(1533, '410305', '涧西区', 3, '0379', '112.395756,34.658033', 1529),
(1534, '410306', '吉利区', 3, '0379', '112.589112,34.900467', 1529),
(1535, '410311', '洛龙区', 3, '0379', '112.463833,34.619711', 1529),
(1536, '410322', '孟津县', 3, '0379', '112.445354,34.825638', 1529),
(1537, '410323', '新安县', 3, '0379', '112.13244,34.728284', 1529),
(1538, '410324', '栾川县', 3, '0379', '111.615768,33.785698', 1529),
(1539, '410325', '嵩县', 3, '0379', '112.085634,34.134516', 1529),
(1540, '410326', '汝阳县', 3, '0379', '112.473139,34.153939', 1529),
(1541, '410327', '宜阳县', 3, '0379', '112.179238,34.514644', 1529),
(1542, '410328', '洛宁县', 3, '0379', '111.653111,34.389197', 1529),
(1543, '410329', '伊川县', 3, '0379', '112.425676,34.421323', 1529),
(1544, '410381', '偃师市', 3, '0379', '112.789534,34.72722', 1529),
(1545, '410400', '平顶山市', 2, '0375', '113.192661,33.766169', 1505),
(1546, '410402', '新华区', 3, '0375', '113.293977,33.737251', 1545),
(1547, '410403', '卫东区', 3, '0375', '113.335192,33.734706', 1545),
(1548, '410404', '石龙区', 3, '0375', '112.898818,33.898713', 1545),
(1549, '410411', '湛河区', 3, '0375', '113.320873,33.725681', 1545),
(1550, '410421', '宝丰县', 3, '0375', '113.054801,33.868434', 1545),
(1551, '410422', '叶县', 3, '0375', '113.357239,33.626731', 1545),
(1552, '410423', '鲁山县', 3, '0375', '112.908202,33.738293', 1545),
(1553, '410425', '郏县', 3, '0375', '113.212609,33.971787', 1545),
(1554, '410481', '舞钢市', 3, '0375', '113.516343,33.314033', 1545),
(1555, '410482', '汝州市', 3, '0375', '112.844517,34.167029', 1545),
(1556, '410500', '安阳市', 2, '0372', '114.392392,36.097577', 1505),
(1557, '410502', '文峰区', 3, '0372', '114.357082,36.090468', 1556),
(1558, '410503', '北关区', 3, '0372', '114.355742,36.10766', 1556),
(1559, '410505', '殷都区', 3, '0372', '114.303553,36.10989', 1556),
(1560, '410506', '龙安区', 3, '0372', '114.301331,36.076225', 1556),
(1561, '410522', '安阳县', 3, '0372', '114.130207,36.130584', 1556),
(1562, '410523', '汤阴县', 3, '0372', '114.357763,35.924514', 1556),
(1563, '410526', '滑县', 3, '0372', '114.519311,35.575417', 1556),
(1564, '410527', '内黄县', 3, '0372', '114.901452,35.971704', 1556),
(1565, '410581', '林州市', 3, '0372', '113.820129,36.083046', 1556),
(1566, '410600', '鹤壁市', 2, '0392', '114.297309,35.748325', 1505),
(1567, '410602', '鹤山区', 3, '0392', '114.163258,35.954611', 1566),
(1568, '410603', '山城区', 3, '0392', '114.184318,35.898033', 1566),
(1569, '410611', '淇滨区', 3, '0392', '114.298789,35.741592', 1566),
(1570, '410621', '浚县', 3, '0392', '114.55091,35.67636', 1566),
(1571, '410622', '淇县', 3, '0392', '114.208828,35.622507', 1566),
(1572, '410700', '新乡市', 2, '0373', '113.926763,35.303704', 1505),
(1573, '410702', '红旗区', 3, '0373', '113.875245,35.30385', 1572),
(1574, '410703', '卫滨区', 3, '0373', '113.865663,35.301992', 1572),
(1575, '410704', '凤泉区', 3, '0373', '113.915184,35.383978', 1572),
(1576, '410711', '牧野区', 3, '0373', '113.908772,35.315039', 1572),
(1577, '410721', '新乡县', 3, '0373', '113.805205,35.190836', 1572),
(1578, '410724', '获嘉县', 3, '0373', '113.657433,35.259808', 1572),
(1579, '410725', '原阳县', 3, '0373', '113.940046,35.065587', 1572),
(1580, '410726', '延津县', 3, '0373', '114.20509,35.141889', 1572),
(1581, '410727', '封丘县', 3, '0373', '114.418882,35.041198', 1572),
(1582, '410728', '长垣县', 3, '0373', '114.668936,35.201548', 1572),
(1583, '410781', '卫辉市', 3, '0373', '114.064907,35.398494', 1572),
(1584, '410782', '辉县市', 3, '0373', '113.805468,35.462312', 1572),
(1585, '410800', '焦作市', 2, '0391', '113.241823,35.215893', 1505),
(1586, '410802', '解放区', 3, '0391', '113.230816,35.240282', 1585),
(1587, '410803', '中站区', 3, '0391', '113.182946,35.236819', 1585),
(1588, '410804', '马村区', 3, '0391', '113.322332,35.256108', 1585),
(1589, '410811', '山阳区', 3, '0391', '113.254881,35.214507', 1585),
(1590, '410821', '修武县', 3, '0391', '113.447755,35.223514', 1585),
(1591, '410822', '博爱县', 3, '0391', '113.064379,35.171045', 1585),
(1592, '410823', '武陟县', 3, '0391', '113.401679,35.099378', 1585),
(1593, '410825', '温县', 3, '0391', '113.08053,34.940189', 1585),
(1594, '410882', '沁阳市', 3, '0391', '112.950716,35.087539', 1585),
(1595, '410883', '孟州市', 3, '0391', '112.791401,34.907315', 1585);
INSERT INTO `mf_area_items` (`areaId`, `areaCode`, `areaName`, `level`, `cityCode`, `center`, `parentId`) VALUES
(1596, '410900', '濮阳市', 2, '0393', '115.029216,35.761829', 1505),
(1597, '410902', '华龙区', 3, '0393', '115.074151,35.777346', 1596),
(1598, '410922', '清丰县', 3, '0393', '115.104389,35.88518', 1596),
(1599, '410923', '南乐县', 3, '0393', '115.204675,36.069476', 1596),
(1600, '410926', '范县', 3, '0393', '115.504201,35.851906', 1596),
(1601, '410927', '台前县', 3, '0393', '115.871906,35.96939', 1596),
(1602, '410928', '濮阳县', 3, '0393', '115.029078,35.712193', 1596),
(1603, '411000', '许昌市', 2, '0374', '113.852454,34.035771', 1505),
(1604, '411002', '魏都区', 3, '0374', '113.822647,34.025341', 1603),
(1605, '411003', '建安区', 3, '0374', '113.822983,34.12466', 1603),
(1606, '411024', '鄢陵县', 3, '0374', '114.177399,34.102332', 1603),
(1607, '411025', '襄城县', 3, '0374', '113.505874,33.851459', 1603),
(1608, '411081', '禹州市', 3, '0374', '113.488478,34.140701', 1603),
(1609, '411082', '长葛市', 3, '0374', '113.813714,34.19592', 1603),
(1610, '411100', '漯河市', 2, '0395', '114.016536,33.580873', 1505),
(1611, '411102', '源汇区', 3, '0395', '114.017948,33.565441', 1610),
(1612, '411103', '郾城区', 3, '0395', '114.006943,33.587409', 1610),
(1613, '411104', '召陵区', 3, '0395', '114.093902,33.586565', 1610),
(1614, '411121', '舞阳县', 3, '0395', '113.609286,33.437876', 1610),
(1615, '411122', '临颍县', 3, '0395', '113.931261,33.828042', 1610),
(1616, '411200', '三门峡市', 2, '0398', '111.200367,34.772792', 1505),
(1617, '411202', '湖滨区', 3, '0398', '111.188397,34.770886', 1616),
(1618, '411203', '陕州区', 3, '0398', '111.103563,34.720547', 1616),
(1619, '411221', '渑池县', 3, '0398', '111.761797,34.767951', 1616),
(1620, '411224', '卢氏县', 3, '0398', '111.047858,34.054324', 1616),
(1621, '411281', '义马市', 3, '0398', '111.87448,34.7474', 1616),
(1622, '411282', '灵宝市', 3, '0398', '110.89422,34.516828', 1616),
(1623, '411300', '南阳市', 2, '0377', '112.528308,32.990664', 1505),
(1624, '411302', '宛城区', 3, '0377', '112.539558,33.003784', 1623),
(1625, '411303', '卧龙区', 3, '0377', '112.528789,32.989877', 1623),
(1626, '411321', '南召县', 3, '0377', '112.429133,33.489877', 1623),
(1627, '411322', '方城县', 3, '0377', '113.012494,33.254391', 1623),
(1628, '411323', '西峡县', 3, '0377', '111.47353,33.307294', 1623),
(1629, '411324', '镇平县', 3, '0377', '112.234697,33.03411', 1623),
(1630, '411325', '内乡县', 3, '0377', '111.849392,33.044864', 1623),
(1631, '411326', '淅川县', 3, '0377', '111.490964,33.13782', 1623),
(1632, '411327', '社旗县', 3, '0377', '112.948245,33.056109', 1623),
(1633, '411328', '唐河县', 3, '0377', '112.807636,32.681335', 1623),
(1634, '411329', '新野县', 3, '0377', '112.360026,32.520805', 1623),
(1635, '411330', '桐柏县', 3, '0377', '113.428287,32.380073', 1623),
(1636, '411381', '邓州市', 3, '0377', '112.087493,32.68758', 1623),
(1637, '411400', '商丘市', 2, '0370', '115.656339,34.414961', 1505),
(1638, '411402', '梁园区', 3, '0370', '115.613965,34.443893', 1637),
(1639, '411403', '睢阳区', 3, '0370', '115.653301,34.388389', 1637),
(1640, '411421', '民权县', 3, '0370', '115.173971,34.648191', 1637),
(1641, '411422', '睢县', 3, '0370', '115.071879,34.445655', 1637),
(1642, '411423', '宁陵县', 3, '0370', '115.313743,34.460399', 1637),
(1643, '411424', '柘城县', 3, '0370', '115.305708,34.091082', 1637),
(1644, '411425', '虞城县', 3, '0370', '115.828319,34.400835', 1637),
(1645, '411426', '夏邑县', 3, '0370', '116.131447,34.237553', 1637),
(1646, '411481', '永城市', 3, '0370', '116.4495,33.929291', 1637),
(1647, '411500', '信阳市', 2, '0376', '114.091193,32.147679', 1505),
(1648, '411502', '浉河区', 3, '0376', '114.058713,32.116803', 1647),
(1649, '411503', '平桥区', 3, '0376', '114.125656,32.101031', 1647),
(1650, '411521', '罗山县', 3, '0376', '114.512872,32.203883', 1647),
(1651, '411522', '光山县', 3, '0376', '114.919152,32.010002', 1647),
(1652, '411523', '新县', 3, '0376', '114.879239,31.643918', 1647),
(1653, '411524', '商城县', 3, '0376', '115.406862,31.798377', 1647),
(1654, '411525', '固始县', 3, '0376', '115.654481,32.168137', 1647),
(1655, '411526', '潢川县', 3, '0376', '115.051908,32.131522', 1647),
(1656, '411527', '淮滨县', 3, '0376', '115.419537,32.473258', 1647),
(1657, '411528', '息县', 3, '0376', '114.740456,32.342792', 1647),
(1658, '411600', '周口市', 2, '0394', '114.69695,33.626149', 1505),
(1659, '411602', '川汇区', 3, '0394', '114.650628,33.647598', 1658),
(1660, '411621', '扶沟县', 3, '0394', '114.394821,34.059968', 1658),
(1661, '411622', '西华县', 3, '0394', '114.529756,33.767407', 1658),
(1662, '411623', '商水县', 3, '0394', '114.611651,33.542138', 1658),
(1663, '411624', '沈丘县', 3, '0394', '115.098583,33.409369', 1658),
(1664, '411625', '郸城县', 3, '0394', '115.177188,33.644743', 1658),
(1665, '411626', '淮阳县', 3, '0394', '114.886153,33.731561', 1658),
(1666, '411627', '太康县', 3, '0394', '114.837888,34.064463', 1658),
(1667, '411628', '鹿邑县', 3, '0394', '115.484454,33.86', 1658),
(1668, '411681', '项城市', 3, '0394', '114.875333,33.465838', 1658),
(1669, '411700', '驻马店市', 2, '0396', '114.022247,33.012885', 1505),
(1670, '411702', '驿城区', 3, '0396', '113.993914,32.973054', 1669),
(1671, '411721', '西平县', 3, '0396', '114.021538,33.387684', 1669),
(1672, '411722', '上蔡县', 3, '0396', '114.264381,33.262439', 1669),
(1673, '411723', '平舆县', 3, '0396', '114.619159,32.96271', 1669),
(1674, '411724', '正阳县', 3, '0396', '114.392773,32.605697', 1669),
(1675, '411725', '确山县', 3, '0396', '114.026429,32.802064', 1669),
(1676, '411726', '泌阳县', 3, '0396', '113.327144,32.723975', 1669),
(1677, '411727', '汝南县', 3, '0396', '114.362379,33.006729', 1669),
(1678, '411728', '遂平县', 3, '0396', '114.013182,33.145649', 1669),
(1679, '411729', '新蔡县', 3, '0396', '114.96547,32.744896', 1669),
(1680, '419001', '济源市', 2, '1391', '112.602256,35.067199', 1505),
(1681, '420000', '湖北省', 1, '[]', '114.341745,30.546557', -1),
(1682, '420100', '武汉市', 2, '027', '114.305469,30.593175', 1681),
(1683, '420102', '江岸区', 3, '027', '114.30911,30.600052', 1682),
(1684, '420103', '江汉区', 3, '027', '114.270867,30.601475', 1682),
(1685, '420104', '硚口区', 3, '027', '114.21492,30.582202', 1682),
(1686, '420105', '汉阳区', 3, '027', '114.21861,30.553983', 1682),
(1687, '420106', '武昌区', 3, '027', '114.31665,30.554408', 1682),
(1688, '420107', '青山区', 3, '027', '114.384968,30.640191', 1682),
(1689, '420111', '洪山区', 3, '027', '114.343796,30.500247', 1682),
(1690, '420112', '东西湖区', 3, '027', '114.137116,30.619917', 1682),
(1691, '420113', '汉南区', 3, '027', '114.084597,30.308829', 1682),
(1692, '420114', '蔡甸区', 3, '027', '114.087285,30.536454', 1682),
(1693, '420115', '江夏区', 3, '027', '114.319097,30.376308', 1682),
(1694, '420116', '黄陂区', 3, '027', '114.375725,30.882174', 1682),
(1695, '420117', '新洲区', 3, '027', '114.801096,30.841425', 1682),
(1696, '420200', '黄石市', 2, '0714', '115.038962,30.201038', 1681),
(1697, '420202', '黄石港区', 3, '0714', '115.065849,30.222938', 1696),
(1698, '420203', '西塞山区', 3, '0714', '115.109955,30.204924', 1696),
(1699, '420204', '下陆区', 3, '0714', '114.961327,30.173912', 1696),
(1700, '420205', '铁山区', 3, '0714', '114.891605,30.203118', 1696),
(1701, '420222', '阳新县', 3, '0714', '115.215227,29.830257', 1696),
(1702, '420281', '大冶市', 3, '0714', '114.980424,30.096147', 1696),
(1703, '420300', '十堰市', 2, '0719', '110.799291,32.629462', 1681),
(1704, '420302', '茅箭区', 3, '0719', '110.813719,32.591904', 1703),
(1705, '420303', '张湾区', 3, '0719', '110.769132,32.652297', 1703),
(1706, '420304', '郧阳区', 3, '0719', '110.81205,32.834775', 1703),
(1707, '420322', '郧西县', 3, '0719', '110.425983,32.993182', 1703),
(1708, '420323', '竹山县', 3, '0719', '110.228747,32.224808', 1703),
(1709, '420324', '竹溪县', 3, '0719', '109.715304,32.318255', 1703),
(1710, '420325', '房县', 3, '0719', '110.733181,32.050378', 1703),
(1711, '420381', '丹江口市', 3, '0719', '111.513127,32.540157', 1703),
(1712, '420500', '宜昌市', 2, '0717', '111.286445,30.691865', 1681),
(1713, '420502', '西陵区', 3, '0717', '111.285646,30.710781', 1712),
(1714, '420503', '伍家岗区', 3, '0717', '111.361037,30.644334', 1712),
(1715, '420504', '点军区', 3, '0717', '111.268119,30.693247', 1712),
(1716, '420505', '猇亭区', 3, '0717', '111.43462,30.530903', 1712),
(1717, '420506', '夷陵区', 3, '0717', '111.32638,30.770006', 1712),
(1718, '420525', '远安县', 3, '0717', '111.640508,31.060869', 1712),
(1719, '420526', '兴山县', 3, '0717', '110.746804,31.348196', 1712),
(1720, '420527', '秭归县', 3, '0717', '110.977711,30.825897', 1712),
(1721, '420528', '长阳土家族自治县', 3, '0717', '111.207242,30.472763', 1712),
(1722, '420529', '五峰土家族自治县', 3, '0717', '111.07374,30.156741', 1712),
(1723, '420581', '宜都市', 3, '0717', '111.450096,30.378299', 1712),
(1724, '420582', '当阳市', 3, '0717', '111.788312,30.821266', 1712),
(1725, '420583', '枝江市', 3, '0717', '111.76053,30.42594', 1712),
(1726, '420600', '襄阳市', 2, '0710', '112.122426,32.009016', 1681),
(1727, '420602', '襄城区', 3, '0710', '112.134052,32.010366', 1726),
(1728, '420606', '樊城区', 3, '0710', '112.135684,32.044832', 1726),
(1729, '420607', '襄州区', 3, '0710', '112.211982,32.087127', 1726),
(1730, '420624', '南漳县', 3, '0710', '111.838905,31.774636', 1726),
(1731, '420625', '谷城县', 3, '0710', '111.652982,32.263849', 1726),
(1732, '420626', '保康县', 3, '0710', '111.261308,31.87831', 1726),
(1733, '420682', '老河口市', 3, '0710', '111.683861,32.359068', 1726),
(1734, '420683', '枣阳市', 3, '0710', '112.771959,32.128818', 1726),
(1735, '420684', '宜城市', 3, '0710', '112.257788,31.719806', 1726),
(1736, '420700', '鄂州市', 2, '0711', '114.894935,30.391141', 1681),
(1737, '420702', '梁子湖区', 3, '0711', '114.684731,30.100141', 1736),
(1738, '420703', '华容区', 3, '0711', '114.729878,30.534309', 1736),
(1739, '420704', '鄂城区', 3, '0711', '114.891586,30.400651', 1736),
(1740, '420800', '荆门市', 2, '0724', '112.199427,31.035395', 1681),
(1741, '420802', '东宝区', 3, '0724', '112.201493,31.051852', 1740),
(1742, '420804', '掇刀区', 3, '0724', '112.207962,30.973451', 1740),
(1743, '420821', '京山县', 3, '0724', '113.119566,31.018457', 1740),
(1744, '420822', '沙洋县', 3, '0724', '112.588581,30.709221', 1740),
(1745, '420881', '钟祥市', 3, '0724', '112.58812,31.167819', 1740),
(1746, '420900', '孝感市', 2, '0712', '113.957037,30.917766', 1681),
(1747, '420902', '孝南区', 3, '0712', '113.910705,30.916812', 1746),
(1748, '420921', '孝昌县', 3, '0712', '113.998009,31.258159', 1746),
(1749, '420922', '大悟县', 3, '0712', '114.127022,31.561164', 1746),
(1750, '420923', '云梦县', 3, '0712', '113.753554,31.020983', 1746),
(1751, '420981', '应城市', 3, '0712', '113.572707,30.92837', 1746),
(1752, '420982', '安陆市', 3, '0712', '113.688941,31.25561', 1746),
(1753, '420984', '汉川市', 3, '0712', '113.839149,30.661243', 1746),
(1754, '421000', '荆州市', 2, '0716', '112.239746,30.335184', 1681),
(1755, '421002', '沙市区', 3, '0716', '112.25193,30.326009', 1754),
(1756, '421003', '荆州区', 3, '0716', '112.190185,30.352853', 1754),
(1757, '421022', '公安县', 3, '0716', '112.229648,30.058336', 1754),
(1758, '421023', '监利县', 3, '0716', '112.904788,29.840179', 1754),
(1759, '421024', '江陵县', 3, '0716', '112.424664,30.041822', 1754),
(1760, '421081', '石首市', 3, '0716', '112.425454,29.720938', 1754),
(1761, '421083', '洪湖市', 3, '0716', '113.475801,29.826916', 1754),
(1762, '421087', '松滋市', 3, '0716', '111.756781,30.174529', 1754),
(1763, '421100', '黄冈市', 2, '0713', '114.872199,30.453667', 1681),
(1764, '421102', '黄州区', 3, '0713', '114.880104,30.434354', 1763),
(1765, '421121', '团风县', 3, '0713', '114.872191,30.643569', 1763),
(1766, '421122', '红安县', 3, '0713', '114.618236,31.288153', 1763),
(1767, '421123', '罗田县', 3, '0713', '115.399222,30.78429', 1763),
(1768, '421124', '英山县', 3, '0713', '115.681359,30.735157', 1763),
(1769, '421125', '浠水县', 3, '0713', '115.265355,30.452115', 1763),
(1770, '421126', '蕲春县', 3, '0713', '115.437007,30.225964', 1763),
(1771, '421127', '黄梅县', 3, '0713', '115.944219,30.070453', 1763),
(1772, '421181', '麻城市', 3, '0713', '115.008163,31.172739', 1763),
(1773, '421182', '武穴市', 3, '0713', '115.561217,29.844107', 1763),
(1774, '421200', '咸宁市', 2, '0715', '114.322616,29.841362', 1681),
(1775, '421202', '咸安区', 3, '0715', '114.298711,29.852891', 1774),
(1776, '421221', '嘉鱼县', 3, '0715', '113.939271,29.970676', 1774),
(1777, '421222', '通城县', 3, '0715', '113.816966,29.245269', 1774),
(1778, '421223', '崇阳县', 3, '0715', '114.039523,29.556688', 1774),
(1779, '421224', '通山县', 3, '0715', '114.482622,29.606372', 1774),
(1780, '421281', '赤壁市', 3, '0715', '113.90038,29.725184', 1774),
(1781, '421300', '随州市', 2, '0722', '113.382515,31.690191', 1681),
(1782, '421303', '曾都区', 3, '0722', '113.37112,31.71628', 1781),
(1783, '421321', '随县', 3, '0722', '113.290634,31.883739', 1781),
(1784, '421381', '广水市', 3, '0722', '113.825889,31.616853', 1781),
(1785, '422800', '恩施土家族苗族自治州', 2, '0718', '109.488172,30.272156', 1681),
(1786, '422801', '恩施市', 3, '0718', '109.479664,30.29468', 1785),
(1787, '422802', '利川市', 3, '0718', '108.936452,30.29098', 1785),
(1788, '422822', '建始县', 3, '0718', '109.722109,30.602129', 1785),
(1789, '422823', '巴东县', 3, '0718', '110.340756,31.042324', 1785),
(1790, '422825', '宣恩县', 3, '0718', '109.489926,29.98692', 1785),
(1791, '422826', '咸丰县', 3, '0718', '109.139726,29.665202', 1785),
(1792, '422827', '来凤县', 3, '0718', '109.407828,29.493484', 1785),
(1793, '422828', '鹤峰县', 3, '0718', '110.033662,29.890171', 1785),
(1794, '429005', '潜江市', 2, '2728', '112.899762,30.402167', 1681),
(1795, '429021', '神农架林区', 2, '1719', '110.675743,31.744915', 1681),
(1796, '429006', '天门市', 2, '1728', '113.166078,30.663337', 1681),
(1797, '429004', '仙桃市', 2, '0728', '113.423583,30.361438', 1681),
(1798, '430000', '湖南省', 1, '[]', '112.9836,28.112743', -1),
(1799, '430100', '长沙市', 2, '0731', '112.938884,28.22808', 1798),
(1800, '430102', '芙蓉区', 3, '0731', '113.032539,28.185389', 1799),
(1801, '430103', '天心区', 3, '0731', '112.989897,28.114526', 1799),
(1802, '430104', '岳麓区', 3, '0731', '112.93132,28.234538', 1799),
(1803, '430105', '开福区', 3, '0731', '112.985884,28.256298', 1799),
(1804, '430111', '雨花区', 3, '0731', '113.03826,28.135722', 1799),
(1805, '430112', '望城区', 3, '0731', '112.831176,28.353434', 1799),
(1806, '430121', '长沙县', 3, '0731', '113.081097,28.246918', 1799),
(1807, '430124', '宁乡市', 3, '0731', '112.551885,28.277483', 1799),
(1808, '430181', '浏阳市', 3, '0731', '113.643076,28.162833', 1799),
(1809, '430200', '株洲市', 2, '0733', '113.133853,27.827986', 1798),
(1810, '430202', '荷塘区', 3, '0733', '113.173487,27.855928', 1809),
(1811, '430203', '芦淞区', 3, '0733', '113.152724,27.78507', 1809),
(1812, '430204', '石峰区', 3, '0733', '113.117731,27.875445', 1809),
(1813, '430211', '天元区', 3, '0733', '113.082216,27.826866', 1809),
(1814, '430221', '株洲县', 3, '0733', '113.144109,27.699232', 1809),
(1815, '430223', '攸县', 3, '0733', '113.396385,27.014583', 1809),
(1816, '430224', '茶陵县', 3, '0733', '113.539094,26.777521', 1809),
(1817, '430225', '炎陵县', 3, '0733', '113.772655,26.489902', 1809),
(1818, '430281', '醴陵市', 3, '0733', '113.496999,27.646096', 1809),
(1819, '430300', '湘潭市', 2, '0732', '112.944026,27.829795', 1798),
(1820, '430302', '雨湖区', 3, '0732', '112.907162,27.856325', 1819),
(1821, '430304', '岳塘区', 3, '0732', '112.969479,27.872028', 1819),
(1822, '430321', '湘潭县', 3, '0732', '112.950831,27.778958', 1819),
(1823, '430381', '湘乡市', 3, '0732', '112.550205,27.718549', 1819),
(1824, '430382', '韶山市', 3, '0732', '112.52667,27.915008', 1819),
(1825, '430400', '衡阳市', 2, '0734', '112.572018,26.893368', 1798),
(1826, '430405', '珠晖区', 3, '0734', '112.620209,26.894765', 1825),
(1827, '430406', '雁峰区', 3, '0734', '112.6154,26.840602', 1825),
(1828, '430407', '石鼓区', 3, '0734', '112.597992,26.943755', 1825),
(1829, '430408', '蒸湘区', 3, '0734', '112.567107,26.911854', 1825),
(1830, '430412', '南岳区', 3, '0734', '112.738604,27.232443', 1825),
(1831, '430421', '衡阳县', 3, '0734', '112.370546,26.969577', 1825),
(1832, '430422', '衡南县', 3, '0734', '112.677877,26.738247', 1825),
(1833, '430423', '衡山县', 3, '0734', '112.868268,27.23029', 1825),
(1834, '430424', '衡东县', 3, '0734', '112.953168,27.08117', 1825),
(1835, '430426', '祁东县', 3, '0734', '112.090356,26.799896', 1825),
(1836, '430481', '耒阳市', 3, '0734', '112.859759,26.422277', 1825),
(1837, '430482', '常宁市', 3, '0734', '112.399878,26.421956', 1825),
(1838, '430500', '邵阳市', 2, '0739', '111.467674,27.23895', 1798),
(1839, '430502', '双清区', 3, '0739', '111.496341,27.232708', 1838),
(1840, '430503', '大祥区', 3, '0739', '111.439091,27.221452', 1838),
(1841, '430511', '北塔区', 3, '0739', '111.452196,27.246489', 1838),
(1842, '430521', '邵东县', 3, '0739', '111.74427,27.258987', 1838),
(1843, '430522', '新邵县', 3, '0739', '111.458656,27.320917', 1838),
(1844, '430523', '邵阳县', 3, '0739', '111.273805,26.990637', 1838),
(1845, '430524', '隆回县', 3, '0739', '111.032437,27.113978', 1838),
(1846, '430525', '洞口县', 3, '0739', '110.575846,27.06032', 1838),
(1847, '430527', '绥宁县', 3, '0739', '110.155655,26.581954', 1838),
(1848, '430528', '新宁县', 3, '0739', '110.856988,26.433367', 1838),
(1849, '430529', '城步苗族自治县', 3, '0739', '110.322239,26.390598', 1838),
(1850, '430581', '武冈市', 3, '0739', '110.631884,26.726599', 1838),
(1851, '430600', '岳阳市', 2, '0730', '113.12873,29.356803', 1798),
(1852, '430602', '岳阳楼区', 3, '0730', '113.129684,29.371814', 1851),
(1853, '430603', '云溪区', 3, '0730', '113.272312,29.472745', 1851),
(1854, '430611', '君山区', 3, '0730', '113.006435,29.461106', 1851),
(1855, '430621', '岳阳县', 3, '0730', '113.116418,29.144066', 1851),
(1856, '430623', '华容县', 3, '0730', '112.540463,29.531057', 1851),
(1857, '430624', '湘阴县', 3, '0730', '112.909426,28.689104', 1851),
(1858, '430626', '平江县', 3, '0730', '113.581234,28.701868', 1851),
(1859, '430681', '汨罗市', 3, '0730', '113.067251,28.806881', 1851),
(1860, '430682', '临湘市', 3, '0730', '113.450423,29.476849', 1851),
(1861, '430700', '常德市', 2, '0736', '111.698784,29.031654', 1798),
(1862, '430702', '武陵区', 3, '0736', '111.683153,29.055163', 1861),
(1863, '430703', '鼎城区', 3, '0736', '111.680783,29.018593', 1861),
(1864, '430721', '安乡县', 3, '0736', '112.171131,29.411309', 1861),
(1865, '430722', '汉寿县', 3, '0736', '111.970514,28.906106', 1861),
(1866, '430723', '澧县', 3, '0736', '111.758702,29.633236', 1861),
(1867, '430724', '临澧县', 3, '0736', '111.647517,29.440793', 1861),
(1868, '430725', '桃源县', 3, '0736', '111.488925,28.902503', 1861),
(1869, '430726', '石门县', 3, '0736', '111.380014,29.584292', 1861),
(1870, '430781', '津市市', 3, '0736', '111.877499,29.60548', 1861),
(1871, '430800', '张家界市', 2, '0744', '110.479148,29.117013', 1798),
(1872, '430802', '永定区', 3, '0744', '110.537138,29.119855', 1871),
(1873, '430811', '武陵源区', 3, '0744', '110.550433,29.34573', 1871),
(1874, '430821', '慈利县', 3, '0744', '111.139775,29.429999', 1871),
(1875, '430822', '桑植县', 3, '0744', '110.204652,29.414111', 1871),
(1876, '430900', '益阳市', 2, '0737', '112.355129,28.554349', 1798),
(1877, '430902', '资阳区', 3, '0737', '112.324272,28.59111', 1876),
(1878, '430903', '赫山区', 3, '0737', '112.374145,28.579494', 1876),
(1879, '430921', '南县', 3, '0737', '112.396337,29.362275', 1876),
(1880, '430922', '桃江县', 3, '0737', '112.155822,28.518084', 1876),
(1881, '430923', '安化县', 3, '0737', '111.212846,28.374107', 1876),
(1882, '430981', '沅江市', 3, '0737', '112.355954,28.847045', 1876),
(1883, '431000', '郴州市', 2, '0735', '113.014984,25.770532', 1798),
(1884, '431002', '北湖区', 3, '0735', '113.011035,25.784054', 1883),
(1885, '431003', '苏仙区', 3, '0735', '113.112105,25.797013', 1883),
(1886, '431021', '桂阳县', 3, '0735', '112.734173,25.754172', 1883),
(1887, '431022', '宜章县', 3, '0735', '112.948712,25.399938', 1883),
(1888, '431023', '永兴县', 3, '0735', '113.116527,26.12715', 1883),
(1889, '431024', '嘉禾县', 3, '0735', '112.36902,25.587519', 1883),
(1890, '431025', '临武县', 3, '0735', '112.563456,25.27556', 1883),
(1891, '431026', '汝城县', 3, '0735', '113.684727,25.532816', 1883),
(1892, '431027', '桂东县', 3, '0735', '113.944614,26.077616', 1883),
(1893, '431028', '安仁县', 3, '0735', '113.26932,26.709061', 1883),
(1894, '431081', '资兴市', 3, '0735', '113.236146,25.976243', 1883),
(1895, '431100', '永州市', 2, '0746', '111.613418,26.419641', 1798),
(1896, '431102', '零陵区', 3, '0746', '111.631109,26.221936', 1895),
(1897, '431103', '冷水滩区', 3, '0746', '111.592343,26.46128', 1895),
(1898, '431121', '祁阳县', 3, '0746', '111.840657,26.58012', 1895),
(1899, '431122', '东安县', 3, '0746', '111.316464,26.392183', 1895),
(1900, '431123', '双牌县', 3, '0746', '111.659967,25.961909', 1895),
(1901, '431124', '道县', 3, '0746', '111.600795,25.526437', 1895),
(1902, '431125', '江永县', 3, '0746', '111.343911,25.273539', 1895),
(1903, '431126', '宁远县', 3, '0746', '111.945844,25.570888', 1895),
(1904, '431127', '蓝山县', 3, '0746', '112.196567,25.369725', 1895),
(1905, '431128', '新田县', 3, '0746', '112.203287,25.904305', 1895),
(1906, '431129', '江华瑶族自治县', 3, '0746', '111.579535,25.185809', 1895),
(1907, '431200', '怀化市', 2, '0745', '110.001923,27.569517', 1798),
(1908, '431202', '鹤城区', 3, '0745', '110.040315,27.578926', 1907),
(1909, '431221', '中方县', 3, '0745', '109.944711,27.440138', 1907),
(1910, '431222', '沅陵县', 3, '0745', '110.393844,28.452686', 1907),
(1911, '431223', '辰溪县', 3, '0745', '110.183917,28.006336', 1907),
(1912, '431224', '溆浦县', 3, '0745', '110.594879,27.908267', 1907),
(1913, '431225', '会同县', 3, '0745', '109.735661,26.887238', 1907),
(1914, '431226', '麻阳苗族自治县', 3, '0745', '109.81701,27.857569', 1907),
(1915, '431227', '新晃侗族自治县', 3, '0745', '109.174932,27.352673', 1907),
(1916, '431228', '芷江侗族自治县', 3, '0745', '109.684629,27.443499', 1907),
(1917, '431229', '靖州苗族侗族自治县', 3, '0745', '109.696273,26.575107', 1907),
(1918, '431230', '通道侗族自治县', 3, '0745', '109.784412,26.158054', 1907),
(1919, '431281', '洪江市', 3, '0745', '109.836669,27.208609', 1907),
(1920, '431300', '娄底市', 2, '0738', '111.994482,27.70027', 1798),
(1921, '431302', '娄星区', 3, '0738', '112.001914,27.729863', 1920),
(1922, '431321', '双峰县', 3, '0738', '112.175163,27.457172', 1920),
(1923, '431322', '新化县', 3, '0738', '111.327412,27.726514', 1920),
(1924, '431381', '冷水江市', 3, '0738', '111.434984,27.686251', 1920),
(1925, '431382', '涟源市', 3, '0738', '111.664329,27.692577', 1920),
(1926, '433100', '湘西土家族苗族自治州', 2, '0743', '109.738906,28.31195', 1798),
(1927, '433101', '吉首市', 3, '0743', '109.698015,28.262376', 1926),
(1928, '433122', '泸溪县', 3, '0743', '110.21961,28.216641', 1926),
(1929, '433123', '凤凰县', 3, '0743', '109.581083,27.958081', 1926),
(1930, '433124', '花垣县', 3, '0743', '109.482078,28.572029', 1926),
(1931, '433125', '保靖县', 3, '0743', '109.660559,28.699878', 1926),
(1932, '433126', '古丈县', 3, '0743', '109.950728,28.616935', 1926),
(1933, '433127', '永顺县', 3, '0743', '109.856933,28.979955', 1926),
(1934, '433130', '龙山县', 3, '0743', '109.443938,29.457663', 1926),
(1935, '440000', '广东省', 1, '[]', '113.26641,23.132324', -1),
(1936, '440100', '广州市', 2, '020', '113.264385,23.12911', 1935),
(1937, '440103', '荔湾区', 3, '020', '113.244258,23.125863', 1936),
(1938, '440104', '越秀区', 3, '020', '113.266835,23.128537', 1936),
(1939, '440105', '海珠区', 3, '020', '113.317443,23.083788', 1936),
(1940, '440106', '天河区', 3, '020', '113.361575,23.124807', 1936),
(1941, '440111', '白云区', 3, '020', '113.273238,23.157367', 1936),
(1942, '440112', '黄埔区', 3, '020', '113.480541,23.181706', 1936),
(1943, '440113', '番禺区', 3, '020', '113.384152,22.937556', 1936),
(1944, '440114', '花都区', 3, '020', '113.220463,23.403744', 1936),
(1945, '440115', '南沙区', 3, '020', '113.525165,22.801624', 1936),
(1946, '440117', '从化区', 3, '020', '113.586679,23.548748', 1936),
(1947, '440118', '增城区', 3, '020', '113.810627,23.261465', 1936),
(1948, '440200', '韶关市', 2, '0751', '113.59762,24.810879', 1935),
(1949, '440203', '武江区', 3, '0751', '113.587756,24.792926', 1948),
(1950, '440204', '浈江区', 3, '0751', '113.611098,24.804381', 1948),
(1951, '440205', '曲江区', 3, '0751', '113.604535,24.682501', 1948),
(1952, '440222', '始兴县', 3, '0751', '114.061789,24.952976', 1948),
(1953, '440224', '仁化县', 3, '0751', '113.749027,25.085621', 1948),
(1954, '440229', '翁源县', 3, '0751', '114.130342,24.350346', 1948),
(1955, '440232', '乳源瑶族自治县', 3, '0751', '113.275883,24.776078', 1948),
(1956, '440233', '新丰县', 3, '0751', '114.206867,24.05976', 1948),
(1957, '440281', '乐昌市', 3, '0751', '113.347545,25.130602', 1948),
(1958, '440282', '南雄市', 3, '0751', '114.311982,25.117753', 1948),
(1959, '440300', '深圳市', 2, '0755', '114.057939,22.543527', 1935),
(1960, '440303', '罗湖区', 3, '0755', '114.131459,22.548389', 1959),
(1961, '440304', '福田区', 3, '0755', '114.055072,22.521521', 1959),
(1962, '440305', '南山区', 3, '0755', '113.930413,22.533287', 1959),
(1963, '440306', '宝安区', 3, '0755', '113.883802,22.554996', 1959),
(1964, '440307', '龙岗区', 3, '0755', '114.246899,22.720974', 1959),
(1965, '440308', '盐田区', 3, '0755', '114.236739,22.557001', 1959),
(1966, '440309', '龙华区', 3, '0755', '114.045422,22.696667', 1959),
(1967, '440310', '坪山区', 3, '0755', '114.350584,22.708881', 1959),
(1968, '440400', '珠海市', 2, '0756', '113.576677,22.270978', 1935),
(1969, '440402', '香洲区', 3, '0756', '113.543784,22.265811', 1968),
(1970, '440403', '斗门区', 3, '0756', '113.296467,22.2092', 1968),
(1971, '440404', '金湾区', 3, '0756', '113.362656,22.147471', 1968),
(1972, '440500', '汕头市', 2, '0754', '116.681972,23.354091', 1935),
(1973, '440507', '龙湖区', 3, '0754', '116.716446,23.372254', 1972),
(1974, '440511', '金平区', 3, '0754', '116.70345,23.365556', 1972),
(1975, '440512', '濠江区', 3, '0754', '116.726973,23.286079', 1972),
(1976, '440513', '潮阳区', 3, '0754', '116.601509,23.265356', 1972),
(1977, '440514', '潮南区', 3, '0754', '116.439178,23.23865', 1972),
(1978, '440515', '澄海区', 3, '0754', '116.755992,23.466709', 1972),
(1979, '440523', '南澳县', 3, '0754', '117.023374,23.421724', 1972),
(1980, '440600', '佛山市', 2, '0757', '113.121435,23.021478', 1935),
(1981, '440604', '禅城区', 3, '0757', '113.122421,23.009551', 1980),
(1982, '440605', '南海区', 3, '0757', '113.143441,23.028956', 1980),
(1983, '440606', '顺德区', 3, '0757', '113.293359,22.80524', 1980),
(1984, '440607', '三水区', 3, '0757', '112.896685,23.155931', 1980),
(1985, '440608', '高明区', 3, '0757', '112.892585,22.900139', 1980),
(1986, '440700', '江门市', 2, '0750', '113.081542,22.57899', 1935),
(1987, '440703', '蓬江区', 3, '0750', '113.078521,22.595149', 1986),
(1988, '440704', '江海区', 3, '0750', '113.111612,22.560473', 1986),
(1989, '440705', '新会区', 3, '0750', '113.034187,22.4583', 1986),
(1990, '440781', '台山市', 3, '0750', '112.794065,22.251924', 1986),
(1991, '440783', '开平市', 3, '0750', '112.698545,22.376395', 1986),
(1992, '440784', '鹤山市', 3, '0750', '112.964252,22.76545', 1986),
(1993, '440785', '恩平市', 3, '0750', '112.305145,22.183206', 1986),
(1994, '440800', '湛江市', 2, '0759', '110.356639,21.270145', 1935),
(1995, '440802', '赤坎区', 3, '0759', '110.365899,21.266119', 1994),
(1996, '440803', '霞山区', 3, '0759', '110.397656,21.192457', 1994),
(1997, '440804', '坡头区', 3, '0759', '110.455332,21.244721', 1994),
(1998, '440811', '麻章区', 3, '0759', '110.334387,21.263442', 1994),
(1999, '440823', '遂溪县', 3, '0759', '110.250123,21.377246', 1994),
(2000, '440825', '徐闻县', 3, '0759', '110.176749,20.325489', 1994),
(2001, '440881', '廉江市', 3, '0759', '110.286208,21.6097', 1994),
(2002, '440882', '雷州市', 3, '0759', '110.096586,20.914178', 1994),
(2003, '440883', '吴川市', 3, '0759', '110.778411,21.441808', 1994),
(2004, '440900', '茂名市', 2, '0668', '110.925439,21.662991', 1935),
(2005, '440902', '茂南区', 3, '0668', '110.918026,21.641337', 2004),
(2006, '440904', '电白区', 3, '0668', '111.013556,21.514163', 2004),
(2007, '440981', '高州市', 3, '0668', '110.853299,21.918203', 2004),
(2008, '440982', '化州市', 3, '0668', '110.639565,21.66463', 2004),
(2009, '440983', '信宜市', 3, '0668', '110.947043,22.354385', 2004),
(2010, '441200', '肇庆市', 2, '0758', '112.465091,23.047191', 1935),
(2011, '441202', '端州区', 3, '0758', '112.484848,23.052101', 2010),
(2012, '441203', '鼎湖区', 3, '0758', '112.567588,23.158447', 2010),
(2013, '441204', '高要区', 3, '0758', '112.457981,23.025305', 2010),
(2014, '441223', '广宁县', 3, '0758', '112.44069,23.634675', 2010),
(2015, '441224', '怀集县', 3, '0758', '112.167742,23.92035', 2010),
(2016, '441225', '封开县', 3, '0758', '111.512343,23.424033', 2010),
(2017, '441226', '德庆县', 3, '0758', '111.785937,23.143722', 2010),
(2018, '441284', '四会市', 3, '0758', '112.734103,23.327001', 2010),
(2019, '441300', '惠州市', 2, '0752', '114.415612,23.112381', 1935),
(2020, '441302', '惠城区', 3, '0752', '114.382474,23.084137', 2019),
(2021, '441303', '惠阳区', 3, '0752', '114.456176,22.789788', 2019),
(2022, '441322', '博罗县', 3, '0752', '114.289528,23.172771', 2019),
(2023, '441323', '惠东县', 3, '0752', '114.719988,22.985014', 2019),
(2024, '441324', '龙门县', 3, '0752', '114.254863,23.727737', 2019),
(2025, '441400', '梅州市', 2, '0753', '116.122523,24.288578', 1935),
(2026, '441402', '梅江区', 3, '0753', '116.116695,24.31049', 2025),
(2027, '441403', '梅县区', 3, '0753', '116.081656,24.265926', 2025),
(2028, '441422', '大埔县', 3, '0753', '116.695195,24.347782', 2025),
(2029, '441423', '丰顺县', 3, '0753', '116.181691,23.739343', 2025),
(2030, '441424', '五华县', 3, '0753', '115.775788,23.932409', 2025),
(2031, '441426', '平远县', 3, '0753', '115.891638,24.567261', 2025),
(2032, '441427', '蕉岭县', 3, '0753', '116.171355,24.658699', 2025),
(2033, '441481', '兴宁市', 3, '0753', '115.731167,24.136708', 2025),
(2034, '441500', '汕尾市', 2, '0660', '115.375431,22.78705', 1935),
(2035, '441502', '城区', 3, '0660', '115.365058,22.779207', 2034),
(2036, '441521', '海丰县', 3, '0660', '115.323436,22.966585', 2034),
(2037, '441523', '陆河县', 3, '0660', '115.660143,23.301616', 2034),
(2038, '441581', '陆丰市', 3, '0660', '115.652151,22.919228', 2034),
(2039, '441600', '河源市', 2, '0762', '114.700961,23.743686', 1935),
(2040, '441602', '源城区', 3, '0762', '114.702517,23.733969', 2039),
(2041, '441621', '紫金县', 3, '0762', '115.184107,23.635745', 2039),
(2042, '441622', '龙川县', 3, '0762', '115.259871,24.100066', 2039),
(2043, '441623', '连平县', 3, '0762', '114.488556,24.369583', 2039),
(2044, '441624', '和平县', 3, '0762', '114.938684,24.44218', 2039),
(2045, '441625', '东源县', 3, '0762', '114.746344,23.788189', 2039),
(2046, '441700', '阳江市', 2, '0662', '111.982589,21.857887', 1935),
(2047, '441702', '江城区', 3, '0662', '111.955058,21.861786', 2046),
(2048, '441704', '阳东区', 3, '0662', '112.006363,21.868337', 2046),
(2049, '441721', '阳西县', 3, '0662', '111.61766,21.752771', 2046),
(2050, '441781', '阳春市', 3, '0662', '111.791587,22.17041', 2046),
(2051, '441800', '清远市', 2, '0763', '113.056042,23.681774', 1935),
(2052, '441802', '清城区', 3, '0763', '113.062692,23.697899', 2051),
(2053, '441803', '清新区', 3, '0763', '113.017747,23.734677', 2051),
(2054, '441821', '佛冈县', 3, '0763', '113.531607,23.879192', 2051),
(2055, '441823', '阳山县', 3, '0763', '112.641363,24.465359', 2051),
(2056, '441825', '连山壮族瑶族自治县', 3, '0763', '112.093617,24.570491', 2051),
(2057, '441826', '连南瑶族自治县', 3, '0763', '112.287012,24.726017', 2051),
(2058, '441881', '英德市', 3, '0763', '113.401701,24.206986', 2051),
(2059, '441882', '连州市', 3, '0763', '112.377361,24.780966', 2051),
(2060, '441900', '东莞市', 2, '0769', '113.751799,23.020673', 1935),
(2061, '442000', '中山市', 2, '0760', '113.39277,22.517585', 1935),
(2062, '445100', '潮州市', 2, '0768', '116.622444,23.657262', 1935),
(2063, '445102', '湘桥区', 3, '0768', '116.628627,23.674387', 2062),
(2064, '445103', '潮安区', 3, '0768', '116.678203,23.462613', 2062),
(2065, '445122', '饶平县', 3, '0768', '117.0039,23.663824', 2062),
(2066, '445200', '揭阳市', 2, '0663', '116.372708,23.549701', 1935),
(2067, '445202', '榕城区', 3, '0663', '116.367012,23.525382', 2066),
(2068, '445203', '揭东区', 3, '0663', '116.412015,23.566126', 2066),
(2069, '445222', '揭西县', 3, '0663', '115.841837,23.431294', 2066),
(2070, '445224', '惠来县', 3, '0663', '116.29515,23.033266', 2066),
(2071, '445281', '普宁市', 3, '0663', '116.165777,23.297493', 2066),
(2072, '445300', '云浮市', 2, '0766', '112.044491,22.915094', 1935),
(2073, '445302', '云城区', 3, '0766', '112.043945,22.92815', 2072),
(2074, '445303', '云安区', 3, '0766', '112.003208,23.071019', 2072),
(2075, '445321', '新兴县', 3, '0766', '112.225334,22.69569', 2072),
(2076, '445322', '郁南县', 3, '0766', '111.535285,23.23456', 2072),
(2077, '445381', '罗定市', 3, '0766', '111.569892,22.768285', 2072),
(2078, '442100', '东沙群岛', 2, '[]', '116.887613,20.617825', 1935),
(2079, '450000', '广西壮族自治区', 1, '[]', '108.327546,22.815478', -1),
(2080, '450100', '南宁市', 2, '0771', '108.366543,22.817002', 2079),
(2081, '450102', '兴宁区', 3, '0771', '108.368871,22.854021', 2080),
(2082, '450103', '青秀区', 3, '0771', '108.494024,22.785879', 2080),
(2083, '450105', '江南区', 3, '0771', '108.273133,22.78136', 2080),
(2084, '450107', '西乡塘区', 3, '0771', '108.313494,22.833928', 2080),
(2085, '450108', '良庆区', 3, '0771', '108.39301,22.752997', 2080),
(2086, '450109', '邕宁区', 3, '0771', '108.487368,22.75839', 2080),
(2087, '450110', '武鸣区', 3, '0771', '108.27467,23.158595', 2080),
(2088, '450123', '隆安县', 3, '0771', '107.696153,23.166028', 2080),
(2089, '450124', '马山县', 3, '0771', '108.177019,23.708321', 2080),
(2090, '450125', '上林县', 3, '0771', '108.602846,23.431908', 2080),
(2091, '450126', '宾阳县', 3, '0771', '108.810326,23.217786', 2080),
(2092, '450127', '横县', 3, '0771', '109.261384,22.679931', 2080),
(2093, '450200', '柳州市', 2, '0772', '109.428608,24.326291', 2079),
(2094, '450202', '城中区', 3, '0772', '109.4273,24.366', 2093),
(2095, '450203', '鱼峰区', 3, '0772', '109.452442,24.318516', 2093),
(2096, '450204', '柳南区', 3, '0772', '109.385518,24.336229', 2093),
(2097, '450205', '柳北区', 3, '0772', '109.402049,24.362691', 2093),
(2098, '450206', '柳江区', 3, '0772', '109.32638,24.254891', 2093),
(2099, '450222', '柳城县', 3, '0772', '109.24473,24.651518', 2093),
(2100, '450223', '鹿寨县', 3, '0772', '109.750638,24.472897', 2093),
(2101, '450224', '融安县', 3, '0772', '109.397538,25.224549', 2093),
(2102, '450225', '融水苗族自治县', 3, '0772', '109.256334,25.065934', 2093),
(2103, '450226', '三江侗族自治县', 3, '0772', '109.607675,25.783198', 2093),
(2104, '450300', '桂林市', 2, '0773', '110.179953,25.234479', 2079),
(2105, '450302', '秀峰区', 3, '0773', '110.264183,25.273625', 2104),
(2106, '450303', '叠彩区', 3, '0773', '110.301723,25.314', 2104),
(2107, '450304', '象山区', 3, '0773', '110.281082,25.261686', 2104),
(2108, '450305', '七星区', 3, '0773', '110.317826,25.252701', 2104),
(2109, '450311', '雁山区', 3, '0773', '110.28669,25.101934', 2104),
(2110, '450312', '临桂区', 3, '0773', '110.212463,25.238628', 2104),
(2111, '450321', '阳朔县', 3, '0773', '110.496593,24.77848', 2104),
(2112, '450323', '灵川县', 3, '0773', '110.319897,25.394781', 2104),
(2113, '450324', '全州县', 3, '0773', '111.072946,25.928387', 2104),
(2114, '450325', '兴安县', 3, '0773', '110.67167,25.611704', 2104),
(2115, '450326', '永福县', 3, '0773', '109.983076,24.979855', 2104),
(2116, '450327', '灌阳县', 3, '0773', '111.160851,25.489383', 2104),
(2117, '450328', '龙胜各族自治县', 3, '0773', '110.011238,25.797931', 2104),
(2118, '450329', '资源县', 3, '0773', '110.6527,26.042443', 2104),
(2119, '450330', '平乐县', 3, '0773', '110.643305,24.633362', 2104),
(2120, '450331', '荔浦县', 3, '0773', '110.395104,24.488342', 2104),
(2121, '450332', '恭城瑶族自治县', 3, '0773', '110.828409,24.831682', 2104),
(2122, '450400', '梧州市', 2, '0774', '111.279115,23.476962', 2079),
(2123, '450403', '万秀区', 3, '0774', '111.320518,23.472991', 2122),
(2124, '450405', '长洲区', 3, '0774', '111.274673,23.485944', 2122),
(2125, '450406', '龙圩区', 3, '0774', '111.246606,23.404772', 2122),
(2126, '450421', '苍梧县', 3, '0774', '111.544007,23.845097', 2122),
(2127, '450422', '藤县', 3, '0774', '110.914849,23.374983', 2122),
(2128, '450423', '蒙山县', 3, '0774', '110.525003,24.19357', 2122),
(2129, '450481', '岑溪市', 3, '0774', '110.994913,22.91835', 2122),
(2130, '450500', '北海市', 2, '0779', '109.120161,21.481291', 2079),
(2131, '450502', '海城区', 3, '0779', '109.117209,21.475004', 2130),
(2132, '450503', '银海区', 3, '0779', '109.139862,21.449308', 2130),
(2133, '450512', '铁山港区', 3, '0779', '109.42158,21.529127', 2130),
(2134, '450521', '合浦县', 3, '0779', '109.207335,21.660935', 2130),
(2135, '450600', '防城港市', 2, '0770', '108.353846,21.68686', 2079),
(2136, '450602', '港口区', 3, '0770', '108.380143,21.643383', 2135),
(2137, '450603', '防城区', 3, '0770', '108.353499,21.769211', 2135),
(2138, '450621', '上思县', 3, '0770', '107.983627,22.153671', 2135),
(2139, '450681', '东兴市', 3, '0770', '107.971828,21.547821', 2135),
(2140, '450700', '钦州市', 2, '0777', '108.654146,21.979933', 2079),
(2141, '450702', '钦南区', 3, '0777', '108.657209,21.938859', 2140),
(2142, '450703', '钦北区', 3, '0777', '108.44911,22.132761', 2140),
(2143, '450721', '灵山县', 3, '0777', '109.291006,22.416536', 2140),
(2144, '450722', '浦北县', 3, '0777', '109.556953,22.271651', 2140),
(2145, '450800', '贵港市', 2, '1755', '109.598926,23.11153', 2079),
(2146, '450802', '港北区', 3, '1755', '109.57224,23.11153', 2145),
(2147, '450803', '港南区', 3, '1755', '109.599556,23.075573', 2145),
(2148, '450804', '覃塘区', 3, '1755', '109.452662,23.127149', 2145),
(2149, '450821', '平南县', 3, '1755', '110.392311,23.539264', 2145),
(2150, '450881', '桂平市', 3, '1755', '110.079379,23.394325', 2145),
(2151, '450900', '玉林市', 2, '0775', '110.18122,22.654032', 2079),
(2152, '450902', '玉州区', 3, '0775', '110.151153,22.628087', 2151),
(2153, '450903', '福绵区', 3, '0775', '110.059439,22.585556', 2151),
(2154, '450921', '容县', 3, '0775', '110.558074,22.857839', 2151),
(2155, '450922', '陆川县', 3, '0775', '110.264052,22.321048', 2151),
(2156, '450923', '博白县', 3, '0775', '109.975985,22.273048', 2151),
(2157, '450924', '兴业县', 3, '0775', '109.875304,22.736421', 2151),
(2158, '450981', '北流市', 3, '0775', '110.354214,22.70831', 2151),
(2159, '451000', '百色市', 2, '0776', '106.618202,23.90233', 2079),
(2160, '451002', '右江区', 3, '0776', '106.618225,23.90097', 2159),
(2161, '451021', '田阳县', 3, '0776', '106.915496,23.735692', 2159),
(2162, '451022', '田东县', 3, '0776', '107.12608,23.597194', 2159),
(2163, '451023', '平果县', 3, '0776', '107.589809,23.329376', 2159),
(2164, '451024', '德保县', 3, '0776', '106.615373,23.32345', 2159),
(2165, '451026', '那坡县', 3, '0776', '105.83253,23.387441', 2159),
(2166, '451027', '凌云县', 3, '0776', '106.56131,24.347557', 2159),
(2167, '451028', '乐业县', 3, '0776', '106.556519,24.776827', 2159),
(2168, '451029', '田林县', 3, '0776', '106.228538,24.294487', 2159),
(2169, '451030', '西林县', 3, '0776', '105.093825,24.489823', 2159),
(2170, '451031', '隆林各族自治县', 3, '0776', '105.34404,24.770896', 2159),
(2171, '451081', '靖西市', 3, '0776', '106.417805,23.134117', 2159),
(2172, '451100', '贺州市', 2, '1774', '111.566871,24.403528', 2079),
(2173, '451102', '八步区', 3, '1774', '111.552095,24.411805', 2172),
(2174, '451103', '平桂区', 3, '1774', '111.479923,24.453845', 2172),
(2175, '451121', '昭平县', 3, '1774', '110.811325,24.169385', 2172),
(2176, '451122', '钟山县', 3, '1774', '111.303009,24.525957', 2172),
(2177, '451123', '富川瑶族自治县', 3, '1774', '111.27745,24.814443', 2172),
(2178, '451200', '河池市', 2, '0778', '108.085261,24.692931', 2079),
(2179, '451202', '金城江区', 3, '0778', '108.037276,24.689703', 2178),
(2180, '451221', '南丹县', 3, '0778', '107.541244,24.975631', 2178),
(2181, '451222', '天峨县', 3, '0778', '107.173802,24.999108', 2178),
(2182, '451223', '凤山县', 3, '0778', '107.04219,24.546876', 2178),
(2183, '451224', '东兰县', 3, '0778', '107.374293,24.510842', 2178),
(2184, '451225', '罗城仫佬族自治县', 3, '0778', '108.904706,24.777411', 2178),
(2185, '451226', '环江毛南族自治县', 3, '0778', '108.258028,24.825664', 2178),
(2186, '451227', '巴马瑶族自治县', 3, '0778', '107.258588,24.142298', 2178),
(2187, '451228', '都安瑶族自治县', 3, '0778', '108.105311,23.932675', 2178),
(2188, '451229', '大化瑶族自治县', 3, '0778', '107.998149,23.736457', 2178),
(2189, '451203', '宜州区', 3, '0778', '108.636414,24.485214', 2178),
(2190, '451300', '来宾市', 2, '1772', '109.221465,23.750306', 2079),
(2191, '451302', '兴宾区', 3, '1772', '109.183333,23.72892', 2190),
(2192, '451321', '忻城县', 3, '1772', '108.665666,24.066234', 2190),
(2193, '451322', '象州县', 3, '1772', '109.705065,23.973793', 2190),
(2194, '451323', '武宣县', 3, '1772', '109.663206,23.59411', 2190),
(2195, '451324', '金秀瑶族自治县', 3, '1772', '110.189462,24.130374', 2190),
(2196, '451381', '合山市', 3, '1772', '108.886082,23.806535', 2190),
(2197, '451400', '崇左市', 2, '1771', '107.365094,22.377253', 2079),
(2198, '451402', '江州区', 3, '1771', '107.353437,22.405325', 2197),
(2199, '451421', '扶绥县', 3, '1771', '107.904186,22.635012', 2197),
(2200, '451422', '宁明县', 3, '1771', '107.076456,22.140192', 2197),
(2201, '451423', '龙州县', 3, '1771', '106.854482,22.342778', 2197),
(2202, '451424', '大新县', 3, '1771', '107.200654,22.829287', 2197),
(2203, '451425', '天等县', 3, '1771', '107.143432,23.081394', 2197),
(2204, '451481', '凭祥市', 3, '1771', '106.766293,22.094484', 2197),
(2205, '460000', '海南省', 1, '[]', '110.349228,20.017377', -1),
(2206, '469025', '白沙黎族自治县', 2, '0802', '109.451484,19.224823', 2205),
(2207, '469029', '保亭黎族苗族自治县', 2, '0801', '109.70259,18.63913', 2205),
(2208, '469026', '昌江黎族自治县', 2, '0803', '109.055739,19.298184', 2205),
(2209, '469023', '澄迈县', 2, '0804', '110.006754,19.738521', 2205),
(2210, '460100', '海口市', 2, '0898', '110.198286,20.044412', 2205),
(2211, '460105', '秀英区', 3, '0898', '110.293603,20.007494', 2210),
(2212, '460106', '龙华区', 3, '0898', '110.328492,20.031006', 2210),
(2213, '460107', '琼山区', 3, '0898', '110.353972,20.003169', 2210),
(2214, '460108', '美兰区', 3, '0898', '110.366358,20.029083', 2210),
(2215, '460200', '三亚市', 2, '0899', '109.511772,18.253135', 2205),
(2216, '460202', '海棠区', 3, '0899', '109.752569,18.400106', 2215),
(2217, '460203', '吉阳区', 3, '0899', '109.578336,18.281406', 2215),
(2218, '460204', '天涯区', 3, '0899', '109.452378,18.298156', 2215),
(2219, '460205', '崖州区', 3, '0899', '109.171841,18.357291', 2215),
(2220, '460300', '三沙市', 2, '2898', '112.338695,16.831839', 2205),
(2221, '460321', '西沙群岛', 3, '1895', '111.792944,16.204546', 2220),
(2222, '460322', '南沙群岛', 3, '1891', '116.749997,11.471888', 2220),
(2223, '460323', '中沙群岛的岛礁及其海域', 3, '2801', '117.740071,15.112855', 2220),
(2224, '460400', '儋州市', 2, '0805', '109.580811,19.521134', 2205),
(2225, '469021', '定安县', 2, '0806', '110.359339,19.681404', 2205),
(2226, '469007', '东方市', 2, '0807', '108.651815,19.095351', 2205),
(2227, '469027', '乐东黎族自治县', 2, '2802', '109.173054,18.750259', 2205),
(2228, '469024', '临高县', 2, '1896', '109.690508,19.912025', 2205),
(2229, '469028', '陵水黎族自治县', 2, '0809', '110.037503,18.506048', 2205),
(2230, '469002', '琼海市', 2, '1894', '110.474497,19.259134', 2205),
(2231, '469030', '琼中黎族苗族自治县', 2, '1899', '109.838389,19.033369', 2205),
(2232, '469022', '屯昌县', 2, '1892', '110.103415,19.351765', 2205),
(2233, '469006', '万宁市', 2, '1898', '110.391073,18.795143', 2205),
(2234, '469005', '文昌市', 2, '1893', '110.797717,19.543422', 2205),
(2235, '469001', '五指山市', 2, '1897', '109.516925,18.775146', 2205),
(2236, '500000', '重庆市', 1, '023', '106.551643,29.562849', -1),
(2237, '500100', '重庆城区', 2, '023', '106.551643,29.562849', 2236),
(2238, '500101', '万州区', 3, '023', '108.408661,30.807667', 2237),
(2239, '500102', '涪陵区', 3, '023', '107.38977,29.703022', 2237),
(2240, '500103', '渝中区', 3, '023', '106.568896,29.552736', 2237),
(2241, '500104', '大渡口区', 3, '023', '106.482346,29.484527', 2237),
(2242, '500105', '江北区', 3, '023', '106.574271,29.606703', 2237),
(2243, '500106', '沙坪坝区', 3, '023', '106.456878,29.541144', 2237),
(2244, '500107', '九龙坡区', 3, '023', '106.510676,29.502272', 2237),
(2245, '500108', '南岸区', 3, '023', '106.644447,29.50126', 2237),
(2246, '500109', '北碚区', 3, '023', '106.395612,29.805107', 2237),
(2247, '500110', '綦江区', 3, '023', '106.651361,29.028066', 2237),
(2248, '500111', '大足区', 3, '023', '105.721733,29.707032', 2237),
(2249, '500112', '渝北区', 3, '023', '106.631187,29.718142', 2237),
(2250, '500113', '巴南区', 3, '023', '106.540256,29.402408', 2237),
(2251, '500114', '黔江区', 3, '023', '108.770677,29.533609', 2237),
(2252, '500115', '长寿区', 3, '023', '107.080734,29.857912', 2237),
(2253, '500116', '江津区', 3, '023', '106.259281,29.290069', 2237),
(2254, '500117', '合川区', 3, '023', '106.27613,29.972084', 2237),
(2255, '500118', '永川区', 3, '023', '105.927001,29.356311', 2237),
(2256, '500119', '南川区', 3, '023', '107.099266,29.15789', 2237),
(2257, '500120', '璧山区', 3, '023', '106.227305,29.592024', 2237),
(2258, '500151', '铜梁区', 3, '023', '106.056404,29.844811', 2237),
(2259, '500152', '潼南区', 3, '023', '105.840431,30.190992', 2237),
(2260, '500153', '荣昌区', 3, '023', '105.594623,29.405002', 2237),
(2261, '500154', '开州区', 3, '023', '108.393135,31.160711', 2237),
(2262, '500200', '重庆郊县', 2, '023', '108.165537,29.293902', 2236),
(2263, '500155', '梁平区', 3, '023', '107.769568,30.654233', 2262),
(2264, '500229', '城口县', 3, '023', '108.664214,31.947633', 2262),
(2265, '500230', '丰都县', 3, '023', '107.730894,29.8635', 2262),
(2266, '500231', '垫江县', 3, '023', '107.33339,30.327716', 2262),
(2267, '500156', '武隆区', 3, '023', '107.760025,29.325601', 2262),
(2268, '500233', '忠县', 3, '023', '108.039002,30.299559', 2262),
(2269, '500235', '云阳县', 3, '023', '108.697324,30.930612', 2262),
(2270, '500236', '奉节县', 3, '023', '109.400403,31.018363', 2262),
(2271, '500237', '巫山县', 3, '023', '109.879153,31.074834', 2262),
(2272, '500238', '巫溪县', 3, '023', '109.570062,31.398604', 2262),
(2273, '500240', '石柱土家族自治县', 3, '023', '108.114069,29.999285', 2262),
(2274, '500241', '秀山土家族苗族自治县', 3, '023', '109.007094,28.447997', 2262),
(2275, '500242', '酉阳土家族苗族自治县', 3, '023', '108.767747,28.841244', 2262),
(2276, '500243', '彭水苗族土家族自治县', 3, '023', '108.165537,29.293902', 2262),
(2277, '510000', '四川省', 1, '[]', '104.075809,30.651239', -1),
(2278, '510100', '成都市', 2, '028', '104.066794,30.572893', 2277),
(2279, '510104', '锦江区', 3, '028', '104.117022,30.598158', 2278),
(2280, '510105', '青羊区', 3, '028', '104.061442,30.673914', 2278),
(2281, '510106', '金牛区', 3, '028', '104.052236,30.691359', 2278),
(2282, '510107', '武侯区', 3, '028', '104.043235,30.641907', 2278),
(2283, '510108', '成华区', 3, '028', '104.101515,30.659966', 2278),
(2284, '510112', '龙泉驿区', 3, '028', '104.274632,30.556506', 2278),
(2285, '510113', '青白江区', 3, '028', '104.250945,30.878629', 2278),
(2286, '510114', '新都区', 3, '028', '104.158705,30.823498', 2278),
(2287, '510115', '温江区', 3, '028', '103.856646,30.682203', 2278),
(2288, '510116', '双流区', 3, '028', '103.923566,30.574449', 2278),
(2289, '510121', '金堂县', 3, '028', '104.411976,30.861979', 2278),
(2290, '510117', '郫都区', 3, '028', '103.901091,30.795854', 2278),
(2291, '510129', '大邑县', 3, '028', '103.511865,30.572268', 2278),
(2292, '510131', '蒲江县', 3, '028', '103.506498,30.196788', 2278),
(2293, '510132', '新津县', 3, '028', '103.811286,30.410346', 2278),
(2294, '510185', '简阳市', 3, '028', '104.54722,30.411264', 2278),
(2295, '510181', '都江堰市', 3, '028', '103.647153,30.988767', 2278),
(2296, '510182', '彭州市', 3, '028', '103.957983,30.990212', 2278),
(2297, '510183', '邛崃市', 3, '028', '103.464207,30.410324', 2278),
(2298, '510184', '崇州市', 3, '028', '103.673001,30.630122', 2278),
(2299, '510300', '自贡市', 2, '0813', '104.778442,29.33903', 2277),
(2300, '510302', '自流井区', 3, '0813', '104.777191,29.337429', 2299),
(2301, '510303', '贡井区', 3, '0813', '104.715288,29.345313', 2299),
(2302, '510304', '大安区', 3, '0813', '104.773994,29.363702', 2299),
(2303, '510311', '沿滩区', 3, '0813', '104.874079,29.272586', 2299),
(2304, '510321', '荣县', 3, '0813', '104.417493,29.445479', 2299),
(2305, '510322', '富顺县', 3, '0813', '104.975048,29.181429', 2299),
(2306, '510400', '攀枝花市', 2, '0812', '101.718637,26.582347', 2277),
(2307, '510402', '东区', 3, '0812', '101.704109,26.546491', 2306),
(2308, '510403', '西区', 3, '0812', '101.630619,26.597781', 2306),
(2309, '510411', '仁和区', 3, '0812', '101.738528,26.497765', 2306),
(2310, '510421', '米易县', 3, '0812', '102.112895,26.897694', 2306),
(2311, '510422', '盐边县', 3, '0812', '101.855071,26.683213', 2306),
(2312, '510500', '泸州市', 2, '0830', '105.442285,28.871805', 2277),
(2313, '510502', '江阳区', 3, '0830', '105.434982,28.87881', 2312),
(2314, '510503', '纳溪区', 3, '0830', '105.371505,28.773134', 2312),
(2315, '510504', '龙马潭区', 3, '0830', '105.437751,28.913257', 2312),
(2316, '510521', '泸县', 3, '0830', '105.381893,29.151534', 2312),
(2317, '510522', '合江县', 3, '0830', '105.830986,28.811164', 2312),
(2318, '510524', '叙永县', 3, '0830', '105.444765,28.155801', 2312),
(2319, '510525', '古蔺县', 3, '0830', '105.812601,28.038801', 2312),
(2320, '510600', '德阳市', 2, '0838', '104.397894,31.126855', 2277),
(2321, '510603', '旌阳区', 3, '0838', '104.416966,31.142633', 2320),
(2322, '510623', '中江县', 3, '0838', '104.678751,31.03307', 2320),
(2323, '510626', '罗江县', 3, '0838', '104.510249,31.317045', 2320),
(2324, '510681', '广汉市', 3, '0838', '104.282429,30.977119', 2320),
(2325, '510682', '什邡市', 3, '0838', '104.167501,31.12678', 2320),
(2326, '510683', '绵竹市', 3, '0838', '104.22075,31.338077', 2320),
(2327, '510700', '绵阳市', 2, '0816', '104.679004,31.467459', 2277),
(2328, '510703', '涪城区', 3, '0816', '104.756944,31.455101', 2327),
(2329, '510704', '游仙区', 3, '0816', '104.766392,31.473779', 2327),
(2330, '510705', '安州区', 3, '0816', '104.567187,31.534886', 2327),
(2331, '510722', '三台县', 3, '0816', '105.094586,31.095979', 2327),
(2332, '510723', '盐亭县', 3, '0816', '105.389453,31.208362', 2327),
(2333, '510725', '梓潼县', 3, '0816', '105.170845,31.642718', 2327),
(2334, '510726', '北川羌族自治县', 3, '0816', '104.46797,31.617203', 2327),
(2335, '510727', '平武县', 3, '0816', '104.555583,32.409675', 2327),
(2336, '510781', '江油市', 3, '0816', '104.745915,31.778026', 2327),
(2337, '510800', '广元市', 2, '0839', '105.843357,32.435435', 2277),
(2338, '510802', '利州区', 3, '0839', '105.845307,32.433756', 2337),
(2339, '510811', '昭化区', 3, '0839', '105.962819,32.323256', 2337),
(2340, '510812', '朝天区', 3, '0839', '105.882642,32.651336', 2337),
(2341, '510821', '旺苍县', 3, '0839', '106.289983,32.229058', 2337),
(2342, '510822', '青川县', 3, '0839', '105.238842,32.575484', 2337),
(2343, '510823', '剑阁县', 3, '0839', '105.524766,32.287722', 2337),
(2344, '510824', '苍溪县', 3, '0839', '105.934756,31.731709', 2337),
(2345, '510900', '遂宁市', 2, '0825', '105.592803,30.53292', 2277),
(2346, '510903', '船山区', 3, '0825', '105.568297,30.525475', 2345),
(2347, '510904', '安居区', 3, '0825', '105.456342,30.355379', 2345),
(2348, '510921', '蓬溪县', 3, '0825', '105.70757,30.757575', 2345),
(2349, '510922', '射洪县', 3, '0825', '105.388412,30.871131', 2345),
(2350, '510923', '大英县', 3, '0825', '105.236923,30.594409', 2345),
(2351, '511000', '内江市', 2, '1832', '105.058432,29.580228', 2277),
(2352, '511002', '市中区', 3, '1832', '105.067597,29.587053', 2351),
(2353, '511011', '东兴区', 3, '1832', '105.075489,29.592756', 2351),
(2354, '511024', '威远县', 3, '1832', '104.668879,29.52744', 2351),
(2355, '511025', '资中县', 3, '1832', '104.851944,29.764059', 2351),
(2356, '511028', '隆昌市', 3, '1832', '105.287612,29.339476', 2351),
(2357, '511100', '乐山市', 2, '0833', '103.765678,29.552115', 2277),
(2358, '511102', '市中区', 3, '0833', '103.761329,29.555374', 2357),
(2359, '511111', '沙湾区', 3, '0833', '103.549991,29.413091', 2357),
(2360, '511112', '五通桥区', 3, '0833', '103.818014,29.406945', 2357),
(2361, '511113', '金口河区', 3, '0833', '103.07862,29.244345', 2357),
(2362, '511123', '犍为县', 3, '0833', '103.949326,29.20817', 2357),
(2363, '511124', '井研县', 3, '0833', '104.069726,29.651287', 2357),
(2364, '511126', '夹江县', 3, '0833', '103.571656,29.73763', 2357),
(2365, '511129', '沐川县', 3, '0833', '103.902334,28.956647', 2357),
(2366, '511132', '峨边彝族自治县', 3, '0833', '103.262048,29.230425', 2357),
(2367, '511133', '马边彝族自治县', 3, '0833', '103.546347,28.83552', 2357),
(2368, '511181', '峨眉山市', 3, '0833', '103.484503,29.601198', 2357),
(2369, '511300', '南充市', 2, '0817', '106.110698,30.837793', 2277),
(2370, '511302', '顺庆区', 3, '0817', '106.09245,30.796803', 2369),
(2371, '511303', '高坪区', 3, '0817', '106.118808,30.781623', 2369),
(2372, '511304', '嘉陵区', 3, '0817', '106.071876,30.758823', 2369),
(2373, '511321', '南部县', 3, '0817', '106.036584,31.347467', 2369),
(2374, '511322', '营山县', 3, '0817', '106.565519,31.076579', 2369),
(2375, '511323', '蓬安县', 3, '0817', '106.412136,31.029091', 2369);
INSERT INTO `mf_area_items` (`areaId`, `areaCode`, `areaName`, `level`, `cityCode`, `center`, `parentId`) VALUES
(2376, '511324', '仪陇县', 3, '0817', '106.303042,31.271561', 2369),
(2377, '511325', '西充县', 3, '0817', '105.90087,30.995683', 2369),
(2378, '511381', '阆中市', 3, '0817', '106.005046,31.558356', 2369),
(2379, '511400', '眉山市', 2, '1833', '103.848403,30.076994', 2277),
(2380, '511402', '东坡区', 3, '1833', '103.831863,30.042308', 2379),
(2381, '511403', '彭山区', 3, '1833', '103.872949,30.193056', 2379),
(2382, '511421', '仁寿县', 3, '1833', '104.133995,29.995635', 2379),
(2383, '511423', '洪雅县', 3, '1833', '103.372863,29.90489', 2379),
(2384, '511424', '丹棱县', 3, '1833', '103.512783,30.01521', 2379),
(2385, '511425', '青神县', 3, '1833', '103.846688,29.831357', 2379),
(2386, '511500', '宜宾市', 2, '0831', '104.642845,28.752134', 2277),
(2387, '511502', '翠屏区', 3, '0831', '104.620009,28.765689', 2386),
(2388, '511503', '南溪区', 3, '0831', '104.969152,28.846382', 2386),
(2389, '511521', '宜宾县', 3, '0831', '104.533212,28.690045', 2386),
(2390, '511523', '江安县', 3, '0831', '105.066879,28.723855', 2386),
(2391, '511524', '长宁县', 3, '0831', '104.921174,28.582169', 2386),
(2392, '511525', '高县', 3, '0831', '104.517748,28.436166', 2386),
(2393, '511526', '珙县', 3, '0831', '104.709202,28.43863', 2386),
(2394, '511527', '筠连县', 3, '0831', '104.512025,28.167831', 2386),
(2395, '511528', '兴文县', 3, '0831', '105.236325,28.303614', 2386),
(2396, '511529', '屏山县', 3, '0831', '104.345974,28.828482', 2386),
(2397, '511600', '广安市', 2, '0826', '106.633088,30.456224', 2277),
(2398, '511602', '广安区', 3, '0826', '106.641662,30.473913', 2397),
(2399, '511603', '前锋区', 3, '0826', '106.886143,30.495804', 2397),
(2400, '511621', '岳池县', 3, '0826', '106.440114,30.537863', 2397),
(2401, '511622', '武胜县', 3, '0826', '106.295764,30.348772', 2397),
(2402, '511623', '邻水县', 3, '0826', '106.93038,30.334768', 2397),
(2403, '511681', '华蓥市', 3, '0826', '106.7831,30.390188', 2397),
(2404, '511700', '达州市', 2, '0818', '107.467758,31.209121', 2277),
(2405, '511702', '通川区', 3, '0818', '107.504928,31.214715', 2404),
(2406, '511703', '达川区', 3, '0818', '107.511749,31.196157', 2404),
(2407, '511722', '宣汉县', 3, '0818', '107.72719,31.353835', 2404),
(2408, '511723', '开江县', 3, '0818', '107.868736,31.082986', 2404),
(2409, '511724', '大竹县', 3, '0818', '107.204795,30.73641', 2404),
(2410, '511725', '渠县', 3, '0818', '106.97303,30.836618', 2404),
(2411, '511781', '万源市', 3, '0818', '108.034657,32.081631', 2404),
(2412, '511800', '雅安市', 2, '0835', '103.042375,30.010602', 2277),
(2413, '511802', '雨城区', 3, '0835', '103.033026,30.005461', 2412),
(2414, '511803', '名山区', 3, '0835', '103.109184,30.069954', 2412),
(2415, '511822', '荥经县', 3, '0835', '102.846737,29.792931', 2412),
(2416, '511823', '汉源县', 3, '0835', '102.645467,29.347192', 2412),
(2417, '511824', '石棉县', 3, '0835', '102.359462,29.227874', 2412),
(2418, '511825', '天全县', 3, '0835', '102.758317,30.066712', 2412),
(2419, '511826', '芦山县', 3, '0835', '102.932385,30.142307', 2412),
(2420, '511827', '宝兴县', 3, '0835', '102.815403,30.37641', 2412),
(2421, '511900', '巴中市', 2, '0827', '106.747477,31.867903', 2277),
(2422, '511902', '巴州区', 3, '0827', '106.768878,31.851478', 2421),
(2423, '511903', '恩阳区', 3, '0827', '106.654386,31.787186', 2421),
(2424, '511921', '通江县', 3, '0827', '107.245033,31.911705', 2421),
(2425, '511922', '南江县', 3, '0827', '106.828697,32.346589', 2421),
(2426, '511923', '平昌县', 3, '0827', '107.104008,31.560874', 2421),
(2427, '512000', '资阳市', 2, '0832', '104.627636,30.128901', 2277),
(2428, '512002', '雁江区', 3, '0832', '104.677091,30.108216', 2427),
(2429, '512021', '安岳县', 3, '0832', '105.35534,30.103107', 2427),
(2430, '512022', '乐至县', 3, '0832', '105.02019,30.276121', 2427),
(2431, '513200', '阿坝藏族羌族自治州', 2, '0837', '102.224653,31.899413', 2277),
(2432, '513201', '马尔康市', 3, '0837', '102.20652,31.905693', 2431),
(2433, '513221', '汶川县', 3, '0837', '103.590179,31.476854', 2431),
(2434, '513222', '理县', 3, '0837', '103.164661,31.435174', 2431),
(2435, '513223', '茂县', 3, '0837', '103.853363,31.681547', 2431),
(2436, '513224', '松潘县', 3, '0837', '103.604698,32.655325', 2431),
(2437, '513225', '九寨沟县', 3, '0837', '104.243841,33.252056', 2431),
(2438, '513226', '金川县', 3, '0837', '102.063829,31.476277', 2431),
(2439, '513227', '小金县', 3, '0837', '102.362984,30.995823', 2431),
(2440, '513228', '黑水县', 3, '0837', '102.990108,32.061895', 2431),
(2441, '513230', '壤塘县', 3, '0837', '100.978526,32.265796', 2431),
(2442, '513231', '阿坝县', 3, '0837', '101.706655,32.902459', 2431),
(2443, '513232', '若尔盖县', 3, '0837', '102.967826,33.578159', 2431),
(2444, '513233', '红原县', 3, '0837', '102.544405,32.790891', 2431),
(2445, '513300', '甘孜藏族自治州', 2, '0836', '101.96231,30.04952', 2277),
(2446, '513301', '康定市', 3, '0836', '101.957146,29.998435', 2445),
(2447, '513322', '泸定县', 3, '0836', '102.234617,29.91416', 2445),
(2448, '513323', '丹巴县', 3, '0836', '101.890358,30.878577', 2445),
(2449, '513324', '九龙县', 3, '0836', '101.507294,29.000347', 2445),
(2450, '513325', '雅江县', 3, '0836', '101.014425,30.031533', 2445),
(2451, '513326', '道孚县', 3, '0836', '101.125237,30.979545', 2445),
(2452, '513327', '炉霍县', 3, '0836', '100.676372,31.39179', 2445),
(2453, '513328', '甘孜县', 3, '0836', '99.99267,31.622933', 2445),
(2454, '513329', '新龙县', 3, '0836', '100.311368,30.939169', 2445),
(2455, '513330', '德格县', 3, '0836', '98.580914,31.806118', 2445),
(2456, '513331', '白玉县', 3, '0836', '98.824182,31.209913', 2445),
(2457, '513332', '石渠县', 3, '0836', '98.102914,32.97896', 2445),
(2458, '513333', '色达县', 3, '0836', '100.332743,32.268129', 2445),
(2459, '513334', '理塘县', 3, '0836', '100.269817,29.996049', 2445),
(2460, '513335', '巴塘县', 3, '0836', '99.110712,30.004677', 2445),
(2461, '513336', '乡城县', 3, '0836', '99.798435,28.931172', 2445),
(2462, '513337', '稻城县', 3, '0836', '100.298403,29.037007', 2445),
(2463, '513338', '得荣县', 3, '0836', '99.286335,28.713036', 2445),
(2464, '513400', '凉山彝族自治州', 2, '0834', '102.267712,27.88157', 2277),
(2465, '513401', '西昌市', 3, '0834', '102.264449,27.894504', 2464),
(2466, '513422', '木里藏族自治县', 3, '0834', '101.280205,27.928835', 2464),
(2467, '513423', '盐源县', 3, '0834', '101.509188,27.422645', 2464),
(2468, '513424', '德昌县', 3, '0834', '102.17567,27.402839', 2464),
(2469, '513425', '会理县', 3, '0834', '102.244683,26.655026', 2464),
(2470, '513426', '会东县', 3, '0834', '102.57796,26.634669', 2464),
(2471, '513427', '宁南县', 3, '0834', '102.751745,27.061189', 2464),
(2472, '513428', '普格县', 3, '0834', '102.540901,27.376413', 2464),
(2473, '513429', '布拖县', 3, '0834', '102.812061,27.706061', 2464),
(2474, '513430', '金阳县', 3, '0834', '103.248772,27.69686', 2464),
(2475, '513431', '昭觉县', 3, '0834', '102.840264,28.015333', 2464),
(2476, '513432', '喜德县', 3, '0834', '102.412518,28.306726', 2464),
(2477, '513433', '冕宁县', 3, '0834', '102.17701,28.549656', 2464),
(2478, '513434', '越西县', 3, '0834', '102.50768,28.639801', 2464),
(2479, '513435', '甘洛县', 3, '0834', '102.771504,28.959157', 2464),
(2480, '513436', '美姑县', 3, '0834', '103.132179,28.32864', 2464),
(2481, '513437', '雷波县', 3, '0834', '103.571696,28.262682', 2464),
(2482, '520000', '贵州省', 1, '[]', '106.70546,26.600055', -1),
(2483, '520100', '贵阳市', 2, '0851', '106.630153,26.647661', 2482),
(2484, '520102', '南明区', 3, '0851', '106.714374,26.567944', 2483),
(2485, '520103', '云岩区', 3, '0851', '106.724494,26.604688', 2483),
(2486, '520111', '花溪区', 3, '0851', '106.67026,26.409817', 2483),
(2487, '520112', '乌当区', 3, '0851', '106.750625,26.630845', 2483),
(2488, '520113', '白云区', 3, '0851', '106.623007,26.678561', 2483),
(2489, '520115', '观山湖区', 3, '0851', '106.622453,26.60145', 2483),
(2490, '520121', '开阳县', 3, '0851', '106.965089,27.057764', 2483),
(2491, '520122', '息烽县', 3, '0851', '106.740407,27.090479', 2483),
(2492, '520123', '修文县', 3, '0851', '106.592108,26.838926', 2483),
(2493, '520181', '清镇市', 3, '0851', '106.470714,26.556079', 2483),
(2494, '520200', '六盘水市', 2, '0858', '104.830458,26.592707', 2482),
(2495, '520201', '钟山区', 3, '0858', '104.843555,26.574979', 2494),
(2496, '520203', '六枝特区', 3, '0858', '105.476608,26.213108', 2494),
(2497, '520221', '水城县', 3, '0858', '104.95783,26.547904', 2494),
(2498, '520222', '盘州市', 3, '0858', '104.471375,25.709852', 2494),
(2499, '520300', '遵义市', 2, '0852', '106.927389,27.725654', 2482),
(2500, '520302', '红花岗区', 3, '0852', '106.8937,27.644754', 2499),
(2501, '520303', '汇川区', 3, '0852', '106.93427,27.750125', 2499),
(2502, '520304', '播州区', 3, '0852', '106.829574,27.536298', 2499),
(2503, '520322', '桐梓县', 3, '0852', '106.825198,28.133311', 2499),
(2504, '520323', '绥阳县', 3, '0852', '107.191222,27.946222', 2499),
(2505, '520324', '正安县', 3, '0852', '107.453945,28.553285', 2499),
(2506, '520325', '道真仡佬族苗族自治县', 3, '0852', '107.613133,28.862425', 2499),
(2507, '520326', '务川仡佬族苗族自治县', 3, '0852', '107.898956,28.563086', 2499),
(2508, '520327', '凤冈县', 3, '0852', '107.716355,27.954695', 2499),
(2509, '520328', '湄潭县', 3, '0852', '107.465407,27.749055', 2499),
(2510, '520329', '余庆县', 3, '0852', '107.905197,27.215491', 2499),
(2511, '520330', '习水县', 3, '0852', '106.197137,28.33127', 2499),
(2512, '520381', '赤水市', 3, '0852', '105.697472,28.590337', 2499),
(2513, '520382', '仁怀市', 3, '0852', '106.40109,27.792514', 2499),
(2514, '520400', '安顺市', 2, '0853', '105.947594,26.253088', 2482),
(2515, '520402', '西秀区', 3, '0853', '105.965116,26.245315', 2514),
(2516, '520403', '平坝区', 3, '0853', '106.256412,26.405715', 2514),
(2517, '520422', '普定县', 3, '0853', '105.743277,26.301565', 2514),
(2518, '520423', '镇宁布依族苗族自治县', 3, '0853', '105.770283,26.058086', 2514),
(2519, '520424', '关岭布依族苗族自治县', 3, '0853', '105.61933,25.94361', 2514),
(2520, '520425', '紫云苗族布依族自治县', 3, '0853', '106.084441,25.751047', 2514),
(2521, '520500', '毕节市', 2, '0857', '105.291702,27.283908', 2482),
(2522, '520502', '七星关区', 3, '0857', '105.30474,27.298458', 2521),
(2523, '520521', '大方县', 3, '0857', '105.613037,27.141735', 2521),
(2524, '520522', '黔西县', 3, '0857', '106.033544,27.007713', 2521),
(2525, '520523', '金沙县', 3, '0857', '106.220227,27.459214', 2521),
(2526, '520524', '织金县', 3, '0857', '105.770542,26.663449', 2521),
(2527, '520525', '纳雍县', 3, '0857', '105.382714,26.777645', 2521),
(2528, '520526', '威宁彝族回族苗族自治县', 3, '0857', '104.253071,26.873806', 2521),
(2529, '520527', '赫章县', 3, '0857', '104.727418,27.123078', 2521),
(2530, '520600', '铜仁市', 2, '0856', '109.189598,27.731514', 2482),
(2531, '520602', '碧江区', 3, '0856', '109.263998,27.815927', 2530),
(2532, '520603', '万山区', 3, '0856', '109.213644,27.517896', 2530),
(2533, '520621', '江口县', 3, '0856', '108.839557,27.69965', 2530),
(2534, '520622', '玉屏侗族自治县', 3, '0856', '108.906411,27.235813', 2530),
(2535, '520623', '石阡县', 3, '0856', '108.223612,27.513829', 2530),
(2536, '520624', '思南县', 3, '0856', '108.253882,27.93755', 2530),
(2537, '520625', '印江土家族苗族自治县', 3, '0856', '108.409751,27.994246', 2530),
(2538, '520626', '德江县', 3, '0856', '108.119807,28.263963', 2530),
(2539, '520627', '沿河土家族自治县', 3, '0856', '108.50387,28.563927', 2530),
(2540, '520628', '松桃苗族自治县', 3, '0856', '109.202886,28.154071', 2530),
(2541, '522300', '黔西南布依族苗族自治州', 2, '0859', '104.906397,25.087856', 2482),
(2542, '522301', '兴义市', 3, '0859', '104.895467,25.09204', 2541),
(2543, '522322', '兴仁县', 3, '0859', '105.186237,25.435183', 2541),
(2544, '522323', '普安县', 3, '0859', '104.953062,25.784135', 2541),
(2545, '522324', '晴隆县', 3, '0859', '105.218991,25.834783', 2541),
(2546, '522325', '贞丰县', 3, '0859', '105.649864,25.38576', 2541),
(2547, '522326', '望谟县', 3, '0859', '106.099617,25.178421', 2541),
(2548, '522327', '册亨县', 3, '0859', '105.811592,24.983663', 2541),
(2549, '522328', '安龙县', 3, '0859', '105.442701,25.099014', 2541),
(2550, '522600', '黔东南苗族侗族自治州', 2, '0855', '107.982874,26.583457', 2482),
(2551, '522601', '凯里市', 3, '0855', '107.97754,26.582963', 2550),
(2552, '522622', '黄平县', 3, '0855', '107.916411,26.905396', 2550),
(2553, '522623', '施秉县', 3, '0855', '108.124379,27.03292', 2550),
(2554, '522624', '三穗县', 3, '0855', '108.675267,26.952967', 2550),
(2555, '522625', '镇远县', 3, '0855', '108.429534,27.049497', 2550),
(2556, '522626', '岑巩县', 3, '0855', '108.81606,27.173887', 2550),
(2557, '522627', '天柱县', 3, '0855', '109.207751,26.909639', 2550),
(2558, '522628', '锦屏县', 3, '0855', '109.200534,26.676233', 2550),
(2559, '522629', '剑河县', 3, '0855', '108.441501,26.728274', 2550),
(2560, '522630', '台江县', 3, '0855', '108.321245,26.667525', 2550),
(2561, '522631', '黎平县', 3, '0855', '109.136932,26.230706', 2550),
(2562, '522632', '榕江县', 3, '0855', '108.52188,25.931893', 2550),
(2563, '522633', '从江县', 3, '0855', '108.905329,25.753009', 2550),
(2564, '522634', '雷山县', 3, '0855', '108.07754,26.378442', 2550),
(2565, '522635', '麻江县', 3, '0855', '107.589359,26.491105', 2550),
(2566, '522636', '丹寨县', 3, '0855', '107.788727,26.19832', 2550),
(2567, '522700', '黔南布依族苗族自治州', 2, '0854', '107.522171,26.253275', 2482),
(2568, '522701', '都匀市', 3, '0854', '107.518847,26.259427', 2567),
(2569, '522702', '福泉市', 3, '0854', '107.520386,26.686335', 2567),
(2570, '522722', '荔波县', 3, '0854', '107.898882,25.423895', 2567),
(2571, '522723', '贵定县', 3, '0854', '107.232793,26.557089', 2567),
(2572, '522725', '瓮安县', 3, '0854', '107.470942,27.078441', 2567),
(2573, '522726', '独山县', 3, '0854', '107.545048,25.822132', 2567),
(2574, '522727', '平塘县', 3, '0854', '107.322323,25.822349', 2567),
(2575, '522728', '罗甸县', 3, '0854', '106.751589,25.426173', 2567),
(2576, '522729', '长顺县', 3, '0854', '106.441805,26.025626', 2567),
(2577, '522730', '龙里县', 3, '0854', '106.979524,26.453154', 2567),
(2578, '522731', '惠水县', 3, '0854', '106.656442,26.13278', 2567),
(2579, '522732', '三都水族自治县', 3, '0854', '107.869749,25.983202', 2567),
(2580, '530000', '云南省', 1, '[]', '102.710002,25.045806', -1),
(2581, '530100', '昆明市', 2, '0871', '102.832891,24.880095', 2580),
(2582, '530102', '五华区', 3, '0871', '102.707262,25.043635', 2581),
(2583, '530103', '盘龙区', 3, '0871', '102.751941,25.116465', 2581),
(2584, '530111', '官渡区', 3, '0871', '102.749026,24.950231', 2581),
(2585, '530112', '西山区', 3, '0871', '102.664382,25.038604', 2581),
(2586, '530113', '东川区', 3, '0871', '103.187824,26.082873', 2581),
(2587, '530114', '呈贡区', 3, '0871', '102.821675,24.885587', 2581),
(2588, '530115', '晋宁区', 3, '0871', '102.595412,24.66974', 2581),
(2589, '530124', '富民县', 3, '0871', '102.4976,25.221935', 2581),
(2590, '530125', '宜良县', 3, '0871', '103.141603,24.919839', 2581),
(2591, '530126', '石林彝族自治县', 3, '0871', '103.290536,24.771761', 2581),
(2592, '530127', '嵩明县', 3, '0871', '103.036908,25.338643', 2581),
(2593, '530128', '禄劝彝族苗族自治县', 3, '0871', '102.471518,25.551332', 2581),
(2594, '530129', '寻甸回族彝族自治县', 3, '0871', '103.256615,25.558201', 2581),
(2595, '530181', '安宁市', 3, '0871', '102.478494,24.919493', 2581),
(2596, '530300', '曲靖市', 2, '0874', '103.796167,25.489999', 2580),
(2597, '530302', '麒麟区', 3, '0874', '103.80474,25.495326', 2596),
(2598, '530303', '沾益区', 3, '0874', '103.822324,25.600507', 2596),
(2599, '530321', '马龙县', 3, '0874', '103.578478,25.42805', 2596),
(2600, '530322', '陆良县', 3, '0874', '103.666663,25.030051', 2596),
(2601, '530323', '师宗县', 3, '0874', '103.985321,24.822233', 2596),
(2602, '530324', '罗平县', 3, '0874', '104.308675,24.884626', 2596),
(2603, '530325', '富源县', 3, '0874', '104.255014,25.674238', 2596),
(2604, '530326', '会泽县', 3, '0874', '103.297386,26.417345', 2596),
(2605, '530381', '宣威市', 3, '0874', '104.10455,26.219735', 2596),
(2606, '530400', '玉溪市', 2, '0877', '102.527197,24.347324', 2580),
(2607, '530402', '红塔区', 3, '0877', '102.540122,24.341215', 2606),
(2608, '530403', '江川区', 3, '0877', '102.75344,24.287485', 2606),
(2609, '530422', '澄江县', 3, '0877', '102.904629,24.675689', 2606),
(2610, '530423', '通海县', 3, '0877', '102.725452,24.111048', 2606),
(2611, '530424', '华宁县', 3, '0877', '102.928835,24.19276', 2606),
(2612, '530425', '易门县', 3, '0877', '102.162531,24.671651', 2606),
(2613, '530426', '峨山彝族自治县', 3, '0877', '102.405819,24.168957', 2606),
(2614, '530427', '新平彝族傣族自治县', 3, '0877', '101.990157,24.07005', 2606),
(2615, '530428', '元江哈尼族彝族傣族自治县', 3, '0877', '101.998103,23.596503', 2606),
(2616, '530500', '保山市', 2, '0875', '99.161761,25.112046', 2580),
(2617, '530502', '隆阳区', 3, '0875', '99.165607,25.121154', 2616),
(2618, '530521', '施甸县', 3, '0875', '99.189221,24.723064', 2616),
(2619, '530523', '龙陵县', 3, '0875', '98.689261,24.586794', 2616),
(2620, '530524', '昌宁县', 3, '0875', '99.605142,24.827839', 2616),
(2621, '530581', '腾冲市', 3, '0875', '98.490966,25.020439', 2616),
(2622, '530600', '昭通市', 2, '0870', '103.717465,27.338257', 2580),
(2623, '530602', '昭阳区', 3, '0870', '103.706539,27.320075', 2622),
(2624, '530621', '鲁甸县', 3, '0870', '103.558042,27.186659', 2622),
(2625, '530622', '巧家县', 3, '0870', '102.930164,26.90846', 2622),
(2626, '530623', '盐津县', 3, '0870', '104.234441,28.10871', 2622),
(2627, '530624', '大关县', 3, '0870', '103.891146,27.747978', 2622),
(2628, '530625', '永善县', 3, '0870', '103.638067,28.229112', 2622),
(2629, '530626', '绥江县', 3, '0870', '103.968978,28.592099', 2622),
(2630, '530627', '镇雄县', 3, '0870', '104.87376,27.441622', 2622),
(2631, '530628', '彝良县', 3, '0870', '104.048289,27.625418', 2622),
(2632, '530629', '威信县', 3, '0870', '105.049027,27.8469', 2622),
(2633, '530630', '水富县', 3, '0870', '104.41603,28.62988', 2622),
(2634, '530700', '丽江市', 2, '0888', '100.22775,26.855047', 2580),
(2635, '530702', '古城区', 3, '0888', '100.225784,26.876927', 2634),
(2636, '530721', '玉龙纳西族自治县', 3, '0888', '100.236954,26.821459', 2634),
(2637, '530722', '永胜县', 3, '0888', '100.750826,26.684225', 2634),
(2638, '530723', '华坪县', 3, '0888', '101.266195,26.629211', 2634),
(2639, '530724', '宁蒗彝族自治县', 3, '0888', '100.852001,27.28207', 2634),
(2640, '530800', '普洱市', 2, '0879', '100.966156,22.825155', 2580),
(2641, '530802', '思茅区', 3, '0879', '100.977256,22.787115', 2640),
(2642, '530821', '宁洱哈尼族彝族自治县', 3, '0879', '101.045837,23.048401', 2640),
(2643, '530822', '墨江哈尼族自治县', 3, '0879', '101.692461,23.431894', 2640),
(2644, '530823', '景东彝族自治县', 3, '0879', '100.833877,24.446731', 2640),
(2645, '530824', '景谷傣族彝族自治县', 3, '0879', '100.702871,23.497028', 2640),
(2646, '530825', '镇沅彝族哈尼族拉祜族自治县', 3, '0879', '101.108595,24.004441', 2640),
(2647, '530826', '江城哈尼族彝族自治县', 3, '0879', '101.86212,22.585867', 2640),
(2648, '530827', '孟连傣族拉祜族佤族自治县', 3, '0879', '99.584157,22.329099', 2640),
(2649, '530828', '澜沧拉祜族自治县', 3, '0879', '99.931975,22.555904', 2640),
(2650, '530829', '西盟佤族自治县', 3, '0879', '99.590123,22.644508', 2640),
(2651, '530900', '临沧市', 2, '0883', '100.08879,23.883955', 2580),
(2652, '530902', '临翔区', 3, '0883', '100.082523,23.895137', 2651),
(2653, '530921', '凤庆县', 3, '0883', '99.928459,24.580424', 2651),
(2654, '530922', '云县', 3, '0883', '100.129354,24.44422', 2651),
(2655, '530923', '永德县', 3, '0883', '99.259339,24.018357', 2651),
(2656, '530924', '镇康县', 3, '0883', '98.825284,23.762584', 2651),
(2657, '530925', '双江拉祜族佤族布朗族傣族自治县', 3, '0883', '99.827697,23.473499', 2651),
(2658, '530926', '耿马傣族佤族自治县', 3, '0883', '99.397126,23.538092', 2651),
(2659, '530927', '沧源佤族自治县', 3, '0883', '99.246196,23.146712', 2651),
(2660, '532300', '楚雄彝族自治州', 2, '0878', '101.527992,25.045513', 2580),
(2661, '532301', '楚雄市', 3, '0878', '101.545906,25.032889', 2660),
(2662, '532322', '双柏县', 3, '0878', '101.641937,24.688875', 2660),
(2663, '532323', '牟定县', 3, '0878', '101.546566,25.313121', 2660),
(2664, '532324', '南华县', 3, '0878', '101.273577,25.192293', 2660),
(2665, '532325', '姚安县', 3, '0878', '101.241728,25.504173', 2660),
(2666, '532326', '大姚县', 3, '0878', '101.336617,25.729513', 2660),
(2667, '532327', '永仁县', 3, '0878', '101.666132,26.049464', 2660),
(2668, '532328', '元谋县', 3, '0878', '101.87452,25.704338', 2660),
(2669, '532329', '武定县', 3, '0878', '102.404337,25.530389', 2660),
(2670, '532331', '禄丰县', 3, '0878', '102.079027,25.150111', 2660),
(2671, '532500', '红河哈尼族彝族自治州', 2, '0873', '103.374893,23.363245', 2580),
(2672, '532501', '个旧市', 3, '0873', '103.160034,23.359121', 2671),
(2673, '532502', '开远市', 3, '0873', '103.266624,23.714523', 2671),
(2674, '532503', '蒙自市', 3, '0873', '103.364905,23.396201', 2671),
(2675, '532504', '弥勒市', 3, '0873', '103.414874,24.411912', 2671),
(2676, '532523', '屏边苗族自治县', 3, '0873', '103.687612,22.983559', 2671),
(2677, '532524', '建水县', 3, '0873', '102.826557,23.6347', 2671),
(2678, '532525', '石屏县', 3, '0873', '102.494983,23.705936', 2671),
(2679, '532527', '泸西县', 3, '0873', '103.766196,24.532025', 2671),
(2680, '532528', '元阳县', 3, '0873', '102.835223,23.219932', 2671),
(2681, '532529', '红河县', 3, '0873', '102.4206,23.369161', 2671),
(2682, '532530', '金平苗族瑶族傣族自治县', 3, '0873', '103.226448,22.779543', 2671),
(2683, '532531', '绿春县', 3, '0873', '102.392463,22.993717', 2671),
(2684, '532532', '河口瑶族自治县', 3, '0873', '103.93952,22.529645', 2671),
(2685, '532600', '文山壮族苗族自治州', 2, '0876', '104.216248,23.400733', 2580),
(2686, '532601', '文山市', 3, '0876', '104.232665,23.386527', 2685),
(2687, '532622', '砚山县', 3, '0876', '104.337211,23.605768', 2685),
(2688, '532623', '西畴县', 3, '0876', '104.672597,23.437782', 2685),
(2689, '532624', '麻栗坡县', 3, '0876', '104.702799,23.125714', 2685),
(2690, '532625', '马关县', 3, '0876', '104.394157,23.012915', 2685),
(2691, '532626', '丘北县', 3, '0876', '104.166587,24.051746', 2685),
(2692, '532627', '广南县', 3, '0876', '105.055107,24.046386', 2685),
(2693, '532628', '富宁县', 3, '0876', '105.630999,23.625283', 2685),
(2694, '532800', '西双版纳傣族自治州', 2, '0691', '100.796984,22.009113', 2580),
(2695, '532801', '景洪市', 3, '0691', '100.799545,22.011928', 2694),
(2696, '532822', '勐海县', 3, '0691', '100.452547,21.957353', 2694),
(2697, '532823', '勐腊县', 3, '0691', '101.564635,21.459233', 2694),
(2698, '532900', '大理白族自治州', 2, '0872', '100.267638,25.606486', 2580),
(2699, '532901', '大理市', 3, '0872', '100.30127,25.678068', 2698),
(2700, '532922', '漾濞彝族自治县', 3, '0872', '99.958015,25.670148', 2698),
(2701, '532923', '祥云县', 3, '0872', '100.550945,25.48385', 2698),
(2702, '532924', '宾川县', 3, '0872', '100.590473,25.829828', 2698),
(2703, '532925', '弥渡县', 3, '0872', '100.49099,25.343804', 2698),
(2704, '532926', '南涧彝族自治县', 3, '0872', '100.509035,25.04351', 2698),
(2705, '532927', '巍山彝族回族自治县', 3, '0872', '100.307174,25.227212', 2698),
(2706, '532928', '永平县', 3, '0872', '99.541236,25.464681', 2698),
(2707, '532929', '云龙县', 3, '0872', '99.37112,25.885595', 2698),
(2708, '532930', '洱源县', 3, '0872', '99.951053,26.11116', 2698),
(2709, '532931', '剑川县', 3, '0872', '99.905559,26.537033', 2698),
(2710, '532932', '鹤庆县', 3, '0872', '100.176498,26.560231', 2698),
(2711, '533100', '德宏傣族景颇族自治州', 2, '0692', '98.584895,24.433353', 2580),
(2712, '533102', '瑞丽市', 3, '0692', '97.85559,24.017958', 2711),
(2713, '533103', '芒市', 3, '0692', '98.588086,24.43369', 2711),
(2714, '533122', '梁河县', 3, '0692', '98.296657,24.804232', 2711),
(2715, '533123', '盈江县', 3, '0692', '97.931936,24.705164', 2711),
(2716, '533124', '陇川县', 3, '0692', '97.792104,24.182965', 2711),
(2717, '533300', '怒江傈僳族自治州', 2, '0886', '98.8566,25.817555', 2580),
(2718, '533301', '泸水市', 3, '0886', '98.857977,25.822879', 2717),
(2719, '533323', '福贡县', 3, '0886', '98.869132,26.901831', 2717),
(2720, '533324', '贡山独龙族怒族自治县', 3, '0886', '98.665964,27.740999', 2717),
(2721, '533325', '兰坪白族普米族自治县', 3, '0886', '99.416677,26.453571', 2717),
(2722, '533400', '迪庆藏族自治州', 2, '0887', '99.702583,27.818807', 2580),
(2723, '533401', '香格里拉市', 3, '0887', '99.700904,27.829578', 2722),
(2724, '533422', '德钦县', 3, '0887', '98.911559,28.486163', 2722),
(2725, '533423', '维西傈僳族自治县', 3, '0887', '99.287173,27.177161', 2722),
(2726, '540000', '西藏自治区', 1, '[]', '91.117525,29.647535', -1),
(2727, '540100', '拉萨市', 2, '0891', '91.172148,29.652341', 2726),
(2728, '540102', '城关区', 3, '0891', '91.140552,29.654838', 2727),
(2729, '540103', '堆龙德庆区', 3, '0891', '91.003339,29.646063', 2727),
(2730, '540121', '林周县', 3, '0891', '91.265287,29.893545', 2727),
(2731, '540122', '当雄县', 3, '0891', '91.101162,30.473118', 2727),
(2732, '540123', '尼木县', 3, '0891', '90.164524,29.431831', 2727),
(2733, '540124', '曲水县', 3, '0891', '90.743853,29.353058', 2727),
(2734, '540126', '达孜县', 3, '0891', '91.349867,29.66941', 2727),
(2735, '540127', '墨竹工卡县', 3, '0891', '91.730732,29.834111', 2727),
(2736, '540200', '日喀则市', 2, '0892', '88.880583,29.266869', 2726),
(2737, '540202', '桑珠孜区', 3, '0892', '88.898483,29.24779', 2736),
(2738, '540221', '南木林县', 3, '0892', '89.099242,29.68233', 2736),
(2739, '540222', '江孜县', 3, '0892', '89.605627,28.911626', 2736),
(2740, '540223', '定日县', 3, '0892', '87.12612,28.658743', 2736),
(2741, '540224', '萨迦县', 3, '0892', '88.021674,28.899664', 2736),
(2742, '540225', '拉孜县', 3, '0892', '87.63704,29.081659', 2736),
(2743, '540226', '昂仁县', 3, '0892', '87.236051,29.294802', 2736),
(2744, '540227', '谢通门县', 3, '0892', '88.261664,29.432476', 2736),
(2745, '540228', '白朗县', 3, '0892', '89.261977,29.107688', 2736),
(2746, '540229', '仁布县', 3, '0892', '89.841983,29.230933', 2736),
(2747, '540230', '康马县', 3, '0892', '89.681663,28.555627', 2736),
(2748, '540231', '定结县', 3, '0892', '87.765872,28.364159', 2736),
(2749, '540232', '仲巴县', 3, '0892', '84.03153,29.770279', 2736),
(2750, '540233', '亚东县', 3, '0892', '88.907093,27.484806', 2736),
(2751, '540234', '吉隆县', 3, '0892', '85.297534,28.852393', 2736),
(2752, '540235', '聂拉木县', 3, '0892', '85.982237,28.155186', 2736),
(2753, '540236', '萨嘎县', 3, '0892', '85.232941,29.328818', 2736),
(2754, '540237', '岗巴县', 3, '0892', '88.520031,28.274601', 2736),
(2755, '540300', '昌都市', 2, '0895', '97.17202,31.140969', 2726),
(2756, '540302', '卡若区', 3, '0895', '97.196021,31.112087', 2755),
(2757, '540321', '江达县', 3, '0895', '98.21843,31.499202', 2755),
(2758, '540322', '贡觉县', 3, '0895', '98.27097,30.860099', 2755),
(2759, '540323', '类乌齐县', 3, '0895', '96.600246,31.211601', 2755),
(2760, '540324', '丁青县', 3, '0895', '95.619868,31.409024', 2755),
(2761, '540325', '察雅县', 3, '0895', '97.568752,30.653943', 2755),
(2762, '540326', '八宿县', 3, '0895', '96.917836,30.053209', 2755),
(2763, '540327', '左贡县', 3, '0895', '97.841022,29.671069', 2755),
(2764, '540328', '芒康县', 3, '0895', '98.593113,29.679907', 2755),
(2765, '540329', '洛隆县', 3, '0895', '95.825197,30.741845', 2755),
(2766, '540330', '边坝县', 3, '0895', '94.7078,30.933652', 2755),
(2767, '540400', '林芝市', 2, '0894', '94.36149,29.649128', 2726),
(2768, '540402', '巴宜区', 3, '0894', '94.361094,29.636576', 2767),
(2769, '540421', '工布江达县', 3, '0894', '93.246077,29.88528', 2767),
(2770, '540422', '米林县', 3, '0894', '94.213679,29.213811', 2767),
(2771, '540423', '墨脱县', 3, '0894', '95.333197,29.325298', 2767),
(2772, '540424', '波密县', 3, '0894', '95.767913,29.859028', 2767),
(2773, '540425', '察隅县', 3, '0894', '97.466919,28.66128', 2767),
(2774, '540426', '朗县', 3, '0894', '93.074702,29.046337', 2767),
(2775, '540500', '山南市', 2, '0893', '91.773134,29.237137', 2726),
(2776, '540502', '乃东区', 3, '0893', '91.761538,29.224904', 2775),
(2777, '540521', '扎囊县', 3, '0893', '91.33725,29.245113', 2775),
(2778, '540522', '贡嘎县', 3, '0893', '90.98414,29.289455', 2775),
(2779, '540523', '桑日县', 3, '0893', '92.015818,29.259189', 2775),
(2780, '540524', '琼结县', 3, '0893', '91.683881,29.024625', 2775),
(2781, '540525', '曲松县', 3, '0893', '92.203738,29.062826', 2775),
(2782, '540526', '措美县', 3, '0893', '91.433509,28.438202', 2775),
(2783, '540527', '洛扎县', 3, '0893', '90.859992,28.385713', 2775),
(2784, '540528', '加查县', 3, '0893', '92.593993,29.14029', 2775),
(2785, '540529', '隆子县', 3, '0893', '92.463308,28.408548', 2775),
(2786, '540530', '错那县', 3, '0893', '91.960132,27.991707', 2775),
(2787, '540531', '浪卡子县', 3, '0893', '90.397977,28.968031', 2775),
(2788, '542400', '那曲地区', 2, '0896', '92.052064,31.476479', 2726),
(2789, '542421', '那曲县', 3, '0896', '92.0535,31.469643', 2788),
(2790, '542422', '嘉黎县', 3, '0896', '93.232528,30.640814', 2788),
(2791, '542423', '比如县', 3, '0896', '93.679639,31.480249', 2788),
(2792, '542424', '聂荣县', 3, '0896', '92.303377,32.10775', 2788),
(2793, '542425', '安多县', 3, '0896', '91.68233,32.265176', 2788),
(2794, '542426', '申扎县', 3, '0896', '88.709852,30.930505', 2788),
(2795, '542427', '索县', 3, '0896', '93.785516,31.886671', 2788),
(2796, '542428', '班戈县', 3, '0896', '90.009957,31.392411', 2788),
(2797, '542429', '巴青县', 3, '0896', '94.053438,31.91847', 2788),
(2798, '542430', '尼玛县', 3, '0896', '87.236772,31.784701', 2788),
(2799, '542431', '双湖县', 3, '0896', '88.837641,33.188514', 2788),
(2800, '542500', '阿里地区', 2, '0897', '80.105804,32.501111', 2726),
(2801, '542521', '普兰县', 3, '0897', '81.176237,30.294402', 2800),
(2802, '542522', '札达县', 3, '0897', '79.802706,31.479216', 2800),
(2803, '542523', '噶尔县', 3, '0897', '80.096419,32.491488', 2800),
(2804, '542524', '日土县', 3, '0897', '79.732427,33.381359', 2800),
(2805, '542525', '革吉县', 3, '0897', '81.145433,32.387233', 2800),
(2806, '542526', '改则县', 3, '0897', '84.06259,32.302713', 2800),
(2807, '542527', '措勤县', 3, '0897', '85.151455,31.017312', 2800),
(2808, '610000', '陕西省', 1, '[]', '108.954347,34.265502', -1),
(2809, '610100', '西安市', 2, '029', '108.93977,34.341574', 2808),
(2810, '610102', '新城区', 3, '029', '108.960716,34.266447', 2809),
(2811, '610103', '碑林区', 3, '029', '108.94059,34.256783', 2809),
(2812, '610104', '莲湖区', 3, '029', '108.943895,34.265239', 2809),
(2813, '610111', '灞桥区', 3, '029', '109.064646,34.272793', 2809),
(2814, '610112', '未央区', 3, '029', '108.946825,34.29292', 2809),
(2815, '610113', '雁塔区', 3, '029', '108.944644,34.214113', 2809),
(2816, '610114', '阎良区', 3, '029', '109.226124,34.662232', 2809),
(2817, '610115', '临潼区', 3, '029', '109.214237,34.367069', 2809),
(2818, '610116', '长安区', 3, '029', '108.907173,34.158926', 2809),
(2819, '610117', '高陵区', 3, '029', '109.088297,34.534829', 2809),
(2820, '610122', '蓝田县', 3, '029', '109.32345,34.151298', 2809),
(2821, '610124', '周至县', 3, '029', '108.222162,34.163669', 2809),
(2822, '610118', '鄠邑区', 3, '029', '108.604894,34.109244', 2809),
(2823, '610200', '铜川市', 2, '0919', '108.945019,34.897887', 2808),
(2824, '610202', '王益区', 3, '0919', '109.075578,35.068964', 2823),
(2825, '610203', '印台区', 3, '0919', '109.099974,35.114492', 2823),
(2826, '610204', '耀州区', 3, '0919', '108.980102,34.909793', 2823),
(2827, '610222', '宜君县', 3, '0919', '109.116932,35.398577', 2823),
(2828, '610300', '宝鸡市', 2, '0917', '107.237743,34.363184', 2808),
(2829, '610302', '渭滨区', 3, '0917', '107.155344,34.355068', 2828),
(2830, '610303', '金台区', 3, '0917', '107.146806,34.376069', 2828),
(2831, '610304', '陈仓区', 3, '0917', '107.369987,34.35147', 2828),
(2832, '610322', '凤翔县', 3, '0917', '107.400737,34.521217', 2828),
(2833, '610323', '岐山县', 3, '0917', '107.621053,34.443459', 2828),
(2834, '610324', '扶风县', 3, '0917', '107.900219,34.37541', 2828),
(2835, '610326', '眉县', 3, '0917', '107.749766,34.274246', 2828),
(2836, '610327', '陇县', 3, '0917', '106.864397,34.89305', 2828),
(2837, '610328', '千阳县', 3, '0917', '107.132441,34.642381', 2828),
(2838, '610329', '麟游县', 3, '0917', '107.793524,34.677902', 2828),
(2839, '610330', '凤县', 3, '0917', '106.515803,33.91091', 2828),
(2840, '610331', '太白县', 3, '0917', '107.319116,34.058401', 2828),
(2841, '610400', '咸阳市', 2, '0910', '108.709136,34.32987', 2808),
(2842, '610402', '秦都区', 3, '0910', '108.706272,34.329567', 2841),
(2843, '610403', '杨陵区', 3, '0910', '108.084731,34.272117', 2841),
(2844, '610404', '渭城区', 3, '0910', '108.737204,34.36195', 2841),
(2845, '610422', '三原县', 3, '0910', '108.940509,34.617381', 2841),
(2846, '610423', '泾阳县', 3, '0910', '108.842622,34.527114', 2841),
(2847, '610424', '乾县', 3, '0910', '108.239473,34.527551', 2841),
(2848, '610425', '礼泉县', 3, '0910', '108.425018,34.481764', 2841),
(2849, '610426', '永寿县', 3, '0910', '108.142311,34.691979', 2841),
(2850, '610427', '彬县', 3, '0910', '108.077658,35.043911', 2841),
(2851, '610428', '长武县', 3, '0910', '107.798757,35.205886', 2841),
(2852, '610429', '旬邑县', 3, '0910', '108.333986,35.111978', 2841),
(2853, '610430', '淳化县', 3, '0910', '108.580681,34.79925', 2841),
(2854, '610431', '武功县', 3, '0910', '108.200398,34.260203', 2841),
(2855, '610481', '兴平市', 3, '0910', '108.490475,34.29922', 2841),
(2856, '610500', '渭南市', 2, '0913', '109.471094,34.52044', 2808),
(2857, '610502', '临渭区', 3, '0913', '109.510175,34.499314', 2856),
(2858, '610503', '华州区', 3, '0913', '109.775247,34.495915', 2856),
(2859, '610522', '潼关县', 3, '0913', '110.246349,34.544296', 2856),
(2860, '610523', '大荔县', 3, '0913', '109.941734,34.797259', 2856),
(2861, '610524', '合阳县', 3, '0913', '110.149453,35.237988', 2856),
(2862, '610525', '澄城县', 3, '0913', '109.93235,35.190245', 2856),
(2863, '610526', '蒲城县', 3, '0913', '109.586403,34.955562', 2856),
(2864, '610527', '白水县', 3, '0913', '109.590671,35.177451', 2856),
(2865, '610528', '富平县', 3, '0913', '109.18032,34.751077', 2856),
(2866, '610581', '韩城市', 3, '0913', '110.442846,35.476788', 2856),
(2867, '610582', '华阴市', 3, '0913', '110.092078,34.566079', 2856),
(2868, '610600', '延安市', 2, '0911', '109.494112,36.651381', 2808),
(2869, '610602', '宝塔区', 3, '0911', '109.48976,36.585472', 2868),
(2870, '610621', '延长县', 3, '0911', '110.012334,36.579313', 2868),
(2871, '610622', '延川县', 3, '0911', '110.193514,36.878117', 2868),
(2872, '610623', '子长县', 3, '0911', '109.675264,37.142535', 2868),
(2873, '610603', '安塞区', 3, '0911', '109.328842,36.863853', 2868),
(2874, '610625', '志丹县', 3, '0911', '108.768432,36.822194', 2868),
(2875, '610626', '吴起县', 3, '0911', '108.175933,36.927215', 2868),
(2876, '610627', '甘泉县', 3, '0911', '109.351019,36.276526', 2868),
(2877, '610628', '富县', 3, '0911', '109.379776,35.987953', 2868),
(2878, '610629', '洛川县', 3, '0911', '109.432369,35.761974', 2868),
(2879, '610630', '宜川县', 3, '0911', '110.168963,36.050178', 2868),
(2880, '610631', '黄龙县', 3, '0911', '109.840314,35.584743', 2868),
(2881, '610632', '黄陵县', 3, '0911', '109.262961,35.579427', 2868),
(2882, '610700', '汉中市', 2, '0916', '107.02305,33.067225', 2808),
(2883, '610702', '汉台区', 3, '0916', '107.031856,33.067771', 2882),
(2884, '610721', '南郑县', 3, '0916', '106.93623,32.999333', 2882),
(2885, '610722', '城固县', 3, '0916', '107.33393,33.157131', 2882),
(2886, '610723', '洋县', 3, '0916', '107.545836,33.222738', 2882),
(2887, '610724', '西乡县', 3, '0916', '107.766613,32.983101', 2882),
(2888, '610725', '勉县', 3, '0916', '106.673221,33.153553', 2882),
(2889, '610726', '宁强县', 3, '0916', '106.257171,32.829694', 2882),
(2890, '610727', '略阳县', 3, '0916', '106.156718,33.327281', 2882),
(2891, '610728', '镇巴县', 3, '0916', '107.895035,32.536704', 2882),
(2892, '610729', '留坝县', 3, '0916', '106.920808,33.617571', 2882),
(2893, '610730', '佛坪县', 3, '0916', '107.990538,33.524359', 2882),
(2894, '610800', '榆林市', 2, '0912', '109.734474,38.285369', 2808),
(2895, '610802', '榆阳区', 3, '0912', '109.721069,38.277046', 2894),
(2896, '610803', '横山区', 3, '0912', '109.294346,37.962208', 2894),
(2897, '610881', '神木市', 3, '0912', '110.498939,38.842578', 2894),
(2898, '610822', '府谷县', 3, '0912', '111.067276,39.028116', 2894),
(2899, '610824', '靖边县', 3, '0912', '108.793988,37.599438', 2894),
(2900, '610825', '定边县', 3, '0912', '107.601267,37.594612', 2894),
(2901, '610826', '绥德县', 3, '0912', '110.263362,37.50294', 2894),
(2902, '610827', '米脂县', 3, '0912', '110.183754,37.755416', 2894),
(2903, '610828', '佳县', 3, '0912', '110.491345,38.01951', 2894),
(2904, '610829', '吴堡县', 3, '0912', '110.739673,37.452067', 2894),
(2905, '610830', '清涧县', 3, '0912', '110.121209,37.088878', 2894),
(2906, '610831', '子洲县', 3, '0912', '110.03525,37.610683', 2894),
(2907, '610900', '安康市', 2, '0915', '109.029113,32.68481', 2808),
(2908, '610902', '汉滨区', 3, '0915', '109.026836,32.695172', 2907),
(2909, '610921', '汉阴县', 3, '0915', '108.508745,32.893026', 2907),
(2910, '610922', '石泉县', 3, '0915', '108.247886,33.038408', 2907),
(2911, '610923', '宁陕县', 3, '0915', '108.314283,33.310527', 2907),
(2912, '610924', '紫阳县', 3, '0915', '108.534228,32.520246', 2907),
(2913, '610925', '岚皋县', 3, '0915', '108.902049,32.307001', 2907),
(2914, '610926', '平利县', 3, '0915', '109.361864,32.388854', 2907),
(2915, '610927', '镇坪县', 3, '0915', '109.526873,31.883672', 2907),
(2916, '610928', '旬阳县', 3, '0915', '109.361024,32.832012', 2907),
(2917, '610929', '白河县', 3, '0915', '110.112629,32.809026', 2907),
(2918, '611000', '商洛市', 2, '0914', '109.91857,33.872726', 2808),
(2919, '611002', '商州区', 3, '0914', '109.941839,33.862599', 2918),
(2920, '611021', '洛南县', 3, '0914', '110.148508,34.090837', 2918),
(2921, '611022', '丹凤县', 3, '0914', '110.32733,33.695783', 2918),
(2922, '611023', '商南县', 3, '0914', '110.881807,33.530995', 2918),
(2923, '611024', '山阳县', 3, '0914', '109.882289,33.532172', 2918),
(2924, '611025', '镇安县', 3, '0914', '109.152892,33.423357', 2918),
(2925, '611026', '柞水县', 3, '0914', '109.114206,33.68611', 2918),
(2926, '620000', '甘肃省', 1, '[]', '103.826447,36.05956', -1),
(2927, '620100', '兰州市', 2, '0931', '103.834303,36.061089', 2926),
(2928, '620102', '城关区', 3, '0931', '103.825307,36.057464', 2927),
(2929, '620103', '七里河区', 3, '0931', '103.785949,36.066146', 2927),
(2930, '620104', '西固区', 3, '0931', '103.627951,36.088552', 2927),
(2931, '620105', '安宁区', 3, '0931', '103.719054,36.104579', 2927),
(2932, '620111', '红古区', 3, '0931', '102.859323,36.345669', 2927),
(2933, '620121', '永登县', 3, '0931', '103.26038,36.736513', 2927),
(2934, '620122', '皋兰县', 3, '0931', '103.947377,36.332663', 2927),
(2935, '620123', '榆中县', 3, '0931', '104.112527,35.843056', 2927),
(2936, '620200', '嘉峪关市', 2, '1937', '98.289419,39.772554', 2926),
(2937, '620300', '金昌市', 2, '0935', '102.188117,38.520717', 2926),
(2938, '620302', '金川区', 3, '0935', '102.194015,38.521087', 2937),
(2939, '620321', '永昌县', 3, '0935', '101.984458,38.243434', 2937),
(2940, '620400', '白银市', 2, '0943', '104.138771,36.545261', 2926),
(2941, '620402', '白银区', 3, '0943', '104.148556,36.535398', 2940),
(2942, '620403', '平川区', 3, '0943', '104.825208,36.728304', 2940),
(2943, '620421', '靖远县', 3, '0943', '104.676774,36.571365', 2940),
(2944, '620422', '会宁县', 3, '0943', '105.053358,35.692823', 2940),
(2945, '620423', '景泰县', 3, '0943', '104.063091,37.183804', 2940),
(2946, '620500', '天水市', 2, '0938', '105.724979,34.580885', 2926),
(2947, '620502', '秦州区', 3, '0938', '105.724215,34.580888', 2946),
(2948, '620503', '麦积区', 3, '0938', '105.889556,34.570384', 2946),
(2949, '620521', '清水县', 3, '0938', '106.137293,34.749864', 2946),
(2950, '620522', '秦安县', 3, '0938', '105.674982,34.858916', 2946),
(2951, '620523', '甘谷县', 3, '0938', '105.340747,34.745486', 2946),
(2952, '620524', '武山县', 3, '0938', '104.890587,34.72139', 2946),
(2953, '620525', '张家川回族自治县', 3, '0938', '106.204517,34.988037', 2946),
(2954, '620600', '武威市', 2, '1935', '102.638201,37.928267', 2926),
(2955, '620602', '凉州区', 3, '1935', '102.642184,37.928224', 2954),
(2956, '620621', '民勤县', 3, '1935', '103.093791,38.62435', 2954),
(2957, '620622', '古浪县', 3, '1935', '102.897533,37.47012', 2954),
(2958, '620623', '天祝藏族自治县', 3, '1935', '103.141757,36.97174', 2954),
(2959, '620700', '张掖市', 2, '0936', '100.449913,38.925548', 2926),
(2960, '620702', '甘州区', 3, '0936', '100.415096,38.944662', 2959),
(2961, '620721', '肃南裕固族自治县', 3, '0936', '99.615601,38.836931', 2959),
(2962, '620722', '民乐县', 3, '0936', '100.812629,38.430347', 2959),
(2963, '620723', '临泽县', 3, '0936', '100.164283,39.152462', 2959),
(2964, '620724', '高台县', 3, '0936', '99.819519,39.378311', 2959),
(2965, '620725', '山丹县', 3, '0936', '101.088529,38.784505', 2959),
(2966, '620800', '平凉市', 2, '0933', '106.665061,35.542606', 2926),
(2967, '620802', '崆峒区', 3, '0933', '106.674767,35.542491', 2966),
(2968, '620821', '泾川县', 3, '0933', '107.36785,35.332666', 2966),
(2969, '620822', '灵台县', 3, '0933', '107.595874,35.070027', 2966),
(2970, '620823', '崇信县', 3, '0933', '107.025763,35.305596', 2966),
(2971, '620824', '华亭县', 3, '0933', '106.653158,35.218292', 2966),
(2972, '620825', '庄浪县', 3, '0933', '106.036686,35.202385', 2966),
(2973, '620826', '静宁县', 3, '0933', '105.732556,35.521976', 2966),
(2974, '620900', '酒泉市', 2, '0937', '98.493927,39.732795', 2926),
(2975, '620902', '肃州区', 3, '0937', '98.507843,39.744953', 2974),
(2976, '620921', '金塔县', 3, '0937', '98.901252,39.983955', 2974),
(2977, '620922', '瓜州县', 3, '0937', '95.782318,40.520538', 2974),
(2978, '620923', '肃北蒙古族自治县', 3, '0937', '94.876579,39.51245', 2974),
(2979, '620924', '阿克塞哈萨克族自治县', 3, '0937', '94.340204,39.633943', 2974),
(2980, '620981', '玉门市', 3, '0937', '97.045661,40.292106', 2974),
(2981, '620982', '敦煌市', 3, '0937', '94.661941,40.142089', 2974),
(2982, '621000', '庆阳市', 2, '0934', '107.643571,35.70898', 2926),
(2983, '621002', '西峰区', 3, '0934', '107.651077,35.730652', 2982),
(2984, '621021', '庆城县', 3, '0934', '107.881802,36.016299', 2982),
(2985, '621022', '环县', 3, '0934', '107.308501,36.568434', 2982),
(2986, '621023', '华池县', 3, '0934', '107.990062,36.461306', 2982),
(2987, '621024', '合水县', 3, '0934', '108.019554,35.819194', 2982),
(2988, '621025', '正宁县', 3, '0934', '108.359865,35.49178', 2982),
(2989, '621026', '宁县', 3, '0934', '107.928371,35.502176', 2982),
(2990, '621027', '镇原县', 3, '0934', '107.200832,35.677462', 2982),
(2991, '621100', '定西市', 2, '0932', '104.592225,35.606978', 2926),
(2992, '621102', '安定区', 3, '0932', '104.610668,35.580629', 2991),
(2993, '621121', '通渭县', 3, '0932', '105.24206,35.210831', 2991),
(2994, '621122', '陇西县', 3, '0932', '104.634983,35.00394', 2991),
(2995, '621123', '渭源县', 3, '0932', '104.215467,35.136755', 2991),
(2996, '621124', '临洮县', 3, '0932', '103.859565,35.394988', 2991),
(2997, '621125', '漳县', 3, '0932', '104.471572,34.848444', 2991),
(2998, '621126', '岷县', 3, '0932', '104.03688,34.438075', 2991),
(2999, '621200', '陇南市', 2, '2935', '104.960851,33.37068', 2926),
(3000, '621202', '武都区', 3, '2935', '104.926337,33.392211', 2999),
(3001, '621221', '成县', 3, '2935', '105.742424,33.75061', 2999),
(3002, '621222', '文县', 3, '2935', '104.683433,32.943815', 2999),
(3003, '621223', '宕昌县', 3, '2935', '104.393385,34.047261', 2999),
(3004, '621224', '康县', 3, '2935', '105.609169,33.329136', 2999),
(3005, '621225', '西和县', 3, '2935', '105.298756,34.014215', 2999),
(3006, '621226', '礼县', 3, '2935', '105.17864,34.189345', 2999),
(3007, '621227', '徽县', 3, '2935', '106.08778,33.768826', 2999),
(3008, '621228', '两当县', 3, '2935', '106.304966,33.908917', 2999),
(3009, '622900', '临夏回族自治州', 2, '0930', '103.210655,35.601352', 2926),
(3010, '622901', '临夏市', 3, '0930', '103.243021,35.604376', 3009),
(3011, '622921', '临夏县', 3, '0930', '103.039826,35.478722', 3009),
(3012, '622922', '康乐县', 3, '0930', '103.708354,35.370505', 3009),
(3013, '622923', '永靖县', 3, '0930', '103.285853,35.958306', 3009),
(3014, '622924', '广河县', 3, '0930', '103.575834,35.488051', 3009),
(3015, '622925', '和政县', 3, '0930', '103.350997,35.424603', 3009),
(3016, '622926', '东乡族自治县', 3, '0930', '103.389346,35.663752', 3009),
(3017, '622927', '积石山保安族东乡族撒拉族自治县', 3, '0930', '102.875843,35.71766', 3009),
(3018, '623000', '甘南藏族自治州', 2, '0941', '102.910995,34.983409', 2926),
(3019, '623001', '合作市', 3, '0941', '102.910484,35.000286', 3018),
(3020, '623021', '临潭县', 3, '0941', '103.353919,34.692747', 3018),
(3021, '623022', '卓尼县', 3, '0941', '103.507109,34.589588', 3018),
(3022, '623023', '舟曲县', 3, '0941', '104.251482,33.793631', 3018),
(3023, '623024', '迭部县', 3, '0941', '103.221869,34.055938', 3018),
(3024, '623025', '玛曲县', 3, '0941', '102.072698,33.997712', 3018),
(3025, '623026', '碌曲县', 3, '0941', '102.487327,34.590944', 3018),
(3026, '623027', '夏河县', 3, '0941', '102.521807,35.202503', 3018),
(3027, '630000', '青海省', 1, '[]', '101.780268,36.620939', -1),
(3028, '630100', '西宁市', 2, '0971', '101.778223,36.617134', 3027),
(3029, '630102', '城东区', 3, '0971', '101.803717,36.599744', 3028),
(3030, '630103', '城中区', 3, '0971', '101.705298,36.545652', 3028),
(3031, '630104', '城西区', 3, '0971', '101.765843,36.628304', 3028),
(3032, '630105', '城北区', 3, '0971', '101.766228,36.650038', 3028),
(3033, '630121', '大通回族土族自治县', 3, '0971', '101.685643,36.926954', 3028),
(3034, '630122', '湟中县', 3, '0971', '101.571667,36.500879', 3028),
(3035, '630123', '湟源县', 3, '0971', '101.256464,36.682426', 3028),
(3036, '630200', '海东市', 2, '0972', '102.104287,36.502039', 3027),
(3037, '630202', '乐都区', 3, '0972', '102.401724,36.482058', 3036),
(3038, '630203', '平安区', 3, '0972', '102.108834,36.500563', 3036),
(3039, '630222', '民和回族土族自治县', 3, '0972', '102.830892,36.320321', 3036),
(3040, '630223', '互助土族自治县', 3, '0972', '101.959271,36.844248', 3036),
(3041, '630224', '化隆回族自治县', 3, '0972', '102.264143,36.094908', 3036),
(3042, '630225', '循化撒拉族自治县', 3, '0972', '102.489135,35.851152', 3036),
(3043, '632200', '海北藏族自治州', 2, '0970', '100.900997,36.954413', 3027),
(3044, '632221', '门源回族自治县', 3, '0970', '101.611539,37.388746', 3043),
(3045, '632222', '祁连县', 3, '0970', '100.253211,38.177112', 3043),
(3046, '632223', '海晏县', 3, '0970', '100.99426,36.896359', 3043),
(3047, '632224', '刚察县', 3, '0970', '100.145833,37.32547', 3043),
(3048, '632300', '黄南藏族自治州', 2, '0973', '102.015248,35.519548', 3027),
(3049, '632321', '同仁县', 3, '0973', '102.018323,35.516063', 3048),
(3050, '632322', '尖扎县', 3, '0973', '102.04014,35.943156', 3048),
(3051, '632323', '泽库县', 3, '0973', '101.466689,35.035313', 3048),
(3052, '632324', '河南蒙古族自治县', 3, '0973', '101.617503,34.734568', 3048),
(3053, '632500', '海南藏族自治州', 2, '0974', '100.622692,36.296529', 3027),
(3054, '632521', '共和县', 3, '0974', '100.620031,36.284107', 3053),
(3055, '632522', '同德县', 3, '0974', '100.578051,35.25479', 3053),
(3056, '632523', '贵德县', 3, '0974', '101.433391,36.040166', 3053),
(3057, '632524', '兴海县', 3, '0974', '99.987965,35.588612', 3053),
(3058, '632525', '贵南县', 3, '0974', '100.747503,35.586714', 3053),
(3059, '632600', '果洛藏族自治州', 2, '0975', '100.244808,34.471431', 3027),
(3060, '632621', '玛沁县', 3, '0975', '100.238888,34.477433', 3059),
(3061, '632622', '班玛县', 3, '0975', '100.737138,32.932723', 3059),
(3062, '632623', '甘德县', 3, '0975', '99.900923,33.969216', 3059),
(3063, '632624', '达日县', 3, '0975', '99.651392,33.74892', 3059),
(3064, '632625', '久治县', 3, '0975', '101.482831,33.429471', 3059),
(3065, '632626', '玛多县', 3, '0975', '98.209206,34.915946', 3059),
(3066, '632700', '玉树藏族自治州', 2, '0976', '97.091934,33.011674', 3027),
(3067, '632701', '玉树市', 3, '0976', '97.008784,32.993106', 3066),
(3068, '632722', '杂多县', 3, '0976', '95.300723,32.893185', 3066),
(3069, '632723', '称多县', 3, '0976', '97.110831,33.369218', 3066),
(3070, '632724', '治多县', 3, '0976', '95.61896,33.844956', 3066),
(3071, '632725', '囊谦县', 3, '0976', '96.48943,32.203432', 3066),
(3072, '632726', '曲麻莱县', 3, '0976', '95.797367,34.126428', 3066),
(3073, '632800', '海西蒙古族藏族自治州', 2, '0977', '97.369751,37.377139', 3027),
(3074, '632801', '格尔木市', 3, '0977', '94.928453,36.406367', 3073),
(3075, '632802', '德令哈市', 3, '0977', '97.360984,37.369436', 3073),
(3076, '632821', '乌兰县', 3, '0977', '98.480195,36.929749', 3073),
(3077, '632822', '都兰县', 3, '0977', '98.095844,36.302496', 3073),
(3078, '632823', '天峻县', 3, '0977', '99.022984,37.300851', 3073),
(3079, '632825', '海西蒙古族藏族自治州直辖', 3, '0977', '95.356546,37.853328', 3073),
(3080, '640000', '宁夏回族自治区', 1, '[]', '106.259126,38.472641', -1),
(3081, '640100', '银川市', 2, '0951', '106.230909,38.487193', 3080),
(3082, '640104', '兴庆区', 3, '0951', '106.28865,38.473609', 3081),
(3083, '640105', '西夏区', 3, '0951', '106.161106,38.502605', 3081),
(3084, '640106', '金凤区', 3, '0951', '106.239679,38.47436', 3081),
(3085, '640121', '永宁县', 3, '0951', '106.253145,38.277372', 3081),
(3086, '640122', '贺兰县', 3, '0951', '106.349861,38.554599', 3081),
(3087, '640181', '灵武市', 3, '0951', '106.340053,38.102655', 3081),
(3088, '640200', '石嘴山市', 2, '0952', '106.383303,38.983236', 3080),
(3089, '640202', '大武口区', 3, '0952', '106.367958,39.01918', 3088),
(3090, '640205', '惠农区', 3, '0952', '106.781176,39.239302', 3088),
(3091, '640221', '平罗县', 3, '0952', '106.523474,38.913544', 3088),
(3092, '640300', '吴忠市', 2, '0953', '106.198913,37.997428', 3080),
(3093, '640302', '利通区', 3, '0953', '106.212613,37.98349', 3092),
(3094, '640303', '红寺堡区', 3, '0953', '106.062113,37.425702', 3092),
(3095, '640323', '盐池县', 3, '0953', '107.407358,37.783205', 3092),
(3096, '640324', '同心县', 3, '0953', '105.895309,36.95449', 3092),
(3097, '640381', '青铜峡市', 3, '0953', '106.078817,38.021302', 3092),
(3098, '640400', '固原市', 2, '0954', '106.24261,36.015855', 3080),
(3099, '640402', '原州区', 3, '0954', '106.287781,36.003739', 3098),
(3100, '640422', '西吉县', 3, '0954', '105.729085,35.963912', 3098),
(3101, '640423', '隆德县', 3, '0954', '106.111595,35.625914', 3098),
(3102, '640424', '泾源县', 3, '0954', '106.330646,35.498159', 3098),
(3103, '640425', '彭阳县', 3, '0954', '106.631809,35.858815', 3098),
(3104, '640500', '中卫市', 2, '1953', '105.196902,37.499972', 3080),
(3105, '640502', '沙坡头区', 3, '1953', '105.173721,37.516883', 3104),
(3106, '640521', '中宁县', 3, '1953', '105.685218,37.491546', 3104),
(3107, '640522', '海原县', 3, '1953', '105.643487,36.565033', 3104),
(3108, '650000', '新疆维吾尔自治区', 1, '[]', '87.627704,43.793026', -1),
(3109, '659002', '阿拉尔市', 2, '1997', '81.280527,40.547653', 3108),
(3110, '659005', '北屯市', 2, '1906', '87.837075,47.332643', 3108),
(3111, '659008', '可克达拉市', 2, '1999', '81.044542,43.944798', 3108),
(3112, '659009', '昆玉市', 2, '1903', '79.291083,37.209642', 3108),
(3113, '659001', '石河子市', 2, '0993', '86.080602,44.306097', 3108),
(3114, '659007', '双河市', 2, '1909', '82.353656,44.840524', 3108),
(3115, '650100', '乌鲁木齐市', 2, '0991', '87.616848,43.825592', 3108),
(3116, '650102', '天山区', 3, '0991', '87.631676,43.794399', 3115),
(3117, '650103', '沙依巴克区', 3, '0991', '87.598195,43.800939', 3115),
(3118, '650104', '新市区', 3, '0991', '87.569431,43.855378', 3115),
(3119, '650105', '水磨沟区', 3, '0991', '87.642481,43.832459', 3115),
(3120, '650106', '头屯河区', 3, '0991', '87.428141,43.877664', 3115),
(3121, '650107', '达坂城区', 3, '0991', '88.311099,43.363668', 3115),
(3122, '650109', '米东区', 3, '0991', '87.655935,43.974784', 3115),
(3123, '650121', '乌鲁木齐县', 3, '0991', '87.409417,43.47136', 3115),
(3124, '650200', '克拉玛依市', 2, '0990', '84.889207,45.579888', 3108),
(3125, '650202', '独山子区', 3, '0990', '84.886974,44.328095', 3124),
(3126, '650203', '克拉玛依区', 3, '0990', '84.867844,45.602525', 3124),
(3127, '650204', '白碱滩区', 3, '0990', '85.131696,45.687854', 3124),
(3128, '650205', '乌尔禾区', 3, '0990', '85.693742,46.089148', 3124),
(3129, '650400', '吐鲁番市', 2, '0995', '89.189752,42.951303', 3108),
(3130, '650402', '高昌区', 3, '0995', '89.185877,42.942327', 3129),
(3131, '650421', '鄯善县', 3, '0995', '90.21333,42.868744', 3129),
(3132, '650422', '托克逊县', 3, '0995', '88.653827,42.792526', 3129),
(3133, '650500', '哈密市', 2, '0902', '93.515224,42.819541', 3108),
(3134, '650502', '伊州区', 3, '0902', '93.514797,42.827254', 3133),
(3135, '650521', '巴里坤哈萨克自治县', 3, '0902', '93.010383,43.599929', 3133),
(3136, '650522', '伊吾县', 3, '0902', '94.697074,43.254978', 3133),
(3137, '652300', '昌吉回族自治州', 2, '0994', '87.308224,44.011182', 3108),
(3138, '652301', '昌吉市', 3, '0994', '87.267532,44.014435', 3137),
(3139, '652302', '阜康市', 3, '0994', '87.952991,44.164402', 3137),
(3140, '652323', '呼图壁县', 3, '0994', '86.871584,44.179361', 3137),
(3141, '652324', '玛纳斯县', 3, '0994', '86.20368,44.284722', 3137),
(3142, '652325', '奇台县', 3, '0994', '89.593967,44.022066', 3137),
(3143, '652327', '吉木萨尔县', 3, '0994', '89.180437,44.000497', 3137),
(3144, '652328', '木垒哈萨克自治县', 3, '0994', '90.286028,43.834689', 3137),
(3145, '652700', '博尔塔拉蒙古自治州', 2, '0909', '82.066363,44.906039', 3108),
(3146, '652701', '博乐市', 3, '0909', '82.051004,44.853869', 3145),
(3147, '652702', '阿拉山口市', 3, '0909', '82.559396,45.172227', 3145),
(3148, '652722', '精河县', 3, '0909', '82.890656,44.599393', 3145),
(3149, '652723', '温泉县', 3, '0909', '81.024816,44.968856', 3145),
(3150, '652800', '巴音郭楞蒙古自治州', 2, '0996', '86.145297,41.764115', 3108),
(3151, '652801', '库尔勒市', 3, '0996', '86.174633,41.725891', 3150),
(3152, '652822', '轮台县', 3, '0996', '84.252156,41.777702', 3150);
INSERT INTO `mf_area_items` (`areaId`, `areaCode`, `areaName`, `level`, `cityCode`, `center`, `parentId`) VALUES
(3153, '652823', '尉犁县', 3, '0996', '86.261321,41.343933', 3150),
(3154, '652824', '若羌县', 3, '0996', '88.167152,39.023241', 3150),
(3155, '652825', '且末县', 3, '0996', '85.529702,38.145485', 3150),
(3156, '652826', '焉耆回族自治县', 3, '0996', '86.574067,42.059759', 3150),
(3157, '652827', '和静县', 3, '0996', '86.384065,42.323625', 3150),
(3158, '652828', '和硕县', 3, '0996', '86.876799,42.284331', 3150),
(3159, '652829', '博湖县', 3, '0996', '86.631997,41.980152', 3150),
(3160, '652900', '阿克苏地区', 2, '0997', '80.260605,41.168779', 3108),
(3161, '652901', '阿克苏市', 3, '0997', '80.263387,41.167548', 3160),
(3162, '652922', '温宿县', 3, '0997', '80.238959,41.276688', 3160),
(3163, '652923', '库车县', 3, '0997', '82.987312,41.714696', 3160),
(3164, '652924', '沙雅县', 3, '0997', '82.781818,41.221666', 3160),
(3165, '652925', '新和县', 3, '0997', '82.618736,41.551206', 3160),
(3166, '652926', '拜城县', 3, '0997', '81.85148,41.795912', 3160),
(3167, '652927', '乌什县', 3, '0997', '79.224616,41.222319', 3160),
(3168, '652928', '阿瓦提县', 3, '0997', '80.375053,40.643647', 3160),
(3169, '652929', '柯坪县', 3, '0997', '79.054497,40.501936', 3160),
(3170, '653000', '克孜勒苏柯尔克孜自治州', 2, '0908', '76.167819,39.714526', 3108),
(3171, '653001', '阿图什市', 3, '0908', '76.1684,39.71616', 3170),
(3172, '653022', '阿克陶县', 3, '0908', '75.947396,39.147785', 3170),
(3173, '653023', '阿合奇县', 3, '0908', '78.446253,40.936936', 3170),
(3174, '653024', '乌恰县', 3, '0908', '75.259227,39.71931', 3170),
(3175, '653100', '喀什地区', 2, '0998', '75.989741,39.47046', 3108),
(3176, '653101', '喀什市', 3, '0998', '75.99379,39.467685', 3175),
(3177, '653121', '疏附县', 3, '0998', '75.862813,39.375043', 3175),
(3178, '653122', '疏勒县', 3, '0998', '76.048139,39.401384', 3175),
(3179, '653123', '英吉沙县', 3, '0998', '76.175729,38.930381', 3175),
(3180, '653124', '泽普县', 3, '0998', '77.259675,38.18529', 3175),
(3181, '653125', '莎车县', 3, '0998', '77.245761,38.41422', 3175),
(3182, '653126', '叶城县', 3, '0998', '77.413836,37.882989', 3175),
(3183, '653127', '麦盖提县', 3, '0998', '77.610125,38.898001', 3175),
(3184, '653128', '岳普湖县', 3, '0998', '76.8212,39.2198', 3175),
(3185, '653129', '伽师县', 3, '0998', '76.723719,39.488181', 3175),
(3186, '653130', '巴楚县', 3, '0998', '78.549296,39.785155', 3175),
(3187, '653131', '塔什库尔干塔吉克自治县', 3, '0998', '75.229889,37.772094', 3175),
(3188, '653200', '和田地区', 2, '0903', '79.922211,37.114157', 3108),
(3189, '653201', '和田市', 3, '0903', '79.913534,37.112148', 3188),
(3190, '653221', '和田县', 3, '0903', '79.81907,37.120031', 3188),
(3191, '653222', '墨玉县', 3, '0903', '79.728683,37.277143', 3188),
(3192, '653223', '皮山县', 3, '0903', '78.283669,37.62145', 3188),
(3193, '653224', '洛浦县', 3, '0903', '80.188986,37.073667', 3188),
(3194, '653225', '策勒县', 3, '0903', '80.806159,36.998335', 3188),
(3195, '653226', '于田县', 3, '0903', '81.677418,36.85708', 3188),
(3196, '653227', '民丰县', 3, '0903', '82.695861,37.06408', 3188),
(3197, '654000', '伊犁哈萨克自治州', 2, '0999', '81.324136,43.916823', 3108),
(3198, '654002', '伊宁市', 3, '0999', '81.27795,43.908558', 3197),
(3199, '654003', '奎屯市', 3, '0999', '84.903267,44.426529', 3197),
(3200, '654004', '霍尔果斯市', 3, '0999', '80.411271,44.213941', 3197),
(3201, '654021', '伊宁县', 3, '0999', '81.52745,43.977119', 3197),
(3202, '654022', '察布查尔锡伯自治县', 3, '0999', '81.151337,43.840726', 3197),
(3203, '654023', '霍城县', 3, '0999', '80.87898,44.055984', 3197),
(3204, '654024', '巩留县', 3, '0999', '82.231718,43.482628', 3197),
(3205, '654025', '新源县', 3, '0999', '83.232848,43.433896', 3197),
(3206, '654026', '昭苏县', 3, '0999', '81.130974,43.157293', 3197),
(3207, '654027', '特克斯县', 3, '0999', '81.836206,43.217183', 3197),
(3208, '654028', '尼勒克县', 3, '0999', '82.511809,43.800247', 3197),
(3209, '654200', '塔城地区', 2, '0901', '82.980316,46.745364', 3108),
(3210, '654201', '塔城市', 3, '0901', '82.986978,46.751428', 3209),
(3211, '654202', '乌苏市', 3, '0901', '84.713396,44.41881', 3209),
(3212, '654221', '额敏县', 3, '0901', '83.628303,46.524673', 3209),
(3213, '654223', '沙湾县', 3, '0901', '85.619416,44.326388', 3209),
(3214, '654224', '托里县', 3, '0901', '83.60695,45.947638', 3209),
(3215, '654225', '裕民县', 3, '0901', '82.982667,46.201104', 3209),
(3216, '654226', '和布克赛尔蒙古自治县', 3, '0901', '85.728328,46.793235', 3209),
(3217, '654300', '阿勒泰地区', 2, '0906', '88.141253,47.844924', 3108),
(3218, '654301', '阿勒泰市', 3, '0906', '88.131842,47.827308', 3217),
(3219, '654321', '布尔津县', 3, '0906', '86.874923,47.702163', 3217),
(3220, '654322', '富蕴县', 3, '0906', '89.525504,46.994115', 3217),
(3221, '654323', '福海县', 3, '0906', '87.486703,47.111918', 3217),
(3222, '654324', '哈巴河县', 3, '0906', '86.418621,48.060846', 3217),
(3223, '654325', '青河县', 3, '0906', '90.37555,46.679113', 3217),
(3224, '654326', '吉木乃县', 3, '0906', '85.874096,47.443101', 3217),
(3225, '659006', '铁门关市', 2, '1996', '85.501217,41.82725', 3108),
(3226, '659003', '图木舒克市', 2, '1998', '79.073963,39.868965', 3108),
(3227, '659004', '五家渠市', 2, '1994', '87.54324,44.166756', 3108),
(3228, '710000', '台湾省', 1, '1886', '121.509062,25.044332', -1),
(3229, '810000', '香港特别行政区', 1, '1852', '114.171203,22.277468', -1),
(3230, '810001', '中西区', 3, '1852', '114.154373,22.281981', 3229),
(3231, '810002', '湾仔区', 3, '1852', '114.182915,22.276389', 3229),
(3232, '810003', '东区', 3, '1852', '114.226003,22.279693', 3229),
(3233, '810004', '南区', 3, '1852', '114.160012,22.245897', 3229),
(3234, '810005', '油尖旺区', 3, '1852', '114.173332,22.311704', 3229),
(3235, '810006', '深水埗区', 3, '1852', '114.163242,22.333854', 3229),
(3236, '810007', '九龙城区', 3, '1852', '114.192847,22.31251', 3229),
(3237, '810008', '黄大仙区', 3, '1852', '114.203886,22.336321', 3229),
(3238, '810009', '观塘区', 3, '1852', '114.214054,22.320838', 3229),
(3239, '810010', '荃湾区', 3, '1852', '114.121079,22.368306', 3229),
(3240, '810011', '屯门区', 3, '1852', '113.976574,22.393844', 3229),
(3241, '810012', '元朗区', 3, '1852', '114.032438,22.441428', 3229),
(3243, '810013', '北区', 3, '1852', '114.147364,22.496104', 3229),
(3245, '810014', '大埔区', 3, '1852', '114.171743,22.445653', 3229),
(3246, '810015', '西贡区', 3, '1852', '114.264645,22.314213', 3229),
(3247, '810016', '沙田区', 3, '1852', '114.195365,22.379532', 3229),
(3248, '810017', '葵青区', 3, '1852', '114.139319,22.363877', 3229),
(3249, '810018', '离岛区', 3, '1852', '113.94612,22.286408', 3229),
(3251, '820000', '澳门特别行政区', 1, '1853', '113.543028,22.186835', -1),
(3252, '820001', '花地玛堂区', 3, '1853', '113.552896,22.20787', 3251),
(3253, '820002', '花王堂区', 3, '1853', '113.548961,22.199207', 3251),
(3254, '820003', '望德堂区', 3, '1853', '113.550183,22.193721', 3251),
(3255, '820004', '大堂区', 3, '1853', '113.553647,22.188539', 3251),
(3256, '820005', '风顺堂区', 3, '1853', '113.541928,22.187368', 3251),
(3257, '820006', '嘉模堂区', 3, '1853', '113.558705,22.15376', 3251),
(3258, '820007', '路凼填海区', 3, '1853', '113.569599,22.13663', 3251),
(3259, '820008', '圣方济各堂区', 3, '1853', '113.559954,22.123486', 3251),
(3260, '340130', '滨湖区', 3, '0551', '117.2956388330,31.7449673481', 1022),
(3261, '340131', '新站区', 3, '0551', '117.2956388330,31.7449673481', 1022),
(3262, '340132', '政务区', 3, '0551', '117.2956388330,31.7449673481', 1022),
(3263, '340151', '高新区', 3, '0551', '117.142251,31.837535', 1022),
(3264, '340130', '滨湖区', 3, '0551', '117.303548,31.761418', 1022),
(3265, '340131', '新站区', 3, '0551', '117.354205,31.897019', 1022),
(3266, '340132', '政务区', 3, '0551', '117.235019,31.827855', 1022),
(3267, '340133', '经开区', 3, '0551', '117.254509,31.791224', 1022);

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_appraisers`
--

CREATE TABLE `mf_assessment_appraisers` (
  `id` int(11) NOT NULL,
  `cop_id` int(11) NOT NULL COMMENT '公司id',
  `create_userid` int(11) NOT NULL COMMENT '录入人',
  `createtime` int(10) NOT NULL COMMENT '录入时间',
  `create_ip` varchar(64) NOT NULL COMMENT '录入IP',
  `last_userid` int(11) NOT NULL COMMENT '修改人',
  `lasttime` int(10) NOT NULL COMMENT '修改时间',
  `last_ip` varchar(64) NOT NULL COMMENT '修改IP',
  `appraiser_name` varchar(1024) NOT NULL COMMENT '评估师姓名',
  `licence_no` varchar(32) NOT NULL COMMENT '证书编号',
  `mobile` varchar(32) NOT NULL COMMENT '联系方式',
  `licence_image` varchar(1024) NOT NULL COMMENT '证书路径'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评估师';

--
-- 转存表中的数据 `mf_assessment_appraisers`
--

INSERT INTO `mf_assessment_appraisers` (`id`, `cop_id`, `create_userid`, `createtime`, `create_ip`, `last_userid`, `lasttime`, `last_ip`, `appraiser_name`, `licence_no`, `mobile`, `licence_image`) VALUES
(1, 1, 0, 1575470147, '', 1, 1604890830, '', '评大师', '3520110048', '18305515152', '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_items`
--

CREATE TABLE `mf_assessment_items` (
  `id` int(11) NOT NULL COMMENT '编号',
  `address` varchar(2048) NOT NULL COMMENT '地址',
  `latitude` double NOT NULL COMMENT '纬度',
  `longitude` double NOT NULL COMMENT '经度',
  `obligee` varchar(96) NOT NULL COMMENT '权利人',
  `share_id` int(11) NOT NULL COMMENT '共有情况',
  `area` float NOT NULL COMMENT '建筑面积',
  `area_addtional` varchar(1024) DEFAULT NULL COMMENT '面积补充信息',
  `share_area` float NOT NULL COMMENT '分摊面积',
  `structure` int(11) NOT NULL COMMENT '建筑结构',
  `landright` int(11) NOT NULL COMMENT '土地使用权性质',
  `right_typeid` int(11) NOT NULL COMMENT '权利类型',
  `planpurpose` int(11) NOT NULL COMMENT '规划用途',
  `age` int(11) NOT NULL COMMENT '建筑年代',
  `right_limit` varchar(64) NOT NULL COMMENT '使用期限年月',
  `floor_count` int(11) NOT NULL COMMENT '楼层总数',
  `location_floor` varchar(23) NOT NULL COMMENT '所在层',
  `property_num` varchar(64) NOT NULL COMMENT '产权字号',
  `property_num_type` int(4) NOT NULL,
  `create_time` int(10) NOT NULL,
  `create_userid` int(11) NOT NULL,
  `state` int(11) NOT NULL COMMENT '状态表0无效 1有效',
  `document_from` varchar(64) NOT NULL COMMENT '来源',
  `need_prereport` tinyint(1) NOT NULL COMMENT '是否需要预评',
  `need_report` tinyint(1) NOT NULL COMMENT '是否需要正评',
  `cop_id` int(11) NOT NULL COMMENT '所属企业',
  `prereport_no` varchar(32) NOT NULL COMMENT '预评编号',
  `prereport_version` int(11) NOT NULL COMMENT '预评版本号',
  `report_no` int(11) NOT NULL,
  `has_prereport` tinyint(1) NOT NULL,
  `has_report` tinyint(1) NOT NULL,
  `prereport_appraiser_id` int(11) NOT NULL,
  `prereport_contents` text NOT NULL,
  `prereport_time` int(10) NOT NULL,
  `prereport_ip` varchar(64) NOT NULL,
  `prereport_userid` int(11) NOT NULL,
  `prereport_path` varchar(128) NOT NULL,
  `prereport_unit` float NOT NULL COMMENT '预评单价',
  `prereport_total` float NOT NULL COMMENT '预评总价',
  `prereport_state` int(11) NOT NULL COMMENT '状态0未预评 1已预评 2审核通过 3审核拒绝',
  `prereport_verify_userid` int(11) NOT NULL COMMENT '审核人',
  `prereport_verify_time` int(11) NOT NULL COMMENT '审核时间',
  `prereport_verify_ip` varchar(64) NOT NULL COMMENT '审核IP',
  `report_state` int(11) NOT NULL COMMENT '0未评估 1已评估 2已审核'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评估标的';

--
-- 转存表中的数据 `mf_assessment_items`
--

INSERT INTO `mf_assessment_items` (`id`, `address`, `latitude`, `longitude`, `obligee`, `share_id`, `area`, `area_addtional`, `share_area`, `structure`, `landright`, `right_typeid`, `planpurpose`, `age`, `right_limit`, `floor_count`, `location_floor`, `property_num`, `property_num_type`, `create_time`, `create_userid`, `state`, `document_from`, `need_prereport`, `need_report`, `cop_id`, `prereport_no`, `prereport_version`, `report_no`, `has_prereport`, `has_report`, `prereport_appraiser_id`, `prereport_contents`, `prereport_time`, `prereport_ip`, `prereport_userid`, `prereport_path`, `prereport_unit`, `prereport_total`, `prereport_state`, `prereport_verify_userid`, `prereport_verify_time`, `prereport_verify_ip`, `report_state`) VALUES
(34, '蜀山区田埠西路', 31.85885, 117.16085, '11111', 3, 93.91, '', 2.87, 1, 1, 1, 1, 2014, '2083年11月', 41, '35', '皖2017合不动产权第0111111号', 1, 1577789965, 1, 1, '', 0, 0, 0, 'W06452', 1, 0, 1, 0, 1, '<p style=\"text-align: center;\"><strong>房地产预评估报告</strong><br/></p><p style=\"text-align: center;\">合康嘉预估字（2019）第W06452号</p><p style=\"text-align: left;\">石楠：</p><p style=\"text-align: left;\">&nbsp; &nbsp; 受您委托，我公司对位于蜀山区田埠西路199号加侨悦山城G6幢3502住宅用房抵押价值进行评估，房地产权证号：皖2017合不动产权第0278679号，权利人：石楠；共有情况：按份共有，权利类型：国有建设用地使用权/房屋所有权，权利性质：出让，用途：成套住宅，房屋建筑面积：120.23㎡，使用期限：国有建设用地使用权2083年11月止，分摊土地面积：2.87㎡，估价对象总层数41层，估价对象所在层为第35层，房屋结构：钢混，竣工日期：2014年；</p><p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp;估价人员依据您提供的《房地产权证》及有关估价材料，根据国家有关法律、法规和政策要求，遵循中华人民共和国国家标准GB/T 50291－2015《房地产估价规范》的操作程序，进行了现场勘察，分析考虑了影响房地产估价的诸多因素，坚持公平、公正、公开的基本原则，运用科学、合理的评估方法，对估价对象房地产抵押价值进行了评估，经过综合测算，确定房地产在估价时点二〇一九年十二月三十一日的抵押单价为19000元/㎡，评估总价值为178.43（大写人民币：壹佰柒拾捌万肆仟叁佰元整）。<span style=\"font-family: 仿宋; font-size: 14px;\"></span></p><p style=\"text-align: right;\"><br/></p><p><span style=\"white-space: nowrap; font-size: 14px; font-family: 仿宋;\"></span></p><p><span style=\"white-space: nowrap;\">注：本预评估单用作估价委托人向银行申请房地产抵押贷款确定贷款额度提供市场价值咨询意见，非正式法律文书，不另作他用；</span></p><p><span style=\"white-space: nowrap;\"><br/></span></p><p><span style=\"white-space: nowrap;\">本次预评中未考虑估价对象的法定优先受偿款及其他的有关因素对估价结果的影响，估价结果应以委托方正式估价所需资料提供齐全后，我公司出具的正式报告结果为准。</span></p><p><span style=\"font-family: 仿宋;\"></span><br/></p><p><span style=\"white-space: nowrap;\"><br/></span></p><p><span style=\"white-space: nowrap;\"><br/></span></p><p style=\"text-align: right;\">合肥康嘉房地产评估咨询有限公司<br/></p><p style=\"text-align: right;\"><br/> 二〇一九年十二月三十一日 <br/></p><p style=\"text-align: left;\">${PAGE}</p><p style=\"text-align: center;\"><span style=\"font-family:微软雅黑, Microsoft YaHei;font-size:24px\">估价对象照片</span></p><p></p>', 1577926623, '114.102.184.202', 1, '', 19000, 178.43, 3, 1, 1608099315, '58.59.17.174', 0),
(51, '安徽省合肥市长丰县清颖路', 31.866842, 117.282699, '张先生 ', 3, 0, '', 0, 0, 0, 0, 0, 2014, '', 0, '', '皖2017合不动产权第0111111号', 2, 1608084827, 1, 1, '', 1, 0, 0, 'W2020-12-00015', 1, 0, 0, 0, 0, '不动产权证号', 1608084935, '', 0, '', 0, 0, 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_pictures`
--

CREATE TABLE `mf_assessment_pictures` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL COMMENT '标的id',
  `createtime` int(10) NOT NULL COMMENT '登记时间',
  `create_userid` int(11) NOT NULL COMMENT '登记人',
  `lasttime` int(10) NOT NULL COMMENT '最后时间',
  `last_userid` int(11) NOT NULL COMMENT '最后修改',
  `path` varchar(1024) NOT NULL COMMENT '图片路径',
  `typeid` int(11) NOT NULL COMMENT '图片类别',
  `mem` varchar(1024) NOT NULL COMMENT '说明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼盘相册';

--
-- 转存表中的数据 `mf_assessment_pictures`
--

INSERT INTO `mf_assessment_pictures` (`id`, `item_id`, `createtime`, `create_userid`, `lasttime`, `last_userid`, `path`, `typeid`, `mem`) VALUES
(11, 4, 1563411814, 1, 0, 1, '/uploads/20190718/4d90cae0ed57eca276e50f8236af8ddf.png', 3, '测试图'),
(12, 4, 1563460452, 1, 0, 1, '/uploads/20190718/a773bee75aa75d08b436f212959fa2b3.jpg', 1, ''),
(13, 4, 1563807665, 1, 0, 1, '/uploads/20190718/99d056705e0bb460dded58ab1d2b012c.jpg', 2, ''),
(14, 5, 1566552981, 1, 0, 1, '/uploads/20190823/6df73b572c3f722ec0c5370c8cbc0d0b.jpg', 1, ''),
(15, 5, 1566552981, 1, 0, 1, '/uploads/20190823/9d1b41d54f97b5c58e0730982ff94f41.jpg', 1, ''),
(16, 5, 1566552981, 1, 0, 1, '/uploads/20190823/69b408822331b93faf2fe01b30cfd997.jpg', 1, ''),
(17, 5, 1566553046, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 2, ''),
(18, 5, 1566553046, 1, 0, 1, '/uploads/20190823/7003dc5a7bf66eebe33cfce58eb7786d.jpg', 2, ''),
(19, 4, 1573739277, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(20, 6, 1573827102, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(21, 7, 1573827125, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(22, 8, 1573827148, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(23, 9, 1573827153, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(24, 10, 1573827200, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(25, 11, 1573827465, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(26, 12, 1573827620, 1, 0, 1, '/uploads/20191113/39ceebc4d672eaab4d5239abf81208cb.jpg', 1, ''),
(27, 12, 1573827620, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(28, 12, 1573827620, 1, 0, 1, '/uploads/20190823/69b408822331b93faf2fe01b30cfd997.jpg', 1, ''),
(29, 13, 1573828216, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(30, 16, 1575868378, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(31, 16, 1575868378, 1, 0, 1, '/uploads/20190823/69b408822331b93faf2fe01b30cfd997.jpg', 2, ''),
(32, 18, 1575894530, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(33, 18, 1575894558, 1, 0, 1, '/uploads/20191209/50ff22c2f01cd60c7f6060116a20e91a.jpg', 2, ''),
(34, 19, 1576145308, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(35, 19, 1576145308, 1, 0, 1, '/uploads/20190823/69b408822331b93faf2fe01b30cfd997.jpg', 2, ''),
(36, 19, 1576145308, 1, 0, 1, '/uploads/20190823/7003dc5a7bf66eebe33cfce58eb7786d.jpg', 2, ''),
(37, 19, 1576145308, 1, 0, 1, '/uploads/20190823/9d1b41d54f97b5c58e0730982ff94f41.jpg', 2, ''),
(38, 19, 1576145308, 1, 0, 1, '/uploads/20190823/6df73b572c3f722ec0c5370c8cbc0d0b.jpg', 2, ''),
(39, 20, 1576219316, 1, 0, 1, '/uploads/20191212/0cd8a00bbaae3ba89f9bab3c10775621.png', 1, ''),
(40, 20, 1576219316, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 2, ''),
(41, 21, 1576505755, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(42, 22, 1576506692, 1, 0, 1, '/uploads/20191212/0cd8a00bbaae3ba89f9bab3c10775621.png', 1, ''),
(43, 22, 1576506774, 1, 0, 1, '/uploads/20191212/0cd8a00bbaae3ba89f9bab3c10775621.png', 2, ''),
(44, 22, 1576506774, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 3, ''),
(45, 22, 1576506774, 1, 0, 1, '/uploads/20190823/7003dc5a7bf66eebe33cfce58eb7786d.jpg', 3, ''),
(46, 23, 1576506909, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(47, 23, 1576506909, 1, 0, 1, '/uploads/20190823/7003dc5a7bf66eebe33cfce58eb7786d.jpg', 1, ''),
(48, 23, 1576506909, 1, 0, 1, '/uploads/20190808/f237d558cd9e33cf5f87ef4cf66e0f7b.png', 1, ''),
(49, 25, 1576559992, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(50, 25, 1576559992, 1, 0, 1, '/uploads/20190823/7003dc5a7bf66eebe33cfce58eb7786d.jpg', 1, ''),
(51, 25, 1576559992, 1, 0, 1, '/uploads/20190808/f237d558cd9e33cf5f87ef4cf66e0f7b.png', 1, ''),
(52, 25, 1576559992, 1, 0, 1, '/uploads/20190718/a773bee75aa75d08b436f212959fa2b3.jpg', 1, ''),
(53, 26, 1576562481, 1, 0, 1, '/uploads/20191212/0cd8a00bbaae3ba89f9bab3c10775621.png', 1, ''),
(54, 26, 1576562482, 1, 0, 1, '/uploads/20190823/6988582cbf8873f8bdea6d8405208a25.jpg', 1, ''),
(56, 26, 1576562482, 1, 0, 1, '/uploads/20190823/9d1b41d54f97b5c58e0730982ff94f41.jpg', 1, ''),
(57, 26, 1576562482, 1, 0, 1, '/uploads/20190823/7003dc5a7bf66eebe33cfce58eb7786d.jpg', 1, ''),
(58, 28, 1576719125, 1, 0, 1, '/uploads/20191219/3a05e96194cd37cd1a619224084451fa.jpg', 1, ''),
(59, 28, 1576719125, 1, 0, 1, '/uploads/20191219/5e29330dac1ab55c0f2b943c84a1554a.jpg', 1, ''),
(60, 28, 1576719125, 1, 0, 1, '/uploads/20191219/d34fcc0f90b36de9731448c18b749660.jpg', 1, ''),
(61, 28, 1576719943, 1, 0, 1, '/uploads/20191219/52db02a6d9313c7f64d9567ab77dc08c.jpg', 2, ''),
(62, 28, 1576719943, 1, 0, 1, '/uploads/20191219/c25f5100c17110fb0e7c67024130b062.jpg', 2, ''),
(63, 28, 1576719943, 1, 0, 1, '/uploads/20191219/f0d142bf8c23912991eff68a5959546f.jpg', 2, ''),
(65, 31, 1577325382, 1, 0, 1, '/uploads/20191226/aa87cfb2b2e55708bce2581a84dede21.jpg', 1, ''),
(66, 31, 1577325382, 1, 0, 1, '/uploads/20191226/d315ae0a096999559f3ae3a2778749ac.jpg', 1, ''),
(67, 31, 1577325382, 1, 0, 1, '/uploads/20191226/3d61bfb1b0f36aacf400e0fbde3d0276.jpg', 1, ''),
(68, 31, 1577325382, 1, 0, 1, '/uploads/20191226/0c720ec82c9b569d714ba21e322bcd44.jpg', 2, ''),
(69, 31, 1577325382, 1, 0, 1, '/uploads/20191226/590342bab3f5b475e17831d7842f29bf.jpg', 2, ''),
(70, 31, 1577325382, 1, 0, 1, '/uploads/20191226/14baa9310d33f8426d2f91f8f5ab0483.jpg', 2, ''),
(71, 31, 1577325382, 1, 0, 1, '/uploads/20191226/3f4df25236257adad4fcb7f637dd3e6e.jpg', 2, ''),
(72, 31, 1577325382, 1, 0, 1, '/uploads/20191226/b2143489d5cecbd8e26017541b671f0b.jpg', 2, ''),
(73, 31, 1577325382, 1, 0, 1, '/uploads/20191226/a96a9931a388fb46cce291f511c3c694.jpg', 2, ''),
(74, 31, 1577325382, 1, 0, 1, '/uploads/20191226/d2d99ded2dbc8b32fd77388a11757e20.jpg', 2, ''),
(75, 31, 1577325382, 1, 0, 1, '/uploads/20191226/5ddcaeb7bdff1f2c535269b39bccf008.jpg', 2, ''),
(76, 32, 1577780749, 1, 0, 1, '/uploads/20191231/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 1, ''),
(77, 32, 1577780749, 1, 0, 1, '/uploads/20191231/4840fc4cd176d3b52f4d689b784e9341.jpg', 1, ''),
(78, 32, 1577780769, 1, 0, 1, '/uploads/20191231/0569efe6fab98296fcb835360a422184.jpg', 2, ''),
(79, 32, 1577780769, 1, 0, 1, '/uploads/20191231/4bed3e158b3b1cac5027da11f905f78c.jpg', 2, ''),
(80, 32, 1577780769, 1, 0, 1, '/uploads/20191231/c152640c8dfa93583bf211fe1e2d95e8.jpg', 2, ''),
(81, 33, 1577783665, 1, 0, 1, '/uploads/20191231/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 1, ''),
(82, 33, 1577783665, 1, 0, 1, '/uploads/20191231/4840fc4cd176d3b52f4d689b784e9341.jpg', 1, ''),
(83, 33, 1577783665, 1, 0, 1, '/uploads/20191231/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 1, ''),
(84, 33, 1577783665, 1, 0, 1, '/uploads/20191231/4840fc4cd176d3b52f4d689b784e9341.jpg', 1, ''),
(85, 33, 1577783665, 1, 0, 1, '/uploads/20191231/0569efe6fab98296fcb835360a422184.jpg', 1, ''),
(86, 33, 1577783665, 1, 0, 1, '/uploads/20191231/4bed3e158b3b1cac5027da11f905f78c.jpg', 1, ''),
(87, 33, 1577783665, 1, 0, 1, '/uploads/20191231/c152640c8dfa93583bf211fe1e2d95e8.jpg', 1, ''),
(88, 33, 1577783665, 1, 0, 1, '/uploads/20191231/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 2, ''),
(89, 33, 1577783665, 1, 0, 1, '/uploads/20191231/4840fc4cd176d3b52f4d689b784e9341.jpg', 2, ''),
(90, 33, 1577783665, 1, 0, 1, '/uploads/20191231/0569efe6fab98296fcb835360a422184.jpg', 2, ''),
(91, 33, 1577783665, 1, 0, 1, '/uploads/20191231/4bed3e158b3b1cac5027da11f905f78c.jpg', 2, ''),
(92, 33, 1577783665, 1, 0, 1, '/uploads/20191231/c152640c8dfa93583bf211fe1e2d95e8.jpg', 2, ''),
(93, 33, 1577783665, 1, 0, 1, '/uploads/20191231/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 2, ''),
(94, 33, 1577783665, 1, 0, 1, '/uploads/20191231/4840fc4cd176d3b52f4d689b784e9341.jpg', 2, ''),
(95, 33, 1577783665, 1, 0, 1, '/uploads/20191231/0569efe6fab98296fcb835360a422184.jpg', 2, ''),
(96, 34, 1577789965, 1, 0, 1, '/uploads/20191231/c52315a92a0615ed474e27d21ea086a9.jpg', 1, ''),
(97, 34, 1577789965, 1, 0, 1, '/uploads/20191231/4045520fc421f77e6a05e6fc444396f1.jpg', 1, ''),
(98, 34, 1577789965, 1, 0, 1, '/uploads/20191231/c52315a92a0615ed474e27d21ea086a9.jpg', 2, ''),
(99, 34, 1577789965, 1, 0, 1, '/uploads/20191231/4045520fc421f77e6a05e6fc444396f1.jpg', 2, ''),
(100, 34, 1577789965, 1, 0, 1, '/uploads/20191231/896086d8497fb1162f78da29e13dcfb4.jpg', 2, ''),
(101, 34, 1577789965, 1, 0, 1, '/uploads/20191231/52613e2b013aefa713c45002df1cc275.jpg', 2, ''),
(102, 34, 1577789965, 1, 0, 1, '/uploads/20191231/0584d90773fc3a21814e6ae628679c5f.jpg', 2, ''),
(103, 34, 1577789965, 1, 0, 1, '/uploads/20191231/5286c6e6bdb086dd8782783c708d9182.jpg', 2, ''),
(104, 34, 1577789965, 1, 0, 1, '/uploads/20191231/84ab175c4c9d28473406a224a37b562a.jpg', 2, ''),
(105, 36, 1577927072, 1, 0, 1, '/uploads/20200102/4840fc4cd176d3b52f4d689b784e9341.jpg', 1, ''),
(106, 36, 1577927072, 1, 0, 1, '/uploads/20200102/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 1, ''),
(107, 36, 1577927072, 1, 0, 1, '/uploads/20200102/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 2, ''),
(108, 36, 1577927072, 1, 0, 1, '/uploads/20200102/4840fc4cd176d3b52f4d689b784e9341.jpg', 2, ''),
(109, 36, 1577927072, 1, 0, 1, '/uploads/20200102/0569efe6fab98296fcb835360a422184.jpg', 2, ''),
(110, 36, 1577927072, 1, 0, 1, '/uploads/20200102/4bed3e158b3b1cac5027da11f905f78c.jpg', 2, ''),
(111, 36, 1577927072, 1, 0, 1, '/uploads/20200102/c152640c8dfa93583bf211fe1e2d95e8.jpg', 2, ''),
(112, 35, 1577929591, 1, 0, 1, '/uploads/20200102/aa87cfb2b2e55708bce2581a84dede21.jpg', 1, ''),
(113, 38, 1577929638, 1, 0, 1, '/uploads/20200102/aa87cfb2b2e55708bce2581a84dede21.jpg', 1, ''),
(114, 38, 1577930643, 1, 0, 1, '/uploads/20200102/0c720ec82c9b569d714ba21e322bcd44.jpg', 2, ''),
(115, 38, 1577930643, 1, 0, 1, '/uploads/20200102/590342bab3f5b475e17831d7842f29bf.jpg', 2, ''),
(116, 38, 1577930643, 1, 0, 1, '/uploads/20200102/14baa9310d33f8426d2f91f8f5ab0483.jpg', 2, ''),
(117, 38, 1577930643, 1, 0, 1, '/uploads/20200102/3f4df25236257adad4fcb7f637dd3e6e.jpg', 2, ''),
(118, 38, 1577930643, 1, 0, 1, '/uploads/20200102/b2143489d5cecbd8e26017541b671f0b.jpg', 2, ''),
(119, 38, 1577930643, 1, 0, 1, '/uploads/20200102/a96a9931a388fb46cce291f511c3c694.jpg', 2, ''),
(120, 38, 1577930643, 1, 0, 1, '/uploads/20200102/d2d99ded2dbc8b32fd77388a11757e20.jpg', 2, ''),
(121, 38, 1577930643, 1, 0, 1, '/uploads/20200102/5ddcaeb7bdff1f2c535269b39bccf008.jpg', 2, ''),
(122, 42, 1578273354, 1, 0, 1, '/uploads/20200106/c930a3f10fdf4b7e6ae93c48ba2ef380.jpg', 1, ''),
(123, 42, 1578273354, 1, 0, 1, '/uploads/20200106/4840fc4cd176d3b52f4d689b784e9341.jpg', 1, ''),
(124, 42, 1578273354, 1, 0, 1, '/uploads/20200106/0569efe6fab98296fcb835360a422184.jpg', 2, ''),
(125, 42, 1578273354, 1, 0, 1, '/uploads/20200106/4bed3e158b3b1cac5027da11f905f78c.jpg', 2, ''),
(126, 42, 1578273354, 1, 0, 1, '/uploads/20200106/c152640c8dfa93583bf211fe1e2d95e8.jpg', 2, ''),
(127, 44, 1578276858, 1, 0, 1, '/uploads/20200106/e73964c3bac134db282548a36f51f030.jpg', 1, ''),
(128, 44, 1578276858, 1, 0, 1, '/uploads/20200106/69ef3c860c4a3661c003b57eba968f27.jpg', 1, ''),
(129, 44, 1578276858, 1, 0, 1, '/uploads/20200106/17577ae371379a632102affe9c3d46ef.jpg', 2, ''),
(130, 44, 1578276858, 1, 0, 1, '/uploads/20200106/02b02de5080bc5609360b749c38d79aa.jpg', 2, ''),
(131, 44, 1578276858, 1, 0, 1, '/uploads/20200106/658b11c42886b08e7625adca919cba22.jpg', 2, ''),
(132, 44, 1578276858, 1, 0, 1, '/uploads/20200106/b4d5c10041fe7372c46536415d5f8a8f.jpg', 2, ''),
(133, 44, 1578276858, 1, 0, 1, '/uploads/20200106/5b358cc7456efa5aad9c56b854d62661.jpg', 2, ''),
(134, 44, 1578276858, 1, 0, 1, '/uploads/20200106/4c29dcb0ba9175fe776aa0407b7258db.jpg', 2, ''),
(135, 44, 1578276858, 1, 0, 1, '/uploads/20200106/2893857fe69c3c6952444b870865c265.jpg', 2, ''),
(136, 44, 1578276858, 1, 0, 1, '/uploads/20200106/01db9fad8611eddc26ac13e3c00f18ce.jpg', 2, ''),
(137, 44, 1578276858, 1, 0, 1, '/uploads/20200106/28bd2596e0719a86d83b8b44e467555f.jpg', 2, ''),
(138, 45, 1578277302, 1, 0, 1, '/uploads/20200106/69ef3c860c4a3661c003b57eba968f27.jpg', 1, ''),
(139, 45, 1578277302, 1, 0, 1, '/uploads/20200106/e73964c3bac134db282548a36f51f030.jpg', 1, ''),
(140, 45, 1578277302, 1, 0, 1, '/uploads/20200106/17577ae371379a632102affe9c3d46ef.jpg', 2, ''),
(141, 45, 1578277302, 1, 0, 1, '/uploads/20200106/02b02de5080bc5609360b749c38d79aa.jpg', 2, ''),
(142, 45, 1578277302, 1, 0, 1, '/uploads/20200106/658b11c42886b08e7625adca919cba22.jpg', 2, ''),
(143, 45, 1578277302, 1, 0, 1, '/uploads/20200106/b4d5c10041fe7372c46536415d5f8a8f.jpg', 2, ''),
(144, 45, 1578277302, 1, 0, 1, '/uploads/20200106/5b358cc7456efa5aad9c56b854d62661.jpg', 2, ''),
(145, 45, 1578277302, 1, 0, 1, '/uploads/20200106/4c29dcb0ba9175fe776aa0407b7258db.jpg', 2, ''),
(146, 45, 1578277302, 1, 0, 1, '/uploads/20200106/2893857fe69c3c6952444b870865c265.jpg', 2, ''),
(147, 45, 1578277302, 1, 0, 1, '/uploads/20200106/01db9fad8611eddc26ac13e3c00f18ce.jpg', 2, ''),
(148, 45, 1578277302, 1, 0, 1, '/uploads/20200106/28bd2596e0719a86d83b8b44e467555f.jpg', 2, ''),
(149, 47, 1578448766, 21, 0, 21, '/uploads/20200108/ded0c474b15d2831b6c7be97acaa21fb.jpg', 1, ''),
(150, 47, 1578448766, 21, 0, 21, '/uploads/20200108/08c949fd8e378b97b698778063548596.jpg', 1, ''),
(151, 47, 1578448766, 21, 0, 21, '/uploads/20200108/008c4b9fa8b116d0e9cfe7dff1780c0e.jpg', 1, ''),
(152, 47, 1578448766, 21, 0, 21, '/uploads/20200108/bbd2975676096849f83865a3e40e1936.jpg', 1, ''),
(153, 47, 1578448766, 21, 0, 21, '/uploads/20200108/20821e013d96b0fe50259ef3292febe6.jpg', 1, ''),
(154, 47, 1578448766, 21, 0, 21, '/uploads/20200108/1b4b8ef1c16fdf859e2af4ec27955157.jpg', 2, ''),
(155, 47, 1578448766, 21, 0, 21, '/uploads/20200108/c9ca985b5ae399e32ddaa22b46674104.jpg', 2, ''),
(156, 47, 1578448766, 21, 0, 21, '/uploads/20200108/864d47f40a75d326c210a054ea4ec94b.jpg', 2, ''),
(157, 47, 1578448766, 21, 0, 21, '/uploads/20200108/df14e7a1cfe2302687c36a2b782a4402.jpg', 2, ''),
(158, 47, 1578448766, 21, 0, 21, '/uploads/20200108/48ef1bd40f31538e3d9a50c0317c16ba.jpg', 2, ''),
(159, 47, 1578448766, 21, 0, 21, '/uploads/20200108/0e281ddaf1ecddfc7220c4c1b0f682b2.jpg', 2, ''),
(160, 47, 1578448766, 21, 0, 21, '/uploads/20200108/c245e775b5ef35096fe64c2dc391d454.jpg', 2, ''),
(161, 47, 1578448766, 21, 0, 21, '/uploads/20200108/7ba4b798c071e86cc5cb08028391c4ff.jpg', 2, ''),
(162, 47, 1578448766, 21, 0, 21, '/uploads/20200108/238d56a16e14d3db10de12badd061d26.jpg', 2, ''),
(163, 47, 1578448766, 21, 0, 21, '/uploads/20200108/5201af2ac678a36f3039443d1e689354.jpg', 2, ''),
(164, 47, 1578448766, 21, 0, 21, '/uploads/20200108/fbcf96c068b9c3a30faaa3f8efcba6f0.jpg', 2, ''),
(165, 47, 1578448766, 21, 0, 21, '/uploads/20200108/c5a523121cccdc3a7fa2bcc0f9b4ea56.jpg', 2, ''),
(166, 48, 1579336036, 1, 0, 1, '/uploads/20200118/45a1cf6a435244399e2a3ddb005a6bcc.jpg', 1, ''),
(167, 48, 1579336036, 1, 0, 1, '/uploads/20200118/2c7244e7381ce1e03389d4f033e5bd45.jpg', 1, ''),
(168, 48, 1579336050, 1, 0, 1, '/uploads/20200118/ef5ee9072d7e1c358913121277dc587b.jpg', 2, ''),
(169, 48, 1579336050, 1, 0, 1, '/uploads/20200118/d392228e8bc0d0a4ac48a01af8cff02c.jpg', 2, ''),
(170, 48, 1579336050, 1, 0, 1, '/uploads/20200118/c5632f654961620f09220156505f687e.jpg', 2, ''),
(171, 49, 1579336734, 1, 0, 1, '/uploads/20200118/1d643d5c7cfc527521b420d63bd3f2fa.jpg', 1, ''),
(172, 49, 1579336734, 1, 0, 1, '/uploads/20200118/a4b32eaecae7edbc7cf23be5aa122ccd.jpg', 2, ''),
(173, 49, 1579336734, 1, 0, 1, '/uploads/20200118/9f32de09ade15deae4e636ac4f10cc27.jpg', 2, ''),
(174, 49, 1579336734, 1, 0, 1, '/uploads/20200118/bde40ae2e17d5e220632eec0ee79dfd4.jpg', 2, '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_picturetype`
--

CREATE TABLE `mf_assessment_picturetype` (
  `id` int(11) NOT NULL,
  `type_name` varchar(16) NOT NULL COMMENT '类型',
  `sort` int(11) NOT NULL COMMENT '排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼盘相册类型';

--
-- 转存表中的数据 `mf_assessment_picturetype`
--

INSERT INTO `mf_assessment_picturetype` (`id`, `type_name`, `sort`) VALUES
(1, '产证信息', 1),
(2, '估价对象照片', 1),
(4, '公司材料附件', 0);

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_prereport`
--

CREATE TABLE `mf_assessment_prereport` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL COMMENT '标的编号',
  `prereport_no` varchar(32) NOT NULL COMMENT '预评编号',
  `create_time` int(10) NOT NULL,
  `create_userid` int(11) NOT NULL COMMENT '操作人',
  `create_ip` varchar(64) NOT NULL COMMENT '操作IP',
  `contents` text NOT NULL COMMENT '内容',
  `version` int(11) NOT NULL COMMENT '版本'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预评版本管理';

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_propertynumtype`
--

CREATE TABLE `mf_assessment_propertynumtype` (
  `id` int(11) NOT NULL,
  `property_num_type` varchar(32) NOT NULL COMMENT '产权证类型'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产权证类型';

--
-- 转存表中的数据 `mf_assessment_propertynumtype`
--

INSERT INTO `mf_assessment_propertynumtype` (`id`, `property_num_type`) VALUES
(1, '房地产权证号'),
(2, '不动产权证号');

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_report`
--

CREATE TABLE `mf_assessment_report` (
  `contents` text NOT NULL COMMENT '评估内容',
  `cop_id` int(11) NOT NULL COMMENT '所属公司',
  `report_path` varchar(1024) NOT NULL COMMENT '生成pdf地址',
  `createtime` int(10) NOT NULL COMMENT '创建时间',
  `create_userid` int(11) NOT NULL COMMENT '创建人',
  `create_ip` varchar(64) NOT NULL,
  `lasttime` int(10) NOT NULL,
  `last_userid` int(11) NOT NULL,
  `last_ip` varchar(64) NOT NULL,
  `id` int(11) NOT NULL COMMENT '评估标的ID',
  `appraiser_id` int(64) NOT NULL COMMENT '评估师'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='预评报告';

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_righttype`
--

CREATE TABLE `mf_assessment_righttype` (
  `id` int(11) NOT NULL,
  `righttype` varchar(32) NOT NULL COMMENT '权利类型'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权利类型';

--
-- 转存表中的数据 `mf_assessment_righttype`
--

INSERT INTO `mf_assessment_righttype` (`id`, `righttype`) VALUES
(1, '国有建设用地使用权/房屋所有权');

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_shares`
--

CREATE TABLE `mf_assessment_shares` (
  `id` int(11) NOT NULL,
  `description` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='共有情况';

--
-- 转存表中的数据 `mf_assessment_shares`
--

INSERT INTO `mf_assessment_shares` (`id`, `description`) VALUES
(1, '单独所有'),
(2, '共同共有'),
(3, '按份共有'),
(4, '私有产权');

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_templates`
--

CREATE TABLE `mf_assessment_templates` (
  `id` int(11) NOT NULL,
  `template_typeid` int(11) NOT NULL COMMENT '模板类型',
  `cop_id` int(11) NOT NULL COMMENT '所属企业',
  `template` text NOT NULL COMMENT '模板',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `create_userid` int(11) NOT NULL COMMENT '创建人',
  `create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `last_time` int(10) NOT NULL COMMENT '修改时间',
  `last_userid` int(11) NOT NULL COMMENT '修改人',
  `last_ip` varchar(64) NOT NULL COMMENT '修改IP'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评估模板';

--
-- 转存表中的数据 `mf_assessment_templates`
--

INSERT INTO `mf_assessment_templates` (`id`, `template_typeid`, `cop_id`, `template`, `create_time`, `create_userid`, `create_ip`, `last_time`, `last_userid`, `last_ip`) VALUES
(1, 1, 0, '<p style=\"text-align: center;\"><strong>房地产预评估报告</strong><br/></p><p style=\"text-align: center;\">合康嘉预估字（2019）第${PREREPORT_NO}号</p><p style=\"text-align: left;\">${OBLIGEE}：</p><p style=\"text-align: left;\">&nbsp; &nbsp; 受您委托，我公司对位于${ADDRESS}<span style=\"white-space: normal;\">${PLANPURPOSE}</span>用房抵押价值进行评估，房地产权证号：${PROPERTY_NUM}，权利人：${OBLIGEE}；共有情况：${SHARETYPE}，权利类型：${RIGHTTYPE}，权利性质：${LANDRIGHT}，用途：${PLANPURPOSE}，房屋建筑面积：${AREA}㎡${AREA_ADDITIONAL}，使用期限：国有建设用地使用权${RIGHTLIMIT}止，分摊土地面积：${SHAREAREA}㎡，估价对象总层数${FLOORCOUNT}层，估价对象所在层为第${LOCATION_FLOOR}层，房屋结构：${STRUCTURE}，竣工日期：${FINISHYEAR}年；<span style=\"font-family: 宋体; font-size: 14pt;\"><span style=\"font-family: 仿宋; font-size: 14px;\"></span></span></p><p style=\"text-align: left;\">&nbsp; &nbsp; &nbsp;估价人员依据您提供的《房地产权证》及有关估价材料，根据国家有关法律、法规和政策要求，遵循中华人民共和国国家标准GB/T 50291－2015《房地产估价规范》的操作程序，进行了现场勘察，分析考虑了影响房地产估价的诸多因素，坚持公平、公正、公开的基本原则，运用科学、合理的评估方法，对估价对象房地产抵押价值进行了评估，经过综合测算，确定房地产在估价时点${DATE_UPPER}的抵押价值：</p><p style=\"text-align: left;\">&nbsp; &nbsp; 单价为<strong>${PREREPORT_UNIT}元/㎡</strong>，</p><p style=\"text-align: left;\">&nbsp; &nbsp; 评估总价值为<strong>${PREREPORT_TOTAL}万元</strong>（大写人民币：<strong>${PRICE_TOTLE_CHINESE}</strong>）。<span style=\"font-family: 仿宋; font-size: 14px;\"></span></p><p style=\"text-align: right;\"><br/></p><p><span style=\"white-space: nowrap; font-size: 14px; font-family: 仿宋;\"></span></p><p><span style=\"white-space: nowrap;\">注：本预评估单用作估价委托人向银行申请房地产抵押贷款确定贷款额度提供市场价值咨询意见，非正式法律文书，不另作他用；</span></p><p><span style=\"white-space: nowrap;\"><br/></span></p><p><span style=\"white-space: nowrap;\">本次预评中未考虑估价对象的法定优先受偿款及其他的有关因素对估价结果的影响，估价结果应以委托方正式估价所需资料提供齐全后，我公司出具的正式报告结果为准。</span></p><p><span style=\"font-family: 仿宋;\"></span><br/></p><p><span style=\"white-space: nowrap;\"><br/></span></p><p><span style=\"white-space: nowrap;\"><br/></span></p><p style=\"text-align: right;\">${COP_NAME}<br/></p><p style=\"text-align: right;\"><br/> ${DATE_UPPER} <br/></p><p style=\"text-align: left;\">${PAGE}</p><p style=\"text-align: center;\"><span style=\"font-family:微软雅黑, Microsoft YaHei;font-size:24px\">估价对象照片</span></p><p>${PICTURES}</p>', 0, 0, '', 0, 0, ''),
(2, 2, 0, '<p style=\"text-align: center;\"><strong><span style=\"font-size:24px\">房地产预评估报告</span></strong><br/></p><p style=\"text-align: center;\">合康嘉预估字（2019）第{$NO}号</p><p style=\"text-align: left;\">{$CUSTOMER}：</p><p style=\"text-align: left;\">&nbsp; &nbsp; 受您委托，我公司估价人员依据您提供的《房地产权证》及有关估价材料，根据国家有关法律、法规和政策要求，遵循中华人民共和国国家标准GB/T 50291－2015《房地产估价规范》的操作程序，进行了现场勘察，分析考虑了影响房地产估价的诸多因素，坚持公平、公正、公开的基本原则，运用科学、合理的评估方法，对估价对象房地产抵押价值进行了评估，经过综合测算，确定房地产在估价时点二零一九年十月十一日的抵押价值合计为：详见下列估价结果一览表：</p><p style=\"text-align: left;\"><br/></p><p style=\"text-align: right;\">估价对象评估总价值：￥639.76万元</p>', 0, 0, '', 0, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_assessment_templatetype`
--

CREATE TABLE `mf_assessment_templatetype` (
  `id` int(11) NOT NULL,
  `template_type` varchar(32) NOT NULL COMMENT '模板名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模板类型表';

--
-- 转存表中的数据 `mf_assessment_templatetype`
--

INSERT INTO `mf_assessment_templatetype` (`id`, `template_type`) VALUES
(1, '预评报告-住宅用房'),
(2, '预评报告-商业住宅'),
(3, '预评报告-房地产权证'),
(4, '预评报告-不动产权证');

-- --------------------------------------------------------

--
-- 表的结构 `mf_attachment`
--

CREATE TABLE `mf_attachment` (
  `id` int(20) UNSIGNED NOT NULL COMMENT 'ID',
  `admin_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) NOT NULL DEFAULT '' COMMENT '高度',
  `imagetype` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filesize` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) NOT NULL DEFAULT '' COMMENT '透传数据',
  `createtime` int(10) DEFAULT NULL COMMENT '创建日期',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `uploadtime` int(10) DEFAULT NULL COMMENT '上传时间',
  `storage` varchar(100) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_attachment`
--

INSERT INTO `mf_attachment` (`id`, `admin_id`, `user_id`, `url`, `imagewidth`, `imageheight`, `imagetype`, `imageframes`, `filesize`, `mimetype`, `extparam`, `createtime`, `updatetime`, `uploadtime`, `storage`, `sha1`) VALUES
(1, 1, 0, '/assets/img/qrcode.png', '150', '150', 'png', 0, 21859, 'image/png', '', 1499681848, 1499681848, 1499681848, 'local', '17163603d0263e4838b9387ff2cd4877e8b018f6'),
(2, 1, 0, '/uploads/20190622/8f28512e4d3d7fbdd65b0960d11c5b02.jpg', '1920', '700', 'jpg', 0, 360309, 'image/jpeg', '{\"name\":\"db75f05c1f9df28874bbea4d91233140.jpg\"}', 1561189247, 1561189247, 1561189247, 'local', '7f0b906de42b0085a21bb192046dde6e396bf4a8'),
(218, 1, 0, '/uploads/20201215/294fb44298f968d08a81ac8af076e94a.xls', '', '', 'xls', 0, 9216, 'application/vnd.ms-excel', '{\"name\":\"20200601143755_99885900.xls\"}', 1608035997, 1608035997, 1608035997, 'local', 'b2290aedccf16e6cf84df675e232898ac01aa7a6'),
(219, 1, 0, '/uploads/20201216/d0ab75f7e3d42e5007333cf389438aa1.png', '146', '60', 'png', 0, 6261, 'image/png', '{\"name\":\"\\u5fae\\u4fe1\\u56fe\\u7247_20201215112656.png\"}', 1608084313, 1608084313, 1608084313, 'local', 'df5bcdd30723f38ea47b7cf948e24b3b780abc9f'),
(220, 1, 0, '/uploads/20201231/aeab68f08e8ea51553cf8afd9aa08c0b.xls', '', '', 'xls', 0, 264704, 'application/vnd.ms-excel', '{\"name\":\"\\u5458\\u5de5\\u8868 (2).xls\"}', 1609380574, 1609380574, 1609380574, 'local', '67ac7f8b4fa5aee616b773ba221b634f9a6a6625');

-- --------------------------------------------------------

--
-- 表的结构 `mf_auth_copnumber`
--

CREATE TABLE `mf_auth_copnumber` (
  `id` int(11) NOT NULL COMMENT '编号',
  `cop_id` int(11) NOT NULL COMMENT '公司',
  `tablename` enum('mf_assessment_prereport') NOT NULL COMMENT '哪个表使用',
  `year` int(11) NOT NULL COMMENT '年份',
  `max_number` int(11) NOT NULL COMMENT '当前编号',
  `primary_id` int(11) NOT NULL COMMENT '主键id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评估编号';

--
-- 转存表中的数据 `mf_auth_copnumber`
--

INSERT INTO `mf_auth_copnumber` (`id`, `cop_id`, `tablename`, `year`, `max_number`, `primary_id`) VALUES
(1, 1, 'mf_assessment_prereport', 2019, 100, 0),
(2, 1, 'mf_assessment_prereport', 2019, 101, 35),
(3, 1, 'mf_assessment_prereport', 2019, 102, 35),
(4, 1, 'mf_assessment_prereport', 2019, 103, 35),
(5, 1, 'mf_assessment_prereport', 2019, 104, 35),
(6, 1, 'mf_assessment_prereport', 2019, 105, 35),
(7, 1, 'mf_assessment_prereport', 2019, 106, 35),
(8, 1, 'mf_assessment_prereport', 2020, 1, 36),
(9, 1, 'mf_assessment_prereport', 2020, 2, 37),
(10, 1, 'mf_assessment_prereport', 2020, 3, 38),
(11, 1, 'mf_assessment_prereport', 2020, 4, 39),
(12, 1, 'mf_assessment_prereport', 2020, 5, 40),
(13, 1, 'mf_assessment_prereport', 2020, 6, 41),
(14, 1, 'mf_assessment_prereport', 2020, 7, 42),
(15, 1, 'mf_assessment_prereport', 2020, 8, 43),
(16, 1, 'mf_assessment_prereport', 2020, 9, 44),
(17, 1, 'mf_assessment_prereport', 2020, 10, 45),
(18, 1, 'mf_assessment_prereport', 2020, 11, 47),
(19, 1, 'mf_assessment_prereport', 2020, 12, 48),
(20, 1, 'mf_assessment_prereport', 2020, 13, 49),
(21, 1, 'mf_assessment_prereport', 2020, 14, 50),
(22, 1, 'mf_assessment_prereport', 2020, 15, 51);

-- --------------------------------------------------------

--
-- 表的结构 `mf_auth_cops`
--

CREATE TABLE `mf_auth_cops` (
  `id` int(11) NOT NULL,
  `cop_name` varchar(128) NOT NULL COMMENT '企业名称',
  `parent_id` int(11) NOT NULL COMMENT '上级组织',
  `levels` int(11) NOT NULL COMMENT '0公司 1门店或直属部门',
  `address` varchar(128) NOT NULL COMMENT '公司地址',
  `areaCode` varchar(32) NOT NULL COMMENT '区域码',
  `stamp_path` varchar(1024) NOT NULL COMMENT '印章地址',
  `business_license` varchar(1024) NOT NULL COMMENT '营业执照',
  `certificate` varchar(1024) NOT NULL COMMENT '资质证书',
  `state` int(11) NOT NULL COMMENT '是否有效 0无效 1有效',
  `add_time` int(11) NOT NULL,
  `add_ip` varchar(64) NOT NULL,
  `add_userid` int(11) NOT NULL,
  `last_time` int(11) NOT NULL,
  `last_userid` int(11) NOT NULL,
  `last_ip` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业管理';

--
-- 转存表中的数据 `mf_auth_cops`
--

INSERT INTO `mf_auth_cops` (`id`, `cop_name`, `parent_id`, `levels`, `address`, `areaCode`, `stamp_path`, `business_license`, `certificate`, `state`, `add_time`, `add_ip`, `add_userid`, `last_time`, `last_userid`, `last_ip`) VALUES
(1, '安徽觅房网络科技有限公司', 0, 0, '安徽觅房网络科技有限公司', '安徽省/合肥市/高新区', '/assets/img/qrcode.png', '/assets/img/qrcode.png', '/assets/img/qrcode.png', 1, 0, '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_auth_group`
--

CREATE TABLE `mf_auth_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父组别',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '组名',
  `rules` text NOT NULL COMMENT '规则ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分组表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_auth_group`
--

INSERT INTO `mf_auth_group` (`id`, `pid`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
(1, 0, 'Admin group', '*', 1490883540, 149088354, 'normal'),
(2, 1, '管理员', '1,9,10,11,13,14,15,16,17,40,41,42,43,44,45,46,47,48,49,50,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,156,164,165,166,167,168,169,170,171,172,173,174,175,189,190,191,192,193,194,209,210,211,212,213,222,223,224,225,226,227,229,230,231,232,233,234,237,238,239,240,241,242,244,245,246,247,248,249,251,252,253,254,255,256,266,267,268,269,270,273,278,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,449,450,451,452,453,454,460,461,462,463,464,650,651,652,653,654,655,656,657,658,659,660,661,662,5,66,271,208,133,265,410,664', 1490883540, 1607935623, 'normal'),
(3, 1, '楼盘代理公司', '13,23,24,29,30,164,165,166,167,168,169,170,171,172,173,174,175,209,210,211,213,222,223,224,225,226,227,237,238,239,240,241,242,244,245,246,247,248,249,265,266,267,268,269,270,271,273,275,278,279,281,282,283,284,285,286,424,425,426,427,428,429,455,456,465,564,1,7,2,8,208,133,423,410', 1490883540, 1584332756, 'normal'),
(4, 1, '楼盘开发商', '13,29,30,165,167,169,171,173,175,208,209,210,211,212,213,265,266,267,268,269,270,278,279,1,8,2,164,271,170,133', 1490883540, 1577149048, 'normal'),
(5, 1, '经纪公司', '13,29,30,164,165,166,167,168,169,208,209,210,211,212,213,273,279,1,8,2,271,133', 1490883540, 1577149103, 'normal'),
(6, 5, '经纪人', '29,30,164,165,166,167,168,169,208,209,210,211,212,213,273,279,8,2,271,133', 1561189380, 1577149107, 'normal'),
(7, 1, '评估公司审核员', '1,13,14,15,16,17,23,24,25,29,30,209,210,211,212,213,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,320,321,322,323,324,325,345,346,347,348,349,350,357,358,359,360,361,362,369,370,371,372,373,374,375,376,377,378,379,380,389,390,409,445,446,7,2,8,208,133,287,313,443', 1562584886, 1578371149, 'normal'),
(8, 7, '评估公司客户', '1,13,14,15,16,17,29,30,209,210,211,212,213,8,2,208,133', 1562585387, 1577149080, 'normal'),
(9, 2, '新房管理员', '1,13,14,15,16,17,133,164,165,166,167,168,169,208,209,210,211,212,213,271', 1562585462, 1607935623, 'normal'),
(10, 2, '评估模块管理员', '1,13,14,15,16,17', 1562585492, 1607935623, 'normal'),
(11, 7, '估价师', '1,13,14,15,16,17,23,24,25,29,30,320,321,322,323,324,325,409,7,2,8,287,313,443,444,457,458,404,403,397,398', 1571710471, 1578371163, 'normal'),
(12, 7, '估价员', '29,30,320,389,390,445,446,8,2,313,443,444,321,322,323,457,458,404,403,397,398', 1576147760, 1578371159, 'normal'),
(13, 3, '代理商案场负责人', '29,30,164,165,166,167,168,169,170,171,172,173,174,175,209,211,237,238,265,267,271,273,275,279,281,282,283,284,429,455,456,2,8,208,133', 1577102058, 1584332767, 'normal'),
(14, 4, '开发商案场负责人', '29,30,165,167,169,171,173,175,208,209,265,266,267,268,269,270,278,8,2,164,271,170,133,455,456', 1577102721, 1577149054, 'normal'),
(15, 2, '二手房管理员', '1,13,156,209,229,230,231,232,233,234,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,436,449,450,451,452,453,454,460,461,462,463,464,208,133,650,651,652,653,654,655,656,657,658,659,660,661,662,664', 1577709084, 1607935623, 'normal'),
(16, 15, '信息员', '1,13,156,209,229,230,410,411,412,413,414,415,416,417,418,423,424,425,426,427,428,436,449,450,451,452,453,454,460,461,462,463,464,208,133', 1577709191, 1607935623, 'normal'),
(17, 15, '二手房经纪人', '156,229,230,410,411,412,417,418,423,424,425,436,449,450,451,452,460,461,462,463,464,650,651,652,653,654,655,656,657,658,659,660,661,662,664', 1577709191, 1608805335, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `mf_auth_group_access`
--

CREATE TABLE `mf_auth_group_access` (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '会员ID',
  `group_id` int(10) UNSIGNED NOT NULL COMMENT '级别ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限分组表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_auth_group_access`
--

INSERT INTO `mf_auth_group_access` (`uid`, `group_id`) VALUES
(1, 1),
(16, 2),
(17, 2),
(34, 6),
(36, 6),
(37, 6),
(38, 6);

-- --------------------------------------------------------

--
-- 表的结构 `mf_auth_rule`
--

CREATE TABLE `mf_auth_rule` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` enum('menu','file') NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='节点表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_auth_rule`
--

INSERT INTO `mf_auth_rule` (`id`, `type`, `pid`, `name`, `title`, `icon`, `condition`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 'file', 0, 'dashboard', '控制台', 'fa fa-dashboard', '', 'Dashboard tips', 1, 1497429920, 1561195767, 300, 'normal'),
(2, 'file', 0, 'general', 'General', 'fa fa-cogs', '', '', 1, 1497429920, 1497430169, 99, 'normal'),
(4, 'file', 0, 'addon', 'Addon', 'fa fa-rocket', '', 'Addon tips', 1, 1502035509, 1502035509, 0, 'normal'),
(5, 'file', 0, 'auth', 'Auth', 'fa fa-group', '', '', 1, 1497429920, 1497430092, 0, 'normal'),
(6, 'file', 2, 'general/config', 'Config', 'fa fa-cog', '', 'Config tips', 1, 1497429920, 1497430683, 60, 'normal'),
(7, 'file', 2, 'general/attachment', 'Attachment', 'fa fa-file-image-o', '', 'Attachment tips', 1, 1497429920, 1497430699, 53, 'normal'),
(8, 'file', 2, 'general/profile', 'Profile', 'fa fa-user', '', '', 1, 1497429920, 1497429920, 34, 'normal'),
(9, 'file', 5, 'auth/admin', 'Admin', 'fa fa-user', '', 'Admin tips', 1, 1497429920, 1497430320, 118, 'normal'),
(10, 'file', 5, 'auth/adminlog', 'Admin log', 'fa fa-list-alt', '', 'Admin log tips', 1, 1497429920, 1497430307, 113, 'normal'),
(11, 'file', 5, 'auth/group', 'Group', 'fa fa-group', '', 'Group tips', 1, 1497429920, 1497429920, 109, 'normal'),
(12, 'file', 5, 'auth/rule', 'Rule', 'fa fa-bars', '', 'Rule tips', 1, 1497429920, 1497430581, 104, 'normal'),
(13, 'file', 1, 'dashboard/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 136, 'normal'),
(14, 'file', 1, 'dashboard/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 135, 'normal'),
(15, 'file', 1, 'dashboard/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 133, 'normal'),
(16, 'file', 1, 'dashboard/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 134, 'normal'),
(17, 'file', 1, 'dashboard/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 132, 'normal'),
(18, 'file', 6, 'general/config/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 52, 'normal'),
(19, 'file', 6, 'general/config/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 51, 'normal'),
(20, 'file', 6, 'general/config/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 50, 'normal'),
(21, 'file', 6, 'general/config/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 49, 'normal'),
(22, 'file', 6, 'general/config/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 48, 'normal'),
(23, 'file', 7, 'general/attachment/index', 'View', 'fa fa-circle-o', '', 'Attachment tips', 0, 1497429920, 1497429920, 59, 'normal'),
(24, 'file', 7, 'general/attachment/select', 'Select attachment', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 58, 'normal'),
(25, 'file', 7, 'general/attachment/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 57, 'normal'),
(26, 'file', 7, 'general/attachment/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 56, 'normal'),
(27, 'file', 7, 'general/attachment/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 55, 'normal'),
(28, 'file', 7, 'general/attachment/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 54, 'normal'),
(29, 'file', 8, 'general/profile/index', 'View', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 33, 'normal'),
(30, 'file', 8, 'general/profile/update', 'Update profile', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 32, 'normal'),
(31, 'file', 8, 'general/profile/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 31, 'normal'),
(32, 'file', 8, 'general/profile/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 30, 'normal'),
(33, 'file', 8, 'general/profile/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 29, 'normal'),
(34, 'file', 8, 'general/profile/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 28, 'normal'),
(40, 'file', 9, 'auth/admin/index', 'View', 'fa fa-circle-o', '', 'Admin tips', 0, 1497429920, 1497429920, 117, 'normal'),
(41, 'file', 9, 'auth/admin/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 116, 'normal'),
(42, 'file', 9, 'auth/admin/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 115, 'normal'),
(43, 'file', 9, 'auth/admin/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 114, 'normal'),
(44, 'file', 10, 'auth/adminlog/index', 'View', 'fa fa-circle-o', '', 'Admin log tips', 0, 1497429920, 1497429920, 112, 'normal'),
(45, 'file', 10, 'auth/adminlog/detail', 'Detail', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 111, 'normal'),
(46, 'file', 10, 'auth/adminlog/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 110, 'normal'),
(47, 'file', 11, 'auth/group/index', 'View', 'fa fa-circle-o', '', 'Group tips', 0, 1497429920, 1497429920, 108, 'normal'),
(48, 'file', 11, 'auth/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 107, 'normal'),
(49, 'file', 11, 'auth/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 106, 'normal'),
(50, 'file', 11, 'auth/group/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 105, 'normal'),
(51, 'file', 12, 'auth/rule/index', 'View', 'fa fa-circle-o', '', 'Rule tips', 0, 1497429920, 1497429920, 103, 'normal'),
(52, 'file', 12, 'auth/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 102, 'normal'),
(53, 'file', 12, 'auth/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 101, 'normal'),
(54, 'file', 12, 'auth/rule/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1497429920, 1497429920, 100, 'normal'),
(55, 'file', 4, 'addon/index', 'View', 'fa fa-circle-o', '', 'Addon tips', 0, 1502035509, 1502035509, 0, 'normal'),
(56, 'file', 4, 'addon/add', 'Add', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(57, 'file', 4, 'addon/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(58, 'file', 4, 'addon/del', 'Delete', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(59, 'file', 4, 'addon/local', 'Local install', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(60, 'file', 4, 'addon/state', 'Update state', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(61, 'file', 4, 'addon/install', 'Install', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(62, 'file', 4, 'addon/uninstall', 'Uninstall', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(63, 'file', 4, 'addon/config', 'Setting', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(64, 'file', 4, 'addon/refresh', 'Refresh', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(65, 'file', 4, 'addon/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1502035509, 1502035509, 0, 'normal'),
(66, 'file', 0, 'user', 'User', 'fa fa-list', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(67, 'file', 66, 'user/user', 'User', 'fa fa-user', '', '', 1, 1516374729, 1608479061, 0, 'normal'),
(68, 'file', 67, 'user/user/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(69, 'file', 67, 'user/user/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(70, 'file', 67, 'user/user/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(71, 'file', 67, 'user/user/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(72, 'file', 67, 'user/user/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(73, 'file', 66, 'user/group', 'User group', 'fa fa-users', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(74, 'file', 73, 'user/group/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(75, 'file', 73, 'user/group/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(76, 'file', 73, 'user/group/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(77, 'file', 73, 'user/group/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(78, 'file', 73, 'user/group/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(79, 'file', 66, 'user/rule', 'User rule', 'fa fa-circle-o', '', '', 1, 1516374729, 1516374729, 0, 'normal'),
(80, 'file', 79, 'user/rule/index', 'View', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(81, 'file', 79, 'user/rule/del', 'Del', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(82, 'file', 79, 'user/rule/add', 'Add', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(83, 'file', 79, 'user/rule/edit', 'Edit', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(84, 'file', 79, 'user/rule/multi', 'Multi', 'fa fa-circle-o', '', '', 0, 1516374729, 1516374729, 0, 'normal'),
(133, 'file', 0, 'newhouse', '新房楼盘', 'fa fa-institution', '', '', 1, 1561195568, 1578355875, 200, 'normal'),
(156, 'file', 0, 'newhouse_config', '二手房设置', 'fa fa-cogs', '', '', 1, 1561293787, 1608479067, 145, 'normal'),
(164, 'file', 271, 'newhouse/customer/items', '新房客户', 'fa fa-user-circle', '', '', 1, 1561294797, 1577108695, 20, 'normal'),
(165, 'file', 164, 'newhouse/customer/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1561294798, 1564384151, 0, 'normal'),
(166, 'file', 164, 'newhouse/customer/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1561294798, 1561379394, 0, 'normal'),
(167, 'file', 164, 'newhouse/customer/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1561294798, 1561379394, 0, 'normal'),
(168, 'file', 164, 'newhouse/customer/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1561294798, 1561379394, 0, 'normal'),
(169, 'file', 164, 'newhouse/customer/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1561294798, 1561379394, 0, 'normal'),
(170, 'file', 271, 'newhouse/report/items', '报备审核', 'fa fa-address-book-o', '', '', 1, 1561295390, 1564969397, 5, 'normal'),
(171, 'file', 170, 'newhouse/report/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1561295390, 1561295390, 0, 'normal'),
(172, 'file', 170, 'newhouse/report/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1561295390, 1561295390, 0, 'normal'),
(173, 'file', 170, 'newhouse/report/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1561295390, 1561295390, 0, 'normal'),
(174, 'file', 170, 'newhouse/report/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1561295390, 1561295390, 0, 'normal'),
(175, 'file', 170, 'newhouse/report/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1561295390, 1561295390, 0, 'normal'),
(189, 'file', 156, 'newhouse/building/picturetype', '楼盘相册类型', 'fa fa-camera-retro', '', '', 1, 1561296978, 1563606903, 0, 'normal'),
(190, 'file', 189, 'building/pcituretype/index', '查看', 'fa fa-circle-o', '', '', 0, 1561296978, 1561296978, 0, 'normal'),
(191, 'file', 189, 'building/pcituretype/add', '添加', 'fa fa-circle-o', '', '', 0, 1561296978, 1561296978, 0, 'normal'),
(192, 'file', 189, 'building/pcituretype/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1561296978, 1561296978, 0, 'normal'),
(193, 'file', 189, 'building/pcituretype/del', '删除', 'fa fa-circle-o', '', '', 0, 1561296978, 1561296978, 0, 'normal'),
(194, 'file', 189, 'building/pcituretype/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1561296978, 1561296978, 0, 'normal'),
(208, 'file', 133, 'newhouse/building/items', '楼盘信息', 'fa fa-bars', '', '', 1, 1561889287, 1563606416, 30, 'normal'),
(209, 'file', 208, 'newhouse/building/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1561889287, 1561889287, 0, 'normal'),
(210, 'file', 208, 'newhouse/building/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1561889287, 1561889287, 0, 'normal'),
(211, 'file', 208, 'newhouse/building/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1561889287, 1561889287, 0, 'normal'),
(212, 'file', 208, 'newhouse/building/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1561889287, 1561889287, 0, 'normal'),
(213, 'file', 208, 'newhouse/building/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1561889287, 1561889287, 0, 'normal'),
(222, 'file', 133, 'newhouse/building/policy', '佣金政策', 'fa fa-yen', '', '', 1, 1561907019, 1563606843, 0, 'normal'),
(223, 'file', 222, 'newhouse/building/policy/index', '查看', 'fa fa-circle-o', '', '', 0, 1561907019, 1561991452, 0, 'normal'),
(224, 'file', 222, 'newhouse/building/policy/add', '添加', 'fa fa-circle-o', '', '', 0, 1561907019, 1561991452, 0, 'normal'),
(225, 'file', 222, 'newhouse/building/policy/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1561907019, 1561991452, 0, 'normal'),
(226, 'file', 222, 'newhouse/building/policy/del', '删除', 'fa fa-circle-o', '', '', 0, 1561907019, 1561991452, 0, 'normal'),
(227, 'file', 222, 'newhouse/building/policy/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1561907019, 1561991452, 0, 'normal'),
(229, 'file', 156, 'newhouse/building/types', '房屋类型', 'fa fa-circle-o', '', '', 1, 1561989322, 1562402818, 0, 'normal'),
(230, 'file', 229, 'newhouse/building/types/index', '查看', 'fa fa-circle-o', '', '', 0, 1561989322, 1561989322, 0, 'normal'),
(231, 'file', 229, 'newhouse/building/types/add', '添加', 'fa fa-circle-o', '', '', 0, 1561989322, 1561989322, 0, 'normal'),
(232, 'file', 229, 'newhouse/building/types/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1561989322, 1561989322, 0, 'normal'),
(233, 'file', 229, 'newhouse/building/types/del', '删除', 'fa fa-circle-o', '', '', 0, 1561989322, 1561989322, 0, 'normal'),
(234, 'file', 229, 'newhouse/building/types/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1561989322, 1561989322, 0, 'normal'),
(237, 'file', 133, 'newhouse/building/follow', '楼盘跟进', 'fa fa-cc', '', '', 1, 1562833879, 1563606872, 0, 'normal'),
(238, 'file', 237, 'newhouse/building/follow/index', '查看', 'fa fa-circle-o', '', '', 0, 1562833879, 1562833879, 0, 'normal'),
(239, 'file', 237, 'newhouse/building/follow/add', '添加', 'fa fa-circle-o', '', '', 0, 1562833879, 1562833879, 0, 'normal'),
(240, 'file', 237, 'newhouse/building/follow/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1562833879, 1562833879, 0, 'normal'),
(241, 'file', 237, 'newhouse/building/follow/del', '删除', 'fa fa-circle-o', '', '', 0, 1562833879, 1562833879, 0, 'normal'),
(242, 'file', 237, 'newhouse/building/follow/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1562833879, 1562833879, 0, 'normal'),
(244, 'file', 133, 'newhouse/building/pictures/index', '楼盘相册', 'fa fa-camera', '', '', 1, 1563193693, 1589092686, 0, 'normal'),
(245, 'file', 244, 'building/pictures/index', '查看', 'fa fa-circle-o', '', '', 0, 1563193693, 1563193693, 0, 'normal'),
(246, 'file', 244, 'building/pictures/add', '添加', 'fa fa-circle-o', '', '', 0, 1563193693, 1563193693, 0, 'normal'),
(247, 'file', 244, 'building/pictures/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1563193693, 1563193693, 0, 'normal'),
(248, 'file', 244, 'building/pictures/del', '删除', 'fa fa-circle-o', '', '', 0, 1563193693, 1563193693, 0, 'normal'),
(249, 'file', 244, 'building/pictures/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1563193693, 1563193693, 0, 'normal'),
(251, 'file', 156, 'newhouse/building/labels', '楼盘标签', 'fa fa-anchor', '', '', 1, 1563541701, 1563606917, 0, 'normal'),
(252, 'file', 251, 'newhouse/building/labels/index', '查看', 'fa fa-circle-o', '', '', 0, 1563541701, 1563541872, 0, 'normal'),
(253, 'file', 251, 'newhouse/building/labels/add', '添加', 'fa fa-circle-o', '', '', 0, 1563541701, 1563541872, 0, 'normal'),
(254, 'file', 251, 'newhouse/building/labels/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1563541701, 1563541872, 0, 'normal'),
(255, 'file', 251, 'newhouse/building/labels/del', '删除', 'fa fa-circle-o', '', '', 0, 1563541701, 1563541872, 0, 'normal'),
(256, 'file', 251, 'newhouse/building/labels/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1563541701, 1563541872, 0, 'normal'),
(265, 'file', 271, 'newhouse/customer/follow', '客户跟进', 'fa fa-circle-o', '', '', 1, 1564005869, 1564006267, 0, 'normal'),
(266, 'file', 265, 'newhouse/customer/follow/index', '查看', 'fa fa-circle-o', '', '', 0, 1564005869, 1564005869, 0, 'normal'),
(267, 'file', 265, 'newhouse/customer/follow/add', '添加', 'fa fa-circle-o', '', '', 0, 1564005869, 1564005869, 0, 'normal'),
(268, 'file', 265, 'newhouse/customer/follow/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1564005869, 1564005869, 0, 'normal'),
(269, 'file', 265, 'newhouse/customer/follow/del', '删除', 'fa fa-circle-o', '', '', 0, 1564005869, 1564005869, 0, 'normal'),
(270, 'file', 265, 'newhouse/customer/follow/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1564005869, 1564005869, 0, 'normal'),
(271, 'file', 0, 'newhouse/customer', '新房客户', 'fa fa-address-book-o', '', '', 1, 1564006219, 1565136735, 198, 'normal'),
(273, 'file', 164, 'newhouse/customer/items/view', '查看客户', 'fa fa-circle-o', '', '', 0, 1561294798, 1564384151, 0, 'normal'),
(275, 'file', 271, 'newhouse/report/items/mycreate', '我的申请', 'fa fa-table', '', '', 1, 1564609581, 1564969118, 10, 'normal'),
(278, 'file', 170, 'newhouse/report/items/view', '审核', 'fa fa-circle-o', '', '', 0, 1561295390, 1561295390, 0, 'normal'),
(279, 'file', 208, 'newhouse/building/items/view', '查看楼盘详情', 'fa fa-circle-o', '', '', 0, 1561889287, 1561889287, 0, 'normal'),
(281, 'file', 271, 'report/joint', '报备共有产权人', 'fa fa-circle-o', '', '', 1, 1566338354, 1566338506, 0, 'normal'),
(282, 'file', 281, 'report/joint/index', '查看', 'fa fa-circle-o', '', '', 0, 1566338354, 1566338354, 0, 'normal'),
(283, 'file', 281, 'report/joint/add', '添加', 'fa fa-circle-o', '', '', 0, 1566338354, 1566338354, 0, 'normal'),
(284, 'file', 281, 'report/joint/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1566338354, 1566338354, 0, 'normal'),
(285, 'file', 281, 'report/joint/del', '删除', 'fa fa-circle-o', '', '', 0, 1566338354, 1566338354, 0, 'normal'),
(286, 'file', 281, 'report/joint/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1566338354, 1566338354, 0, 'normal'),
(287, 'file', 0, 'house', '评估设置', 'fa fa-list', '', '', 1, 1571711838, 1571712288, 137, 'normal'),
(295, 'file', 287, 'house/structure', '建筑结构', 'fa fa-circle-o', '', '', 1, 1571711957, 1571711957, 0, 'normal'),
(296, 'file', 295, 'house/structure/index', '查看', 'fa fa-circle-o', '', '', 0, 1571711957, 1571711957, 0, 'normal'),
(297, 'file', 295, 'house/structure/add', '添加', 'fa fa-circle-o', '', '', 0, 1571711957, 1571711957, 0, 'normal'),
(298, 'file', 295, 'house/structure/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1571711957, 1571711957, 0, 'normal'),
(299, 'file', 295, 'house/structure/del', '删除', 'fa fa-circle-o', '', '', 0, 1571711957, 1571711957, 0, 'normal'),
(300, 'file', 295, 'house/structure/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1571711957, 1571711957, 0, 'normal'),
(301, 'file', 287, 'house/landright', '土地使用权性质', 'fa fa-circle-o', '', '', 1, 1571712196, 1571712196, 0, 'normal'),
(302, 'file', 301, 'house/landright/index', '查看', 'fa fa-circle-o', '', '', 0, 1571712196, 1571712436, 0, 'normal'),
(303, 'file', 301, 'house/landright/add', '添加', 'fa fa-circle-o', '', '', 0, 1571712196, 1571712436, 0, 'normal'),
(304, 'file', 301, 'house/landright/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1571712196, 1571712436, 0, 'normal'),
(305, 'file', 301, 'house/landright/del', '删除', 'fa fa-circle-o', '', '', 0, 1571712196, 1571712436, 0, 'normal'),
(306, 'file', 301, 'house/landright/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1571712196, 1571712436, 0, 'normal'),
(307, 'file', 287, 'house/planpurpose', '规划用途', 'fa fa-circle-o', '', '', 1, 1571713176, 1571713176, 0, 'normal'),
(308, 'file', 307, 'house/planpurpose/index', '查看', 'fa fa-circle-o', '', '', 0, 1571713176, 1571713176, 0, 'normal'),
(309, 'file', 307, 'house/planpurpose/add', '添加', 'fa fa-circle-o', '', '', 0, 1571713176, 1571713176, 0, 'normal'),
(310, 'file', 307, 'house/planpurpose/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1571713176, 1571713176, 0, 'normal'),
(311, 'file', 307, 'house/planpurpose/del', '删除', 'fa fa-circle-o', '', '', 0, 1571713176, 1571713176, 0, 'normal'),
(312, 'file', 307, 'house/planpurpose/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1571713176, 1571713176, 0, 'normal'),
(313, 'file', 0, 'assessment', '评估管理', 'fa fa-list', '', '', 1, 1571713755, 1571714137, 140, 'normal'),
(320, 'file', 313, 'assessment/items/noprereport', '评估标的', 'fa fa-circle-o', '', '', 1, 1571714067, 1587289335, 10, 'normal'),
(321, 'file', 409, 'assessment/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1571714067, 1576476895, 0, 'normal'),
(322, 'file', 409, 'assessment/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1571714067, 1576476895, 0, 'normal'),
(323, 'file', 409, 'assessment/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1571714067, 1576476895, 0, 'normal'),
(324, 'file', 409, 'assessment/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1571714067, 1576476895, 0, 'normal'),
(325, 'file', 409, 'assessment/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1571714067, 1576476895, 0, 'normal'),
(345, 'file', 287, 'assessment/picturetype', '评估相册类型', 'fa fa-circle-o', '', '', 1, 1573738640, 1573738934, 0, 'normal'),
(346, 'file', 345, 'assessment/picturetype/index', '查看', 'fa fa-circle-o', '', '', 0, 1573738640, 1573738640, 0, 'normal'),
(347, 'file', 345, 'assessment/picturetype/add', '添加', 'fa fa-circle-o', '', '', 0, 1573738640, 1573738640, 0, 'normal'),
(348, 'file', 345, 'assessment/picturetype/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1573738640, 1573738640, 0, 'normal'),
(349, 'file', 345, 'assessment/picturetype/del', '删除', 'fa fa-circle-o', '', '', 0, 1573738640, 1573738640, 0, 'normal'),
(350, 'file', 345, 'assessment/picturetype/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1573738640, 1573738640, 0, 'normal'),
(357, 'file', 287, 'assessment/templatetype', '模板类型管理', 'fa fa-circle-o', '', '', 1, 1575381846, 1575381908, 0, 'normal'),
(358, 'file', 357, 'assessment/templatetype/index', '查看', 'fa fa-circle-o', '', '', 0, 1575381846, 1575381846, 0, 'normal'),
(359, 'file', 357, 'assessment/templatetype/add', '添加', 'fa fa-circle-o', '', '', 0, 1575381846, 1575381846, 0, 'normal'),
(360, 'file', 357, 'assessment/templatetype/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1575381846, 1575381846, 0, 'normal'),
(361, 'file', 357, 'assessment/templatetype/del', '删除', 'fa fa-circle-o', '', '', 0, 1575381846, 1575381846, 0, 'normal'),
(362, 'file', 357, 'assessment/templatetype/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1575381846, 1575381846, 0, 'normal'),
(363, 'file', 5, 'auth/cops', '企业管理', 'fa fa-circle-o', '', '', 1, 1575382120, 1576713257, 120, 'normal'),
(364, 'file', 363, 'auth/cops/index', '查看', 'fa fa-circle-o', '', '', 0, 1575382120, 1576557553, 0, 'normal'),
(365, 'file', 363, 'auth/cops/add', '添加', 'fa fa-circle-o', '', '', 0, 1575382120, 1576557553, 0, 'normal'),
(366, 'file', 363, 'auth/cops/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1575382120, 1576557553, 0, 'normal'),
(367, 'file', 363, 'auth/cops/del', '删除', 'fa fa-circle-o', '', '', 0, 1575382120, 1576557553, 0, 'normal'),
(368, 'file', 363, 'auth/cops/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1575382120, 1576557553, 0, 'normal'),
(369, 'file', 287, 'assessment/templates', '评估模板', 'fa fa-circle-o', '', '', 1, 1575382421, 1575469592, 0, 'normal'),
(370, 'file', 369, 'assessment/templates/index', '查看', 'fa fa-circle-o', '', '', 0, 1575382421, 1575382835, 0, 'normal'),
(371, 'file', 369, 'assessment/templates/add', '添加', 'fa fa-circle-o', '', '', 0, 1575382421, 1575382835, 0, 'normal'),
(372, 'file', 369, 'assessment/templates/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1575382421, 1575382835, 0, 'normal'),
(373, 'file', 369, 'assessment/templates/del', '删除', 'fa fa-circle-o', '', '', 0, 1575382421, 1575382835, 0, 'normal'),
(374, 'file', 369, 'assessment/templates/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1575382421, 1575382835, 0, 'normal'),
(375, 'file', 287, 'assessment/appraisers', '评估师', 'fa fa-circle-o', '', '', 1, 1575469525, 1575469582, 0, 'normal'),
(376, 'file', 375, 'assessment/appraisers/index', '查看', 'fa fa-circle-o', '', '', 0, 1575469525, 1575469525, 0, 'normal'),
(377, 'file', 375, 'assessment/appraisers/add', '添加', 'fa fa-circle-o', '', '', 0, 1575469525, 1575469525, 0, 'normal'),
(378, 'file', 375, 'assessment/appraisers/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1575469525, 1575469525, 0, 'normal'),
(379, 'file', 375, 'assessment/appraisers/del', '删除', 'fa fa-circle-o', '', '', 0, 1575469525, 1575469525, 0, 'normal'),
(380, 'file', 375, 'assessment/appraisers/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1575469525, 1575469525, 0, 'normal'),
(389, 'file', 313, 'assessment/items/prereport_notverify', '待审核预评报告', 'fa fa-circle-o', '', '', 1, 1576206679, 1576206679, 9, 'normal'),
(390, 'file', 313, 'assessment/items/prereport_verified', '已审核预评报告', 'fa fa-circle-o', '', '', 1, 1576206696, 1576206696, 5, 'normal'),
(397, 'file', 287, 'assessment/shares', '共有情况', 'fa fa-circle-o', '', '', 1, 1576211824, 1576211862, 0, 'normal'),
(398, 'file', 397, 'assessment/shares/index', '查看', 'fa fa-circle-o', '', '', 0, 1576211824, 1576211824, 0, 'normal'),
(399, 'file', 397, 'assessment/shares/add', '添加', 'fa fa-circle-o', '', '', 0, 1576211824, 1576211824, 0, 'normal'),
(400, 'file', 397, 'assessment/shares/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1576211824, 1576211824, 0, 'normal'),
(401, 'file', 397, 'assessment/shares/del', '删除', 'fa fa-circle-o', '', '', 0, 1576211824, 1576211824, 0, 'normal'),
(402, 'file', 397, 'assessment/shares/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1576211824, 1576211824, 0, 'normal'),
(403, 'file', 287, 'assessment/righttype', '权利类型', 'fa fa-circle-o', '', '', 1, 1576212053, 1576216879, 0, 'normal'),
(404, 'file', 403, 'assessment/righttype/index', '查看', 'fa fa-circle-o', '', '', 0, 1576212053, 1576212053, 0, 'normal'),
(405, 'file', 403, 'assessment/righttype/add', '添加', 'fa fa-circle-o', '', '', 0, 1576212053, 1576212053, 0, 'normal'),
(406, 'file', 403, 'assessment/righttype/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1576212053, 1576212053, 0, 'normal'),
(407, 'file', 403, 'assessment/righttype/del', '删除', 'fa fa-circle-o', '', '', 0, 1576212053, 1576212053, 0, 'normal'),
(408, 'file', 403, 'assessment/righttype/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1576212053, 1576212053, 0, 'normal'),
(409, 'file', 287, 'assessment/items', '评估标的', 'fa fa-circle-o', '', '', 1, 1576476543, 1576477155, 0, 'normal'),
(410, 'file', 0, 'second', '二手房', 'fa fa-building', '', '', 1, 1576589851, 1580699568, 160, 'normal'),
(411, 'file', 156, 'second/buildings', '楼盘管理', 'fa fa-circle-o', '', '', 1, 1576589851, 1577711427, 0, 'normal'),
(412, 'file', 411, 'second/buildings/index', '查看', 'fa fa-circle-o', '', '', 0, 1576589851, 1576591376, 0, 'normal'),
(413, 'file', 411, 'second/buildings/add', '添加', 'fa fa-circle-o', '', '', 0, 1576589851, 1576591376, 0, 'normal'),
(414, 'file', 411, 'second/buildings/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1576589851, 1576591376, 0, 'normal'),
(415, 'file', 411, 'second/buildings/del', '删除', 'fa fa-circle-o', '', '', 0, 1576589851, 1576591376, 0, 'normal'),
(416, 'file', 411, 'second/buildings/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1576589851, 1576591376, 0, 'normal'),
(417, 'file', 156, 'second/decoration', '装修设置', 'fa fa-circle-o', '', '', 1, 1577006311, 1577711402, 0, 'normal'),
(418, 'file', 417, 'second/decoration/index', '查看', 'fa fa-circle-o', '', '', 0, 1577006311, 1577007387, 0, 'normal'),
(419, 'file', 417, 'second/decoration/add', '添加', 'fa fa-circle-o', '', '', 0, 1577006311, 1577007387, 0, 'normal'),
(420, 'file', 417, 'second/decoration/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1577006311, 1577007387, 0, 'normal'),
(421, 'file', 417, 'second/decoration/del', '删除', 'fa fa-circle-o', '', '', 0, 1577006311, 1577007387, 0, 'normal'),
(422, 'file', 417, 'second/decoration/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1577006311, 1577007387, 0, 'normal'),
(423, 'file', 410, 'second/items', '出售房源', 'fa fa-building-o', '', '', 1, 1577007325, 1577023891, 10, 'normal'),
(424, 'file', 423, 'second/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(425, 'file', 423, 'second/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(426, 'file', 423, 'second/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(427, 'file', 423, 'second/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(428, 'file', 423, 'second/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(429, 'file', 170, 'newhouse/report/items/add_ajax', '添加', 'fa fa-circle-o', '', '', 0, 1561295390, 1561295390, 0, 'normal'),
(430, 'file', 156, 'newhouse/customer/source', '客户来源分类', 'fa fa-circle-o', '', '', 1, 1577109898, 1577111978, 0, 'normal'),
(431, 'file', 430, 'newhouse/source/index', '查看', 'fa fa-circle-o', '', '', 0, 1577109898, 1577109980, 0, 'normal'),
(432, 'file', 430, 'newhouse/source/add', '添加', 'fa fa-circle-o', '', '', 0, 1577109898, 1577109980, 0, 'normal'),
(433, 'file', 430, 'newhouse/source/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1577109898, 1577109980, 0, 'normal'),
(434, 'file', 430, 'newhouse/source/del', '删除', 'fa fa-circle-o', '', '', 0, 1577109898, 1577109980, 0, 'normal'),
(435, 'file', 430, 'newhouse/source/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1577109898, 1577109980, 0, 'normal'),
(436, 'file', 423, 'second/items/view', '查看出售房源', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(437, 'file', 5, 'auth/copnumber', '评估编号', 'fa fa-circle-o', '', '', 1, 1577802954, 1577802954, 0, 'normal'),
(438, 'file', 437, 'auth/copnumber/index', '查看', 'fa fa-circle-o', '', '', 0, 1577802954, 1577802954, 0, 'normal'),
(439, 'file', 437, 'auth/copnumber/add', '添加', 'fa fa-circle-o', '', '', 0, 1577802954, 1577802954, 0, 'normal'),
(440, 'file', 437, 'auth/copnumber/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1577802954, 1577802954, 0, 'normal'),
(441, 'file', 437, 'auth/copnumber/del', '删除', 'fa fa-circle-o', '', '', 0, 1577802954, 1577802954, 0, 'normal'),
(442, 'file', 437, 'auth/copnumber/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1577802954, 1577802954, 0, 'normal'),
(443, 'file', 313, 'assessment/prereport', '预评版本管理', 'fa fa-circle-o', '', '', 1, 1577840233, 1577840233, 0, 'normal'),
(444, 'file', 443, 'assessment/prereport/index', '查看', 'fa fa-circle-o', '', '', 0, 1577840233, 1577840233, 0, 'normal'),
(445, 'file', 443, 'assessment/prereport/add', '添加', 'fa fa-circle-o', '', '', 0, 1577840233, 1577840233, 0, 'normal'),
(446, 'file', 443, 'assessment/prereport/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1577840233, 1577840233, 0, 'normal'),
(447, 'file', 443, 'assessment/prereport/del', '删除', 'fa fa-circle-o', '', '', 0, 1577840233, 1577840233, 0, 'normal'),
(448, 'file', 443, 'assessment/prereport/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1577840233, 1577840233, 0, 'normal'),
(449, 'file', 410, 'second/follow', '二手房跟进', 'fa fa-circle-o', '', '', 1, 1577975224, 1581246265, 0, 'normal'),
(450, 'file', 449, 'second/follow/index', '查看', 'fa fa-circle-o', '', '', 0, 1577975224, 1577975224, 0, 'normal'),
(451, 'file', 449, 'second/follow/add', '添加', 'fa fa-circle-o', '', '', 0, 1577975224, 1577975224, 0, 'normal'),
(452, 'file', 449, 'second/follow/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1577975224, 1577975224, 0, 'normal'),
(453, 'file', 449, 'second/follow/del', '删除', 'fa fa-circle-o', '', '', 0, 1577975224, 1577975224, 0, 'normal'),
(454, 'file', 449, 'second/follow/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1577975224, 1577975224, 0, 'normal'),
(455, 'file', 265, 'newhouse/customer/follow/addstate', '状态跟进', 'fa fa-circle-o', '', '', 0, 1564005869, 1564005869, 0, 'normal'),
(456, 'file', 265, 'newhouse/customer/follow/addmobile', '号码跟进', 'fa fa-circle-o', '', '', 0, 1564005869, 1564005869, 0, 'normal'),
(457, 'file', 409, 'assessment/prereport/previewpdf', '预览pdf', 'fa fa-circle-o', '', '', 0, 1571714067, 1576476895, 0, 'normal'),
(458, 'file', 409, 'assessment/prereport/downloadpdf', '预览pdf', 'fa fa-circle-o', '', '', 0, 1571714067, 1576476895, 0, 'normal'),
(459, 'file', 443, 'assessment/prereport/verify', '编辑', 'fa fa-circle-o', '', '', 0, 1577840233, 1577840233, 0, 'normal'),
(460, 'file', 411, 'second/buildings/getBuilding', '按名称查楼盘', 'fa fa-circle-o', '', '', 0, 1576589851, 1576591376, 0, 'normal'),
(461, 'file', 411, 'second/buildings/getAddress', '查询楼盘地址', 'fa fa-circle-o', '', '', 0, 1576589851, 1576591376, 0, 'normal'),
(462, 'file', 423, 'second/items/getFloorTotal', '查询楼栋总层数', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(463, 'file', 423, 'second/items/checkExistsDongShi', '查询栋室是否已存在', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(464, 'file', 423, 'second/items/checkExistsMobile', '号码有没有登记房源', 'fa fa-circle-o', '', '', 0, 1577007325, 1577007390, 0, 'normal'),
(465, 'file', 410, 'second/items/inspects', '第三方链接', 'fa fa-link', '', '', 1, 1581246255, 1581246308, 0, 'normal'),
(565, 'file', 133, 'newhouse/department/items', '售楼部', 'fa fa-university', '', '', 1, 1589034725, 1589037926, 0, 'normal'),
(566, 'file', 565, 'newhouse/department/index', '查看', 'fa fa-circle-o', '', '', 0, 1589034725, 1589034725, 0, 'normal'),
(567, 'file', 565, 'newhouse/department/add', '添加', 'fa fa-circle-o', '', '', 0, 1589034725, 1589034725, 0, 'normal'),
(568, 'file', 565, 'newhouse/department/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1589034725, 1589034725, 0, 'normal'),
(569, 'file', 565, 'newhouse/department/del', '删除', 'fa fa-circle-o', '', '', 0, 1589034725, 1589034725, 0, 'normal'),
(570, 'file', 565, 'newhouse/department/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1589034725, 1589034725, 0, 'normal'),
(572, 'file', 565, 'newhouse/department/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1589035063, 1589035063, 0, 'normal'),
(573, 'file', 565, 'newhouse/department/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1589035063, 1589035063, 0, 'normal'),
(574, 'file', 565, 'newhouse/department/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1589035063, 1589035063, 0, 'normal'),
(575, 'file', 565, 'newhouse/department/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1589035063, 1589035063, 0, 'normal'),
(576, 'file', 565, 'newhouse/department/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1589035063, 1589035063, 0, 'normal'),
(577, 'file', 133, 'newhouse/department/cameras', '摄像机管理', 'fa fa-video-camera', '', '', 1, 1589091679, 1589091828, 0, 'normal'),
(585, 'file', 133, 'newhouse/department/logs', '抓拍记录', 'fa fa-circle-o', '', '', 1, 1589098832, 1589098974, 0, 'normal'),
(586, 'file', 585, 'newhouse/department/logs/index', '查看', 'fa fa-circle-o', '', '', 0, 1589098832, 1589099819, 0, 'normal'),
(587, 'file', 585, 'newhouse/department/logs/add', '添加', 'fa fa-circle-o', '', '', 0, 1589098832, 1589099819, 0, 'normal'),
(588, 'file', 585, 'newhouse/department/logs/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1589098832, 1589099819, 0, 'normal'),
(589, 'file', 585, 'newhouse/department/logs/del', '删除', 'fa fa-circle-o', '', '', 0, 1589098832, 1589099819, 0, 'normal'),
(590, 'file', 585, 'newhouse/department/logs/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1589098832, 1589099819, 0, 'normal'),
(592, 'file', 313, 'assessment/propertynumtype', '产权证类型', 'fa fa-circle-o', '', '', 1, 1589370940, 1589370940, 0, 'normal'),
(593, 'file', 592, 'assessment/propertynumtype/index', '查看', 'fa fa-circle-o', '', '', 0, 1589370940, 1589370940, 0, 'normal'),
(594, 'file', 592, 'assessment/propertynumtype/add', '添加', 'fa fa-circle-o', '', '', 0, 1589370940, 1589370940, 0, 'normal'),
(595, 'file', 592, 'assessment/propertynumtype/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1589370940, 1589370940, 0, 'normal'),
(596, 'file', 592, 'assessment/propertynumtype/del', '删除', 'fa fa-circle-o', '', '', 0, 1589370940, 1589370940, 0, 'normal'),
(597, 'file', 592, 'assessment/propertynumtype/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1589370940, 1589370940, 0, 'normal'),
(598, 'file', 66, 'user/favorate', '收藏管理', 'fa fa-circle-o', '', '', 1, 1589934169, 1589934169, 0, 'normal'),
(599, 'file', 598, 'user/favorate/index', '查看', 'fa fa-circle-o', '', '', 0, 1589934169, 1589934169, 0, 'normal'),
(600, 'file', 598, 'user/favorate/add', '添加', 'fa fa-circle-o', '', '', 0, 1589934169, 1589934169, 0, 'normal'),
(601, 'file', 598, 'user/favorate/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1589934169, 1589934169, 0, 'normal'),
(602, 'file', 598, 'user/favorate/del', '删除', 'fa fa-circle-o', '', '', 0, 1589934169, 1589934169, 0, 'normal'),
(603, 'file', 598, 'user/favorate/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1589934169, 1589934169, 0, 'normal'),
(618, 'file', 0, 'cert/loan', '贷款业务', 'fa fa-money', '', '', 1, 1596087174, 1596114890, 141, 'normal'),
(619, 'file', 618, 'cert/loan/state', '贷款状态', 'fa fa-circle-o', '', '', 1, 1596087174, 1596087174, 0, 'normal'),
(620, 'file', 619, 'cert/loan/state/index', '查看', 'fa fa-circle-o', '', '', 0, 1596087174, 1596087334, 0, 'normal'),
(621, 'file', 619, 'cert/loan/state/add', '添加', 'fa fa-circle-o', '', '', 0, 1596087174, 1596087334, 0, 'normal'),
(622, 'file', 619, 'cert/loan/state/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1596087174, 1596087334, 0, 'normal'),
(623, 'file', 619, 'cert/loan/state/del', '删除', 'fa fa-circle-o', '', '', 0, 1596087174, 1596087334, 0, 'normal'),
(624, 'file', 619, 'cert/loan/state/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1596087174, 1596087334, 0, 'normal'),
(625, 'file', 618, 'cert/loan/bank', '贷款银行', 'fa fa-circle-o', '', '', 1, 1596087249, 1596267999, 80, 'normal'),
(626, 'file', 625, 'cert/loan/bank/index', '查看', 'fa fa-circle-o', '', '', 0, 1596087249, 1596087335, 0, 'normal'),
(627, 'file', 625, 'cert/loan/bank/add', '添加', 'fa fa-circle-o', '', '', 0, 1596087249, 1596087335, 0, 'normal'),
(628, 'file', 625, 'cert/loan/bank/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1596087249, 1596087335, 0, 'normal'),
(629, 'file', 625, 'cert/loan/bank/del', '删除', 'fa fa-circle-o', '', '', 0, 1596087249, 1596087335, 0, 'normal'),
(630, 'file', 625, 'cert/loan/bank/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1596087249, 1596087335, 0, 'normal'),
(631, 'file', 618, 'cert/loan/items', '贷款业务', 'fa fa-circle-o', '', '', 1, 1596087363, 1596267973, 100, 'normal'),
(632, 'file', 631, 'cert/loan/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1596087363, 1596087363, 0, 'normal'),
(633, 'file', 631, 'cert/loan/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1596087363, 1596087363, 0, 'normal'),
(634, 'file', 631, 'cert/loan/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1596087363, 1596087363, 0, 'normal'),
(635, 'file', 631, 'cert/loan/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1596087363, 1596087363, 0, 'normal'),
(636, 'file', 631, 'cert/loan/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1596087363, 1596087363, 0, 'normal'),
(637, 'file', 618, 'cert/loan/follow', '贷款跟进', 'fa fa-circle-o', '', '', 1, 1596087379, 1596267983, 90, 'normal'),
(638, 'file', 637, 'cert/loan/log/index', '查看', 'fa fa-circle-o', '', '', 0, 1596087379, 1596087379, 0, 'normal'),
(639, 'file', 637, 'cert/loan/log/add', '添加', 'fa fa-circle-o', '', '', 0, 1596087379, 1596087379, 0, 'normal'),
(640, 'file', 637, 'cert/loan/log/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1596087379, 1596087379, 0, 'normal'),
(641, 'file', 637, 'cert/loan/log/del', '删除', 'fa fa-circle-o', '', '', 0, 1596087379, 1596087379, 0, 'normal'),
(642, 'file', 637, 'cert/loan/log/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1596087379, 1596087379, 0, 'normal'),
(644, 'file', 618, 'cert/loan/assessment', '评估公司', 'fa fa-circle-o', '', '', 1, 1596200040, 1596268007, 70, 'normal'),
(645, 'file', 644, 'cert/loan/assessment/index', '查看', 'fa fa-circle-o', '', '', 0, 1596200040, 1596200070, 0, 'normal'),
(646, 'file', 644, 'cert/loan/assessment/add', '添加', 'fa fa-circle-o', '', '', 0, 1596200040, 1596200070, 0, 'normal'),
(647, 'file', 644, 'cert/loan/assessment/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1596200040, 1596200070, 0, 'normal'),
(648, 'file', 644, 'cert/loan/assessment/del', '删除', 'fa fa-circle-o', '', '', 0, 1596200040, 1596200070, 0, 'normal'),
(649, 'file', 644, 'cert/loan/assessment/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1596200040, 1596200070, 0, 'normal'),
(650, 'file', 0, 'rent', '租房', 'fa fa-tachometer', '', '', 1, 1607172735, 1607172786, 150, 'normal'),
(651, 'file', 650, 'rent/items', '出租房源', 'fa fa-circle-o', '', '', 1, 1607172735, 1607172809, 0, 'normal'),
(652, 'file', 651, 'rent/items/index', '查看', 'fa fa-circle-o', '', '', 0, 1607172735, 1607172735, 0, 'normal'),
(653, 'file', 651, 'rent/items/add', '添加', 'fa fa-circle-o', '', '', 0, 1607172735, 1607172735, 0, 'normal'),
(654, 'file', 651, 'rent/items/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1607172735, 1607172735, 0, 'normal'),
(655, 'file', 651, 'rent/items/del', '删除', 'fa fa-circle-o', '', '', 0, 1607172735, 1607172735, 0, 'normal'),
(656, 'file', 651, 'rent/items/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1607172735, 1607172735, 0, 'normal'),
(657, 'file', 650, 'rent/follow', '租房跟进', 'fa fa-circle-o', '', '', 1, 1607172879, 1607173278, 0, 'normal'),
(658, 'file', 657, 'rent/follow/index', '查看', 'fa fa-circle-o', '', '', 0, 1607172879, 1607172879, 0, 'normal'),
(659, 'file', 657, 'rent/follow/add', '添加', 'fa fa-circle-o', '', '', 0, 1607172879, 1607172879, 0, 'normal'),
(660, 'file', 657, 'rent/follow/edit', '编辑', 'fa fa-circle-o', '', '', 0, 1607172879, 1607172879, 0, 'normal'),
(661, 'file', 657, 'rent/follow/del', '删除', 'fa fa-circle-o', '', '', 0, 1607172879, 1607172879, 0, 'normal'),
(662, 'file', 657, 'rent/follow/multi', '批量更新', 'fa fa-circle-o', '', '', 0, 1607172879, 1607172879, 0, 'normal'),
(663, 'file', 651, 'rent/items/view', '查看', 'fa fa-circle-o', '', '', 0, 1607172735, 1607172735, 0, 'normal'),
(664, 'file', 651, 'rent/follow/view', '查看', 'fa fa-circle-o', '', '', 0, 1607172735, 1607172735, 0, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `mf_category`
--

CREATE TABLE `mf_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父ID',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '栏目类型',
  `name` varchar(30) NOT NULL DEFAULT '',
  `nickname` varchar(50) NOT NULL DEFAULT '',
  `flag` set('hot','index','recommend') NOT NULL DEFAULT '',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '图片',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `diyname` varchar(30) NOT NULL DEFAULT '' COMMENT '自定义名称',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_category`
--

INSERT INTO `mf_category` (`id`, `pid`, `type`, `name`, `nickname`, `flag`, `image`, `keywords`, `description`, `diyname`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(5, 0, 'page', '交易合同', 'agreement', 'recommend', '/assets/img/qrcode.png', '', '', 'software', 1495262336, 1563605820, 5, 'normal'),
(9, 6, 'page', '移动端', 'website-mobile', 'recommend', '/assets/img/qrcode.png', '', '', 'website-mobile', 1495262456, 1495262456, 9, 'normal'),
(13, 0, 'article', '购房政策', 'policy', 'recommend', '/assets/img/qrcode.png', '', '', 'test2', 1497015738, 1563605809, 13, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `mf_cert_loan_assessment`
--

CREATE TABLE `mf_cert_loan_assessment` (
  `assessment_id` int(11) NOT NULL COMMENT '编号',
  `assessment_name` varchar(96) NOT NULL COMMENT '评估公司名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评估公司';

--
-- 转存表中的数据 `mf_cert_loan_assessment`
--

INSERT INTO `mf_cert_loan_assessment` (`assessment_id`, `assessment_name`) VALUES
(1, '某评估公司');

-- --------------------------------------------------------

--
-- 表的结构 `mf_cert_loan_bank`
--

CREATE TABLE `mf_cert_loan_bank` (
  `bank_id` int(11) NOT NULL,
  `bank_name` varchar(64) NOT NULL COMMENT '银行名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='贷款银行';

--
-- 转存表中的数据 `mf_cert_loan_bank`
--

INSERT INTO `mf_cert_loan_bank` (`bank_id`, `bank_name`) VALUES
(1, '中国工商银行(合肥和平路支行)');

-- --------------------------------------------------------

--
-- 表的结构 `mf_cert_loan_follow`
--

CREATE TABLE `mf_cert_loan_follow` (
  `follow_id` varchar(32) NOT NULL,
  `item_id` varchar(32) NOT NULL COMMENT '贷款编号',
  `create_userid` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_ip` varchar(64) NOT NULL,
  `comment` varchar(4000) NOT NULL COMMENT '跟进内容',
  `state` int(11) NOT NULL COMMENT '跟进状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='贷款跟进';

-- --------------------------------------------------------

--
-- 表的结构 `mf_cert_loan_items`
--

CREATE TABLE `mf_cert_loan_items` (
  `loan_id` varchar(32) NOT NULL COMMENT '贷款id',
  `item_no` varchar(16) NOT NULL COMMENT '编号',
  `buyer_name` varchar(32) NOT NULL COMMENT '买方姓名',
  `buyer_mobile` varchar(32) NOT NULL COMMENT '买房电话',
  `seller_name` varchar(32) NOT NULL COMMENT '卖方姓名',
  `seller_mobile` varchar(32) NOT NULL COMMENT '卖方电话',
  `address` varchar(256) NOT NULL COMMENT '房屋地址',
  `total_price` float NOT NULL COMMENT '总价',
  `down_payment` float NOT NULL COMMENT '首付款',
  `loan_fee` float NOT NULL COMMENT '贷款金额',
  `area` float NOT NULL COMMENT '面积',
  `sign_bank` int(11) NOT NULL COMMENT '签字银行',
  `sign_date` date NOT NULL COMMENT '签字时间',
  `state` int(11) NOT NULL,
  `assessment` varchar(128) NOT NULL COMMENT '评估公司',
  `mortage_date` date NOT NULL COMMENT '抵押时间',
  `make_loan_date` date NOT NULL COMMENT '放款时间',
  `fee` float NOT NULL COMMENT '收费',
  `mem` varchar(4000) NOT NULL COMMENT '备注',
  `create_userid` int(11) NOT NULL COMMENT '业务员',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `last_userid` int(11) NOT NULL COMMENT '更新人',
  `last_time` datetime NOT NULL COMMENT '最后时间',
  `create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `last_ip` varchar(64) NOT NULL COMMENT '最后IP'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='贷款业务';

--
-- 转存表中的数据 `mf_cert_loan_items`
--

INSERT INTO `mf_cert_loan_items` (`loan_id`, `item_no`, `buyer_name`, `buyer_mobile`, `seller_name`, `seller_mobile`, `address`, `total_price`, `down_payment`, `loan_fee`, `area`, `sign_bank`, `sign_date`, `state`, `assessment`, `mortage_date`, `make_loan_date`, `fee`, `mem`, `create_userid`, `create_time`, `last_userid`, `last_time`, `create_ip`, `last_ip`) VALUES
('82b4095c539141cda727f063c94c4747', '2020L000003', '买房', '1993332233', '11232323333', '', '安徽省合肥市长丰县', 22, 33, 22, 33, 1, '2020-08-02', 4, '1', '2020-08-02', '2020-08-02', 11, '33', 1, '2020-08-02 13:58:14', 1, '2020-08-02 02:08:54', '112.32.90.183', '112.32.90.183');

-- --------------------------------------------------------

--
-- 表的结构 `mf_cert_loan_state`
--

CREATE TABLE `mf_cert_loan_state` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(32) NOT NULL COMMENT '贷款状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='贷款状态';

--
-- 转存表中的数据 `mf_cert_loan_state`
--

INSERT INTO `mf_cert_loan_state` (`state_id`, `state_name`) VALUES
(1, '业务终止'),
(2, '待签约'),
(3, '待抵押'),
(4, '待放款'),
(5, '业务完成');

-- --------------------------------------------------------

--
-- 表的结构 `mf_cert_types`
--

CREATE TABLE `mf_cert_types` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='办理种类';

-- --------------------------------------------------------

--
-- 表的结构 `mf_config`
--

CREATE TABLE `mf_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) NOT NULL DEFAULT '' COMMENT '分组',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '变量标题',
  `tip` varchar(100) NOT NULL DEFAULT '' COMMENT '变量描述',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '类型:string,text,int,bool,array,datetime,date,file',
  `value` text NOT NULL COMMENT '变量值',
  `content` text NOT NULL COMMENT '变量字典数据',
  `rule` varchar(100) NOT NULL DEFAULT '' COMMENT '验证规则',
  `extend` varchar(255) NOT NULL DEFAULT '' COMMENT '扩展属性'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_config`
--

INSERT INTO `mf_config` (`id`, `name`, `group`, `title`, `tip`, `type`, `value`, `content`, `rule`, `extend`) VALUES
(1, 'name', 'basic', 'Site name', '请填写站点名称', 'string', '房盟云台', '', 'required', ''),
(2, 'beian', 'basic', 'Beian', '粤ICP备15000000号-1', 'string', '皖ICP备15006137号-4', '', '', ''),
(3, 'cdnurl', 'basic', 'Cdn url', '如果静态资源使用第三方云储存请配置该值', 'string', '', '', '', ''),
(4, 'version', 'basic', 'Version', '如果静态资源有变动请重新配置该值', 'string', '1.0.0', '', 'required', ''),
(5, 'timezone', 'basic', 'Timezone', '', 'string', 'Asia/Shanghai', '', 'required', ''),
(6, 'forbiddenip', 'basic', 'Forbidden ip', '一行一条记录', 'text', '', '', '', ''),
(7, 'languages', 'basic', 'Languages', '', 'array', '{\"backend\":\"zh-cn\",\"frontend\":\"zh-cn\"}', '', 'required', ''),
(8, 'fixedpage', 'basic', 'Fixed page', '请尽量输入左侧菜单栏存在的链接', 'string', 'dashboard', '', 'required', ''),
(9, 'categorytype', 'dictionary', 'Category type', '', 'array', '{\"default\":\"Default\",\"page\":\"Page\",\"article\":\"Article\",\"test\":\"Test\"}', '', '', ''),
(10, 'configgroup', 'dictionary', 'Config group', '', 'array', '{\"basic\":\"Basic\",\"email\":\"Email\",\"dictionary\":\"Dictionary\",\"user\":\"User\",\"baidu_ak\":\"zi9BinyY60C3daHbrUGjeCEkTcFGtPIP\",\"second_sort\":\"update_time\",\"second_sort_name\":\"更新时间\"}', '', '', ''),
(11, 'mail_type', 'email', 'Mail type', '选择邮件发送方式', 'select', '1', '[\"Please select\",\"SMTP\",\"Mail\"]', '', ''),
(12, 'mail_smtp_host', 'email', 'Mail smtp host', '错误的配置发送邮件会导致服务器超时', 'string', 'smtp.163.com', '', '', ''),
(13, 'mail_smtp_port', 'email', 'Mail smtp port', '(不加密默认25,SSL默认465,TLS默认587)', 'string', '465', '', '', ''),
(14, 'mail_smtp_user', 'email', 'Mail smtp user', '（填写完整用户名）', 'string', 'mifunadmin@163.com', '', '', ''),
(15, 'mail_smtp_pass', 'email', 'Mail smtp password', '（填写您的密码）', 'string', 'CYTXALUYFXSANFMI', '', '', ''),
(16, 'mail_verify_type', 'email', 'Mail vertify type', '（SMTP验证方式[推荐SSL]）', 'select', '2', '[\"None\",\"TLS\",\"SSL\"]', '', ''),
(17, 'mail_from', 'email', 'Mail from', '', 'string', 'mifunadmin@163.com', '', '', ''),
(18, 'second_show_fanghao', 'basic', '二手房列表显示房号', '二手房列表显示房号', 'radio', '1', '[\"不显示\",\"显示\"]', '', ''),
(19, 'second_assistant', 'basic', '二手房录入助手', '二手房录入助手', 'radio', '1', '[\"不使用\",\"使用\"]', '', ''),
(20, 'privacy_show_fanghao', 'basic', '私房显示房号', '', 'radio', '1', '[\"不显示\",\"显示\"]', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_ems`
--

CREATE TABLE `mf_ems` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `event` varchar(30) NOT NULL DEFAULT '' COMMENT '事件',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP',
  `createtime` int(10) UNSIGNED DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邮箱验证码表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_ems`
--

INSERT INTO `mf_ems` (`id`, `event`, `email`, `code`, `times`, `ip`, `createtime`) VALUES
(3, 'register', 'xundh@qq.com', '7246', 0, '211.162.8.34', 1587742100);

-- --------------------------------------------------------

--
-- 表的结构 `mf_house_landright`
--

CREATE TABLE `mf_house_landright` (
  `id` int(11) NOT NULL COMMENT '编号',
  `rights` varchar(16) NOT NULL COMMENT '土地使用权'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='土地使用权性质';

--
-- 转存表中的数据 `mf_house_landright`
--

INSERT INTO `mf_house_landright` (`id`, `rights`) VALUES
(1, '出让'),
(2, '划拨'),
(3, '有偿（出让）');

-- --------------------------------------------------------

--
-- 表的结构 `mf_house_planpurpose`
--

CREATE TABLE `mf_house_planpurpose` (
  `id` int(11) NOT NULL COMMENT '编号',
  `purpose` varchar(32) NOT NULL COMMENT '用途'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='规划用途';

--
-- 转存表中的数据 `mf_house_planpurpose`
--

INSERT INTO `mf_house_planpurpose` (`id`, `purpose`) VALUES
(1, '成套住宅'),
(2, '商铺'),
(3, '门面'),
(4, '写字楼');

-- --------------------------------------------------------

--
-- 表的结构 `mf_house_structure`
--

CREATE TABLE `mf_house_structure` (
  `id` int(11) NOT NULL COMMENT '编号',
  `structure` varchar(16) NOT NULL COMMENT '结构'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='建筑结构';

--
-- 转存表中的数据 `mf_house_structure`
--

INSERT INTO `mf_house_structure` (`id`, `structure`) VALUES
(1, '钢混'),
(2, '砖混'),
(3, '钢构'),
(4, '混合'),
(5, '砖木');

-- --------------------------------------------------------

--
-- 表的结构 `mf_invite`
--

CREATE TABLE `mf_invite` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `invited_user_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '被邀请人',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT '注册IP',
  `createtime` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='邀请表';

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_building_follow`
--

CREATE TABLE `mf_newhouse_building_follow` (
  `id` int(11) NOT NULL COMMENT '跟进编号',
  `building_id` int(11) NOT NULL COMMENT '楼盘编号',
  `create_userid` int(11) NOT NULL COMMENT '跟进人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '跟进时间',
  `state` enum('待售','停售','在售','售罄') NOT NULL COMMENT '楼盘状态',
  `comment` varchar(8000) NOT NULL COMMENT '跟进内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼盘跟进';

--
-- 转存表中的数据 `mf_newhouse_building_follow`
--

INSERT INTO `mf_newhouse_building_follow` (`id`, `building_id`, `create_userid`, `create_time`, `state`, `comment`) VALUES
(5, 58, 1, '2020-12-16 02:02:43', '待售', '111');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_building_items`
--

CREATE TABLE `mf_newhouse_building_items` (
  `id` int(11) NOT NULL,
  `building_name` varchar(256) NOT NULL COMMENT '楼盘名称',
  `labels` varchar(1024) NOT NULL COMMENT '标签',
  `is_top` tinyint(1) NOT NULL COMMENT '置顶显示',
  `picture` varchar(256) NOT NULL COMMENT '封面图片',
  `average_price` float NOT NULL COMMENT '均价',
  `contact_tel` varchar(128) NOT NULL COMMENT '联系电话',
  `work_begin` time NOT NULL COMMENT '接待时间起',
  `work_end` time NOT NULL COMMENT '接待时间止',
  `validate_begin` date NOT NULL COMMENT '有效期起',
  `validate_end` date NOT NULL COMMENT '有效期止',
  `address` varchar(512) NOT NULL COMMENT '地址',
  `lng` float NOT NULL COMMENT '地址经度',
  `lat` float NOT NULL COMMENT '地址纬度',
  `last_sell` date NOT NULL COMMENT '最新开盘',
  `get_date` date NOT NULL COMMENT '交房',
  `property_year` int(11) NOT NULL COMMENT '产权',
  `volume_ratio` float NOT NULL COMMENT '容积率',
  `property_fee` int(11) NOT NULL COMMENT '物业费',
  `greening_rate` float NOT NULL COMMENT '绿化率',
  `parking_spaces` int(11) NOT NULL COMMENT '车位数量',
  `house_quantity` int(11) NOT NULL COMMENT '户数',
  `developer` varchar(256) NOT NULL COMMENT '开发商',
  `state` enum('待售','停售','在售','售罄') NOT NULL COMMENT '楼盘状态',
  `property_company` varchar(256) NOT NULL COMMENT '物业公司',
  `scene_person` varchar(64) NOT NULL COMMENT '案场对接',
  `scene_person_tel` varchar(64) NOT NULL COMMENT '案场对接电话',
  `introduction` text NOT NULL COMMENT '楼盘介绍',
  `reporting_policy` varchar(256) NOT NULL COMMENT '报备政策',
  `on_selling` varchar(256) NOT NULL COMMENT '在售房源介绍',
  `createtime` int(10) NOT NULL COMMENT '录入时间',
  `create_userid` int(11) NOT NULL COMMENT '录入人',
  `updatetime` int(10) NOT NULL COMMENT '修改时间',
  `update_userid` int(11) NOT NULL COMMENT '修改人',
  `area` varchar(32) NOT NULL COMMENT '区域',
  `area_province` varchar(32) NOT NULL COMMENT '省',
  `area_city` varchar(32) NOT NULL COMMENT '市',
  `area_district` varchar(32) NOT NULL COMMENT '区',
  `developer_adminid` int(11) NOT NULL COMMENT '开发商账号',
  `agent_adminid` int(11) NOT NULL COMMENT '代理商账号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼盘信息';

--
-- 转存表中的数据 `mf_newhouse_building_items`
--

INSERT INTO `mf_newhouse_building_items` (`id`, `building_name`, `labels`, `is_top`, `picture`, `average_price`, `contact_tel`, `work_begin`, `work_end`, `validate_begin`, `validate_end`, `address`, `lng`, `lat`, `last_sell`, `get_date`, `property_year`, `volume_ratio`, `property_fee`, `greening_rate`, `parking_spaces`, `house_quantity`, `developer`, `state`, `property_company`, `scene_person`, `scene_person_tel`, `introduction`, `reporting_policy`, `on_selling`, `createtime`, `create_userid`, `updatetime`, `update_userid`, `area`, `area_province`, `area_city`, `area_district`, `developer_adminid`, `agent_adminid`) VALUES
(58, '一个大楼盘', '', 0, '/uploads/20190622/8f28512e4d3d7fbdd65b0960d11c5b02.jpg', 11000, '', '08:30:00', '18:30:00', '2020-03-01', '2020-04-14', '安徽省合肥市肥东县某条路', 117.451, 31.8988, '2020-04-14', '2020-04-14', 70, 2.2, 2, 40, 2030, 1940, '合肥房地产开发有限公司', '待售', '物业公司', '张经理', '1888888888', '123', '前三后四', '目前主力在售二期高层26#、32#、29#楼，一期准现房同步在售，户型面积91㎡-107㎡，均价11000元/㎡。', 1586854363, 11, 1608084195, 1, '安徽省/合肥市/肥东县', '安徽省', '合肥市', '肥东县', 0, 0),
(59, '杭州高德置地广场', '', 0, '', 0, '', '10:06:27', '10:06:27', '2020-12-31', '2020-12-31', '', 0, 0, '2020-12-31', '2020-12-31', 0, 0, 0, 0, 0, 0, '', '待售', '', '', '', '', '', '', 1609380413, 1, 1609380413, 0, '', '', '', '', 0, 0),
(60, '高德', '', 0, '', 0, '', '22:19:01', '22:19:01', '2021-01-03', '2021-01-03', '杭州', 0, 0, '2021-01-03', '2021-01-03', 0, 0, 0, 0, 0, 0, '', '待售', '', '', '', '', '', '', 1609683567, 1, 1609683567, 0, '', '', '', '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_building_labels`
--

CREATE TABLE `mf_newhouse_building_labels` (
  `id` int(11) NOT NULL COMMENT '编号',
  `label_name` varchar(32) NOT NULL COMMENT '标签',
  `label_color` varchar(16) NOT NULL COMMENT '颜色'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼盘标签';

--
-- 转存表中的数据 `mf_newhouse_building_labels`
--

INSERT INTO `mf_newhouse_building_labels` (`id`, `label_name`, `label_color`) VALUES
(1, '低密度', '#c4c42c'),
(2, '低总价', 'red'),
(3, '多功能', '#ccc'),
(4, '上市房企', 'blue'),
(5, '地铁物业', '#e31638'),
(6, '学区房', '#000000'),
(7, '文旅地产', '#000000'),
(8, '现房', '#000000'),
(9, '自持物业', '#000000'),
(10, '品牌房企', '#000000');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_building_pictures`
--

CREATE TABLE `mf_newhouse_building_pictures` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL COMMENT '楼盘id',
  `createtime` int(10) NOT NULL COMMENT '登记时间',
  `create_userid` int(11) NOT NULL COMMENT '登记人',
  `lasttime` int(10) NOT NULL COMMENT '最后时间',
  `last_userid` int(11) NOT NULL COMMENT '最后修改',
  `path` varchar(1024) NOT NULL COMMENT '图片路径',
  `typeid` int(11) NOT NULL COMMENT '图片类别',
  `mem` varchar(1024) NOT NULL COMMENT '说明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼盘相册';

--
-- 转存表中的数据 `mf_newhouse_building_pictures`
--

INSERT INTO `mf_newhouse_building_pictures` (`id`, `building_id`, `createtime`, `create_userid`, `lasttime`, `last_userid`, `path`, `typeid`, `mem`) VALUES
(1, 58, 1608084195, 1, 0, 1, '/uploads/20190622/8f28512e4d3d7fbdd65b0960d11c5b02.jpg', 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_building_picturetype`
--

CREATE TABLE `mf_newhouse_building_picturetype` (
  `id` int(11) NOT NULL,
  `type_name` varchar(16) NOT NULL COMMENT '类型',
  `sort` int(11) NOT NULL COMMENT '排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼盘相册类型';

--
-- 转存表中的数据 `mf_newhouse_building_picturetype`
--

INSERT INTO `mf_newhouse_building_picturetype` (`id`, `type_name`, `sort`) VALUES
(1, '户型图', 0),
(2, '效果图', 0),
(3, '实景图', 0),
(4, '小区配套', 0),
(5, '样板间', 0),
(6, '项目现场', 1);

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_building_policy`
--

CREATE TABLE `mf_newhouse_building_policy` (
  `id` int(11) NOT NULL COMMENT '佣金政策编号',
  `building_id` int(11) NOT NULL COMMENT '楼盘编号',
  `type_id` int(11) NOT NULL COMMENT '房屋类型',
  `price` float NOT NULL COMMENT '均价',
  `commission_policy` varchar(256) NOT NULL COMMENT '佣金政策',
  `createtime` int(10) NOT NULL COMMENT '创建时间',
  `create_userid` int(11) NOT NULL COMMENT '创建人'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='佣金政策';

--
-- 转存表中的数据 `mf_newhouse_building_policy`
--

INSERT INTO `mf_newhouse_building_policy` (`id`, `building_id`, `type_id`, `price`, `commission_policy`, `createtime`, `create_userid`) VALUES
(7, 4, 1, 22, '佣金政策1', 1586842076, 1),
(8, 4, 2, 4, '商铺政策', 1586842076, 1),
(9, 3, 2, 1, '什么政策', 1564370707, 3),
(10, 6, 2, 200, '8%', 1577101893, 1);

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_building_types`
--

CREATE TABLE `mf_newhouse_building_types` (
  `id` int(11) NOT NULL,
  `type_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='房屋类型';

--
-- 转存表中的数据 `mf_newhouse_building_types`
--

INSERT INTO `mf_newhouse_building_types` (`id`, `type_name`) VALUES
(1, '住宅'),
(2, '商铺'),
(3, '公寓'),
(4, '写字楼'),
(5, '门面'),
(6, '车库'),
(7, '别墅');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_customer_follow`
--

CREATE TABLE `mf_newhouse_customer_follow` (
  `id` int(11) NOT NULL COMMENT '跟进编号',
  `customer_id` int(11) NOT NULL COMMENT '客户编号',
  `follow_content` varchar(4096) NOT NULL COMMENT '跟进内容',
  `create_time` int(10) NOT NULL COMMENT '跟进时间',
  `create_userid` int(11) NOT NULL COMMENT '跟进人',
  `state` enum('不确定','无效','有效','观望','带看','已购','跟进号码') NOT NULL COMMENT '跟进状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户跟进';

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_customer_items`
--

CREATE TABLE `mf_newhouse_customer_items` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(64) NOT NULL COMMENT '客户姓名',
  `cop_id` int(11) NOT NULL COMMENT '所属公司',
  `state` enum('不确定','无效','有效','观望','带看','已购') NOT NULL COMMENT '跟进状态',
  `verify_state` enum('未审核','有效','无效') NOT NULL,
  `verify_userid` int(11) NOT NULL COMMENT '审核人',
  `verify_time` datetime NOT NULL COMMENT '审核时间',
  `verify_mem` varchar(4096) NOT NULL COMMENT '审核说明',
  `verify_friend` tinyint(1) NOT NULL COMMENT '老带新审核',
  `customer_tel` varchar(128) NOT NULL COMMENT '客户电话',
  `customer_tel2` varchar(128) NOT NULL COMMENT '号码2',
  `customer_tel3` varchar(128) NOT NULL COMMENT '号码3',
  `hide_mobile` tinyint(1) NOT NULL COMMENT '是否隐藏号码',
  `sex_radio` enum('男','女','未知') NOT NULL COMMENT '客户性别',
  `want_province` varchar(32) NOT NULL COMMENT '期望省',
  `want_city` varchar(32) NOT NULL COMMENT '期望市',
  `want_district` varchar(32) NOT NULL COMMENT '期望区域',
  `want_area` varchar(96) NOT NULL COMMENT '期望省市区',
  `want_acre_low` int(11) NOT NULL COMMENT '期望面积起',
  `want_acre_high` int(11) NOT NULL COMMENT '期望面积止',
  `want_price_low` int(11) NOT NULL COMMENT '期望价格起',
  `want_price_high` int(11) NOT NULL COMMENT '期望价格止',
  `want_floor_low` int(11) NOT NULL COMMENT '期望楼层起',
  `want_floor_high` int(11) NOT NULL COMMENT '期望楼层止',
  `customer_from` varchar(128) NOT NULL COMMENT '客户来源',
  `source_id` int(11) NOT NULL COMMENT '来源分类',
  `mem` varchar(2048) NOT NULL COMMENT '备注',
  `property_quantity` int(11) NOT NULL COMMENT '名下房产',
  `marriage_radio` enum('未知','未婚','已婚','离异','丧偶') NOT NULL COMMENT '婚姻状况',
  `first_house` enum('是','否','未知') NOT NULL COMMENT '是否首套',
  `social_security` enum('有','无','未知') NOT NULL COMMENT '社保',
  `payment_method` enum('未知','现金','刷卡','支付宝','微信') NOT NULL COMMENT '付款方式',
  `payment_percentage` int(11) NOT NULL COMMENT '首付比例',
  `credit_investigation` varchar(2048) NOT NULL COMMENT '个人征信',
  `account_statement` varchar(2048) NOT NULL COMMENT '银行流水',
  `building_id` varchar(1024) NOT NULL COMMENT '报备楼盘',
  `createtime` int(10) NOT NULL COMMENT '登记时间',
  `create_userid` int(11) NOT NULL COMMENT '登记人',
  `updatetime` int(10) NOT NULL COMMENT '更新时间',
  `update_userid` int(11) NOT NULL COMMENT '更新人',
  `from_friend` tinyint(1) NOT NULL COMMENT '老带新',
  `friend_mobile` varchar(64) NOT NULL COMMENT '老顾客手机号',
  `friend_name` varchar(32) NOT NULL COMMENT '推荐人姓名',
  `friend_relation` varchar(32) NOT NULL COMMENT '推荐人关系',
  `customer_level` enum('A','B','C','D') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新房客户';

--
-- 转存表中的数据 `mf_newhouse_customer_items`
--

INSERT INTO `mf_newhouse_customer_items` (`id`, `customer_name`, `cop_id`, `state`, `verify_state`, `verify_userid`, `verify_time`, `verify_mem`, `verify_friend`, `customer_tel`, `customer_tel2`, `customer_tel3`, `hide_mobile`, `sex_radio`, `want_province`, `want_city`, `want_district`, `want_area`, `want_acre_low`, `want_acre_high`, `want_price_low`, `want_price_high`, `want_floor_low`, `want_floor_high`, `customer_from`, `source_id`, `mem`, `property_quantity`, `marriage_radio`, `first_house`, `social_security`, `payment_method`, `payment_percentage`, `credit_investigation`, `account_statement`, `building_id`, `createtime`, `create_userid`, `updatetime`, `update_userid`, `from_friend`, `friend_mobile`, `friend_name`, `friend_relation`, `customer_level`) VALUES
(1, '123', 1, '不确定', '未审核', 0, '0000-00-00 00:00:00', '', 0, '15138658889', '15138658888', '15138658888', 0, '男', '安徽省', '芜湖市', '弋江区', '安徽省/芜湖市/弋江区', 100, 150, 150, 150, 1, 4, '1', 4, '22', 0, '未知', '未知', '未知', '未知', 0, '1', '12', '58', 1609207327, 1, 1609207366, 1, 0, '', '', '', 'A');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_customer_source`
--

CREATE TABLE `mf_newhouse_customer_source` (
  `id` int(11) NOT NULL,
  `source` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户来源分类';

--
-- 转存表中的数据 `mf_newhouse_customer_source`
--

INSERT INTO `mf_newhouse_customer_source` (`id`, `source`) VALUES
(1, '自然来访'),
(2, '朋友介绍'),
(3, '广告媒体'),
(4, '中介渠道'),
(5, '老带新'),
(6, '其他');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_department_items`
--

CREATE TABLE `mf_newhouse_department_items` (
  `id` int(11) NOT NULL COMMENT '售楼部id',
  `name` varchar(64) NOT NULL COMMENT '售楼部名称',
  `cop_id` int(11) NOT NULL COMMENT '所属公司',
  `address` varchar(256) NOT NULL COMMENT '地址',
  `state` tinyint(1) NOT NULL COMMENT 'true有效 false无效',
  `create_time` datetime NOT NULL COMMENT '录入时间',
  `create_ip` varchar(64) NOT NULL COMMENT '录入ip',
  `create_userid` int(11) NOT NULL COMMENT '录入人员',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_ip` varchar(64) NOT NULL COMMENT '更新ip',
  `update_userid` int(11) NOT NULL COMMENT '更新人员'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='售楼部';

--
-- 转存表中的数据 `mf_newhouse_department_items`
--

INSERT INTO `mf_newhouse_department_items` (`id`, `name`, `cop_id`, `address`, `state`, `create_time`, `create_ip`, `create_userid`, `update_time`, `update_ip`, `update_userid`) VALUES
(1, '一个售楼部', 1, '安徽省合肥市包河区滨湖新区大道交口', 1, '2020-05-09 23:17:15', '211.162.8.9', 1, '2020-05-10 14:12:50', '112.32.88.221', 1);

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_department_logs`
--

CREATE TABLE `mf_newhouse_department_logs` (
  `id` int(11) NOT NULL,
  `camera_id` int(11) NOT NULL COMMENT '摄像机',
  `cop_id` int(11) NOT NULL COMMENT '公司',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_ip` varchar(64) NOT NULL COMMENT '创建ip',
  `face_token` varchar(64) DEFAULT NULL COMMENT '人脸token',
  `customer` int(64) NOT NULL COMMENT '绑定顾客',
  `update_userid` varchar(64) NOT NULL COMMENT '更新人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `capture_img` varchar(1024) NOT NULL COMMENT '抓拍图片路径'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `mf_newhouse_department_logs`
--

INSERT INTO `mf_newhouse_department_logs` (`id`, `camera_id`, `cop_id`, `create_time`, `create_ip`, `face_token`, `customer`, `update_userid`, `update_time`, `capture_img`) VALUES
(1, 1, 1, '2020-05-10 00:00:00', '192.19.200.2', '1', 1, '1', '2020-05-10 00:00:00', '/uploads/20200315/7aa2456df9117c3bf7b08b73bac90991.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_depart_cameras`
--

CREATE TABLE `mf_newhouse_depart_cameras` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_userid` int(11) NOT NULL,
  `create_ip` varchar(64) NOT NULL,
  `update_time` datetime NOT NULL,
  `update_userid` int(11) NOT NULL,
  `update_ip` varchar(64) NOT NULL,
  `cop_id` int(11) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `ip` varchar(64) NOT NULL COMMENT '地址',
  `brand` varchar(64) NOT NULL COMMENT '品牌'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='摄像机';

--
-- 转存表中的数据 `mf_newhouse_depart_cameras`
--

INSERT INTO `mf_newhouse_depart_cameras` (`id`, `department_id`, `create_time`, `create_userid`, `create_ip`, `update_time`, `update_userid`, `update_ip`, `cop_id`, `description`, `ip`, `brand`) VALUES
(1, 1, '2020-05-10 14:36:56', 1, '112.32.88.221', '0000-00-00 00:00:00', 0, '', 1, '地铁阜南路工地内-1', '192.168.1.14', '海康');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_report_follow`
--

CREATE TABLE `mf_newhouse_report_follow` (
  `id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL COMMENT '报备id',
  `createtime` int(10) NOT NULL COMMENT '登记时间',
  `create_userid` int(11) NOT NULL COMMENT '登记人',
  `updatetime` int(10) NOT NULL COMMENT '更新时间',
  `update_userid` int(11) NOT NULL COMMENT '更新人',
  `contents` text NOT NULL COMMENT '跟进内容',
  `state` int(11) NOT NULL COMMENT '跟进状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新房跟进';

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_report_items`
--

CREATE TABLE `mf_newhouse_report_items` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL COMMENT '客户id',
  `cop_id` int(11) NOT NULL COMMENT '所属公司',
  `building_id` int(11) NOT NULL COMMENT '楼盘id',
  `createtime` int(11) NOT NULL COMMENT '登记时间',
  `verify_friend` tinyint(1) NOT NULL COMMENT '老带新审核',
  `create_userid` int(11) NOT NULL COMMENT '登记人',
  `prepare_visit` int(11) NOT NULL COMMENT '预计看房时间',
  `updatetime` int(11) NOT NULL COMMENT '更新时间',
  `update_userid` int(11) NOT NULL COMMENT '更新人',
  `contents` varchar(4096) NOT NULL COMMENT '报备说明',
  `verify_userid` int(11) NOT NULL COMMENT '审核人',
  `verify_mem` varchar(4096) NOT NULL COMMENT '审核说明',
  `verify_time` int(11) NOT NULL COMMENT '审核时间',
  `verify_state` enum('未审核','有效','无效') NOT NULL DEFAULT '未审核' COMMENT '审核状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新房报备';

--
-- 转存表中的数据 `mf_newhouse_report_items`
--

INSERT INTO `mf_newhouse_report_items` (`id`, `customer_id`, `cop_id`, `building_id`, `createtime`, `verify_friend`, `create_userid`, `prepare_visit`, `updatetime`, `update_userid`, `contents`, `verify_userid`, `verify_mem`, `verify_time`, `verify_state`) VALUES
(2, 3, 0, 5, 1577156884, 0, 11, 0, 1577157678, 11, '', 0, '', 0, '未审核'),
(3, 2, 0, 5, 1577157085, 0, 11, 1577329825, 1577158807, 0, '', 0, '', 0, '未审核'),
(5, 2, 0, 4, 1577157212, 0, 11, 1577416225, 1577158807, 0, '', 0, '', 0, '未审核'),
(6, 3, 0, 7, 1577157225, 0, 11, 1577157595, 1577157678, 0, '', 0, '', 0, '未审核'),
(7, 612, 0, 7, 1577610034, 0, 13, 0, 1577610034, 13, '', 0, '', 0, '未审核'),
(8, 614, 0, 7, 1577610192, 0, 13, 0, 1577610192, 13, '', 0, '', 0, '未审核'),
(9, 615, 0, 7, 1577610467, 0, 13, 0, 1577610467, 13, '', 0, '', 0, '未审核'),
(10, 616, 0, 7, 1577610578, 0, 13, 0, 1577610578, 13, '', 0, '', 0, '未审核'),
(11, 617, 0, 7, 1577610686, 0, 13, 0, 1577610686, 13, '', 0, '', 0, '未审核'),
(12, 618, 0, 7, 1577610756, 0, 13, 0, 1577610756, 13, '', 0, '', 0, '未审核'),
(13, 619, 0, 7, 1577610808, 0, 13, 0, 1577610808, 13, '', 0, '', 0, '未审核'),
(14, 684, 0, 7, 1577927944, 0, 13, 0, 1577927944, 13, '', 0, '', 0, '未审核'),
(15, 686, 0, 7, 1577928116, 0, 13, 0, 1577928116, 13, '', 0, '', 0, '未审核'),
(16, 687, 0, 7, 1577928205, 0, 13, 0, 1577928205, 13, '', 0, '', 0, '未审核'),
(17, 688, 0, 7, 1577928393, 0, 13, 0, 1577928393, 13, '', 0, '', 0, '未审核'),
(18, 689, 0, 7, 1577928687, 0, 13, 0, 1577928687, 13, '', 0, '', 0, '未审核'),
(19, 690, 0, 7, 1577928760, 0, 13, 0, 1577928760, 13, '', 0, '', 0, '未审核'),
(20, 691, 0, 7, 1577928853, 0, 13, 0, 1577928853, 13, '', 0, '', 0, '未审核'),
(21, 692, 0, 7, 1577929100, 0, 13, 0, 1577929100, 13, '', 0, '', 0, '未审核'),
(22, 693, 0, 7, 1577929204, 0, 13, 0, 1577929204, 13, '', 0, '', 0, '未审核'),
(23, 698, 0, 7, 1577947004, 0, 13, 0, 1577947004, 13, '', 0, '', 0, '未审核'),
(24, 699, 0, 7, 1577947654, 0, 13, 0, 1577947654, 13, '', 0, '', 0, '未审核'),
(25, 700, 0, 7, 1577947765, 0, 13, 0, 1577947765, 13, '', 0, '', 0, '未审核'),
(26, 701, 0, 7, 1577947927, 0, 13, 0, 1577947927, 13, '', 0, '', 0, '未审核'),
(27, 702, 0, 7, 1577948056, 0, 13, 0, 1577948056, 13, '', 0, '', 0, '未审核'),
(28, 703, 0, 7, 1577948142, 0, 13, 0, 1577948142, 13, '', 0, '', 0, '未审核'),
(29, 704, 0, 7, 1577948298, 0, 13, 0, 1577948298, 13, '', 0, '', 0, '未审核'),
(30, 726, 0, 7, 1578190573, 0, 13, 0, 1578190573, 13, '', 0, '', 0, '未审核'),
(31, 752, 0, 7, 1578199044, 0, 13, 0, 1578199044, 13, '', 0, '', 0, '未审核'),
(32, 753, 0, 7, 1578199189, 0, 13, 0, 1578199189, 13, '', 0, '', 0, '未审核'),
(33, 754, 0, 7, 1578199503, 0, 13, 0, 1578199503, 13, '', 0, '', 0, '未审核'),
(34, 755, 0, 7, 1578199619, 0, 13, 0, 1578199619, 13, '', 0, '', 0, '未审核'),
(35, 756, 0, 7, 1578199764, 0, 13, 0, 1578199764, 13, '', 0, '', 0, '未审核'),
(36, 757, 0, 7, 1578199843, 0, 13, 0, 1578199843, 13, '', 0, '', 0, '未审核'),
(37, 758, 0, 7, 1578199937, 0, 13, 0, 1578199937, 13, '', 0, '', 0, '未审核'),
(38, 784, 0, 7, 1578475842, 0, 13, 0, 1578475842, 13, '', 0, '', 0, '未审核'),
(39, 785, 0, 7, 1578476142, 0, 13, 0, 1578476142, 13, '', 0, '', 0, '未审核'),
(40, 801, 0, 7, 1578724389, 0, 13, 0, 1578724389, 13, '', 0, '', 0, '未审核'),
(41, 802, 0, 7, 1578724522, 0, 13, 0, 1578724522, 13, '', 0, '', 0, '未审核'),
(42, 803, 0, 7, 1578724688, 0, 13, 0, 1578724688, 13, '', 0, '', 0, '未审核'),
(43, 804, 0, 7, 1578724806, 0, 13, 0, 1578724806, 13, '', 0, '', 0, '未审核'),
(44, 805, 0, 7, 1578724958, 0, 13, 0, 1578724958, 13, '', 0, '', 0, '未审核'),
(45, 806, 0, 7, 1578725216, 0, 13, 0, 1578725216, 13, '', 0, '', 0, '未审核'),
(46, 807, 0, 7, 1578725379, 0, 13, 0, 1578725379, 13, '', 0, '', 0, '未审核'),
(47, 808, 0, 7, 1578725526, 0, 13, 0, 1578725526, 13, '', 0, '', 0, '未审核'),
(48, 809, 0, 7, 1578725613, 0, 13, 0, 1578725613, 13, '', 0, '', 0, '未审核'),
(49, 810, 0, 7, 1578725734, 0, 13, 0, 1578725734, 13, '', 0, '', 0, '未审核'),
(50, 811, 0, 7, 1578725900, 0, 13, 0, 1578725900, 13, '', 0, '', 0, '未审核'),
(51, 812, 0, 7, 1578726010, 0, 13, 0, 1578726010, 13, '', 0, '', 0, '未审核'),
(52, 813, 0, 7, 1578726172, 0, 13, 0, 1578726172, 13, '', 0, '', 0, '未审核'),
(53, 814, 0, 7, 1578726265, 0, 13, 0, 1578726265, 13, '', 0, '', 0, '未审核'),
(54, 815, 0, 7, 1578726349, 0, 13, 0, 1578726349, 13, '', 0, '', 0, '未审核'),
(55, 816, 0, 7, 1578726432, 0, 13, 0, 1578726432, 13, '', 0, '', 0, '未审核'),
(56, 817, 0, 7, 1578726542, 0, 13, 0, 1578726542, 13, '', 0, '', 0, '未审核'),
(57, 843, 0, 7, 1578967075, 0, 13, 0, 1578967075, 13, '', 0, '', 0, '未审核'),
(58, 844, 0, 7, 1578967245, 0, 13, 0, 1578967245, 13, '', 0, '', 0, '未审核'),
(59, 845, 0, 7, 1578967320, 0, 13, 0, 1578967320, 13, '', 0, '', 0, '未审核'),
(60, 846, 0, 7, 1579055429, 0, 13, 0, 1579055429, 13, '', 0, '', 0, '未审核'),
(61, 847, 0, 7, 1579055535, 0, 13, 0, 1579055535, 13, '', 0, '', 0, '未审核'),
(62, 848, 0, 7, 1579055667, 0, 13, 0, 1579055667, 13, '', 0, '', 0, '未审核'),
(63, 849, 0, 7, 1579400995, 0, 13, 0, 1579400995, 13, '', 0, '', 0, '未审核'),
(64, 850, 0, 7, 1579402242, 0, 13, 0, 1579402242, 13, '', 0, '', 0, '未审核'),
(65, 851, 0, 7, 1579402476, 0, 13, 0, 1579402476, 13, '', 0, '', 0, '未审核'),
(66, 852, 0, 7, 1579402735, 0, 13, 0, 1579402735, 13, '', 0, '', 0, '未审核'),
(67, 853, 0, 7, 1579417163, 0, 13, 0, 1579417163, 13, '', 0, '', 0, '未审核'),
(68, 854, 0, 7, 1579417271, 0, 13, 0, 1579417271, 13, '', 0, '', 0, '未审核'),
(69, 855, 0, 7, 1579417403, 0, 13, 0, 1579417403, 13, '', 0, '', 0, '未审核'),
(70, 856, 0, 7, 1579417560, 0, 13, 0, 1579417560, 13, '', 0, '', 0, '未审核'),
(71, 857, 0, 7, 1579417663, 0, 13, 0, 1579417663, 13, '', 0, '', 0, '未审核'),
(72, 858, 0, 7, 1579417755, 0, 13, 0, 1579417755, 13, '', 0, '', 0, '未审核'),
(73, 860, 0, 7, 1583117482, 0, 13, 0, 1583117482, 13, '', 0, '', 0, '未审核'),
(74, 968, 0, 5, 1584329309, 0, 11, 0, 1584329309, 0, '', 0, '', 0, '未审核'),
(75, 1089, 0, 7, 1585299044, 0, 13, 0, 1585299044, 13, '', 0, '', 0, '未审核'),
(76, 1, 0, 58, 1609207344, 0, 1, 1608861742, 1609207427, 0, '', 1, '123123', 1609207427, '未审核');

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_report_joint`
--

CREATE TABLE `mf_newhouse_report_joint` (
  `id` int(11) NOT NULL,
  `cratetime` int(11) NOT NULL COMMENT '报备时间',
  `report_item_id` int(11) NOT NULL COMMENT '报备id',
  `alias_name` varchar(64) NOT NULL COMMENT '共有产权人姓名',
  `alias_tel` varchar(64) NOT NULL COMMENT '共有产权人电话',
  `create_userid` int(11) NOT NULL COMMENT '操作人',
  `relationship` enum('未知','夫妻','父母','子女','亲戚','朋友') NOT NULL COMMENT '客户关系'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报备共有产权人';

-- --------------------------------------------------------

--
-- 表的结构 `mf_newhouse_report_state`
--

CREATE TABLE `mf_newhouse_report_state` (
  `id` int(11) NOT NULL,
  `state` varchar(16) NOT NULL COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `mf_newhouse_report_state`
--

INSERT INTO `mf_newhouse_report_state` (`id`, `state`) VALUES
(1, '无效'),
(2, '有效'),
(3, '洽谈中'),
(4, '已认筹'),
(5, '已签约'),
(6, '已结佣');

-- --------------------------------------------------------

--
-- 表的结构 `mf_rent_follow`
--

CREATE TABLE `mf_rent_follow` (
  `id` int(11) NOT NULL COMMENT '跟进编号',
  `item_id` int(11) NOT NULL COMMENT '租房编号',
  `create_userid` int(11) NOT NULL COMMENT '跟进人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '跟进时间',
  `state` int(11) NOT NULL COMMENT '房源状态 0在租 1停租 2已租 3无效	',
  `comment` varchar(8000) NOT NULL COMMENT '跟进内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='租房跟进';

--
-- 转存表中的数据 `mf_rent_follow`
--

INSERT INTO `mf_rent_follow` (`id`, `item_id`, `create_userid`, `create_time`, `state`, `comment`) VALUES
(1, 1, 1, '2020-12-05 13:50:06', 0, '2222'),
(2, 1, 1, '2020-12-05 14:16:00', 1, '22222'),
(3, 1, 1, '2020-12-31 01:46:12', 0, '1515');

-- --------------------------------------------------------

--
-- 表的结构 `mf_rent_items`
--

CREATE TABLE `mf_rent_items` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL COMMENT '小区',
  `areaCode` int(11) NOT NULL COMMENT '地区码',
  `customer_name` varchar(32) NOT NULL COMMENT '房主姓名',
  `shi_count` int(11) NOT NULL COMMENT '几室',
  `ting_count` int(11) NOT NULL COMMENT '几厅',
  `floor` int(11) NOT NULL COMMENT '所在层',
  `floor_total` int(11) NOT NULL COMMENT '总层',
  `add_userid` int(11) NOT NULL,
  `add_cop_id` int(11) NOT NULL,
  `add_time` int(11) NOT NULL,
  `update_userid` int(11) NOT NULL COMMENT '最后更新',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `update_ip` varchar(64) NOT NULL COMMENT '更新IP',
  `update_cop_id` int(11) NOT NULL COMMENT '更新组织id',
  `address` varchar(96) NOT NULL COMMENT '地址',
  `dong` varchar(16) NOT NULL COMMENT '栋',
  `decoration_id` int(11) NOT NULL COMMENT '装修',
  `type_id` int(11) NOT NULL COMMENT '房屋类型',
  `shi` varchar(16) NOT NULL COMMENT '室',
  `view_level` int(11) NOT NULL COMMENT '0不限 1本公司 2本门店 3个人',
  `build_year` int(11) DEFAULT NULL COMMENT '建成年代',
  `add_ip` varchar(64) NOT NULL,
  `area` float NOT NULL COMMENT '面积',
  `mobile` varchar(1024) NOT NULL COMMENT '电话',
  `unit_price` float NOT NULL COMMENT '单价',
  `deposit` float NOT NULL COMMENT '押金',
  `state` int(11) NOT NULL COMMENT '0在售 1停售 2已售 3无效',
  `mem` varchar(1024) NOT NULL COMMENT '备注'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='租房';

--
-- 转存表中的数据 `mf_rent_items`
--

INSERT INTO `mf_rent_items` (`id`, `building_id`, `areaCode`, `customer_name`, `shi_count`, `ting_count`, `floor`, `floor_total`, `add_userid`, `add_cop_id`, `add_time`, `update_userid`, `update_time`, `update_ip`, `update_cop_id`, `address`, `dong`, `decoration_id`, `type_id`, `shi`, `view_level`, `build_year`, `add_ip`, `area`, `mobile`, `unit_price`, `deposit`, `state`, `mem`) VALUES
(1, 37282, 0, '先生', 2, 3, 2, 2, 0, 0, 0, 1, 1609379172, '36.27.127.195', 1, '天山路与紫云路交叉口', '1', 2, 1, '221', 0, 2001, '', 222, '1398888888', 1000, 1000, 0, ''),
(2, 37266, 450305, '余先生', 2, 1, 12, 32, 1, 1, 1608805153, 1, 1608805153, '113.102.160.209', 1, 'AABB', '1栋', 2, 1, '1203', 0, 0, '113.102.160.209', 75, '19966668888', 2300, 4600, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_second_buildings`
--

CREATE TABLE `mf_second_buildings` (
  `id` int(11) NOT NULL,
  `building_name` varchar(96) NOT NULL COMMENT '楼盘名称',
  `building_name_en` varchar(64) NOT NULL COMMENT '拼音首字母',
  `address` varchar(256) DEFAULT NULL COMMENT '地址',
  `business` varchar(256) DEFAULT NULL COMMENT '商业',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `areaCode` varchar(32) NOT NULL,
  `areaName` varchar(96) NOT NULL COMMENT '区域广本',
  `alias_name1` varchar(32) DEFAULT NULL COMMENT '别名1',
  `alias_name2` varchar(32) DEFAULT NULL COMMENT '别名2',
  `second_count` int(11) NOT NULL COMMENT '二手房数量',
  `second_onsell` int(11) NOT NULL COMMENT '二手房在售数量',
  `metro_count` int(11) NOT NULL COMMENT '周围1.5公里地铁站数量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='二手房楼盘表';

--
-- 转存表中的数据 `mf_second_buildings`
--

INSERT INTO `mf_second_buildings` (`id`, `building_name`, `building_name_en`, `address`, `business`, `latitude`, `longitude`, `areaCode`, `areaName`, `alias_name1`, `alias_name2`, `second_count`, `second_onsell`, `metro_count`) VALUES
(37200, '东方新城', '', '安徽省合肥市包河区淝河路,王大郢', '淝河路,王大郢,铜陵路', 31.839101, 117.332709, '340111', '安徽省/合肥市/包河区', '', '', 2, 2, 0),
(37201, '华悦公馆', '', '安徽省合肥市肥西县仙霞路', '仙霞路', 31.722583, 117.165134, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37202, '颐和花园耕苑', '', '青阳路,淠河路', '史河路,青阳路,淠河路', 31.864606, 117.245613, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37203, '文一锦门学府里', '', '安徽省合肥市瑶海区襄水路', '临泉东路,新城开发区', 31.870166, 117.40204, '340102', '安徽省/合肥市/瑶海区', '', '', 2, 2, 0),
(37204, '曙光路21号 ', '', '曙光路21号 ', '曙光路21号 ', 31.866842, 117.282699, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37205, '巢湖碧桂园翠山映麓苑', '', '安徽省合肥市巢湖市', '巢湖市七中', 31.586645, 117.833312, '341402', '安徽省/合肥市/居巢区', '', '', 1, 1, 0),
(37206, '凤阳东路南侧综合楼', '', '凤阳东路', '凤阳东路', 31.866842, 117.282699, '340131', '安徽省/合肥市/新站区', '', '', 1, 1, 0),
(37207, '天鹅湖万达广场', '', '南二环与怀宁路交口', '二环路,绿地蓝海,南七', 31.826136, 117.227699, '340132', '安徽省/合肥市/政务区', '', '', 6, 6, 0),
(37208, '天玥中心', '', '安徽省合肥市蜀山区潜山路', '淠河路,凤凰城,十里庙', 31.860341, 117.236578, '340104', '安徽省/合肥市/蜀山区', '', '', 4, 4, 0),
(37209, '融侨观澜', '', '安徽省合肥市包河区龙川路辅路', '宿松路,荷叶地', 31.802017, 117.260131, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37210, '庐阳区世纪中心', '', '安徽省合肥市庐阳区濉溪路295号', '沿河路,亳州路,濉溪路', 31.884887, 117.271459, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37212, '金寨路369号教院宿舍', '', '金寨路369号', '金寨路369号', 31.866842, 117.282699, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37213, '静安新天地', '', '临泉东路,通达路', '临泉东路,通达路,龙岗', 31.882817, 117.368816, '340102', '安徽省/合肥市/瑶海区', '', '', 2, 2, 0),
(37214, '荣盛华府', '', '云南路与遵义路交叉口', '云南路', 31.703736, 117.274045, '340130', '安徽省/合肥市/滨湖区', '', '', 1, 1, 0),
(37215, '阳光理想橙', '', '安徽省合肥市肥西县长安路', '科学大道', 31.813481, 117.187816, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37216, '望湖水苑', '', '安徽省合肥市包河区祁门路118号', '南二环,葛大店,马鞍山路/马鞍山', 31.818235, 117.307424, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37217, '玲珑大厦', '', '安徽省合肥市包河区青年路11号', '马鞍山路/马鞍山,宁国路,南二环', 31.825937, 117.305645, '340111', '安徽省/合肥市/包河区', '', '', 4, 4, 0),
(37218, '金色假日', '', '安徽省合肥市长丰县鹤翔湖路', '鹤翔湖路', 32.011927, 117.276913, '340121', '安徽省/合肥市/长丰县', '', '', 2, 2, 0),
(37219, '古城家园', '', '肥西县三河镇', '三河镇', 31.866842, 117.282699, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37220, '临湖南苑', '', '幸福三路与幸福二路交叉口', '幸福路', 31.689222, 117.477889, '340122', '安徽省/合肥市/肥东县', '', '', 1, 1, 0),
(37221, '文苑小区', '', '中兴路与官亭路交叉口西100米', '中兴路与官亭路交叉口西100米', 31.866842, 117.282699, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37222, '云顶雅苑', '', '安徽省合肥市庐阳区阜阳北路辅路', '阜阳北路', 31.943298, 117.279152, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37223, '新华实验中学', '', '阜阳北路187号', '阜阳北路187号', 31.938781, 117.281354, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37224, '紫荆花园紫荆阁', '', '安徽省合肥市包河区黄山路23号', '徽州大道,东陈岗,屯溪路', 31.847588, 117.289907, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37225, '天润大厦', '', '芙蓉路642号', '明珠广场,桃花', 31.793134, 117.230178, '340133', '安徽省/合肥市/经开区', '', '', 1, 1, 0),
(37226, '繁华新园', '', '繁华大道长古路', '繁华大道长古路', 31.78192, 117.175739, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37227, '国贸天悦', '', '安徽省合肥市包河区西递路', '宿松路,常青,东流路', 31.807323, 117.271342, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37228, '蓝光禹洲城', '', '淠河路', '十里庙,淠河路,凤凰城', 31.861281, 117.232963, '340104', '安徽省/合肥市/蜀山区', '', '', 2, 2, 0),
(37229, '淮河路422号', '', '淮河路422号', '淮河路422号', 31.866842, 117.282699, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37230, '奥园城市天地', '', '安徽省合肥市蜀山区田埠东路', '井岗,大铺头', 31.858457, 117.173212, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37231, '安徽国际金融中心', '', '安徽省合肥市蜀山区梅山路18-7号', '芜湖路,屯溪路,三孝口', 31.856553, 117.275449, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37232, '四方花苑', '', '安徽省合肥市蜀山区金寨南路1049号', '桃花,明珠广场', 31.780052, 117.225455, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37233, '悦方国际中心', '', '湖北路', '湖北路', 31.745768, 117.312725, '340130', '安徽省/合肥市/滨湖区', '', '', 1, 1, 0),
(37234, '招商雍华府', '', '滨湖新区包河大道与南京路交叉口东北方向110米', '包河大道与南京路', 31.756546, 117.327481, '340130', '安徽省/合肥市/滨湖区', '', '', 1, 1, 0),
(37235, '置地创新中心', '', '安徽省合肥市蜀山区环形路', '井岗', 31.855583, 117.139719, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37236, '金龙国际广场', '', '安徽省合肥市庐阳区肥西北路', '亳州路,濉溪路,四里河路', 31.887051, 117.260702, '340103', '安徽省/合肥市/庐阳区', '', '', 2, 2, 0),
(37237, '缤纷南国沐风居', '', '安徽省合肥市庐阳区义井路1号', '濉溪路,亳州路,颍上路', 31.889625, 117.275465, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37238, '金寨路高速管理局宿舍', '', '金寨路888号', '金寨路', 31.866842, 117.282699, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37240, '柏景湾青云轩', '', '安徽省合肥市庐阳区可苑路中段', '亳州路,财富广场,濉溪路', 31.882968, 117.278023, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37241, '宝宸时代花园', '', '安徽省合肥市庐阳区官塘路22号', '官塘路', 31.9373, 117.282266, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37242, '肥西四中宿舍', '', '安徽省合肥市肥西县人民路', '人民路', 31.721751, 117.177159, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37243, '紫荆花园福乐阁', '', '安徽省合肥市包河区黄山路23号', '东陈岗,徽州大道,桐城路', 31.846877, 117.289542, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37244, '天王巷', '', '安徽省合肥市庐阳区天王巷4号', '长江中路,三孝口,安庆路', 31.866494, 117.276248, '340103', '安徽省/合肥市/庐阳区', '', '', 3, 3, 0),
(37245, '置地栢景轩', '', '铜陵路,大通路', '铜陵路,大通路,长江东大街', 31.862205, 117.331873, '340102', '安徽省/合肥市/瑶海区', '', '', 1, 1, 0),
(37246, '益民街36号', '', '益民街36号', '益民街36号', 31.866842, 117.282699, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37247, '弘阳时光里', '', '安徽省合肥市长丰县阜阳北路辅路', '阜阳北路辅路', 31.967943, 117.270935, '340121', '安徽省/合肥市/长丰县', '', '', 1, 1, 0),
(37248, '巴厘阳光', '', '安徽省合肥市蜀山区金寨南路1054号', '桃花,大学城,中环城', 31.774238, 117.222319, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37249, '金水苑', '', '安徽省合肥市肥西县巢湖路74号', '巢湖路', 31.720019, 117.162978, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37250, '金色池塘二期', '', '环湖东路,清溪路,', '环湖东路,清溪路,科学大道', 31.873213, 117.216934, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37251, '求实领势学府', '', '安徽省合肥市蜀山区潜山路372号', '陈村路,青阳路,贵池路', 31.852535, 117.238483, '340104', '安徽省/合肥市/蜀山区', '', '', 2, 2, 0),
(37252, '福泉花园', '', '安徽省合肥市肥东县浮槎山路', '石塘', 31.898393, 117.483385, '340122', '安徽省/合肥市/肥东县', '', '', 1, 1, 0),
(37253, '宁静苑', '', '安徽省合肥市瑶海区肥东路', '长江东大街,铜陵路,新安江路', 31.868212, 117.334454, '340102', '安徽省/合肥市/瑶海区', '', '', 1, 1, 0),
(37254, '国贸天琴湾', '', '习友路与黄宾虹路交口(新八中东南侧)', '科学大道,奥体中心,港澳广场', 31.809219, 117.215375, '340132', '安徽省/合肥市/政务区', '', '', 2, 2, 0),
(37255, '领势公馆', '', '贵池路,东至路', '贵池路,东至路,青阳路', 31.853935, 117.253442, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37256, '世纪阳光花园橙阳苑', '', '马鞍山路/马鞍山', '宁国路,马鞍山路/马鞍山,九华山路', 31.844046, 117.303587, '340111', '安徽省/合肥市/包河区', '', '', 13, 12, 0),
(37257, '蒙城路112号', '', '安徽省合肥市庐阳区蒙城路112号', '城隍庙,寿春路,淮河路步行街', 31.873144, 117.284602, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37258, '繁裕小区', '', '安徽省合肥市瑶海区繁昌路27号', '大通路,合裕路,铜陵路', 31.859354, 117.320112, '340102', '安徽省/合肥市/瑶海区', '', '', 1, 1, 0),
(37259, '中侨中心', '', '安徽省合肥市蜀山区', '绿地蓝海,荷叶地,南七', 31.830424, 117.236049, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37260, '华润万象城', '', '潜山路111号', '奥体中心,置地广场,明珠广场', 31.805831, 117.236895, '340132', '安徽省/合肥市/政务区', '', '', 1, 1, 0),
(37261, '安通缘梦天地', '', '金寨南路辅路', '桃花,大学城', 31.758781, 117.208272, '340133', '安徽省/合肥市/经开区', '', '', 1, 1, 0),
(37262, '合瓦路170号三十六中宿舍', '', ',砀山路,蒙城北路', '杏林,砀山路,蒙城北路', 31.899696, 117.286659, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37263, '芜湖路医药公司宿舍', '', '芜湖路', '芜湖路', 31.866842, 117.282699, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37264, '星海城', '', '安徽省合肥市包河区马鞍山南路5号', '马鞍山路/马鞍山,望江东路,南二环', 31.826847, 117.30768, '340111', '安徽省/合肥市/包河区', '', '', 3, 3, 0),
(37265, '宿州路163号', '', '三中对面', '淮河路步行街,寿春路,百花井', 31.873407, 117.296175, '340103', '安徽省/合肥市/庐阳区', '', '', 2, 2, 0),
(37266, '万科公园大道', '', '安徽省合肥市长丰县清颖路', '清颖路', 32.038105, 117.255826, '340121', '安徽省/合肥市/长丰县', '', '', 1, 0, 0),
(37267, '双岗老街5号', '', '双岗老街5号', '双岗老街5号', 31.866842, 117.282699, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37268, '万邻坊', '', '安徽省合肥市瑶海区明皇路', '临泉东路,当涂路,东二环', 31.886803, 117.363057, '340102', '安徽省/合肥市/瑶海区', '', '', 3, 3, 0),
(37269, '南岗畅园', '', '安徽省合肥市蜀山区磨子潭路', '井岗', 31.859783, 117.142134, '340104', '安徽省/合肥市/蜀山区', '', '', 3, 3, 0),
(37270, '绩溪路东村', '', '安徽省合肥市蜀山区绩溪路177号', '屯溪路,宿松路,芜湖路', 31.851181, 117.27774, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37271, '文峰中心', '', '金寨南路辅路', '明珠广场,桃花', 31.78393, 117.228881, '340133', '安徽省/合肥市/经开区', '', '', 1, 1, 0),
(37272, '加侨波普驻区', '', '安徽省合肥市肥西县玉兰大道', '玉兰大道', 31.80491, 117.164617, '340123', '安徽省/合肥市/肥西县', '', '', 2, 2, 0),
(37273, '北城世纪金源购物中心', '', '长丰县蒙城北路与双墩路交叉口', '蒙城北路与双墩路交叉口', 32.010586, 117.252746, '340121', '安徽省/合肥市/长丰县', '', '', 2, 2, 0),
(37274, '世纪阳光花园青阳苑', '', '安徽省合肥市包河区马鞍山路辅路', '合肥工业大学,马鞍山路/马鞍山,太湖路', 31.84159, 117.306916, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37275, '董铺大厦', '', '安徽省合肥市庐阳区西二环路辅路', '清溪路', 31.898226, 117.219628, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37276, '滨湖桂园', '', '玉龙路', '高速时代广场', 31.726839, 117.276922, '340133', '安徽省/合肥市/经开区', '', '', 3, 3, 0),
(37277, '融胜大厦', '', '安徽省合肥市蜀山区G312(长江西路)', '长江西路', 31.857624, 117.130905, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37278, '东方广场', '', '安徽省合肥市包河区香港街412号', '芜湖路,宁国路,屯溪路', 31.856051, 117.293742, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37279, '华夏国际茶博城', '', '安徽省合肥市包河区南翔二路', '骆岗', 31.774489, 117.327667, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37280, '至美大厦', '', '安徽省合肥市包河区一环路264号', '芜湖路,屯溪路,宁国路', 31.854098, 117.301526, '340111', '安徽省/合肥市/包河区', '', '', 1, 1, 0),
(37281, '濉溪路216号国防宿舍', '', '濉溪路216号', '濉溪路216号', 31.866842, 117.282699, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37282, '人才苑北村', '', '安徽省合肥市瑶海区定远路', '临泉东路,铜陵路,方庙', 31.879913, 117.337392, '340102', '安徽省/合肥市/瑶海区', '', '', 1, 1, 0),
(37283, '招商公园1872', '', '安徽省合肥市瑶海区梦溪路', '磨店', 31.90869, 117.393669, '340102', '安徽省/合肥市/瑶海区', '', '', 1, 1, 0),
(37284, '汇城雅居', '', '安徽省合肥市肥东县城关中学旁', '城关中学', 31.884888, 117.464593, '340122', '安徽省/合肥市/肥东县', '', '', 1, 1, 0),
(37285, '益民街25号', '', '益民街25号', '益民街25号', 31.866842, 117.282699, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37286, '金寨路330号', '', '金寨路330号', '金寨路330号', 31.866842, 117.282699, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37287, '合作经济广场', '', '安徽省合肥市庐阳区庐江路105号', '三孝口,桐城路,逍遥津', 31.86248, 117.286933, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37288, '绿缘居秋硕园', '', '安徽省合肥市蜀山区', '清溪路,环湖东路,科学大道', 31.877187, 117.223996, '340104', '安徽省/合肥市/蜀山区', '', '', 2, 1, 0),
(37289, '蜀沁园', '', '安徽省合肥市蜀山区洞山路', '史河路,淠河路,环湖东路', 31.868037, 117.228258, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37290, '金源雅苑', '', '安徽省合肥市瑶海区郎溪路', '龙岗,新安江路,临泉东路', 31.872403, 117.366613, '340102', '安徽省/合肥市/瑶海区', '', '', 1, 1, 0),
(37291, '朝东新村', '', '安徽省合肥市肥西县新华街42号', '新华街', 31.720433, 117.178955, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37292, '金科庐州樾', '', '安徽省合肥市庐阳区五河路178号', '五河路,濉溪路,蒙城北路', 31.882868, 117.292319, '340103', '安徽省/合肥市/庐阳区', '', '', 1, 1, 0),
(37293, '华冶家园', '', '安徽省合肥市蜀山区', '宿松路,屯溪路,芜湖路', 31.848796, 117.273205, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0),
(37294, '新华御湖庄园', '', '安徽省合肥市肥西县习友路与观澜路交汇处', '习友路与观澜路交汇处', 31.821515, 117.162474, '340123', '安徽省/合肥市/肥西县', '', '', 1, 1, 0),
(37295, '滨湖宝文中心', '', '庐州大道', '高速时代广场', 31.710574, 117.307702, '340130', '安徽省/合肥市/滨湖区', '', '', 1, 1, 0),
(37296, '西子曼城', '', '安徽省合肥市蜀山区望江西路辅路', '望江西路', 31.836742, 117.094916, '340104', '安徽省/合肥市/蜀山区', '', '', 1, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `mf_second_decoration`
--

CREATE TABLE `mf_second_decoration` (
  `id` int(11) NOT NULL,
  `decoration` varchar(32) NOT NULL COMMENT '装修'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='装修';

--
-- 转存表中的数据 `mf_second_decoration`
--

INSERT INTO `mf_second_decoration` (`id`, `decoration`) VALUES
(0, '未知'),
(1, '毛坯'),
(2, '简装'),
(3, '精装'),
(4, '豪装');

-- --------------------------------------------------------

--
-- 表的结构 `mf_second_follow`
--

CREATE TABLE `mf_second_follow` (
  `id` int(11) NOT NULL COMMENT '跟进编号',
  `item_id` int(11) NOT NULL COMMENT '二手房编号',
  `create_userid` int(11) NOT NULL COMMENT '跟进人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '跟进时间',
  `state` int(11) NOT NULL COMMENT '房源状态 0在售 1停售 2已售 3无效	',
  `comment` varchar(8000) NOT NULL COMMENT '跟进内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='二手房跟进';

--
-- 转存表中的数据 `mf_second_follow`
--

INSERT INTO `mf_second_follow` (`id`, `item_id`, `create_userid`, `create_time`, `state`, `comment`) VALUES
(3, 56, 1, '2019-07-14 08:28:08', 1, 'ef'),
(4, 7, 13, '2019-12-23 13:26:38', 1, '新活动'),
(5, 7, 1, '2020-01-02 14:39:26', 0, '测试跟进内容'),
(6, 7, 1, '2020-01-02 14:39:56', 1, '停售'),
(7, 10, 1, '2020-02-01 03:06:29', 1, '1'),
(8, 336238, 1, '2020-02-01 06:02:42', 0, '其中含阁楼26.81平米'),
(9, 336242, 1, '2020-02-01 06:46:03', 0, '无证'),
(10, 336246, 1, '2020-02-01 10:55:40', 0, ' 无证'),
(11, 336259, 1, '2020-02-01 13:00:10', 0, '  下9o.80     上49，几'),
(12, 309474, 1, '2020-02-02 11:02:57', 0, '245万'),
(13, 319242, 1, '2020-02-03 02:21:26', 0, '此房在售138万 里面有租客 看房有准客户可以联系房主看'),
(14, 336342, 18, '2020-02-03 05:22:16', 0, '  直更名'),
(15, 336345, 18, '2020-02-03 05:33:02', 0, '下面是自行车市场'),
(16, 336349, 18, '2020-02-03 06:26:22', 0, '2018.11 月份证'),
(17, 336355, 18, '2020-02-03 06:35:56', 0, '复试 '),
(18, 318381, 18, '2020-02-03 06:39:14', 0, '228W'),
(19, 336379, 18, '2020-02-04 01:41:21', 0, '57+101.ji 平方'),
(20, 336381, 18, '2020-02-04 01:50:51', 0, '  钥匙在1102'),
(21, 336383, 18, '2020-02-04 01:53:12', 0, '87+40.ji  平方 复试'),
(22, 336392, 18, '2020-02-04 03:32:15', 0, ' 说出租在暂时不卖了，看房不方便'),
(23, 336394, 18, '2020-02-04 03:43:24', 0, '证12年 钥匙放在易居房友'),
(24, 336395, 18, '2020-02-04 03:43:38', 0, '证12年 钥匙放在易居房友'),
(25, 336399, 18, '2020-02-04 05:11:03', 0, ' 老人接的，要孩子号码说暂时不给，要一次性付款才卖'),
(26, 336404, 18, '2020-02-04 05:42:11', 0, '再打说不卖了'),
(27, 317579, 18, '2020-02-04 07:54:41', 0, '  398万    精装'),
(28, 228002, 18, '2020-02-04 07:58:14', 0, '证有5/6年了'),
(29, 336411, 18, '2020-02-04 08:07:33', 0, ' 钥匙在和平广场的辉达'),
(30, 336412, 18, '2020-02-04 08:10:52', 0, '  自住    不能贷款必须一次性付款 '),
(31, 336413, 18, '2020-02-04 08:11:39', 0, '  朋友在住'),
(32, 336418, 18, '2020-02-04 08:42:09', 0, '带车位  '),
(33, 336422, 18, '2020-02-04 08:47:54', 0, ' 说暂时不卖了，家里人不同意'),
(34, 292662, 18, '2020-02-04 08:51:38', 0, '价格不合适暂时不卖  低于195万都不卖'),
(35, 336423, 18, '2020-02-04 09:33:22', 0, ' 说给中介代理'),
(36, 336439, 18, '2020-02-04 10:01:32', 0, ' 含一个产权车位  一期 '),
(37, 336450, 18, '2020-02-05 02:04:00', 0, '证拿到一年半  '),
(38, 247319, 18, '2020-02-05 02:05:00', 0, ' 43万 可以谈  满5唯一'),
(39, 336452, 18, '2020-02-05 02:07:29', 0, ' 自住'),
(40, 336458, 18, '2020-02-05 02:14:48', 0, '四五月份在卖'),
(41, 336460, 18, '2020-02-05 02:23:39', 0, '11年买的  17年有一次变更 复试'),
(42, 290978, 18, '2020-02-05 02:26:14', 0, '  93.7  145万无税'),
(43, 336463, 18, '2020-02-05 02:27:30', 0, '17年8月份证 '),
(44, 179482, 18, '2020-02-05 07:50:30', 0, '55万'),
(45, 336521, 18, '2020-02-05 09:41:22', 0, '有一个车位11万不包含在内'),
(46, 336525, 18, '2020-02-05 10:12:43', 0, ' 没下证还不知道证面积多大'),
(47, 336551, 18, '2020-02-06 03:35:00', 0, '9#1211-1212'),
(48, 336560, 18, '2020-02-06 04:09:59', 0, '复式 90+楼上证上125  '),
(49, 336557, 18, '2020-02-06 04:10:30', 0, '57+101.几平方 复试'),
(50, 336564, 18, '2020-02-06 05:50:05', 0, '出租了    直接去敲门  无税 精装 '),
(51, 309257, 18, '2020-02-06 11:13:53', 0, '105万'),
(52, 336584, 18, '2020-02-06 11:34:08', 0, '房子的价格是228万+15万的车位必须绑定一起卖'),
(53, 336606, 18, '2020-02-07 01:52:53', 0, '钥匙在家天下三期的链家'),
(54, 161456, 18, '2020-02-07 02:14:03', 0, '180万 满五唯一 尾款30多万 目前还在里面住 看房提前约'),
(55, 336690, 18, '2020-02-08 01:44:41', 0, '1701、1704 '),
(56, 336695, 18, '2020-02-08 02:27:07', 0, '1509-1510 '),
(57, 336789, 18, '2020-02-10 02:47:20', 0, '证上就是商住楼401'),
(58, 336794, 18, '2020-02-10 02:55:05', 0, '商住楼 '),
(59, 336827, 18, '2020-02-10 06:47:37', 0, '商住楼'),
(60, 336831, 18, '2020-02-10 06:52:48', 0, '有车位 无税'),
(61, 336834, 18, '2020-02-10 06:54:27', 0, '实际面积大，现做门面，在双岗小学门口   '),
(62, 336835, 18, '2020-02-10 07:05:43', 0, '915、916、917、三套打通一起卖    '),
(63, 336836, 18, '2020-02-10 07:05:50', 0, '915、916、917、三套打通一起卖    '),
(64, 336837, 18, '2020-02-10 07:05:57', 0, '915、916、917、三套打通一起卖    '),
(65, 336840, 18, '2020-02-10 07:10:41', 0, '是沿阜阳北路的门面，租给永辉房产，一半单间上下两层，证地址记不清，应该是杏林南村综合楼1号10号'),
(66, 336870, 18, '2020-02-11 00:33:12', 0, '实际220平方'),
(67, 336871, 18, '2020-02-11 00:33:42', 0, '带个院子  '),
(68, 336880, 18, '2020-02-11 01:32:22', 0, '法拍的  ，面积大概515，1337是起拍价'),
(69, 336885, 18, '2020-02-11 03:10:30', 0, ' 最低190万 多卖四六分 证四年多'),
(70, 336958, 18, '2020-02-11 07:18:28', 0, '实际60-70 平米 '),
(71, 337022, 18, '2020-02-11 08:41:44', 0, '东区  挑高'),
(72, 337023, 18, '2020-02-11 08:42:13', 0, '有个院子 新装修'),
(73, 337041, 18, '2020-02-11 09:43:32', 0, '复试，楼下82楼上90'),
(74, 337073, 18, '2020-02-12 02:42:13', 0, '包含车位'),
(75, 337119, 18, '2020-02-12 07:51:35', 0, '说是 胜利路生活小区'),
(76, 337163, 18, '2020-02-13 02:25:08', 0, '钥匙在上乘房产'),
(77, 337175, 18, '2020-02-13 04:02:48', 0, '1201-1210  10间'),
(78, 337180, 18, '2020-02-13 05:57:04', 0, '包含车位'),
(79, 337192, 18, '2020-02-13 06:32:26', 0, '楼上自己盖了有50平方'),
(80, 336989, 18, '2020-02-13 07:05:08', 0, '精装 60P院子'),
(81, 337234, 18, '2020-02-14 00:12:10', 0, '带暖气'),
(82, 337246, 18, '2020-02-14 01:40:44', 0, '自己在住'),
(83, 337283, 18, '2020-02-14 06:38:54', 0, '107-207'),
(84, 337311, 18, '2020-02-14 08:31:23', 0, '106-206号'),
(85, 337339, 18, '2020-02-15 01:45:15', 0, '615-622 '),
(86, 332016, 18, '2020-02-15 05:42:50', 0, ' 有个院子'),
(87, 53943, 18, '2020-02-15 06:15:59', 0, '一楼的4.5m高带院子'),
(88, 337435, 18, '2020-02-17 01:58:23', 0, '带院子30平'),
(89, 337454, 18, '2020-02-17 05:06:34', 0, '去年才拿的证'),
(90, 337463, 18, '2020-02-17 06:34:07', 0, '楼底下中介有钥匙  '),
(91, 337483, 18, '2020-02-17 07:52:39', 0, '送19平 '),
(92, 337493, 18, '2020-02-17 08:20:50', 0, '顶楼复式，一楼面积131.68，二楼面积120'),
(93, 337512, 18, '2020-02-17 09:10:38', 0, '12平米地下室'),
(94, 337521, 18, '2020-02-18 00:33:03', 0, '下115上60平方'),
(95, 337642, 18, '2020-02-19 07:10:35', 0, '另有阁楼33.几 '),
(96, 337696, 18, '2020-02-20 03:19:59', 0, '工抵房 要求全款'),
(97, 337690, 18, '2020-02-20 03:28:42', 0, '一期'),
(98, 337701, 18, '2020-02-20 03:29:21', 0, '人在苏州，想快点卖掉，价格可谈'),
(99, 337772, 18, '2020-02-21 06:36:23', 0, '大市场那边的链家有钥匙'),
(100, 337805, 18, '2020-02-21 08:05:04', 0, '调高的 2楼大概30平方赠送'),
(101, 337847, 18, '2020-02-22 02:51:41', 0, '2204-2205 '),
(102, 337874, 18, '2020-02-22 07:34:19', 0, '众安绿色港湾达利庄园 '),
(103, 337941, 18, '2020-02-25 01:37:26', 0, '513-514 '),
(104, 337954, 18, '2020-02-25 01:45:35', 0, '1407-1408 '),
(105, 337975, 18, '2020-02-25 04:16:40', 0, '车位6万，钥匙在物业'),
(106, 337978, 18, '2020-02-25 04:20:18', 0, '六楼复式带70平院子现对外出售。房产证面积104.79平方'),
(107, 337981, 18, '2020-02-25 04:22:35', 0, '实际使用面积约135平方。3房2厅2卫1厨，1楼1房2厅1卫1厨，2楼2房1卫'),
(108, 337995, 18, '2020-02-25 06:18:45', 0, '钥匙在1913'),
(109, 338031, 18, '2020-02-25 08:38:49', 0, ' 实际80平 复式 '),
(110, 338122, 18, '2020-02-27 02:15:34', 0, ' 4#102-202号 '),
(111, 338170, 18, '2020-02-27 13:45:21', 0, '8栋3单元501  '),
(112, 338239, 18, '2020-02-29 02:46:53', 0, ' 带车位'),
(113, 338268, 18, '2020-02-29 05:32:03', 0, '楼下101.3平楼上53.4平带露台'),
(114, 338456, 18, '2020-03-04 06:29:23', 0, '顶楼复式的 产证面积126平方'),
(115, 338489, 18, '2020-03-04 08:19:54', 0, '  要全款   '),
(116, 338505, 18, '2020-03-04 08:44:12', 0, '1楼带40平院子自住'),
(117, 338517, 18, '2020-03-05 01:40:43', 0, ' 实际230P包含车位'),
(118, 338574, 18, '2020-03-06 01:48:48', 0, '实际价格142万'),
(119, 338601, 18, '2020-03-06 06:45:13', 0, '送20平方院子'),
(120, 338619, 18, '2020-03-06 08:19:49', 0, '  证上 西园3号路  1#101  '),
(121, 338725, 18, '2020-03-09 02:32:20', 0, '90.JI+70.几平方'),
(122, 338729, 18, '2020-03-09 02:35:04', 0, '价格面议'),
(123, 338849, 18, '2020-03-11 04:56:52', 0, '  2020.6 月份满2 年 '),
(124, 338868, 18, '2020-03-11 07:10:08', 0, '证 A3#503'),
(125, 338881, 18, '2020-03-11 08:07:46', 0, '证上是B1#1609'),
(126, 338910, 18, '2020-03-12 00:37:09', 0, '14#(13A#)'),
(127, 338946, 18, '2020-03-12 07:27:20', 0, ' 复试  楼下 102+20几平方'),
(128, 339021, 18, '2020-03-13 03:33:10', 0, ' 全款95万 精装满五唯一 随时看 '),
(129, 339034, 18, '2020-03-13 04:06:26', 0, ' 价格面议  '),
(130, 339067, 18, '2020-03-14 01:28:36', 0, '去年交房 证不满两年'),
(131, 339129, 18, '2020-03-14 09:44:10', 0, '顶楼三层复式'),
(132, 339237, 18, '2020-03-16 08:49:48', 0, '顶楼复式房产证面积158.47其中楼下119.27楼上39.2'),
(133, 339239, 18, '2020-03-16 08:52:37', 0, '2单元'),
(134, 339406, 18, '2020-03-17 04:53:10', 0, '钥匙在物业'),
(135, 339546, 18, '2020-03-18 06:10:10', 0, '独栋 凤阳东路南侧综合楼2#202  70年产权    1-2 楼'),
(136, 339600, 18, '2020-03-19 00:56:48', 0, '公寓楼# 1203   着急卖  可以直更名'),
(137, 339603, 18, '2020-03-19 01:29:09', 0, '中装  在出租 满五 不唯一 看房提前联系 '),
(138, 298336, 18, '2020-03-19 01:31:30', 0, '低于155 不要打电话'),
(139, 339622, 18, '2020-03-19 04:51:39', 0, ' 16证 钥匙在物业'),
(140, 339640, 18, '2020-03-19 05:55:16', 0, '1701-1702-1703 '),
(141, 339646, 18, '2020-03-19 06:08:02', 0, '1908-1909-1910'),
(142, 339702, 18, '2020-03-20 03:39:08', 0, ' 2#505-606  复试'),
(143, 339735, 18, '2020-03-20 05:45:33', 0, '一楼带20平方左右的院子'),
(144, 339784, 18, '2020-03-20 10:06:28', 0, ' 看房吉大有钥匙 '),
(145, 339786, 18, '2020-03-20 10:09:19', 0, '  钥匙在德祐'),
(146, 339789, 18, '2020-03-20 10:12:44', 0, '  证19年12月'),
(147, 339791, 18, '2020-03-21 00:32:23', 0, ' 钥匙在物业 '),
(148, 339811, 18, '2020-03-21 01:55:45', 0, ' 中午和晚上可以看   '),
(149, 339809, 18, '2020-03-21 02:02:31', 0, '04年证 在空着 复式 二楼大概有50P左右 产证是138.01P'),
(150, 339852, 18, '2020-03-21 06:57:09', 0, '复式  正上 80多'),
(151, 339898, 18, '2020-03-22 12:51:39', 0, ' B#4016-4017 '),
(152, 339947, 18, '2020-03-23 01:55:53', 0, '人在上海看房打妈妈的电话 13856027349  证有税  '),
(153, 340014, 18, '2020-03-23 04:27:46', 0, '六安路78号302 '),
(154, 340031, 18, '2020-03-23 07:05:35', 0, '2105-2107 '),
(155, 340033, 18, '2020-03-23 07:06:28', 0, '315-316号'),
(156, 340042, 18, '2020-03-23 08:48:50', 0, '7#1818-1819'),
(157, 340166, 18, '2020-03-24 08:33:51', 0, '证未下'),
(158, 65793, 18, '2020-03-24 08:56:47', 0, ' 钥匙在六期链家 '),
(159, 340170, 18, '2020-03-24 09:35:27', 0, '有车位 价格另谈'),
(160, 340221, 18, '2020-03-25 01:59:58', 0, '物业头钥匙'),
(161, 340226, 18, '2020-03-25 03:48:03', 0, ' 证有8年了 精装 净得56万'),
(162, 340216, 18, '2020-03-25 03:53:22', 0, '胡岗小区4#01 '),
(163, 322492, 18, '2020-03-25 04:19:04', 0, '170万精装带车位 70年产权  复式  无税  证08年'),
(164, 340252, 18, '2020-03-25 05:10:47', 0, '出租在到月底到期，可以再出租1600/月  也卖'),
(165, 340260, 18, '2020-03-25 06:58:07', 0, '2712-2715'),
(166, 340299, 18, '2020-03-26 00:54:26', 0, 'J#116-117号 '),
(167, 340314, 18, '2020-03-26 02:24:16', 0, 'A#1304-1305'),
(168, 340356, 18, '2020-03-26 06:19:31', 0, ' 人在外地看不了房  下个月回来    证无税  精装'),
(169, 340384, 18, '2020-03-26 07:45:49', 0, '3 满5 精装 自住'),
(170, 340393, 18, '2020-03-26 09:38:17', 0, ' 带一个40平方的独立院子  '),
(171, 340392, 18, '2020-03-26 09:38:38', 0, '无证 去年拿的房子'),
(172, 340423, 18, '2020-03-27 01:05:45', 0, '赠送 80 平 '),
(173, 338619, 18, '2020-03-27 01:32:30', 0, '证 西园路3号1#101'),
(174, 340480, 18, '2020-03-27 05:39:33', 0, '96.6实际面积 75.76产证面积'),
(175, 340516, 18, '2020-03-28 01:22:16', 0, '实际面积90平方'),
(176, 340533, 18, '2020-03-28 01:49:14', 0, '12年证 在出租 人不在合肥'),
(177, 340542, 18, '2020-03-28 02:49:04', 0, '证有四五年了 看房提前预约  精装修'),
(178, 340561, 18, '2020-03-28 06:45:39', 0, '只接受全款160万'),
(179, 340583, 18, '2020-03-28 07:03:53', 0, '也可以租 1800元/月 准备明年6月在卖，但是可以先看看，现在优先出租'),
(180, 340594, 18, '2020-03-28 07:13:52', 0, '精装带地暖  16年 产证'),
(181, 340613, 18, '2020-03-28 08:35:11', 0, 'A#2316 2317 '),
(182, 340614, 18, '2020-03-28 08:36:11', 0, '2301-2328 '),
(183, 340690, 18, '2020-03-30 00:49:47', 0, 'A#2383-2385-2387-2389号 '),
(184, 340767, 18, '2020-03-30 06:52:57', 0, ' 满2 在出租 看房提前联系'),
(185, 340830, 18, '2020-03-31 01:26:41', 0, '盛景融城A区综合楼4-1102/1102上室   269.78'),
(186, 340837, 18, '2020-03-31 01:34:07', 0, '另车位12万 出租在6月到期'),
(187, 340831, 18, '2020-03-31 01:37:25', 0, '包括家电 '),
(188, 279607, 18, '2020-03-31 01:39:17', 0, '  租2500/月'),
(189, 279608, 18, '2020-03-31 01:39:38', 0, '租 2800 /月'),
(190, 340846, 18, '2020-03-31 01:45:23', 0, ' 墙8# 证4#607  '),
(191, 340858, 18, '2020-03-31 02:08:05', 0, '    精装 没怎么住  满五不唯一  钥匙在链家'),
(192, 190496, 18, '2020-03-31 03:04:49', 0, '他说房产证是603，但是实际就是605'),
(193, 340876, 18, '2020-03-31 03:21:19', 0, '316-317 也是他的'),
(194, 340879, 18, '2020-03-31 03:25:45', 0, 'E区10号11号'),
(195, 340884, 18, '2020-03-31 03:31:58', 0, '钥匙在物业  '),
(196, 340889, 18, '2020-03-31 04:00:37', 0, '中装，去年的证'),
(197, 340973, 18, '2020-04-01 05:37:20', 0, '说在考虑中暂时不卖  '),
(198, 340997, 18, '2020-04-01 06:39:17', 0, ' 钥匙在站塘路辉达  '),
(199, 341005, 18, '2020-04-01 07:39:01', 0, '含车位 毛坯 证快满2年 门口德祐有钥匙'),
(200, 338826, 18, '2020-04-01 09:03:35', 0, '  钥匙广利花园门口义和  空着'),
(201, 341056, 18, '2020-04-02 01:30:54', 0, ' 100万看房钥匙在东门新开的房产公司 证满两年'),
(202, 341059, 18, '2020-04-02 01:37:59', 0, '满五唯一 随时看房 精装修'),
(203, 341128, 18, '2020-04-02 07:21:22', 0, '业主只能短信沟通不能打电话）这个业主好像是“残障”人士 不能说话'),
(204, 341134, 18, '2020-04-02 07:42:26', 0, '110-111号'),
(205, 341206, 18, '2020-04-03 06:23:10', 0, '有租客 不方便看房'),
(206, 341215, 18, '2020-04-03 06:42:57', 0, ' 证89.几 赠送十个平方左右 看房提前约 满五唯一'),
(207, 336475, 18, '2020-04-03 07:26:19', 0, '人在外地 物业有钥匙'),
(208, 341241, 18, '2020-04-03 07:41:36', 0, '楼下大概100平  05证'),
(209, 341255, 18, '2020-04-03 08:29:09', 0, '过两天把钥匙放物业 简装 证满两年 '),
(210, 341259, 18, '2020-04-03 08:33:44', 0, ' 280万，包括车位，中装，证12年'),
(211, 341263, 18, '2020-04-03 09:42:32', 0, '钥匙在楼下好莱坞  '),
(212, 341309, 18, '2020-04-04 02:54:20', 0, '159.9+71平方 复试'),
(213, 341312, 18, '2020-04-04 02:56:41', 0, '柏林春天链家有钥匙'),
(214, 267901, 18, '2020-04-04 05:42:53', 0, '钥匙在丰汇房产'),
(215, 341351, 18, '2020-04-04 08:05:17', 0, ' 物业有钥匙 车位算10万，必须绑定车位卖'),
(216, 341363, 18, '2020-04-04 09:03:39', 0, '委托给德祐了'),
(218, 341380, 18, '2020-04-05 02:55:53', 0, 'S6#35-36号'),
(219, 341385, 18, '2020-04-05 03:00:58', 0, 'S12#102-102上 '),
(220, 341386, 18, '2020-04-05 03:01:14', 0, 's12#105-105上'),
(221, 341387, 18, '2020-04-05 03:01:34', 0, ' s10#107-107中-107上'),
(222, 341379, 18, '2020-04-05 03:01:58', 0, '9#202-203 '),
(223, 341471, 18, '2020-04-06 09:40:48', 0, '看房提前约 精装 证7月满两年'),
(224, 341481, 18, '2020-04-06 09:48:10', 0, ' 129平  +储藏室40平'),
(225, 341482, 18, '2020-04-06 09:48:40', 0, ' 面议'),
(226, 341493, 18, '2020-04-06 09:54:42', 0, ' 看房钥匙在物业 '),
(227, 340561, 18, '2020-04-06 09:57:20', 0, '看房物业有钥匙 '),
(228, 341499, 18, '2020-04-06 10:10:54', 0, '339-340号 '),
(229, 341501, 18, '2020-04-06 10:11:54', 0, 'C#1011-1012 '),
(230, 339299, 18, '2020-04-06 10:30:07', 0, '钥匙在中环云公馆的德祐A店'),
(231, 341461, 18, '2020-04-06 10:31:43', 0, ' 看房联系沈先生17755133750'),
(232, 341591, 18, '2020-04-07 05:45:00', 0, '3单元'),
(233, 341602, 18, '2020-04-07 06:14:01', 0, '07年证 330万 自住'),
(234, 341615, 18, '2020-04-07 06:36:04', 0, ' 满5 现在人不在合肥 看房在约'),
(235, 341633, 18, '2020-04-07 07:30:44', 0, '现在出租给别人做仓库'),
(236, 341638, 18, '2020-04-07 07:41:24', 0, '488号3幢（产证2幢）3单元房号：609'),
(237, 341645, 18, '2020-04-07 07:49:26', 0, '自住 精装  看房提前半小时联系'),
(238, 341652, 18, '2020-04-07 08:10:51', 0, ' 钥匙在万科那边的租售中心'),
(239, 341673, 18, '2020-04-07 08:37:08', 0, '洋房  钥匙在物业  人在外地 '),
(240, 341635, 18, '2020-04-07 08:54:22', 0, '证 合瓦路170号 产证4#403 墙上1#403'),
(241, 341678, 18, '2020-04-07 09:49:59', 0, '6楼88平方 阁楼33平方'),
(242, 341691, 18, '2020-04-08 01:31:01', 0, '综合楼210 '),
(243, 341708, 18, '2020-04-08 01:46:51', 0, '看房出租 白天一般都有人 证两年'),
(244, 341709, 18, '2020-04-08 01:47:58', 0, '证14年的，豪装，包括车位'),
(245, 341715, 18, '2020-04-08 01:51:10', 0, ' 看房提前两个小时约 满2年 '),
(246, 341725, 18, '2020-04-08 02:49:34', 0, '匙在门口21世纪 '),
(247, 311563, 18, '2020-04-08 02:52:01', 0, ' 包车位  09证'),
(248, 341733, 18, '2020-04-08 03:11:24', 0, '看房提前约 满五唯一'),
(249, 341748, 18, '2020-04-08 04:53:00', 0, '送设备平台，实际有180平，证还下 毛坯有税，带北院子'),
(250, 341810, 18, '2020-04-09 02:06:38', 0, '302-303号 价格面议'),
(251, 253291, 18, '2020-04-09 06:43:26', 0, ' 物业张姐有钥匙  '),
(252, 341873, 18, '2020-04-09 07:27:55', 0, ' 制伞厂路  107号1#304'),
(253, 341909, 18, '2020-04-09 10:14:17', 0, '是商住楼413'),
(254, 341935, 18, '2020-04-10 01:42:52', 0, '暂时不卖了，看房不方便 '),
(255, 310128, 18, '2020-04-10 01:43:53', 0, '看房提前约  看好再谈 '),
(256, 341947, 18, '2020-04-10 02:15:17', 0, '南门链家也有钥匙'),
(257, 341963, 18, '2020-04-10 02:57:21', 0, '贷款149 全款145 看房楼下21世纪房产有钥匙'),
(258, 341965, 18, '2020-04-10 02:58:55', 0, '看房21楼那边链家有钥匙'),
(259, 341969, 18, '2020-04-10 03:03:51', 0, '07年买的房   带一个产权车位   '),
(260, 341976, 18, '2020-04-10 03:23:56', 0, '空着 润地星城德祐有钥匙'),
(261, 341989, 18, '2020-04-10 04:41:09', 0, '2020.8月份满2 年 钥匙 在物业'),
(262, 341990, 18, '2020-04-10 04:41:32', 0, '  跟链家签过独家了'),
(263, 342054, 18, '2020-04-11 01:28:48', 0, '复式 毛坯 出租了'),
(264, 342057, 18, '2020-04-11 02:45:06', 0, ' 徽州大道省建材宿舍 '),
(265, 342075, 18, '2020-04-11 02:55:39', 0, '可出租 3500元/月 产证满2年 '),
(266, 342077, 18, '2020-04-11 02:56:56', 0, '满五唯一，有约40万贷款'),
(267, 342078, 18, '2020-04-11 02:57:08', 0, ' 看房提前约 证满2年 没有贷款了'),
(268, 342006, 18, '2020-04-11 04:06:16', 0, '房产证地址：宿州路249号1幢505'),
(269, 342110, 18, '2020-04-11 06:31:25', 0, '大概12年证  也出租 1850/月 家电齐全'),
(270, 342135, 18, '2020-04-11 07:32:08', 0, '商住楼也叫A#，产证上写的是1#，刚拿的产证'),
(271, 342169, 18, '2020-04-12 01:24:21', 0, 'B#604-605 '),
(272, 342176, 18, '2020-04-12 01:41:48', 0, '01-02号'),
(273, 342257, 18, '2020-04-13 01:56:50', 0, '1301-1304'),
(274, 342306, 18, '2020-04-13 05:22:00', 0, '带车位，车位号A468，'),
(275, 342333, 18, '2020-04-13 13:21:08', 0, '毛坯  复式洋房  实际有242平 '),
(276, 76976, 18, '2020-04-13 13:23:02', 0, '证是 双岗街74号  以前是中行宿舍'),
(277, 339692, 18, '2020-04-13 13:27:24', 0, ' 钥匙在德祐  '),
(278, 342344, 18, '2020-04-13 13:52:09', 0, '5月30日满2  毛坯 现在出租给二房东了'),
(279, 342359, 18, '2020-04-13 14:07:16', 0, '物业有钥匙 '),
(280, 342363, 18, '2020-04-13 14:10:39', 0, '钥匙在楼下的链家'),
(281, 342367, 18, '2020-04-13 14:12:44', 0, '  470包括两个车位价 精装'),
(282, 342395, 18, '2020-04-14 01:47:00', 0, '实际有210平左右  精装'),
(283, 342401, 18, '2020-04-14 02:08:36', 0, '去年的证，复式，下80多，上30多，毛坯'),
(284, 342421, 18, '2020-04-14 02:41:32', 0, '150几  刚开始办房产证'),
(285, 342468, 18, '2020-04-14 07:49:35', 0, 'S1#301-302号 '),
(286, 342471, 18, '2020-04-14 07:52:32', 0, '301-303 '),
(287, 342585, 18, '2020-04-15 06:41:51', 0, '顶楼复式 证15年的 有按揭贷款'),
(288, 342597, 18, '2020-04-15 07:04:26', 0, ' 6、7月下证    毛坯'),
(289, 342600, 18, '2020-04-15 07:06:28', 0, '德祐 房友有钥匙'),
(290, 342353, 18, '2020-04-15 07:25:55', 0, ' 1# 2单元 1312'),
(291, 342613, 18, '2020-04-15 07:47:49', 0, '空着  钥匙在链家'),
(292, 342633, 18, '2020-04-15 09:51:27', 0, '市政小区    乐水4号 1#502  '),
(293, 342636, 18, '2020-04-15 10:13:06', 0, ' 物业有钥匙  证7月2号满2年'),
(294, 342669, 18, '2020-04-16 01:25:47', 0, '出租了   看房看505的  房东就住在505'),
(295, 342686, 18, '2020-04-16 02:44:48', 0, '房号怎么问都不说   说家里孩子在准备中考  '),
(296, 342700, 18, '2020-04-16 03:01:28', 0, '3万一平 看房有租客  证满2年 还有贷款未还清'),
(297, 342698, 18, '2020-04-16 03:44:56', 0, '价格面议 '),
(298, 342691, 18, '2020-04-16 03:45:12', 0, '一期洋房'),
(299, 342704, 18, '2020-04-16 03:47:19', 0, '看房物业有钥匙 简装 证满两年  135万带一个产权车位'),
(300, 342712, 18, '2020-04-16 03:49:41', 0, ' 全款的话120万  贷款价格面议'),
(301, 342719, 18, '2020-04-16 06:22:57', 0, ' 钥匙在岳西路中科  委托给他们了'),
(302, 342723, 18, '2020-04-16 06:26:35', 0, '17F-23F'),
(303, 342724, 18, '2020-04-16 06:27:33', 0, 'B#1914-1915-1916 '),
(304, 342743, 18, '2020-04-16 08:49:17', 0, 'F#2610-2611 '),
(305, 342821, 18, '2020-04-17 05:45:59', 0, ' 15年证 空着 141P 二期'),
(306, 342822, 18, '2020-04-17 05:46:57', 0, '  带院子 '),
(307, 342822, 18, '2020-04-17 05:48:05', 0, ' 带院子  有个地下室二十平'),
(308, 337041, 18, '2020-04-17 08:01:15', 0, ' 快5年   81+90  复式'),
(309, 342860, 18, '2020-04-17 09:55:20', 0, ' 8月底满2年'),
(310, 342862, 18, '2020-04-17 09:59:10', 0, ' 说自己是中介 房子是自己的  有税'),
(311, 342885, 18, '2020-04-18 01:18:26', 0, ' 钥匙在辉达'),
(312, 342902, 18, '2020-04-18 03:05:42', 0, '13年证    最低110万      家电家具全丢'),
(313, 342930, 18, '2020-04-18 07:27:08', 0, '戴安桥巷1号'),
(314, 342935, 18, '2020-04-18 07:29:41', 0, ' 看房钥匙在易居房友 还没满两年'),
(315, 342934, 18, '2020-04-18 07:29:51', 0, '钥匙在锦园春天北门云涛房产'),
(316, 342939, 18, '2020-04-18 07:33:32', 0, ' 自住 看房最好约在周末'),
(317, 342949, 18, '2020-04-18 08:08:44', 0, '豪装 无税 贷款10几万 看房提前联系'),
(318, 342954, 18, '2020-04-18 08:32:21', 0, '303-306'),
(319, 299799, 18, '2020-04-18 09:29:14', 0, '带车位无税，有抵押，暂时只接受全'),
(320, 299799, 18, '2020-04-18 09:29:32', 0, '暂时只接受全款'),
(321, 342962, 18, '2020-04-18 09:30:02', 0, '无税无贷款，老式装修 看房提前预约   '),
(322, 300359, 18, '2020-04-19 08:30:12', 0, ' B#1301-1302 '),
(323, 342979, 18, '2020-04-19 08:34:39', 0, 'A#1113-1114 '),
(324, 342980, 18, '2020-04-19 08:35:13', 0, 'A#1110-1111-1112'),
(325, 342989, 18, '2020-04-19 08:40:14', 0, '1#401-402'),
(326, 343049, 18, '2020-04-20 01:38:06', 0, '有税'),
(327, 343052, 18, '2020-04-20 01:39:57', 0, '钥匙在房管家 '),
(328, 343054, 18, '2020-04-20 01:41:02', 0, '钥匙在恒大帝景链家 '),
(329, 343095, 18, '2020-04-20 03:14:25', 0, '说137是他妹妹，房子是妹妹的，不要打137，直接打199就行'),
(330, 343045, 18, '2020-04-20 03:16:18', 0, '共两层，175万，'),
(331, 343104, 18, '2020-04-20 03:57:14', 0, ' 复式的 85+25 平方'),
(332, 343106, 18, '2020-04-20 04:01:09', 0, '带车位 '),
(333, 343123, 18, '2020-04-20 06:52:49', 0, '8楼一层'),
(334, 100, 13, '2020-05-23 08:37:04', 0, 'wefef'),
(335, 100, 13, '2020-05-23 08:37:38', 0, 'wefef'),
(336, 100, 13, '2020-05-23 08:44:14', 0, 'wefefef'),
(337, 100, 13, '2020-05-24 07:21:28', 0, 'wfefe'),
(338, 100, 13, '2020-05-24 07:25:51', 0, 'wefefe'),
(339, 100, 13, '2020-05-24 07:26:34', 0, 'wefef'),
(340, 100, 13, '2020-05-24 07:27:41', 0, 'efwef'),
(341, 100, 13, '2020-05-24 07:29:51', 0, 'fefef'),
(342, 100, 13, '2020-05-24 07:30:36', 0, 'wefefef'),
(343, 100, 13, '2020-05-24 07:31:50', 0, 'efewfe'),
(344, 100, 13, '2020-05-24 07:32:23', 0, 'wefef'),
(345, 100, 13, '2020-05-24 07:32:41', 0, 'wef'),
(346, 100, 13, '2020-05-24 07:33:01', 0, 'wefef'),
(347, 100, 13, '2020-05-24 07:33:59', 0, 'weef'),
(348, 100, 13, '2020-05-24 07:34:45', 0, 'wefef'),
(349, 100, 13, '2020-05-24 07:36:17', 0, 'wef'),
(350, 100, 13, '2020-05-24 07:36:45', 0, 'wefef'),
(351, 100, 13, '2020-05-24 08:27:04', 0, 'wefe'),
(352, 100, 13, '2020-05-24 08:30:23', 0, 'weffee'),
(353, 100, 13, '2020-05-24 09:11:08', 0, 'efefef'),
(354, 100, 13, '2020-05-24 09:11:08', 0, 'efefef'),
(355, 100, 13, '2020-05-24 09:12:06', 0, 'efefef'),
(356, 100, 13, '2020-05-24 09:12:06', 0, 'efefef'),
(357, 100, 13, '2020-05-24 09:13:51', 0, 'wefef'),
(358, 100, 13, '2020-05-24 09:14:06', 0, 'aaaaa'),
(359, 100, 13, '2020-05-24 09:16:53', 0, 'wefef'),
(360, 100, 13, '2020-05-24 09:23:59', 0, 'wefef'),
(361, 100, 13, '2020-05-24 09:25:27', 0, 'wf'),
(362, 100, 13, '2020-05-24 09:25:34', 0, 'aaa'),
(363, 82, 1, '2020-08-02 06:15:53', 1, 'aaa'),
(364, 56, 13, '2020-11-26 00:45:50', 0, '在售'),
(365, 56, 1, '2020-12-03 06:06:09', 0, '44554545'),
(366, 56, 1, '2020-12-03 06:06:18', 3, '4544545gggg'),
(367, 56, 1, '2020-12-03 06:06:27', 0, 'erttttttt'),
(368, 56, 1, '2020-12-04 06:25:45', 2, 'eee'),
(369, 100, 1, '2020-12-15 10:06:05', 0, '111'),
(370, 100, 1, '2020-12-15 10:06:17', 0, '1121231'),
(371, 343179, 1, '2020-12-16 05:05:13', 0, '反反复复');

-- --------------------------------------------------------

--
-- 表的结构 `mf_second_items`
--

CREATE TABLE `mf_second_items` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL COMMENT '小区',
  `areaCode` int(11) NOT NULL COMMENT '地区码',
  `customer_name` varchar(32) NOT NULL COMMENT '房主姓名',
  `shi_count` int(11) NOT NULL COMMENT '几室',
  `ting_count` int(11) NOT NULL COMMENT '几厅',
  `floor` int(11) NOT NULL COMMENT '所在层',
  `floor_total` int(11) NOT NULL COMMENT '总层',
  `add_userid` int(11) NOT NULL,
  `add_cop_id` int(11) NOT NULL,
  `add_time` int(11) NOT NULL,
  `update_userid` int(11) NOT NULL COMMENT '最后更新',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `update_ip` varchar(64) NOT NULL COMMENT '更新IP',
  `update_cop_id` int(11) NOT NULL COMMENT '更新组织id',
  `address` varchar(96) NOT NULL COMMENT '地址',
  `dong` varchar(16) NOT NULL COMMENT '栋',
  `decoration_id` int(11) NOT NULL COMMENT '装修',
  `type_id` int(11) NOT NULL COMMENT '房屋类型',
  `shi` varchar(16) NOT NULL COMMENT '室',
  `view_level` int(11) NOT NULL COMMENT '0不限 1本公司 2本门店 3个人',
  `build_year` int(11) DEFAULT NULL COMMENT '建成年代',
  `add_ip` varchar(64) NOT NULL,
  `area` float NOT NULL COMMENT '面积',
  `mobile` varchar(1024) NOT NULL COMMENT '电话',
  `unit_price` float NOT NULL COMMENT '单价',
  `total_price` float NOT NULL COMMENT '总价',
  `state` int(11) NOT NULL COMMENT '0在售 1停售 2已售 3无效',
  `is_auction` tinyint(1) NOT NULL COMMENT '是否是法拍房',
  `auction_price` float NOT NULL COMMENT '标的金额',
  `auction_time_start` datetime NOT NULL COMMENT '开拍时间开始',
  `auction_time_end` datetime NOT NULL COMMENT '开拍时间截止',
  `auction_url` varchar(2000) NOT NULL COMMENT '法拍房二维码',
  `mem` varchar(1024) NOT NULL COMMENT '备注'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `mf_second_items`
--

INSERT INTO `mf_second_items` (`id`, `building_id`, `areaCode`, `customer_name`, `shi_count`, `ting_count`, `floor`, `floor_total`, `add_userid`, `add_cop_id`, `add_time`, `update_userid`, `update_time`, `update_ip`, `update_cop_id`, `address`, `dong`, `decoration_id`, `type_id`, `shi`, `view_level`, `build_year`, `add_ip`, `area`, `mobile`, `unit_price`, `total_price`, `state`, `is_auction`, `auction_price`, `auction_time_start`, `auction_time_end`, `auction_url`, `mem`) VALUES
(56, 37266, 340121, '王女士', 1, 0, 1, 2, 1, 1, 1451577600, 1, 1606975587, '58.59.222.66', 1, '安徽省合肥市长丰县清颖路', '29', 0, 1, '4', 0, 2000, '127.0.0.1', 72, '18888888888', 5556, 40, 2, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(100, 37274, 340111, '先生', 3, 2, 1, 6, 1, 1, 1451577600, 1, 1608026777, '119.123.68.233', 1, '安徽省合肥市包河区马鞍山路辅路', '2', 0, 1, '107', 0, 2003, '127.0.0.1', 167, '18888888888', 5988, 100, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(343178, 37268, 340102, '先生', 1, 1, 1, 1, 2, 1, 1606652341, 1, 1606652341, '112.32.90.99', 1, '天山路与紫云路交叉口', '1', 2, 1, '123', 3, 1, '112.32.90.99', 1, '14343434343', 10000, 1, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(343179, 37268, 340102, '先生', 11, 1, 1, 2, 1, 1, 1606652387, 1, 1608095113, '58.59.17.174', 1, '天山路与紫云路交叉口', '2', 2, 1, '122', 3, 1111, '112.32.90.99', 222, '13911111111', 110.87, 2.46142, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(343180, 37268, 340102, '先生', 111, 1, 1, 1, 1, 1, 1608126332, 1, 1608126351, '39.162.143.110', 1, '安徽省合肥市瑶海区明皇路', '1', 1, 1, '112', 0, 1111, '39.162.143.110', 111, '111123232432432', 10000, 111, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_sms`
--

CREATE TABLE `mf_sms` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `event` varchar(30) NOT NULL DEFAULT '' COMMENT '事件',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `code` varchar(10) NOT NULL DEFAULT '' COMMENT '验证码',
  `times` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '验证次数',
  `ip` varchar(30) NOT NULL DEFAULT '' COMMENT 'IP',
  `createtime` int(10) UNSIGNED DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='短信验证码表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_sms`
--

INSERT INTO `mf_sms` (`id`, `event`, `mobile`, `code`, `times`, `ip`, `createtime`) VALUES
(17, 'find', '13645604580', '4319', 0, '36.7.110.63', 1587709723),
(22, 'register', '18305515155', '8914', 0, '112.32.90.166', 1587778105),
(26, 'register', '18305515155', '3226', 0, '112.32.90.166', 1587779037),
(27, 'register', '18305515155', '5166', 0, '112.32.90.166', 1587779102),
(28, 'register', '18305515155', '5400', 0, '112.32.90.166', 1587779135),
(29, 'register', '18305515155', '1032', 0, '112.32.90.166', 1587783810),
(30, 'register', '18305515155', '4968', 0, '112.32.90.166', 1587864844),
(31, 'register', '18305515155', '9238', 0, '112.32.90.166', 1587886970),
(32, 'register', '18305515155', '8771', 0, '112.32.90.166', 1587887382),
(34, 'register', '13645604580', '4447', 0, '36.7.110.63', 1588122354),
(35, 'register', '13645604580', '7546', 0, '36.7.110.63', 1588811726),
(36, 'register', '13645604580', '9215', 0, '36.7.110.63', 1588813330),
(37, 'register', '13645604580', '8803', 0, '36.7.110.63', 1589079130),
(38, 'register', '13645604580', '5299', 0, '36.7.110.63', 1589079412),
(39, 'register', '13645604580', '4211', 0, '36.7.110.63', 1589079919),
(40, 'register', '13645604580', '5769', 0, '36.7.110.63', 1589079990),
(41, 'register', '13645604580', '9307', 0, '36.7.110.63', 1589080769),
(44, 'register', '18305515155', '9389', 0, '220.178.61.203', 1589787904),
(51, 'resetpwd', '18305515155', '1539', 0, '112.32.92.82', 1590308371),
(55, 'changemobile', '13645604581', '4549', 2, '36.34.21.22', 1590320697),
(56, 'resetmobile', '18305515155', '5022', 0, '112.32.92.82', 1590320939),
(57, 'changemobile', '13645604581', '2189', 0, '36.34.21.22', 1590322222),
(58, 'changemobile', '13645604581', '4131', 3, '36.34.21.22', 1590322512),
(59, 'changemobile', '13645604584', '1098', 0, '36.34.21.22', 1590589380),
(60, 'changemobile', '13645604581', '7809', 0, '36.34.21.22', 1590590898);

-- --------------------------------------------------------

--
-- 表的结构 `mf_test`
--

CREATE TABLE `mf_test` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `admin_id` int(10) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '分类ID(单选)',
  `category_ids` varchar(100) NOT NULL COMMENT '分类ID(多选)',
  `week` enum('monday','tuesday','wednesday') NOT NULL COMMENT '星期(单选):monday=星期一,tuesday=星期二,wednesday=星期三',
  `flag` set('hot','index','recommend') NOT NULL DEFAULT '' COMMENT '标志(多选):hot=热门,index=首页,recommend=推荐',
  `genderdata` enum('male','female') NOT NULL DEFAULT 'male' COMMENT '性别(单选):male=男,female=女',
  `hobbydata` set('music','reading','swimming') NOT NULL COMMENT '爱好(多选):music=音乐,reading=读书,swimming=游泳',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '图片',
  `images` varchar(1500) NOT NULL DEFAULT '' COMMENT '图片组',
  `attachfile` varchar(100) NOT NULL DEFAULT '' COMMENT '附件',
  `keywords` varchar(100) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `city` varchar(100) NOT NULL DEFAULT '' COMMENT '省市',
  `json` varchar(255) DEFAULT NULL COMMENT '配置:key=名称,value=值',
  `price` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '价格',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '点击',
  `startdate` date DEFAULT NULL COMMENT '开始日期',
  `activitytime` datetime DEFAULT NULL COMMENT '活动时间(datetime)',
  `year` year(4) DEFAULT NULL COMMENT '年',
  `times` time DEFAULT NULL COMMENT '时间',
  `refreshtime` int(10) DEFAULT NULL COMMENT '刷新时间(int)',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `deletetime` int(10) DEFAULT NULL COMMENT '删除时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `switch` tinyint(1) NOT NULL DEFAULT '0' COMMENT '开关',
  `status` enum('normal','hidden') NOT NULL DEFAULT 'normal' COMMENT '状态',
  `state` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '状态值:0=禁用,1=正常,2=推荐'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_test`
--

INSERT INTO `mf_test` (`id`, `admin_id`, `category_id`, `category_ids`, `week`, `flag`, `genderdata`, `hobbydata`, `title`, `content`, `image`, `images`, `attachfile`, `keywords`, `description`, `city`, `json`, `price`, `views`, `startdate`, `activitytime`, `year`, `times`, `refreshtime`, `createtime`, `updatetime`, `deletetime`, `weigh`, `switch`, `status`, `state`) VALUES
(1, 0, 12, '12,13', 'monday', 'hot,index', 'male', 'music,reading', '我是一篇测试文章', '<p>我是测试内容</p>', '/assets/img/avatar.png', '/assets/img/avatar.png,/assets/img/qrcode.png', '/assets/img/avatar.png', '关键字', '描述', '广西壮族自治区/百色市/平果县', '{\"a\":\"1\",\"b\":\"2\"}', 0.00, 0, '2017-07-10', '2017-07-10 18:24:45', 2017, '18:24:45', 1499682285, 1499682526, 1499682526, NULL, 0, 1, 'normal', '1');

-- --------------------------------------------------------

--
-- 表的结构 `mf_user`
--

CREATE TABLE `mf_user` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'ID',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '组别ID',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `email` varchar(100) DEFAULT '' COMMENT '电子邮箱',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `level` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '等级',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `bio` varchar(100) NOT NULL DEFAULT '' COMMENT '格言',
  `money` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '积分',
  `successions` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT '连续登录天数',
  `maxsuccessions` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT '最大连续登录天数',
  `prevtime` int(10) DEFAULT NULL COMMENT '上次登录时间',
  `logintime` int(10) DEFAULT NULL COMMENT '登录时间',
  `loginip` varchar(50) NOT NULL DEFAULT '' COMMENT '登录IP',
  `loginfailure` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '失败次数',
  `joinip` varchar(50) NOT NULL DEFAULT '' COMMENT '加入IP',
  `jointime` int(10) DEFAULT NULL COMMENT '加入时间',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(50) NOT NULL DEFAULT '' COMMENT 'Token',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  `verification` varchar(255) NOT NULL DEFAULT '' COMMENT '验证',
  `applicationdescription` varchar(1024) NOT NULL COMMENT '申请说明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_user`
--

INSERT INTO `mf_user` (`id`, `group_id`, `username`, `nickname`, `password`, `salt`, `email`, `mobile`, `avatar`, `level`, `gender`, `birthday`, `bio`, `money`, `score`, `successions`, `maxsuccessions`, `prevtime`, `logintime`, `loginip`, `loginfailure`, `joinip`, `jointime`, `createtime`, `updatetime`, `token`, `status`, `verification`, `applicationdescription`) VALUES
(1, 2, 'admin', 'admin', '50c420bb263b721b922ce673b056a6c5', 'AD0Ih5', 'admin@163.com', '13888888888', '', 0, 0, '2017-04-15', '', '0.00', 0, 1, 2, 1597025851, 1597319371, '112.32.88.160', 0, '127.0.0.1', 1491461418, 0, 1597319371, '', 'normal', '', ''),
(5, 2, 'rby1006', 'rby1006', 'aa6aac03d1005e713f83f1b275d2ffcf', 'UwpIyt', '280915761@qq.com', '13956906494', '', 1, 0, '0000-00-00', '', '0.00', 0, 1, 1, 1583653782, 1583653782, '117.136.103.140', 0, '117.136.103.140', 1583653782, 1583653782, 1589936220, '', 'normal', '', ''),
(8, 2, '1admin', '1admin', '8343b6db1e8aa7da8c9e659d9309e0b2', 'zRTAKs', '432@fw.occ', '18305515156', '', 1, 0, '0000-00-00', '', '0.00', 0, 1, 1, 1587892817, 1587892817, '112.32.90.166', 0, '112.32.90.166', 1587892817, 1587892817, 1589936212, '', 'normal', '', ''),
(9, 2, '1admin1', '1admin1', 'e128610777b16faf1af3e4fc8d647111', 'K0NfGF', '432@fw.occ1', '18305515157', '', 1, 0, '0000-00-00', '', '0.00', 0, 1, 1, 1587892968, 1587892968, '112.32.90.166', 0, '112.32.90.166', 1587892968, 1587892968, 1589936216, '', 'normal', '', ''),
(10, 2, '1admin12', '1admin12', '3dde06af8822b05bec080ba6281912e3', '475Acl', '4132@fw.occ1', '18305515158', '', 1, 0, '0000-00-00', '', '0.00', 0, 1, 1, 1587892995, 1587892995, '112.32.90.166', 0, '112.32.90.166', 1587892995, 1587892995, 1587909138, '', 'hidden', '', 'ewf'),
(12, 2, '45604580', '45604580', 'c6a5b2884b3324ebe7b719e704e5642a', 'qs1Jih', '', '13645604580', '', 1, 0, '0000-00-00', '', '0.00', 0, 1, 3, 1597510208, 1597726733, '36.34.4.237', 0, '36.7.110.63', 1588813351, 1588813351, 1597726733, '', 'normal', '', ''),
(13, 1, 'admin1', 'admin1', '0eddf68d0352a0c2e79182833574819a', 'XLzxDC', 'xundh2@qq.com', '18305515155', '', 1, 0, '0000-00-00', '', '0.00', 0, 2, 10, 1609807344, 1609807469, '39.162.148.234', 0, '220.178.61.203', 1589787964, 1589787964, 1609807469, '', 'normal', '', ''),
(14, 2, 'admin2', 'admin2', '94d1a57f0a1c0feaec2ebe82cfd20985', '7Ap52g', 'xundh@qq.com', '18305515152', '', 1, 0, NULL, '', '0.00', 0, 1, 1, 1590488490, 1590488490, '220.178.61.203', 0, '220.178.61.203', 1590488490, 1590488490, 1590488490, '', 'hidden', '', ''),
(15, 1, 'aaa', 'aaa', 'ac329aa0645a307cb9bad3a3e4a6866b', 'L6T4yX', '215856637@qq.com', '13854548548', '', 1, 0, NULL, '', '0.00', 0, 1, 1, 1596162469, 1596162469, '220.178.61.203', 0, '220.178.61.203', 1596162469, 1596162469, 1596162469, '', 'hidden', '', '1111'),
(16, 2, 'admin4', 'admin4', 'c871c3c4e5e7bdba681d2ad746c06a11', '67YQU3', '1367432711@qq.com', '15153933164', '', 1, 0, NULL, '', '0.00', 0, 1, 1, 1607587342, 1607587342, '112.228.58.243', 0, '112.228.58.243', 1607587342, 1607587342, 1607587342, '', 'hidden', '', ''),
(17, 2, 'vivi', 'vivi', 'd5f266eb548cd152ed6165a98b4f4b2c', 'z34wGx', 'vivi@163.com', '13135632689', '', 1, 0, '0000-00-00', '', '0.00', 0, 1, 1, 1609758872, 1609758872, '39.162.129.217', 0, '39.162.129.217', 1609758872, 1609758872, 1609807124, '', 'normal', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `mf_user_favorate`
--

CREATE TABLE `mf_user_favorate` (
  `fav_id` varchar(64) NOT NULL COMMENT '主键',
  `cop_id` int(11) NOT NULL COMMENT '所属公司',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `create_time` datetime NOT NULL COMMENT '收藏时间',
  `create_ip` varchar(64) NOT NULL COMMENT '创建IP',
  `target` enum('二手房出售房源','二手房出租房源','新房出售房源','新房出租房源','新房楼盘') NOT NULL,
  `target_id` varchar(64) NOT NULL COMMENT '主键',
  `comment` varchar(1024) NOT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收藏表';

--
-- 转存表中的数据 `mf_user_favorate`
--

INSERT INTO `mf_user_favorate` (`fav_id`, `cop_id`, `user_id`, `create_time`, `create_ip`, `target`, `target_id`, `comment`) VALUES
('08d22e167c1644f290b0eb623c23f1ba', 0, 12, '2020-05-28 00:00:00', '', '二手房出售房源', '94', '南湖春城 面积:125.44'),
('0d22f1e26c2f446b9d31e0bda0fe6143', 0, 12, '2020-05-28 00:00:00', '', '二手房出售房源', '85', '庆丰小区 面积:125'),
('1c3b68ecb9d94bc3beb07d1e669a3c13', 0, 12, '2020-05-28 00:00:00', '', '二手房出售房源', '92', '来安新村 面积:80'),
('2151ea3447fe422696ad1811fa037cc4', 0, 13, '2020-05-31 00:00:00', '', '二手房出售房源', '98', '板桥商住楼 面积:385'),
('2300c02a11244fb2ab794e09dc03fcd9', 0, 12, '2020-05-28 00:00:00', '', '二手房出售房源', '59', '元一柏庄 面积:128'),
('2d3cf8b58a3e4e5e991a5b2a21b7c2d2', 0, 12, '2020-05-24 00:00:00', '', '二手房出售房源', '77', '胜利新村 面积:30'),
('40d63addbbfc4a3e84a551670fa3f855', 0, 12, '2020-07-31 00:00:00', '', '二手房出售房源', '79', '安粮双景佳苑 面积:124'),
('4cbad6276040459b9cfdb7f2d014a720', 0, 12, '2020-08-16 00:00:00', '', '新房楼盘', '57', '金科集美天辰'),
('4ea50768914640d79af95398dea97f6a', 0, 12, '2020-05-24 00:00:00', '', '二手房出售房源', '97', '墅时代花园 面积:146'),
('593ef42040814cec8bd90500d5e539b5', 0, 13, '2020-05-31 00:00:00', '', '二手房出售房源', '77', '胜利新村 面积:30'),
('7963fd014dd24fbb9f4e32d9fd99e860', 0, 1, '2020-08-13 00:00:00', '', '新房楼盘', '1', '丰辉国际广场'),
('79a6b0dd5e8f4f34a29e35852eb2c9a7', 0, 12, '2020-05-28 00:00:00', '', '二手房出售房源', '89', '浅水湾 面积:135'),
('7c021dc65a414353b71f7e63d61d4b3d', 0, 12, '2020-05-24 00:00:00', '', '二手房出售房源', '87', '华润澜溪镇 面积:206'),
('9e4db109fc914579a935c2d74502c6bb', 0, 12, '2020-05-24 00:00:00', '', '二手房出售房源', '78', '斯瑞新景苑 面积:112'),
('a82e815b8d0d4144a565239625a27384', 0, 12, '2020-05-24 00:00:00', '', '二手房出售房源', '98', '板桥商住楼 面积:385'),
('ae223008b8804c8e96a1cb43c8f6f64c', 0, 13, '2020-05-31 00:00:00', '', '二手房出售房源', '94', '南湖春城 面积:125.44'),
('b48a8254ad0c4123abf594463817b46b', 0, 12, '2020-05-28 00:00:00', '', '二手房出售房源', '95', '顶尖花园 面积:173.3'),
('c0792d3cdbb240de9eeec9c0262c2a2f', 0, 13, '2020-05-20 00:00:00', '', '二手房出售房源', '58', '金水黄金广场 面积:126'),
('c3cee116ed4e4872a3fcbbf3f3b79d1d', 0, 12, '2020-05-24 00:00:00', '', '二手房出售房源', '96', '蜀新苑 面积:45'),
('de43979fed994d60848eedb46b5c8f39', 0, 13, '2020-05-31 00:00:00', '', '二手房出售房源', '57', '中奥花园 面积:73');

-- --------------------------------------------------------

--
-- 表的结构 `mf_user_group`
--

CREATE TABLE `mf_user_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) DEFAULT '' COMMENT '组名',
  `rules` text COMMENT '权限节点',
  `createtime` int(10) DEFAULT NULL COMMENT '添加时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `status` enum('normal','hidden') DEFAULT NULL COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员组表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_user_group`
--

INSERT INTO `mf_user_group` (`id`, `name`, `rules`, `createtime`, `updatetime`, `status`) VALUES
(1, '普通用户', '1,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,34,4,2', 1515386468, 1608080572, 'normal'),
(2, '中介公司经纪人', '33,32,30,29,27,26,25,24,34,20,19,18,22,17,16,15,14,35,11,10,9,12,7,6,5,8,28,23,21,13,4,2,3,1', 1563606225, 1608080614, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `mf_user_money_log`
--

CREATE TABLE `mf_user_money_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更余额',
  `before` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更前余额',
  `after` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '变更后余额',
  `memo` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员余额变动表' ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- 表的结构 `mf_user_rule`
--

CREATE TABLE `mf_user_rule` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(10) DEFAULT NULL COMMENT '父ID',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `title` varchar(50) DEFAULT '' COMMENT '标题',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `ismenu` tinyint(1) DEFAULT NULL COMMENT '是否菜单',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(10) DEFAULT NULL COMMENT '更新时间',
  `weigh` int(10) DEFAULT '0' COMMENT '权重',
  `status` enum('normal','hidden') DEFAULT NULL COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员规则表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_user_rule`
--

INSERT INTO `mf_user_rule` (`id`, `pid`, `name`, `title`, `remark`, `ismenu`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, 0, 'index', '前台', '', 1, 1516168079, 1516168079, 1, 'normal'),
(2, 0, 'api', 'API接口', '', 1, 1516168062, 1516168062, 2, 'normal'),
(3, 1, 'user', '会员模块', '', 1, 1515386221, 1516168103, 12, 'normal'),
(4, 2, 'user', '会员模块', '', 1, 1515386221, 1516168092, 11, 'normal'),
(5, 3, 'index/user/login', '登录', '', 0, 1515386247, 1515386247, 5, 'normal'),
(6, 3, 'index/user/register', '注册', '', 0, 1515386262, 1516015236, 7, 'normal'),
(7, 3, 'index/user/index', '会员中心', '', 0, 1516015012, 1516015012, 9, 'normal'),
(8, 3, 'index/user/profile', '个人资料', '', 0, 1516015012, 1516015012, 4, 'normal'),
(9, 4, 'api/user/login', '登录', '', 0, 1515386247, 1515386247, 6, 'normal'),
(10, 4, 'api/user/register', '注册', '', 0, 1515386262, 1516015236, 8, 'normal'),
(11, 4, 'api/user/index', '会员中心', '', 0, 1516015012, 1516015012, 10, 'normal'),
(12, 4, 'api/user/profile', '个人资料', '', 0, 1516015012, 1516015012, 3, 'normal'),
(13, 0, 'api/seconds', '二手房', '', 1, 1587910448, 1587910448, 13, 'normal'),
(14, 13, 'api/second/items/list', '二手房列表', '', 1, 1587910467, 1590218791, 14, 'normal'),
(15, 13, 'api/second/items/housetypes', '房屋类型，二手房新房通用', '', 1, 1587912122, 1590218785, 15, 'normal'),
(16, 13, 'api/second/items/item', '房源详情', '', 1, 1589935977, 1590218774, 16, 'normal'),
(17, 13, 'api/second/items/building', '二手房楼盘详情', '', 1, 1589936011, 1590218764, 17, 'normal'),
(18, 21, 'api/second/favorates/addfavorate', '收藏二手房', '', 1, 1589936079, 1590203542, 18, 'normal'),
(19, 21, 'api/second/favorates/delfavorate', '删除收藏二手房源', '', 1, 1589945279, 1590203534, 19, 'normal'),
(20, 21, 'api/second/favorates/lists', '收藏房源列表', '', 1, 1590199173, 1608080628, 20, 'normal'),
(21, 0, 'api/second/favorates', '二手房收藏房源', '', 1, 1590201419, 1590202137, 21, 'normal'),
(22, 13, 'api/second/follow/list', '跟进列表', '', 1, 1590220493, 1590220493, 22, 'normal'),
(23, 0, 'api/newhouse/buidlingitems', '新房', '新房', 1, 1590730281, 1590730937, 23, 'normal'),
(24, 23, 'api/newhouse/buildingitems/list', '新房楼盘列表', '', 1, 1590730303, 1590730941, 24, 'normal'),
(25, 23, 'api/newhouse/buildingitems/labels', '标签值列表', '', 1, 1590731714, 1590731714, 25, 'normal'),
(26, 23, 'api/newhouse/buildingitems/pictures', '楼盘相册', '', 1, 1590732706, 1590733091, 26, 'normal'),
(27, 23, 'api/newhouse/buildingitems/item', '新房详情', '', 1, 1593764503, 1593764503, 27, 'normal'),
(28, 0, 'api/newhouse/favorate', '新房楼盘收藏楼盘', '', 1, 1597319539, 1597319559, 28, 'normal'),
(29, 28, 'api/newhouse/favorates/list', '新房楼盘收藏列表', '', 1, 1597319643, 1597319643, 29, 'normal'),
(30, 28, 'api/newhouse/favorates/delfavorate', '删除收藏新房楼盘', '', 1, 1597319740, 1597319740, 30, 'normal'),
(32, 28, 'api/newhouse/favorates/addfavorate', '收藏新房楼盘', '', 1, 1597320551, 1597320551, 32, 'normal'),
(33, 28, 'api/newhouse/favorates/checkfavorate', '新房楼盘是否收藏', '', 1, 1597321188, 1597321188, 33, 'normal'),
(34, 21, 'api/second/favorates/checkfavorate', '二手房源是否收藏', '', 1, 1597321233, 1597321233, 34, 'normal'),
(35, 4, 'api/user/agents', '获取推荐中介', '', 1, 1597321854, 1597321854, 35, 'normal');

-- --------------------------------------------------------

--
-- 表的结构 `mf_user_score_log`
--

CREATE TABLE `mf_user_score_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '变更积分',
  `before` int(10) NOT NULL DEFAULT '0' COMMENT '变更前积分',
  `after` int(10) NOT NULL DEFAULT '0' COMMENT '变更后积分',
  `memo` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员积分变动表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_user_score_log`
--

INSERT INTO `mf_user_score_log` (`id`, `user_id`, `score`, `before`, `after`, `memo`, `createtime`) VALUES
(1, 10, 0, 0, 0, '管理员变更积分', 1587909105),
(2, 10, 0, 0, 0, '管理员变更积分', 1587909138),
(3, 7, 0, 0, 0, '管理员变更积分', 1587910500),
(4, 11, 0, 0, 0, '管理员变更积分', 1588812400),
(5, 12, 0, 0, 0, '管理员变更积分', 1589078061),
(6, 12, 0, 0, 0, '管理员变更积分', 1589199431),
(7, 12, 0, 0, 0, '管理员变更积分', 1589199436),
(8, 12, 0, 0, 0, '管理员变更积分', 1589936200),
(9, 9, 0, 0, 0, '管理员变更积分', 1589936205),
(10, 9, 0, 0, 0, '管理员变更积分', 1589936208),
(11, 8, 0, 0, 0, '管理员变更积分', 1589936212),
(12, 9, 0, 0, 0, '管理员变更积分', 1589936216),
(13, 5, 0, 0, 0, '管理员变更积分', 1589936220),
(14, 1, 0, 0, 0, '管理员变更积分', 1589936224),
(15, 1, 0, 0, 0, '管理员变更积分', 1590730332),
(16, 13, 0, 0, 0, '管理员变更积分', 1590730726),
(17, 1, 0, 0, 0, '管理员变更积分', 1597025848),
(18, 13, 0, 0, 0, '管理员变更积分', 1608805431),
(19, 17, 0, 0, 0, '管理员变更积分', 1609807124);

-- --------------------------------------------------------

--
-- 表的结构 `mf_user_token`
--

CREATE TABLE `mf_user_token` (
  `token` varchar(50) NOT NULL COMMENT 'Token',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '会员ID',
  `createtime` int(10) DEFAULT NULL COMMENT '创建时间',
  `expiretime` int(10) DEFAULT NULL COMMENT '过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员Token表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_user_token`
--

INSERT INTO `mf_user_token` (`token`, `user_id`, `createtime`, `expiretime`) VALUES
('0383bd3a475cab14ece6bfd09f16fff69fc4580f', 13, 1608773154, 1611365154),
('0937ac9ffb89f01626b12507331ac8839953a2cf', 13, 1607996529, 1610588529),
('0aabb53b5d92f7c0c9385bb296a0dfc8f5823f57', 9, 1587892968, 1590484968),
('0b39aa262397d9afc5cd6fe0bc3c96c6fe494818', 12, 1589078445, 1591670445),
('0e8e9d33fdca0ec8504ddd3cedb1987f61282120', 12, 1590724119, 1593316119),
('12c6c35c0febbdc376832b209467824cc310d2db', 13, 1608021643, 1610613643),
('14d86ef09f704290f1d687a26dd898edd128af79', 12, 1596598434, 1599190434),
('168446e3258908fe5e1d9e27d282e08a99300697', 16, 1607587342, 1610179342),
('16eb6883067838934074f9f4a2825e09faa4204b', 13, 1608020368, 1610612368),
('19439645ddd73399a87ce93798619fb0f2846d97', 4, 1583832200, 1586424200),
('1f72efaa1d26bf4f882d849f53ab6d971683eb0d', 13, 1606876947, 1609468947),
('21a174add6f474ec40d23beff6ead931006ad5eb', 12, 1590592277, 1593184277),
('22e445af609ba2b4ef5276b8a1dc7fc97feede42', 14, 1590488490, 1593080490),
('23f7f410773679ee7475e069723725cfdda70c06', 13, 1606794454, 1609386454),
('25c62acb51beced6d20aa213d6c26c5aacb0c3f1', 12, 1590672215, 1593264215),
('26c2d85ecf3c4da7bbc4aed43208470980437d37', 13, 1608037790, 1610629790),
('28028f0514dd46732225183cb8464000bacfdf8f', 12, 1590299732, 1592891732),
('28dd77177b5eb128ed6a751eb90a6fdb68ec4d9e', 12, 1589078822, 1591670822),
('29653c03743dff0681bf8db02f5e15c455d835f4', 12, 1590308486, 1592900486),
('29bb98bbdf7225e2344a1ef0e9c06f73c605e19a', 12, 1590302818, 1592894818),
('2e696f13ceafb4da680aa64cceed0f6ef181f4b1', 12, 1590070007, 1592662007),
('2f1b732edc51d7ded86c91bb30f7894e64e08c9a', 12, 1590069650, 1592661650),
('30e942b6dc81bde680ffab716ddd1b4812ed5070', 1, 1563806708, 1566398708),
('32c855f885c0bed8003175f1448b2ca15f6a7299', 12, 1590671964, 1593263964),
('35160eacc3beacee2460f6eafc938df7bcaad7dc', 12, 1597510208, 1600102208),
('3650469c2b050a7ff1e85d9fcb01880f8fde7e19', 13, 1608711265, 1611303265),
('38c428b51b2dc850234fbddb408da49d912a36c7', 13, 1589787964, 1592379964),
('38c4ff134e7a8926a3638cd4f91732fc8d0dc6f3', 13, 1608794801, 1611386801),
('396d0d256a88985cb8d78e20b3d47226fdc7b51d', 13, 1590149195, 1592741195),
('3a62c686eafce27d0dd8c998f18b6f6c79621e31', 13, 1608007476, 1610599476),
('3b6953155b076abdfc13cb9bcff5f4126cd94efd', 13, 1607773026, 1610365026),
('3ca19f9cfdfc69cb7f05e8ff9c03763b3f57c99a', 12, 1594915291, 1597507291),
('3db5daa07737e4e8d19d138d1ae7ab34a1aa9645', 12, 1590589512, 1593181512),
('3e600471d435ea990a120fa9b2cd7187bcbcd54c', 6, 1587864898, 1590456898),
('40352bd8e36e30851869a3e47155a949f8657063', 12, 1589682826, 1592274826),
('40db3cd928cc5953670660b2b3bf5e309154f601', 12, 1590292142, 1592884142),
('4107b46f7e882c8b8f18ac704ea5416bd09eebf0', 12, 1589078785, 1591670785),
('412cdfd1c00f1669a4811f5051d499a3493d64a1', 4, 1583418851, 1586010851),
('44709b3a091fb1ddf49df7f8f8e7440e4943bca4', 13, 1608039930, 1610631930),
('44e15d6a2fc3919a6135202b2126cbd717e6e56d', 17, 1609758872, 1612350872),
('48cdcb535c4f2d9e0ff3551b624393f5f1857999', 13, 1606837964, 1609429964),
('4a200d9bf86e4e92b90e87e7e02ec62412a99d11', 13, 1608216178, 1610808178),
('4b1c254ef55ac6cee4f5247a27189675634959a8', 12, 1590594512, 1593186512),
('4bdbac13e44fda06a8e3b6ee03f22ecb0ab80a28', 13, 1608194406, 1610786406),
('4caf9dc09b9bb93defab8adc95fc6e6ee77a361d', 12, 1596682704, 1599274704),
('4d861f1cb85b44b4c84100a9990ebe4f5e6a9837', 12, 1590715014, 1593307014),
('4f3cf89f24d80c2bdd0070f060817b3ee0a64518', 12, 1590301927, 1592893927),
('5128ea5b347c013cb4a0541fb7fe98160b7d4248', 11, 1588812421, 1591404421),
('54552942e69cbf547229d9ac26287e0a85abe613', 13, 1606868621, 1609460621),
('552d757e043e97206dcb912ed681788f51b359f1', 13, 1606827530, 1609419530),
('554bb22271751225e16a0ac9e9f910593c3c9a02', 1, 1597025851, 1599617851),
('55ea1e963c21715df7358f45a28cbf9a0c81a26b', 13, 1607581233, 1610173233),
('575e461fff5bd2d6a65088c5b1bb1e6c2515b40a', 13, 1608019671, 1610611671),
('57bf4cb52663e6299b43f1100d54ce29e684ca9e', 13, 1608031647, 1610623647),
('59915ae95148e419b28a2bb7faa56d9d1b23a0e9', 12, 1590148769, 1592740769),
('5ab933e048c3fcf7e8b8392f0372a5a9debf25bd', 12, 1590677035, 1593269035),
('5cd40f3c95ee3ec8a9a796750016ce4c11a7ce2e', 13, 1608271760, 1610863760),
('5d09096191c7b38dffac1f2849703074414c900e', 13, 1606790990, 1609382990),
('5eb6d99552d0bf09fccfb4de82fa4bfbf6f0bc0e', 13, 1607955463, 1610547463),
('60d4d1d311bc0203709083e680593c84a8773e40', 13, 1607995581, 1610587581),
('656948851218feee11dff7f0d5a8cc6b2b566c83', 13, 1589976499, 1592568499),
('66c0f81d07fa7e6f935634dfc42a6ec585149be8', 4, 1583416469, 1586008469),
('68388bcbee3976051f871f646006af56edeeaa62', 13, 1608017040, 1610609040),
('687fe06e87b399f2f682852a9317d869b2f55017', 12, 1590568697, 1593160697),
('68813a772cf497a87b2e7286e0e2d7ce716b36a0', 13, 1608022909, 1610614909),
('68fc2319b88fa008d4079b996b419ba856d1b7ba', 13, 1608996687, 1611588687),
('6a5641ca5b97a89902b75a659d0e8728006d76cb', 13, 1595064726, 1597656726),
('6ad647031fa27f2986283e801d621c02763773e5', 8, 1587892817, 1590484817),
('6b762673820b51831aa17a693c706206e03339c7', 13, 1606892453, 1609484453),
('6efabd4a0d8463a4847d91c105aca5ee96b89819', 10, 1587892995, 1590484995),
('6fbec933609b0d04dc4471f6164d7a13d7321110', 12, 1596599928, 1599191928),
('7004449edda7ca24eaa9dbb969542b069db27a4b', 13, 1608106935, 1610698935),
('70490c1b2eeafee773b32f61aaf694642322d3f3', 13, 1607494835, 1610086835),
('705b352ebb13e19265fdffe60b1208a310f2b901', 12, 1590674671, 1593266671),
('70bd1d5005f16df6090ddccb4fc87c9ff1d03118', 13, 1608027115, 1610619115),
('715b7d7719b9daf7bf06574e467260bd9c602b4c', 13, 1607869927, 1610461927),
('719faa5c017a9bfed3cbacc79ae2a786e6b85106', 13, 1607079979, 1609671979),
('71cee4f6e5dc0bb1867996a4aa64beedfaf81a36', 13, 1608012840, 1610604840),
('732307c1818dcd595635f6c46feda8691a3666c3', 7, 1587893226, 1590485226),
('74dfc31f63a3af395825f8f47f4703a48eca0e72', 12, 1589787536, 1592379536),
('785f29ff31ab67a894b45f8c211eb2f4d217b6f9', 1, 1561190216, 1563782216),
('7b5fc3a432d4eed05c7a8a6aa320ac9a3d707051', 5, 1583653782, 1586245782),
('7bcc49fa11bdb34986420bad40fa0d5e8b7165e9', 13, 1608035594, 1610627594),
('7e46aa008ae37900067674ebdb9000f5b4936b96', 12, 1590291996, 1592883996),
('7e7c3cd89b3de79763e84228098fa3f3aac0c729', 13, 1609746137, 1612338137),
('8041874befd82bbc9b9302387aa49978511b448d', 15, 1596162469, 1598754469),
('81ee380a661c93774429083fd3ff554fd7e9e61c', 1, 1563785354, 1566377354),
('8481c85d02b412dae1ca6ee80d63bc5cbdbc9020', 7, 1587910347, 1590502347),
('8555f2430cfd26cb945f1149f41a682ed224c782', 12, 1590312731, 1592904731),
('8563d7c0085081c0e5d32241d46bcb83ff38bda8', 13, 1606974663, 1609566663),
('8723b27f3953ad768f69eabb2d7c9c5c1f4633ac', 12, 1596130535, 1598722535),
('8b91b153e1c2191c327449547d7f22cbb74bec6b', 13, 1608041286, 1610633286),
('8eb6193dc14828563c6a2e29eef12721ecedfe80', 13, 1607999477, 1610591477),
('8ef7ab6ef67ac443aefbd9262c2eb7532cf760f6', 13, 1607688487, 1610280487),
('9181e57d54bf678bca98b2558de299d011912554', 13, 1608171876, 1610763876),
('91d61d23df74369d09db2ff13f8dfce4c98d6397', 11, 1588811741, 1591403741),
('953340fe075938dfb893fa657729b79ff4e99d9a', 13, 1608028270, 1610620270),
('95530bedd11287685f9f6227f6c9a7cd834221ee', 13, 1608886661, 1611478661),
('9a0e03a4299f5e6036b55d2c0f64115ed8dfbeda', 12, 1590714110, 1593306110),
('9b3520b259a81544960a574b4d8f14bb634a7f06', 13, 1609378989, 1611970989),
('9de93df3feebd68cbfbbb8800d853aee8b60baed', 4, 1583654138, 1586246138),
('9e4d8266b2e1516be5c178088d34fb8e91caca53', 12, 1590301690, 1592893690),
('a1c1d4613a1b0f565260cc360204d1f5ca5ac984', 12, 1589467738, 1592059738),
('a3f00ef38eafe1e2142059bf1d27ed5924e2a324', 13, 1608003386, 1610595386),
('a5941e54b7da2eb2799429058ef0b6bf414d611c', 12, 1589078066, 1591670066),
('a6d21e38c4d8355b5ad298d2f12a534d8cd4abdb', 12, 1588813351, 1591405351),
('a7a4db875a6a8198d4dd34c0142681c0dec62ea9', 13, 1590488587, 1593080587),
('af7715ca5c6f44094500d67ad8b78b70a263321f', 13, 1608018775, 1610610775),
('b05dbe74cf9434aaac3876b8f6a6ce2643db5389', 12, 1597726733, 1600318733),
('b0b85dbf71e209905d8d8526666e5f1332fe27b6', 13, 1608801603, 1611393603),
('b1083fd4ed18bf0011b0c8e076ab7e641f2781e0', 13, 1608533647, 1611125647),
('b1f715f4a0a123b688448bb91daa4df416c47ca3', 13, 1608126150, 1610718150),
('b30e22a0476bff81833fe276c693d8e2e95bb269', 13, 1608013682, 1610605682),
('b4937d7f7d67c80a86f78d6ec141a8e9141edfe4', 13, 1608255594, 1610847594),
('b651c586689fb2b155c432fd1152539f2778b5b1', 12, 1590669469, 1593261469),
('b7207860dcdf5e6f3a28ff4e2a6ae03b2f2a64dc', 12, 1590149526, 1592741526),
('b8e0dc1e9c8b23c63cf9746100a8fee5f0f5eea6', 12, 1590677131, 1593269131),
('ba6d90bd1c76506f4064e7122ed2e65bf32685fb', 12, 1590149206, 1592741206),
('bb0a30ccb8f55d77faecda513ca53613510ec5d7', 12, 1594914376, 1597506376),
('bb7c23e4a11270a35176d991134efb6e3f4567bb', 12, 1590654740, 1593246740),
('bd3731531935f06de55f1a4c2c757580a74e7467', 13, 1608080456, 1610672456),
('bfa49aaa1c278fb652a46f80b547be343d75d022', 12, 1597507037, 1600099037),
('bff22a3999bc1c725ebcfd98262b088b235aec49', 12, 1589885136, 1592477136),
('c09fa59d6bba2db700d176b9eec4615a2372afaa', 13, 1606351206, 1608943206),
('c361fa74d236293ce461cfc96aefef1b26202940', 13, 1606704803, 1609296803),
('c47b54d14a080b4f4bff0a545d84e232f29970af', 13, 1608277315, 1610869315),
('cb735d9154ee793b13b4538ae779ff767f128b0a', 12, 1589680425, 1592272425),
('cd0f5b786d6ab7a85e8def01610f7c2c4e881861', 1, 1563785850, 1566377850),
('cd43b93002362d3b5953cba566febca8ebc3e176', 13, 1595062556, 1597654556),
('ce2f03e23c890973036d74949bcb48ea7f5b7ded', 1, 1563791050, 1566383050),
('cffe78a4555a87e9baf83e5d1f0bd77737324d35', 12, 1590592242, 1593184242),
('d1c51cfcad7929db4a3397a2cf62d9213ed70f3c', 13, 1606532473, 1609124473),
('d297373487ec27f9e06064743d9b3ac0a4a3a875', 13, 1608177699, 1610769699),
('d30463b44615229dd875ba9287f38a82a7ed7e40', 12, 1589467367, 1592059367),
('d4b4f06fa774e75b5dd64d7cde2ad2e2cce34334', 12, 1590675366, 1593267366),
('d68cc4eb7c5dbd0931ca6c7b913e39ee5cfeda97', 12, 1590589340, 1593181340),
('d724bcd751365c082b18f36f10b0e889f260db4b', 13, 1606975389, 1609567389),
('d925517613c42a62bdbb26811570a49508341c13', 13, 1609807469, 1612399469),
('d99571e6f835644edf8c151a28604f01aa49c35c', 1, 1563790360, 1566382360),
('da8501f9b539306a45c7457984cb1c1d4761e4e7', 12, 1589467297, 1592059297),
('db24df9b67b5e27a78e59472d3ad88f50373002d', 1, 1563870204, 1566462204),
('db5f65ce8f3508fe5e4c7c6ee0ecdba34b87986a', 12, 1589079009, 1591671009),
('e02eae74085bcb33103a47dd7af5b943180b4932', 13, 1607992568, 1610584568),
('e143afb56b588eba943ed0286cfde3d4d91f3dfc', 7, 1587887414, 1590479414),
('e31b4785adbfa20316b1c153caa1159ee0356b54', 12, 1590672121, 1593264121),
('e34f8b312622deb7982e9c509ffeaaa00e2ce552', 12, 1590592327, 1593184327),
('e5047e2967c69674fb3615026663b9bc089f280c', 12, 1589078703, 1591670703),
('eb1f4c1ea99ddd804b2f3add5a71237900e88c43', 12, 1597250859, 1599842859),
('ee24493503a40096ba0c82be128d0fd05f659bb9', 4, 1587863881, 1590455881),
('ef782fc851c5a2a91093026df8e580ba83732b31', 13, 1608805451, 1611397451),
('efee7127df5a0718f1ed272b29457f5db38dd6d2', 13, 1608026024, 1610618024),
('f2096bd68f497896f05d6bb057d812da84f10eda', 12, 1590714874, 1593306874),
('f2a672ccab2951d7c63e66f942ed52e8acff748b', 13, 1608215942, 1610807942),
('f60fd4572524ab4170683fc0f0f2cfa0b5ddcd38', 13, 1590419362, 1593011362),
('f6b7f60cfa1e1b05ee06b945989001fc6c7771f9', 13, 1608120719, 1610712719),
('f6b88ff2477b34da53e923df522642c42330d3a5', 13, 1608014209, 1610606209),
('f71551994c1e937885293818aaa8ecb619fd6914', 13, 1607878629, 1610470629),
('fa1f416dabcf317b58627811ebf1c0d506bafdb8', 13, 1609758914, 1612350914),
('fca460ab793514036bd644619e455cd3527e8545', 1, 1597319371, 1599911371);

-- --------------------------------------------------------

--
-- 表的结构 `mf_version`
--

CREATE TABLE `mf_version` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `oldversion` varchar(30) NOT NULL DEFAULT '' COMMENT '旧版本号',
  `newversion` varchar(30) NOT NULL DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) NOT NULL DEFAULT '' COMMENT '包大小',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '升级内容',
  `downloadurl` varchar(255) NOT NULL DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '强制更新',
  `createtime` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='版本表' ROW_FORMAT=COMPACT;

--
-- 转存表中的数据 `mf_version`
--

INSERT INTO `mf_version` (`id`, `oldversion`, `newversion`, `packagesize`, `content`, `downloadurl`, `enforce`, `createtime`, `updatetime`, `weigh`, `status`) VALUES
(1, '1.1.1,2', '1.2.1', '20M', '更新内容', 'https://www.fastadmin.net/download.html', 1, 1520425318, 0, 0, 'normal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mf_admin`
--
ALTER TABLE `mf_admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- Indexes for table `mf_admin_log`
--
ALTER TABLE `mf_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`username`);

--
-- Indexes for table `mf_area_items`
--
ALTER TABLE `mf_area_items`
  ADD PRIMARY KEY (`areaId`),
  ADD KEY `areaCode` (`areaCode`),
  ADD KEY `parentId` (`parentId`),
  ADD KEY `level` (`level`),
  ADD KEY `areaName` (`areaName`);

--
-- Indexes for table `mf_assessment_appraisers`
--
ALTER TABLE `mf_assessment_appraisers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_items`
--
ALTER TABLE `mf_assessment_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_pictures`
--
ALTER TABLE `mf_assessment_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_picturetype`
--
ALTER TABLE `mf_assessment_picturetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_prereport`
--
ALTER TABLE `mf_assessment_prereport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_propertynumtype`
--
ALTER TABLE `mf_assessment_propertynumtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_report`
--
ALTER TABLE `mf_assessment_report`
  ADD UNIQUE KEY `itemid` (`id`);

--
-- Indexes for table `mf_assessment_righttype`
--
ALTER TABLE `mf_assessment_righttype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_shares`
--
ALTER TABLE `mf_assessment_shares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_assessment_templates`
--
ALTER TABLE `mf_assessment_templates`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `mf_assessment_templatetype`
--
ALTER TABLE `mf_assessment_templatetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_attachment`
--
ALTER TABLE `mf_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_auth_copnumber`
--
ALTER TABLE `mf_auth_copnumber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_auth_cops`
--
ALTER TABLE `mf_auth_cops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_auth_group`
--
ALTER TABLE `mf_auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_auth_group_access`
--
ALTER TABLE `mf_auth_group_access`
  ADD UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `mf_auth_rule`
--
ALTER TABLE `mf_auth_rule`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`) USING BTREE,
  ADD KEY `pid` (`pid`),
  ADD KEY `weigh` (`weigh`);

--
-- Indexes for table `mf_category`
--
ALTER TABLE `mf_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `weigh` (`weigh`,`id`),
  ADD KEY `pid` (`pid`);

--
-- Indexes for table `mf_cert_loan_assessment`
--
ALTER TABLE `mf_cert_loan_assessment`
  ADD PRIMARY KEY (`assessment_id`);

--
-- Indexes for table `mf_cert_loan_bank`
--
ALTER TABLE `mf_cert_loan_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `mf_cert_loan_follow`
--
ALTER TABLE `mf_cert_loan_follow`
  ADD PRIMARY KEY (`follow_id`);

--
-- Indexes for table `mf_cert_loan_items`
--
ALTER TABLE `mf_cert_loan_items`
  ADD PRIMARY KEY (`loan_id`),
  ADD UNIQUE KEY `item_no` (`item_no`);

--
-- Indexes for table `mf_cert_loan_state`
--
ALTER TABLE `mf_cert_loan_state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `mf_cert_types`
--
ALTER TABLE `mf_cert_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `mf_config`
--
ALTER TABLE `mf_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `mf_ems`
--
ALTER TABLE `mf_ems`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `mf_house_landright`
--
ALTER TABLE `mf_house_landright`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_house_planpurpose`
--
ALTER TABLE `mf_house_planpurpose`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_house_structure`
--
ALTER TABLE `mf_house_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_invite`
--
ALTER TABLE `mf_invite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `mf_newhouse_building_follow`
--
ALTER TABLE `mf_newhouse_building_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_building_items`
--
ALTER TABLE `mf_newhouse_building_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_building_labels`
--
ALTER TABLE `mf_newhouse_building_labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_building_pictures`
--
ALTER TABLE `mf_newhouse_building_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_building_picturetype`
--
ALTER TABLE `mf_newhouse_building_picturetype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_building_policy`
--
ALTER TABLE `mf_newhouse_building_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_building_types`
--
ALTER TABLE `mf_newhouse_building_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_customer_follow`
--
ALTER TABLE `mf_newhouse_customer_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_customer_items`
--
ALTER TABLE `mf_newhouse_customer_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_customer_source`
--
ALTER TABLE `mf_newhouse_customer_source`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_department_items`
--
ALTER TABLE `mf_newhouse_department_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_department_logs`
--
ALTER TABLE `mf_newhouse_department_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_depart_cameras`
--
ALTER TABLE `mf_newhouse_depart_cameras`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `mf_newhouse_report_follow`
--
ALTER TABLE `mf_newhouse_report_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_report_items`
--
ALTER TABLE `mf_newhouse_report_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_report_joint`
--
ALTER TABLE `mf_newhouse_report_joint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_newhouse_report_state`
--
ALTER TABLE `mf_newhouse_report_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_rent_follow`
--
ALTER TABLE `mf_rent_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_rent_items`
--
ALTER TABLE `mf_rent_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `update_time` (`update_time`),
  ADD KEY `update_time_2` (`update_time`);

--
-- Indexes for table `mf_second_buildings`
--
ALTER TABLE `mf_second_buildings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_second_decoration`
--
ALTER TABLE `mf_second_decoration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_second_follow`
--
ALTER TABLE `mf_second_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_second_items`
--
ALTER TABLE `mf_second_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `update_time` (`update_time`),
  ADD KEY `update_time_2` (`update_time`);

--
-- Indexes for table `mf_sms`
--
ALTER TABLE `mf_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_test`
--
ALTER TABLE `mf_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_user`
--
ALTER TABLE `mf_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `mf_user_favorate`
--
ALTER TABLE `mf_user_favorate`
  ADD PRIMARY KEY (`fav_id`),
  ADD UNIQUE KEY `fav_id` (`fav_id`);

--
-- Indexes for table `mf_user_group`
--
ALTER TABLE `mf_user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_user_money_log`
--
ALTER TABLE `mf_user_money_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_user_rule`
--
ALTER TABLE `mf_user_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_user_score_log`
--
ALTER TABLE `mf_user_score_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mf_user_token`
--
ALTER TABLE `mf_user_token`
  ADD PRIMARY KEY (`token`);

--
-- Indexes for table `mf_version`
--
ALTER TABLE `mf_version`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `mf_admin`
--
ALTER TABLE `mf_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=40;
--
-- 使用表AUTO_INCREMENT `mf_admin_log`
--
ALTER TABLE `mf_admin_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=459;
--
-- 使用表AUTO_INCREMENT `mf_area_items`
--
ALTER TABLE `mf_area_items`
  MODIFY `areaId` int(20) NOT NULL AUTO_INCREMENT COMMENT '地区Id', AUTO_INCREMENT=3268;
--
-- 使用表AUTO_INCREMENT `mf_assessment_appraisers`
--
ALTER TABLE `mf_assessment_appraisers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `mf_assessment_items`
--
ALTER TABLE `mf_assessment_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号', AUTO_INCREMENT=52;
--
-- 使用表AUTO_INCREMENT `mf_assessment_pictures`
--
ALTER TABLE `mf_assessment_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;
--
-- 使用表AUTO_INCREMENT `mf_assessment_picturetype`
--
ALTER TABLE `mf_assessment_picturetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `mf_assessment_prereport`
--
ALTER TABLE `mf_assessment_prereport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `mf_assessment_propertynumtype`
--
ALTER TABLE `mf_assessment_propertynumtype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `mf_assessment_righttype`
--
ALTER TABLE `mf_assessment_righttype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_assessment_shares`
--
ALTER TABLE `mf_assessment_shares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `mf_assessment_templates`
--
ALTER TABLE `mf_assessment_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `mf_assessment_templatetype`
--
ALTER TABLE `mf_assessment_templatetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `mf_attachment`
--
ALTER TABLE `mf_attachment`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=221;
--
-- 使用表AUTO_INCREMENT `mf_auth_copnumber`
--
ALTER TABLE `mf_auth_copnumber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号', AUTO_INCREMENT=23;
--
-- 使用表AUTO_INCREMENT `mf_auth_cops`
--
ALTER TABLE `mf_auth_cops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `mf_auth_group`
--
ALTER TABLE `mf_auth_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- 使用表AUTO_INCREMENT `mf_auth_rule`
--
ALTER TABLE `mf_auth_rule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=665;
--
-- 使用表AUTO_INCREMENT `mf_category`
--
ALTER TABLE `mf_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- 使用表AUTO_INCREMENT `mf_cert_loan_assessment`
--
ALTER TABLE `mf_cert_loan_assessment`
  MODIFY `assessment_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号', AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_cert_loan_bank`
--
ALTER TABLE `mf_cert_loan_bank`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_cert_loan_state`
--
ALTER TABLE `mf_cert_loan_state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `mf_cert_types`
--
ALTER TABLE `mf_cert_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `mf_config`
--
ALTER TABLE `mf_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- 使用表AUTO_INCREMENT `mf_ems`
--
ALTER TABLE `mf_ems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `mf_house_landright`
--
ALTER TABLE `mf_house_landright`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号', AUTO_INCREMENT=4;
--
-- 使用表AUTO_INCREMENT `mf_house_planpurpose`
--
ALTER TABLE `mf_house_planpurpose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号', AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `mf_house_structure`
--
ALTER TABLE `mf_house_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号', AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `mf_invite`
--
ALTER TABLE `mf_invite`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- 使用表AUTO_INCREMENT `mf_newhouse_building_follow`
--
ALTER TABLE `mf_newhouse_building_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '跟进编号', AUTO_INCREMENT=6;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_building_items`
--
ALTER TABLE `mf_newhouse_building_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_building_labels`
--
ALTER TABLE `mf_newhouse_building_labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号', AUTO_INCREMENT=11;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_building_pictures`
--
ALTER TABLE `mf_newhouse_building_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_building_picturetype`
--
ALTER TABLE `mf_newhouse_building_picturetype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_building_policy`
--
ALTER TABLE `mf_newhouse_building_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '佣金政策编号', AUTO_INCREMENT=11;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_building_types`
--
ALTER TABLE `mf_newhouse_building_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_customer_follow`
--
ALTER TABLE `mf_newhouse_customer_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '跟进编号';
--
-- 使用表AUTO_INCREMENT `mf_newhouse_customer_items`
--
ALTER TABLE `mf_newhouse_customer_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_customer_source`
--
ALTER TABLE `mf_newhouse_customer_source`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_department_items`
--
ALTER TABLE `mf_newhouse_department_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '售楼部id', AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_department_logs`
--
ALTER TABLE `mf_newhouse_department_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_depart_cameras`
--
ALTER TABLE `mf_newhouse_depart_cameras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_report_follow`
--
ALTER TABLE `mf_newhouse_report_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_report_items`
--
ALTER TABLE `mf_newhouse_report_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_report_joint`
--
ALTER TABLE `mf_newhouse_report_joint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `mf_newhouse_report_state`
--
ALTER TABLE `mf_newhouse_report_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- 使用表AUTO_INCREMENT `mf_rent_follow`
--
ALTER TABLE `mf_rent_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '跟进编号', AUTO_INCREMENT=4;
--
-- 使用表AUTO_INCREMENT `mf_rent_items`
--
ALTER TABLE `mf_rent_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `mf_second_buildings`
--
ALTER TABLE `mf_second_buildings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37298;
--
-- 使用表AUTO_INCREMENT `mf_second_decoration`
--
ALTER TABLE `mf_second_decoration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- 使用表AUTO_INCREMENT `mf_second_follow`
--
ALTER TABLE `mf_second_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '跟进编号', AUTO_INCREMENT=372;
--
-- 使用表AUTO_INCREMENT `mf_second_items`
--
ALTER TABLE `mf_second_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=343181;
--
-- 使用表AUTO_INCREMENT `mf_sms`
--
ALTER TABLE `mf_sms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=66;
--
-- 使用表AUTO_INCREMENT `mf_test`
--
ALTER TABLE `mf_test`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `mf_user`
--
ALTER TABLE `mf_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=18;
--
-- 使用表AUTO_INCREMENT `mf_user_group`
--
ALTER TABLE `mf_user_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- 使用表AUTO_INCREMENT `mf_user_money_log`
--
ALTER TABLE `mf_user_money_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `mf_user_rule`
--
ALTER TABLE `mf_user_rule`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- 使用表AUTO_INCREMENT `mf_user_score_log`
--
ALTER TABLE `mf_user_score_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- 使用表AUTO_INCREMENT `mf_version`
--
ALTER TABLE `mf_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
