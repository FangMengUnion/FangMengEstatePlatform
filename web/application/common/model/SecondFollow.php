<?php

namespace app\common\model;

use think\Cache;
use think\Model;

/**
 * 二手房源
 */
class SecondFollow extends Model
{
    // 表名,不含前缀
    protected $name = 'second_follow';
    // 定义时间戳字段名
    protected $createTime = 'create_time';
   
    public function seconditem(){
        return $this->belongsTo('app\common\model\SecondItems', 'item_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    // public function admin()
    // {
    //     return $this->belongsTo('app\admin\model\Admin', 'add_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    // }
}