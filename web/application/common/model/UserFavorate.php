<?php

namespace app\common\model;

use think\Model;

class UserFavorate extends Model
{

    // 表名
    protected $name = 'user_favorate';
    // 自动写入时间戳字段
    // 定义时间戳字段名
    protected $createTime = 'create_time';
    // 追加属性
    protected $append = [
    ];

    public function seconditem(){
        return $this->belongsTo('app\common\model\SecondItems', 'target_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function buildingitem(){
        return $this->belongsTo('app\admin\model\newhouse\building\Items', 'target_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
