<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\library\Ems;
use app\common\library\Sms;
use fast\Random;
use think\Validate;

/**
 * 会员接口
 */
class User extends Api
{
    protected $noNeedLogin = ['login', 'mobilelogin', 'register', 'resetpwd', 'changeemail', 'third','getGroups','agents']; //changemobile
    protected $noNeedRight = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 会员中心
     */
    public function index()
    {
        $this->success('', ['welcome' => $this->auth->nickname]);
    }

    /**
     * 会员登录
     *
     * @param string $account  账号
     * @param string $password 密码
     */
    public function login()
    {
        $account = $this->request->request('account');
        $password = $this->request->request('password');
        if (!$account || !$password) {
            $this->error(__('Invalid parameters'));
        }
        $ret = $this->auth->login($account, $password);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 获取会员分组列表
     * 
     */
    public function getGroups(){
        $list = \app\common\model\UserGroup::all();
        foreach ($list as $row) {
            $row->visible(['id','name','status']);
        }
        $this->success(__('获取组信息操作成功'),$list);
    }
    /**
     * 手机验证码登录
     *
     * @param string $mobile  手机号
     * @param string $captcha 验证码
     */
    public function mobilelogin()
    {
        $mobile = $this->request->request('mobile');
        $captcha = $this->request->request('captcha');
        if (!$mobile || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        if (!Sms::check($mobile, $captcha, 'mobilelogin')) {
            $this->error(__('Captcha is incorrect'));
        }
        $user = \app\common\model\User::getByMobile($mobile);
        if ($user) {
            if ($user->status != 'normal') {
                $this->error(__('Account is locked'));
            }
            //如果已经有账号则直接登录
            $ret = $this->auth->direct($user->id);
        } else {
            $ret = $this->auth->register($mobile, Random::alnum(), '', $mobile, []);
        }
        if ($ret) {
            Sms::flush($mobile, 'mobilelogin');
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 注册会员
     *
     * @param string $username 用户名
     * @param string $password 密码
     * @param string $group_id 分组信息
     * @param string $email    邮箱
     * @param string $mobile   手机号
     * @param string $code   验证码
     * @param string $applicationdescription 申请说明
     */
    public function register()
    {
        $username = $this->request->request('username');
        $password = $this->request->request('password');
        $group_id = $this->request->request('group_id');
        $email = $this->request->request('email');
        $mobile = $this->request->request('mobile');
        $code = $this->request->request('code');
        $applicationdescription = $this->request->request('applicationdescription');

        if (!$username || !$password) {
            $this->error(__('Invalid parameters'));
        }
        // 密码复杂度
        if(!$this->valid_pass($password)){
            $this->error(__('密码要包含大、小写字母和数字'));
        }
        // 分组必填
        if(empty($group_id)){
            $this->error(__('分组必须填写'));
        }
        // 分组必须有效
        $groupModel = \app\common\model\UserGroup::get($group_id);
        if(!$groupModel){
            $this->error(__('分组无效'));
        }
        if($group_id>1 && empty($applicationdescription)){
            $this->error(__('填写申请说明（所在公司名称）'));
        }
        if ($email && !Validate::is($email, "email")) {
            $this->error(__('Email is incorrect'));
        }
        if ($mobile && !Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        $ret = Sms::check($mobile, $code, 'register');
        if (!$ret) {
            $this->error(__('Captcha is incorrect'));
        }
        $ret = $this->auth->register($username, $password, $email, $mobile, ['applicationdescription'=>$applicationdescription],'hidden',$group_id);
        if ($ret) {
            $data = ['userinfo' => $this->auth->getUserinfo()];
            $this->success(__('Sign up successful'), $data);
        } else {
            $this->error($this->auth->getError());
        }
    }


    /**
     * 密码复杂度校验,内部使用，一般不需要调用
     * 
     * @param string $candidate 要校验的密码
     * @return string 返回字符串
     */
    function valid_pass($candidate) {
        $r1='/[A-Z]/';  //uppercase
        $r2='/[a-z]/';  //lowercase
        $r3='/[0-9]/';  //numbers
        $r4='/[~!@#$%^&*()\-_=+{};:<,.>?]/';  // special char

        if(preg_match_all($r1,$candidate, $o)<1) {
            //echo "密码必须包含至少一个大写字母，请返回修改！<br />";
            return false;
        }
        if(preg_match_all($r2,$candidate, $o)<1) {
            //echo "密码必须包含至少一个小写字母，请返回修改！<br />";
            return false;
        }
        if(preg_match_all($r3,$candidate, $o)<1) {
           // echo "密码必须包含至少一个数字，请返回修改！<br />";
            return false;
        }
        // if(preg_match_all($r4,$candidate, $o)<1) {
        //     echo "密码必须包含至少一个特殊符号：[~!@#$%^&*()\-_=+{};:<,.>?]，请返回修改！<br />";
        //     return FALSE;
        // }
        if(strlen($candidate)<6) {
           // echo "密码必须包含至少含有6个字符，请返回修改！<br />";
            return false;
        }
        return true;
    }    
    /**
     * 注销登录
     */
    public function logout()
    {
        $this->auth->logout();
        $this->success(__('Logout successful'));
    }

    /**
     * 修改会员个人信息
     *
     * @param string $avatar   头像地址
     * @param string $username 用户名
     * @param string $nickname 昵称
     * @param string $bio      个人简介
     */
    public function profile()
    {
        $user = $this->auth->getUser();
        $username = $this->request->request('username');
        $nickname = $this->request->request('nickname');
        $bio = $this->request->request('bio');
        $avatar = $this->request->request('avatar', '', 'trim,strip_tags,htmlspecialchars');
        if ($username) {
            $exists = \app\common\model\User::where('username', $username)->where('id', '<>', $this->auth->id)->find();
            if ($exists) {
                $this->error(__('Username already exists'));
            }
            $user->username = $username;
        }
        $user->nickname = $nickname;
        $user->bio = $bio;
        $user->avatar = $avatar;
        $user->save();
        $this->success();
    }

    /**
     * 修改邮箱
     *
     * @param string $email   邮箱
     * @param string $captcha 验证码
     */
    public function changeemail()
    {
        $user = $this->auth->getUser();
        $email = $this->request->post('email');
        $captcha = $this->request->request('captcha');
        if (!$email || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::is($email, "email")) {
            $this->error(__('Email is incorrect'));
        }
        if (\app\common\model\User::where('email', $email)->where('id', '<>', $user->id)->find()) {
            $this->error(__('Email already exists'));
        }
        $result = Ems::check($email, $captcha, 'changeemail');
        if (!$result) {
            $this->error(__('Captcha is incorrect'));
        }
        $verification = $user->verification;
        $verification->email = 1;
        $user->verification = $verification;
        $user->email = $email;
        $user->save();

        Ems::flush($email, 'changeemail');
        $this->success();
    }

    /**
     * 修改手机号
     *
     * @param string $mobile   手机号
     * @param string $captcha 验证码
     */
    public function changemobile()
    {
        $user = $this->auth->getUser();
        if(empty($user)){
            $this->error('没有登陆');
        }
        $mobile  = $this->request->request('mobile');
        $captcha = $this->request->request('captcha');
        if (!$mobile || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if (!Validate::regex($mobile, "^1\d{10}$")) {
            $this->error(__('Mobile is incorrect'));
        }
        if (\app\common\model\User::where('mobile', $mobile)->where('id', '<>', $user->id)->find()) {
            $this->error(__('Mobile already exists'));
        }
        $result = Sms::check($mobile, $captcha, 'changemobile');
        if (!$result) {
            $this->error(__('Captcha is incorrect'));
        }
        $verification = $user->verification;
        $verification->mobile = 1;
        $user->verification = $verification;
        $user->mobile = $mobile;
        $user->save();

        Sms::flush($mobile, 'changemobile');
        $this->success();
    }

    /**
     * 第三方登录
     *
     * @param string $platform 平台名称
     * @param string $code     Code码
     */
    public function third()
    {
        $url = url('user/index');
        $platform = $this->request->request("platform");
        $code = $this->request->request("code");
        $config = get_addon_config('third');
        if (!$config || !isset($config[$platform])) {
            $this->error(__('Invalid parameters'));
        }
        $app = new \addons\third\library\Application($config);
        //通过code换access_token和绑定会员
        $result = $app->{$platform}->getUserInfo(['code' => $code]);
        if ($result) {
            $loginret = \addons\third\library\Service::connect($platform, $result);
            if ($loginret) {
                $data = [
                    'userinfo'  => $this->auth->getUserinfo(),
                    'thirdinfo' => $result
                ];
                $this->success(__('Logged in successful'), $data);
            }
        }
        $this->error(__('Operation failed'), $url);
    }

    /**
     * 重置密码
     * @ApiTitle    (重置密码)
     * @ApiMethod   (GET)
     * @ApiRoute    (/api/user/resetpwd/)
     * @param string $type  mobile或email
     * @param string $mobile      手机号
     * @param string $newpassword 新密码
     * @param string $captcha     验证码
     */
    public function resetpwd()
    {
        $type = $this->request->request("type");
        $mobile = $this->request->request("mobile");
        $email = $this->request->request("email");
        $newpassword = $this->request->request("newpassword");
        // 密码复杂度
        if(!$this->valid_pass($newpassword)){
            $this->error(__('密码要包含大、小写字母和数字'));
        }
    
        $captcha = $this->request->request("captcha");
        if (!$newpassword || !$captcha) {
            $this->error(__('Invalid parameters'));
        }
        if ($type == 'mobile') {
            if (!Validate::regex($mobile, "^1\d{10}$")) {
                $this->error(__('Mobile is incorrect'));
            }
            $user = \app\common\model\User::getByMobile($mobile);
            if (!$user) {
                $this->error(__('User not found'));
            }
            $ret = Sms::check($mobile, $captcha, 'resetpwd');
            if (!$ret) {
                $this->error(__('Captcha is incorrect'));
            }
            Sms::flush($mobile, 'resetpwd');
        } else {
            if (!Validate::is($email, "email")) {
                $this->error(__('Email is incorrect'));
            }
            $user = \app\common\model\User::getByEmail($email);
            if (!$user) {
                $this->error(__('User not found'));
            }
            $ret = Ems::check($email, $captcha, 'resetpwd');
            if (!$ret) {
                $this->error(__('Captcha is incorrect'));
            }
            Ems::flush($email, 'resetpwd');
        }
        //模拟一次登录
        $this->auth->direct($user->id);
        $ret = $this->auth->changepwd($newpassword, '', true);
        if ($ret) {
            $this->success(__('Reset password successful'));
        } else {
            $this->error($this->auth->getError());
        }
    }

        /**
     * @ApiTitle  (获取中介)
     * @ApiSummary (新房使用,暂时可以使用logintime倒排序获取最新登录的)
     * @ApiMethod (Get)
     * @ApiRoute  (/api/user/agents)
     * @ApiParams   (name="offset", type="integer", required=true, description="分页偏移值,0开始")
     * @ApiParams   (name="limit", type="integer", required=true, description="分页大小，默认15，最大50")
     * @ApiParams   (name="sort", type="string", required=true, description="排序字段,如: logintime")
     * @ApiParams   (name="order", type="string", required=true, description="排序顺序,asc或desc")
     * @ApiReturn ({
           'code' :'1',
           'msg':'返回成功',
           'data':{
               "total":1,
               "rows":[]
           } 
      })
     */
    public function agents(){
        $sort1   = $this->request->request('sort');
        $order1  = $this->request->request('order');
        if(empty($sort1)){
            $this->error('排序字段不能为空,可使用updatetime');
        }
        if(empty($order1)){
            $this->error('排序不能为空');
        }                
        $model = new \app\common\model\User;

        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $where1 = "group_id=2 and status='normal'";//2是经纪人分组
        list($where, $sort, $order, $offset, $limit) = $this->buildparams('');

        $total = $model
            ->where($where)
            ->where($where1)
            ->order($sort, $order)
            ->count();

        $list = $model
                ->where($where)
                ->where($where1)->orderRaw('rand()')
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

        foreach ($list as $key=>$row) {
            $row->visible(['nickname','username','mobile','email','avatar','gender']);
        }                
        $list = collection($list)->toArray();
    
        $ret = array("total" => $total, "rows" => $list);
        $this->success('查询中介操作成功',$ret);                

    }
}
