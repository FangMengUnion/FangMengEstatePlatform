<?php

namespace app\api\controller\newhouse;
use fast\Random;
use app\common\controller\Api;

/**
 * 新房接口
 */
class BuildingItems extends Api
{

    // 无需登录的接口,*表示全部
    protected $noNeedLogin = ['lists','labels','picturetype','pictures','item','getNewhouseItem'];
    // 无需鉴权的接口,*表示全部
    protected $noNeedRight = [];
    /**
     * Items模型对象
     * @var \app\admin\model\newhouse\building\Items
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\newhouse\building\Items;

    }
    /**
     * 获取新房楼盘列表
     *
     * @ApiTitle    (获取新房楼盘列表)
     * @ApiSummary  (可查参数 楼盘名称building_name building_id  )
     * @ApiMethod   (GET)
     * @ApiRoute    (/api/newhouse/building_items/lists)
     * @ApiParams   (name="search", type="string", required=false, description="模糊搜索")
     * @ApiParams   (name="offset", type="integer", required=true, description="分页偏移值,0开始")
     * @ApiParams   (name="limit", type="integer", required=true, description="分页大小，默认15，最大50")
     * @ApiParams   (name="sort", type="string", required=true, description="排序字段,如: updatetime")
     * @ApiParams   (name="order", type="string", required=true, description="排序顺序,asc或desc")
     * @ApiParams   (name="filter", type="string", required=false, description="查询字段，例: {'id':'56','building_name':'漕冲批发市场'}")
     * @ApiParams   (name="op", type="string", required=false, description="查询字段，例: {'id':'=','building_name':'LIKE'}")
     * @ApiReturnParams   (name="code", type="integer", required=true, sample="0")
     * @ApiReturnParams   (name="msg", type="string", required=true, sample="返回成功")
     * @ApiReturnParams   (name="data", type="object", sample="", description="扩展数据返回")
     * @ApiReturn   ({
         'code':'1',
         'msg':'返回成功',
         'data':{
            "total": 1,
            "rows": []
         }
        })
     */
    public function lists()
    {
        $sort1   = $this->request->request('sort');
        $order1  = $this->request->request('order');
        if(empty($sort1)){
            $this->error('排序字段不能为空,可使用updatetime');
        }
        if(empty($order1)){
            $this->error('排序不能为空');
        }
       
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
     
        list($where, $sort, $order, $offset, $limit) = $this->buildparams('building_name');

        $total = $this->model
            ->where($where)
            ->order($sort, $order)
            ->count();
            // var_dump($sort);die();
        $list = $this->model
                ->where($where)
                ->order('is_top','desc')
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select()
                ;
    
        $list = collection($list)->toArray();
        $result = array("total" => $total, "rows" => $list);

        $this->success('查询新房楼盘列表操作成功',$result);
    }


    /**
     * 获取房源详情
     * @ApiTitle  (获取房源详情)
     * @ApiSummary (新房楼盘使用)
     * @ApiMethod (Get)
     * @ApiRoute  (/api/newhouse/building_items/item)
     * @ApiParams   (name="ids", type="string", required=false, description="新房楼盘编号")
     * @ApiReturn ({
           'code' :'1',
           'msg':'返回成功',
           'data':{
               "total":1,
               "rows":[]
           } 
      })
     */
    public function item(){
        $ids = $this->request->request("ids");

        $ret = $this->getNewhouseItem($ids);
        if($ret==null){
            $this->error('楼盘信息不存在');
        }
        $user = $this->auth->getUser();
        if(empty($user)){
            $ret['isFavorate']=false;
        }else{
            $isFavorate = (new Favorates())->checkfavorate($ids);
            $ret['isFavorate'] = $isFavorate;
        }
        $this->success('查询房源详情操作成功',$ret);
    }
    /**
     * 查询一个房源
     */
    protected function getNewhouseItem($ids){
        $model = new \app\admin\model\newhouse\building\Items;

        $list = $model->where(['mf_newhouse_building_items.id'=>$ids])->select();
        if(empty($list)){
            return null;
        }
        
        $ret=['item'=>$list[0]];
        
        // TODO: 权限限制
        // 跟进信息
        $followList = \app\admin\model\newhouse\building\Follow::select(['item_id'=>$ids]);
        $ret['follow'] = $followList;

        return $ret;
    }
    /**
     * 获取标签列表
     * @ApiTitle    (获取标签列表)
     * @ApiMethod   (GET)
     * @ApiRoute    (/api/newhouse/building_items/labels)
     * @ApiReturnParams   (name="code", type="integer", required=true, sample="0")
     * @ApiReturnParams   (name="msg", type="string", required=true, sample="返回成功")
     * @ApiReturnParams   (name="data", type="object", sample="", description="扩展数据返回")
     * @ApiReturn   ({
         'code':'1',
         'msg':'返回成功',
         'data':{
            "total": 1,
            "rows": []
         }
        })
     */
    public function labels(){
        $list = \app\admin\model\newhouse\building\Labels::all();
        $list = collection($list)->toArray();
        $result = array("total" => sizeof($list), "rows" => $list);
        $this->success('获取楼盘标签列表操作成功',$result);
    }

    /**
     * 获取楼盘图片分类
     * @ApiTitle   (获取楼盘图片分类)
     * @ApiMethod  (GET)
     * @ApiRoute (/api/newhouse/building_items/picturetype)
     * @ApiReturn   ({
         'code':'1',
         'msg':'返回成功',
         'data':{
            "total": 1,
            "rows": []
         }
        })
     */
    public function picturetype(){
        $list = \app\admin\model\newhouse\building\Picturetype::all();
        $result = array("total" => sizeof($list), "rows" => $list);
        $this->success("获取楼盘图片类型操作成功",$result);
    }
    /**
     * 获取楼盘图片
     * @ApiTitle   (获取楼盘图片)
     * @ApiMethod  (GET)
     * @ApiParams  (name="ids", type="integer", required=true, description="楼盘id")
     * @ApiRoute (/api/newhouse/building_items/pictures)
     * @ApiReturn   ({
         'code':'1',
         'msg':'返回成功',
         'data':{
            "total": 1,
            "rows": []
         }
        })
     */
    public function pictures($ids){
        if(empty($ids)){
            $this->error('参数不正确');
        }
        $model = new \app\admin\model\newhouse\building\Pictures;
        $list = $model->with(['newhousebuildingpicturetype'])
            ->where(['building_id'=>$ids])
            ->select();
        $list = collection($list)->toArray();
        $result = array("total" => sizeof($list), "rows" => $list);
        $this->success('获取获取楼盘图片操作成功',$result);       
    }
}