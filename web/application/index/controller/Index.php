<?php

namespace app\index\controller;

use app\common\controller\Frontend;

class Index extends Frontend
{

    protected $noNeedLogin = ['index','book2'];
    protected $noNeedRight = '*';
   

    public function index()
    {
         return $this->view->fetch();
        //$this->redirect('/admin2020.php');
    }

    public function buildingshow()
    {
        //TODO: 加分页
        //list($where, $sort, $order, $offset, $limit) = $this->buildparams();
        $model = new \app\admin\model\newhouse\building\Items;
        $total = $model
          //      ->where($where)
            //    ->order($sort, $order)
                ->count();

        $list = $model
              //  ->where($where)
               // ->order($sort, $order)
                //->limit($offset, $limit)
                ->select();

     
        $all_labels=\app\admin\model\newhouse\building\Labels::all();
     
        // $list = collection($list)->toArray();
        $result = array("total" => $total, "rows" => $list);
        $this->view->assign("total",$total);
        $this->view->assign("row",$list);
        $this->view->assign("all_labels",$all_labels);
        return $this->view->fetch();
    }

    public function book2(){
       
        return $this->view->fetch();
    }
}
