<?php
namespace app\index\controller;

use app\common\controller\Frontend;

class Customer extends Frontend
{

    protected $noNeedLogin = '';
    protected $noNeedRight = '*';

    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\newhouse\customer\Items;
        $this->view->assign("sexRadioList", $this->model->getSexRadioList());
        $this->view->assign("marriageRadioList", $this->model->getMarriageRadioList());
        $this->view->assign("firstHouseRadioList", $this->model->getFirstHouseRadioList());
        $this->view->assign("socialSecurityRadioList", $this->model->getSocialSecurityRadioList());
        $this->view->assign("paymentMethodRadioList", $this->model->getPaymentMethodRadioList());

    }
    
    public function customeritemslist(){
        $model = new \app\admin\model\newhouse\customer\Items;
         //设置过滤方法
         $this->request->filter(['strip_tags']);
         if ($this->request->isAjax()) {
              //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
         }
        $total = $model
          //      ->where($where)
            //    ->order($sort, $order)
                ->count();

        $list = $model
              //  ->where($where)
               // ->order($sort, $order)
                //->limit($offset, $limit)
                ->select();

     
        // $list = collection($list)->toArray();
        $result = array("total" => $total, "rows" => $list);
        $this->view->assign("total",$total);
        $this->view->assign("row",$list);
        return $this->view->fetch();
    }

    public function customeritemsview(){
        $id = $this->request->request("id");
        $model = \app\admin\model\newhouse\customer\Items::get($id);
        $building_model = \app\admin\model\newhouse\building\Items::all($model->building_id);
        $this->view->assign('buildings',$building_model);
        $this->view->assign("row",$model);
        $this->view->assign("customerid",$id);
        $this->view->assign("stateList", $model->getStateList());
        // 报备记录
        $follow_model = new \app\admin\model\newhouse\report\Items;
        $list=$follow_model->with(['newhousebuilding'])->select();
        // var_dump($list);die();
        //$list = \app\admin\model\newhouse\report\Items::all(["create_userid"=>$this->auth->id,'customer_id'=>$id]);
        $this->view->assign("report_list",$list);
        return $this->view->fetch();
    }
    /**
     * 添加
     */
    public function customeritemsadd()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if(isset($params['want_area'])){
                    $array=explode('/',$params['want_area']);

                    if(sizeof($array)>0) $params['want_province']=$array[0];
                    if(sizeof($array)>1) $params['want_city']=$array[1];
                    if(sizeof($array)>2) $params['want_district']=$array[2];
                }
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    /// 报备同步写到报备表里
                    $buildings=$params->building_id;

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if(isset($params['want_area'])){
                    $array=explode('/',$params['want_area']);

                    if(sizeof($array)>0) $params['want_province']=$array[0];
                    if(sizeof($array)>1) $params['want_city']=$array[1];
                    if(sizeof($array)>2) $params['want_district']=$array[2];
                }

                
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);       
                    /// 报备同步写到报备表里
                    $report = new \app\admin\controller\newhouse\report\Items;
                    $report->sync($ids,$row->building_id);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
    ////////////////////
    public function customerfollowadd($ids = null)
    {
        $model=new \app\admin\model\newhouse\customer\Follow;
        if ($this->request->isAjax()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $result = $model->allowField(true)->save($params);
                  
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
}

?>