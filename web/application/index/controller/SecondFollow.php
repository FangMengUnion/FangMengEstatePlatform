<?php

namespace app\index\controller;

use think\Db;
use app\common\controller\Frontend;

class SecondFollow extends Frontend
{

    protected $noNeedLogin = '';
    protected $noNeedRight = '*';
    protected $selectpageFields = '*';
    
    
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\common\model\SecondFollow;
        $this->view->assign("stateList", (new \app\common\model\SecondItems)->getStateList());
    }
    /**
     * 跟进二手房源信息
     */
    public function add(){
        $item_id = $this->request->request('item_id');
        $state   = $this->request->request('state');
        $this->view->assign('item_id',$item_id);
        $this->view->assign('state',  $state);

        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
               // $params = $this->preExcludeFields($params);

                // if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                //     $params[$this->dataLimitField] = $this->auth->id;
                // }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    // if ($this->modelValidate) {
                    //     $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                    //     $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                    //     $this->model->validateFailException(true)->validate($validate);
                    // }
                    $params['create_userid'] = $this->auth->id;
                    $result = $this->model->allowField(true)->save($params);
                    /**
                     * 更改房源信息
                     */
                    $second_item_model =  \app\common\model\SecondItems::get($item_id);
                    $second_item_model->state = $this->model->state;
                    $second_item_model->update_userid=$this->auth->id;
                    // if($this->model->state==0){
                    //     //在售的往前更新跳
                    //     $second_item_model->update_time=time();
                    // }
                    // $second_item_model->update_cop_id = $this->auth->cop_id;
                    $second_item_model->update_ip  = $this->request->ip();
                    $second_item_model->update_time = time();
                    $second_item_model->save();
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
}