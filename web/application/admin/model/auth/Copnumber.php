<?php

namespace app\admin\model\auth;

use think\Model;


class Copnumber extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'auth_copnumber';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'tablename_text'
    ];
    

    
    public function getTablenameList()
    {
        return ['mf_assessment_prereport' => __('Mf_assessment_prereport')];
    }


    public function getTablenameTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['tablename']) ? $data['tablename'] : '');
        $list = $this->getTablenameList();
        return isset($list[$value]) ? $list[$value] : '';
    }




    public function authcops()
    {
        return $this->belongsTo('app\admin\model\auth\Cops', 'cop_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
