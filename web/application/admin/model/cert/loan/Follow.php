<?php

namespace app\admin\model\cert\loan;

use think\Model;


class Follow extends Model
{

    

    

    // 表名
    protected $name = 'cert_loan_follow';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function certloanstate()
    {
        return $this->belongsTo('app\admin\model\cert\loan\State', 'state', 'state_id', [], 'LEFT')->setEagerlyType(0);
    }
}
