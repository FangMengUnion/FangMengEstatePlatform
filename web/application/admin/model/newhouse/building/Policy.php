<?php

namespace app\admin\model\newhouse\building;

use think\Model;


class Policy extends Model
{

    

    //数据库
    protected $connection = 'database';
    // 表名
    protected $name = 'newhouse_building_policy';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function newhousebuildingitems()
    {
        return $this->belongsTo('app\admin\model\newhouse\building\Items', 'building_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function newhousebuildingtypes(){
        return $this->belongsTo('app\admin\model\newhouse\building\Types','type_id','id',[],'LEFT')->setEagerlyType(0);
    }
}
