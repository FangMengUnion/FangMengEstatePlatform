<?php

namespace app\admin\model\newhouse\department;

use think\Model;


class Cameras extends Model
{

    

    

    // 表名
    protected $name = 'newhouse_depart_cameras';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function authcops()
    {
        return $this->belongsTo('app\admin\model\auth\Cops', 'cop_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
