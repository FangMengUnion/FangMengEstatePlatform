<?php

namespace app\admin\model\newhouse;

use think\Model;


class Department extends Model
{

    

    

    // 表名
    protected $name = 'newhouse_department_items';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function authcops()
    {
        return $this->belongsTo('app\admin\model\AuthCops', 'cop_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function admin()
    {
        return $this->belongsTo('app\admin\model\Admin', 'create_userid', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
