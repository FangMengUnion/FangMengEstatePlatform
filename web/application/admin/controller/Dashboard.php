<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Config;

/**
 * 控制台
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{

    /**
     * 查看
     */
    public function index()
    {
        $seventtime = \fast\Date::unixtime('day', -7);
        $paylist = $createlist = [];
        for ($i = 0; $i < 7; $i++)
        {
            $day = date("Y-m-d", $seventtime + ($i * 86400));
            $createlist[$day] = mt_rand(20, 200);
            $paylist[$day] = mt_rand(1, mt_rand(1, $createlist[$day]));
        }
        $hooks = config('addons.hooks');
        $uploadmode = isset($hooks['upload_config_init']) && $hooks['upload_config_init'] ? implode(',', $hooks['upload_config_init']) : 'local';
        $addonComposerCfg = ROOT_PATH . '/vendor/karsonzhang/fastadmin-addons/composer.json';
        Config::parse($addonComposerCfg, "json", "composer");
        $config = Config::get("composer");
        $addonVersion = isset($config['version']) ? $config['version'] : __('Unknown');
        // 今日的时间戳
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));

        // 会员数
        $user_count = \app\common\model\User::count();
        $this->view->assign([
            'total_second'        => \app\admin\model\second\Items::count(),
            'totalbuilding'       => \app\admin\model\newhouse\building\Items::count(),
            'totalcustomer'       => \app\admin\model\newhouse\customer\Items::count(),
            'assessment_prereport' => \app\admin\model\assessment\Items::count(),
            'today_newhouse_customer'   => \app\admin\model\newhouse\customer\Items::where('createtime>='.$beginToday)->count(),
            'todaysecond'         => \app\admin\model\second\Items::where('add_time>=' . $beginToday)->count(),
            'today_assessment_prereport'       => \app\admin\model\assessment\Items::where('prereport_time>=' . $beginToday)->count(),
            'today_user_login_count'    => \app\admin\model\User::where('logintime>='.$beginToday )->count(),
            'unsettleorder'    => 0,
            'sevendnu'         => '0%',
            'sevendau'         => '0%',
            'paylist'          => $paylist,
            'createlist'       => $createlist,
            'addonversion'     => $addonVersion,
            'uploadmode'       => $uploadmode,
            'user_count'       => $user_count
        ]);

        return $this->view->fetch();
    }

}
