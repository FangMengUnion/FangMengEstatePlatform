<?php

namespace app\admin\controller\second;

use app\common\controller\Backend;
use think\Db;

/**
 * 二手房跟进
 *
 * @icon fa fa-circle-o
 */
class Follow extends Backend
{
    
    /**
     * Follow模型对象
     * @var \app\admin\model\second\Follow
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\second\Follow;
        $this->view->assign("stateList", $this->model->getStateList());
    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        $item_id = $this->request->request('item_id');
        $tmpwhere='1=1';
        if(!empty($item_id))
            $tmpwhere .= ' AND item_id=' . $item_id;

        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['seconditems','admin'])
                    ->where($where)
                    ->where($tmpwhere)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['seconditems','admin'])
                    ->where($where)
                    ->where($tmpwhere)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('seconditems')->visible(['address']);
				$row->getRelation('admin')->visible(['nickname']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $item_id = $this->request->request('item_id');
        $state   = $this->request->request('state');
        $this->view->assign('item_id',$item_id);
        $this->view->assign('state',  $state);

        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $params['create_userid'] = $this->auth->id;
                    $result = $this->model->allowField(true)->save($params);
                    /**
                     * 更改房源信息
                     */
                    $second_item_model =  \app\admin\model\second\Items::get($item_id);
                    $second_item_model->state = $this->model->state;
                    $second_item_model->update_userid=$this->auth->id;
                    if($this->model->state==0){
                        //在售的往前更新跳
                        $second_item_model->update_time=time();
                    }
                    $second_item_model->update_cop_id = $this->auth->cop_id;
                    $second_item_model->update_ip  = $this->request->ip();


                    $second_item_model->save();
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

}
