<?php

namespace app\admin\controller\assessment;

use app\common\controller\Backend;
use \think\Db;

/**
 * 评估标的
 *
 * @icon fa fa-circle-o
 */
class Items extends Backend
{
    
    /**
     * Items模型对象
     * @var \app\admin\model\assessment\Items
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\assessment\Items;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();


            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 未预评标的
     */
    public function noprereport()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where1="items.prereport_state=0";
            $total = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername'])
                    ->where($where)->where($where1)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername'])
                    ->where($where)->where($where1)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();


            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 待审核标的
     */
    public function prereport_notverify()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where1="items.prereport_state=1";
            $total = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername'])
                    ->where($where)->where($where1)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername'])
                    ->where($where)->where($where1)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();


            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 已审核预审
     */
    public function prereport_verified()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $where1="items.prereport_state>=2";
            $total = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername','prereportadmin'])
                    ->where($where)->where($where1)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['houselandright','housestructure','houseplanpurpose','admin','prereportappraisername','prereportadmin'])
                    ->where($where)->where($where1)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();


            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $params['create_time'] = time();  // 审核时间
                $params['create_userid'] = $this->auth->id;
                $params['state']=1;
                $params['prereport_version']=1;
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    // 同步pictures
                    $picture_controller=new \app\admin\controller\assessment\Pictures();
                    $pictures_params = $this->request->post("picture/a");
                    $ids = $this->model->id;
                    $picture_controller->sync($ids,$pictures_params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        //建筑结构
        $structurelist = \app\admin\model\house\Structure::all();
        $landrightlist = \app\admin\model\house\Landright::all();
        $planpurposelist=\app\admin\model\house\Planpurpose::all();

        $this->view->assign('structurelist',$structurelist);
        $this->view->assign('landrightlist',$landrightlist);
        $this->view->assign('planpurposelist',$planpurposelist);

        $assessmentPictureType = \app\admin\model\assessment\Picturetype::all();
        $this->view->assign('assessmentPictureType',$assessmentPictureType);

         /// 加载图片
         $picture_values=[];
         $pictures = [];
         
         foreach($assessmentPictureType as $key=>$value){
             $picture_values['v_' . $value->id] = ''; // 初始化
         }
         foreach($pictures as $key=>$value){
             if(!empty($picture_values['v_' . $value->typeid])) $picture_values['v_' . $value->typeid] .= ',';
             $picture_values['v_' . $value->typeid] .= $value->path;
         }
         /// 初始定位TODO:以实际值为准
         $cop = \app\admin\model\auth\Cops::get(1);//$this->auth->cop_id);
         $address = $cop->areaCode;

         $this->view->assign('address',$address);
         $this->view->assign('pictures', $picture_values);

        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
            return;
        }
        
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
                return;
            }
        }
        $assessmentPictureType = \app\admin\model\assessment\Picturetype::all();
        $this->view->assign('assessmentPictureType',$assessmentPictureType);

         /// 加载图片
         $picture_values=[];
         $pictures = \app\admin\model\assessment\Pictures::all(['item_id'=>$ids]);
         
         foreach($assessmentPictureType as $key=>$value){
             $picture_values['v_' . $value->id] = ''; // 初始化
         }
         foreach($pictures as $key=>$value){
             if(!empty($picture_values['v_' . $value->typeid])) $picture_values['v_' . $value->typeid] .= ',';
             $picture_values['v_' . $value->typeid] .= $value->path;
         }
         $this->view->assign('pictures', $picture_values);
        if ($this->request->isPost()) {
            if($row->prereport_state==2){
                //审核通过，禁止再次修改
                $this->error('已完成预评，禁止修改。');
                return;
            }
            $params = $this->request->post("row/a");
            if ($params) {
                $params['last_time'] = time();  // 审核时间
                $params['last_userid'] = $this->auth->id;

                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    // 记录图片
                    // 同步pictures
                    $picture_controller=new \app\admin\controller\assessment\Pictures();
                    $pictures_params = $this->request->post("picture/a");

                    $picture_controller->sync($ids,$pictures_params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        //建筑结构
        $structurelist = \app\admin\model\house\Structure::all();
        $landrightlist = \app\admin\model\house\Landright::all();
        $planpurposelist=\app\admin\model\house\Planpurpose::all();
      

        $this->view->assign('structurelist',$structurelist);
        $this->view->assign('landrightlist',$landrightlist);
        $this->view->assign('planpurposelist',$planpurposelist);

        $assessmentPictureType = \app\admin\model\assessment\Picturetype::all();
        $this->view->assign('assessmentPictureType',$assessmentPictureType);
        /// 加载图片
        $picture_values=[];
        $pictures = \app\admin\model\assessment\Pictures::all(['item_id'=>$ids]);
        
        foreach($assessmentPictureType as $key=>$value){
            $picture_values['v_' . $value->id] = ''; // 初始化
        }
        foreach($pictures as $key=>$value){
            if(!empty($picture_values['v_' . $value->typeid])) $picture_values['v_' . $value->typeid] .= ',';
            $picture_values['v_' . $value->typeid] .= $value->path;
        }
        $this->view->assign('pictures', $picture_values);
       
        return $this->view->fetch();
    }
}
