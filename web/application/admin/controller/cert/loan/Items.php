<?php

namespace app\admin\controller\cert\loan;

use app\common\controller\Backend;
use think\Db;
use fast\Random;

/**
 * 贷款业务
 *
 * @icon fa fa-circle-o
 */
class Items extends Backend
{
    
    /**
     * Items模型对象
     * @var \app\admin\model\cert\loan\Items
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\cert\loan\Items;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['certloanstate','certloanbank','admin'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['certloanstate','certloanbank','admin'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('certloanstate')->visible(['state_name']);
				$row->getRelation('certloanbank')->visible(['bank_name']);
				$row->getRelation('admin')->visible(['nickname']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }

        return $this->view->fetch();
    }
    /**
     * 生成最大编号,编号格式2020L000001
     */
    public function maxItemNo(){
        $sql = 'select max(item_no) from mf_cert_loan_assessment limit 1,1 where create_time=' . date('Y') + '-1-1 order by item_no desc ';
        $list = $this->model
            ->whereTime('create_time','year')
            ->order('item_no','desc')
            ->limit(1,1)
            ->find();
        if(empty($list)){
            return date('Y') . 'L000001';
        }else{
            $arr =explode('L', $list->item_no);
            $inc =  $arr[1]+1;
            $padding0 = str_pad($inc, 6, "0", STR_PAD_LEFT );
            $target = $arr[0] . 'L' . $padding0;
            return $target;
        }
    }
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    // 生成编号
                    $params['item_no']      = $this->maxItemNo();
                    $params['loan_id']      = str_replace('-','', Random::uuid());
                    $params['create_time']  = date('Y-m-d H:i:s', time());
                    $params['create_userid']= $this->auth->id;
                    $params['create_ip']    = $this->request->ip();
                    $params['last_time']    = date('Y-m-d H:i:s', time());
                    $params['last_userid']  = $this->auth->id;
                    $params['last_ip']      = $this->request->ip();
                    $params['state']        = 2;    // 临时用常量，2代表待签约，写在loan_state表里的

                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $stateList = \app\admin\model\cert\loan\State::all();
        $this->view->assign('stateList',$stateList);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $params['last_time']    = date('Y-m-d H:i:s', time());
                    $params['last_userid']  = $this->auth->id;
                    $params['last_ip']      = $this->request->ip();
                    $result = $row->allowField(true)->save($params);
                    // var_dump($result);die();
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $stateList = \app\admin\model\cert\loan\State::all();
        $this->view->assign('stateList',$stateList);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 查看
     */
    public function view($ids){
        $this->view->assign('ids',$ids);
        $row = \app\admin\model\cert\loan\Items::get($ids);
        $bank = \app\admin\model\cert\loan\Bank::get($row->sign_bank);
        $assessment = \app\admin\model\cert\loan\Assessment::get($row->assessment);

        $this->view->assign('row', $row);
        $this->view->assign('bank', $bank);
        $this->view->assign('assessment', $assessment);
        return $this->view->fetch();
    }
}
