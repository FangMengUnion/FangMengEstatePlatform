<?php

return [
    'Building_name' => '楼盘名称',
    'Alias_name1'   => '别名1',
    'Alias_name2'   => '别名2',
    'Second_count'  => '二手房数量',
    'Second_onsell' => '二手房在售数量',
    'Latitude'=>'纬度',
    'Longitude'=>'经度',
    'Areacode'=>'区域',
    'AreaName'=>'区域'
];
