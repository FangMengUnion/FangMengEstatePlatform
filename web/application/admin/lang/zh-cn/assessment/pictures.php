<?php

return [
    'Item_id'                         => '标的id',
    'Createtime'                      => '登记时间',
    'Create_userid'                   => '登记人',
    'Lasttime'                        => '最后时间',
    'Last_userid'                     => '最后修改',
    'Path'                            => '图片路径',
    'Typeid'                          => '图片类别',
    'Mem'                             => '说明',
    'Assessmentpicturetype.type_name' => '类型',
    'Assessmentpicturetype.sort'      => '排序'
];
