<?php

return [
    'Template_typeid'                      => '模板类型',
    'Cop_id'                               => '所属企业',
    'Template'                             => '模板',
    'Create_time'                          => '创建时间',
    'Create_userid'                        => '创建人',
    'Create_ip'                            => '创建IP',
    'Last_time'                            => '修改时间',
    'Last_userid'                          => '修改人',
    'Last_ip'                              => '修改IP',
    'Authcops.cop_name'                    => '企业名称',
    'Assessmenttemplatetype.template_type' => '模板名'
];
