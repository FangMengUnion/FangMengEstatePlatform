<?php

return [
    'Id'                                     => '佣金政策编号',
    'Building_id'                            => '楼盘编号',
    'Type_id'          => '房屋类型',
    'Price'                                  => '均价',
    'Commission_policy'                      => '佣金政策',
    'Createtime'                             => '创建时间',
    'Create_userid'                          => '创建人',
    'Newhousebuildingitems.building_name'    => '楼盘名称',
    'Newhousebuildingitems.average_price'    => '均价',
    'Newhousebuildingitems.contact_tel'      => '联系电话',
    'Newhousebuildingitems.work_begin'       => '接待时间起',
    'Newhousebuildingitems.work_end'         => '接待时间止',
    'Newhousebuildingitems.validate_begin'   => '有效期起',
    'Newhousebuildingitems.validate_end'     => '有效期止',
    'Newhousebuildingitems.address'          => '地址',
    'Newhousebuildingitems.lng'              => '地址经度',
    'Newhousebuildingitems.lat'              => '地址纬度',
    'Newhousebuildingitems.last_sell'        => '最新开盘',
    'Newhousebuildingitems.get_date'         => '交房',
    'Newhousebuildingitems.building_type'    => '类型',
    'Newhousebuildingitems.property_year'    => '产权',
    'Newhousebuildingitems.volume_ratio'     => '容积率',
    'Newhousebuildingitems.property_fee'     => '物业费',
    'Newhousebuildingitems.greening_rate'    => '绿化率',
    'Newhousebuildingitems.parking_spaces'   => '车位数量',
    'Newhousebuildingitems.house_quantity'   => '户数',
    'Newhousebuildingitems.developer'        => '开发商',
    'Newhousebuildingitems.property_company' => '物业公司',
    'Newhousebuildingitems.scene_person'     => '案场对接',
    'Newhousebuildingitems.scene_person_tel' => '案场对接电话',
    'Newhousebuildingitems.introduction'     => '楼盘介绍',
    'Newhousebuildingitems.reporting_policy' => '报备政策',
    'Newhousebuildingitems.on_selling'       => '在售房源介绍',
    'Newhousebuildingitems.createtime'       => '录入时间',
    'Newhousebuildingitems.create_userid'    => '录入人',
    'Newhousebuildingitems.updatetime'       => '修改时间',
    'Newhousebuildingitems.update_userid'    => '修改人',
    'Newhousebuildingitems.area'           => '区域',
    'Newhousebuildingtypes.type_name'        => '房屋类型'
];
