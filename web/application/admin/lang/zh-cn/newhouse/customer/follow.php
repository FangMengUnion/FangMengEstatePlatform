<?php

return [
    'Id'                                  => '跟进编号',
    'Customer_id'                         => '客户编号',
    'Follow_content'                      => '跟进内容',
    'Create_time'                         => '跟进时间',
    'Create_userid'                       => '跟进人',
    'State'                               => '跟进状态',
    'Newhousecustomeritems.customer_name' => '客户姓名'
];
