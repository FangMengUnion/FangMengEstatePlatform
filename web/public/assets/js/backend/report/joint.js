define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'report/joint/index' + location.search,
                    add_url: 'report/joint/add',
                    edit_url: 'report/joint/edit',
                    del_url: 'report/joint/del',
                    multi_url: 'report/joint/multi',
                    table: 'newhouse_report_joint',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'cratetime', title: __('Cratetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'report_item_id', title: __('Report_item_id')},
                        {field: 'alias_name', title: __('Alias_name')},
                        {field: 'alias_tel', title: __('Alias_tel')},
                        {field: 'create_userid', title: __('Create_userid')},
                        {field: 'relationship', title: __('Relationship'), searchList: {"未知":__('未知'),"夫妻":__('夫妻'),"父母":__('父母'),"子女":__('子女'),"亲戚":__('亲戚'),"朋友":__('朋友')}, formatter: Table.api.formatter.normal},
                        {field: 'newhousereportitems.id', title: __('Newhousereportitems.id')},
                        {field: 'newhousereportitems.customer_id', title: __('Newhousereportitems.customer_id')},
                        {field: 'newhousereportitems.building_id', title: __('Newhousereportitems.building_id')},
                        {field: 'newhousereportitems.createtime', title: __('Newhousereportitems.createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'newhousereportitems.verify_friend', title: __('Newhousereportitems.verify_friend')},
                        {field: 'newhousereportitems.create_userid', title: __('Newhousereportitems.create_userid')},
                        {field: 'newhousereportitems.prepare_visit', title: __('Newhousereportitems.prepare_visit')},
                        {field: 'newhousereportitems.updatetime', title: __('Newhousereportitems.updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'newhousereportitems.update_userid', title: __('Newhousereportitems.update_userid')},
                        {field: 'newhousereportitems.contents', title: __('Newhousereportitems.contents')},
                        {field: 'newhousereportitems.verify_userid', title: __('Newhousereportitems.verify_userid')},
                        {field: 'newhousereportitems.verify_mem', title: __('Newhousereportitems.verify_mem')},
                        {field: 'newhousereportitems.verify_time', title: __('Newhousereportitems.verify_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'newhousereportitems.verify_state', title: __('Newhousereportitems.verify_state')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});