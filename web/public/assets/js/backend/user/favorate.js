define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/favorate/index' + location.search,
                    add_url: 'user/favorate/add',
                    edit_url: 'user/favorate/edit',
                    del_url: 'user/favorate/del',
                    multi_url: 'user/favorate/multi',
                    table: 'user_favorate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'fav_id',
                sortName: 'fav_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'fav_id', title: __('Fav_id')},
                        {field: 'cop_id', title: __('Cop_id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'create_ip', title: __('Create_ip')},
                        {field: 'target', title: __('Target'), searchList: {"二手房出售房源":__('二手房出售房源'),"二手房出租房源":__('二手房出租房源'),"新房出售房源":__('新房出售房源'),"新房出租房源":__('新房出租房源'),"新房楼盘":__('新房楼盘')}, formatter: Table.api.formatter.normal},
                        {field: 'target_id', title: __('Target_id')},
                        {field: 'comment', title: __('Comment')},
                        {field: 'user.nickname', title: __('User.nickname')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});