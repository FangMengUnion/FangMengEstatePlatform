define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cert/loan/assessment/index' + location.search,
                    add_url: 'cert/loan/assessment/add',
                    edit_url: 'cert/loan/assessment/edit',
                    del_url: 'cert/loan/assessment/del',
                    multi_url: 'cert/loan/assessment/multi',
                    table: 'cert_loan_assessment',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'assessment_id',
                sortName: 'assessment_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'assessment_id', title: __('Assessment_id')},
                        {field: 'assessment_name', title: __('Assessment_name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});