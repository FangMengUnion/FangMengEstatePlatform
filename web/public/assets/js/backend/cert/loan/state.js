define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cert/loan/state/index' + location.search,
                    add_url: 'cert/loan/state/add',
                    edit_url: 'cert/loan/state/edit',
                    del_url: 'cert/loan/state/del',
                    multi_url: 'cert/loan/state/multi',
                    table: 'cert_loan_state',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'state_id',
                sortName: 'state_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'state_id', title: __('State_id')},
                        {field: 'state_name', title: __('State_name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});