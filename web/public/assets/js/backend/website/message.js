define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            Table.api.init({
                extend: {
                    index_url: 'website/message/index',
                    add_url: '',
                    edit_url: '',
                    del_url: 'website/message/del',
                    multi_url: '',
                    table: '',
                }
            });

            var table = $("#table");

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
						{field: 'state', checkbox: true,},
                        {field: 'id', title: __('ID')},
                        {field: 'xingming', title: __('Full name')},
                        {field: 'moblie', title: __('Telephone')},
                        {field: 'content', title: __('content')},
						{
                            field: 'createtime',
                            title: __('Release time'),
                            sortable: true,
                            formatter: Table.api.formatter.datetime,
                            operate: 'RANGE',
                            addclass: 'datetimerange'
                        },
                        {
                            field: 'operate',
                            width: "130px",
                            title: __('operation'),
                            table: table,
                            events: Table.api.events.operate,
                            formatter: Table.api.formatter.operate
                        },
                    ]
                ],
                search: true,
                showExport: false,
                searchFormVisible: false,

            });

            Table.api.bindevent(table);
			
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});