define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'newhouse/department/cameras/index' + location.search,
                    add_url: 'newhouse/department/cameras/add',
                    edit_url: 'newhouse/department/cameras/edit',
                    del_url: 'newhouse/department/cameras/del',
                    multi_url: 'newhouse/department/cameras/multi',
                    table: 'newhouse_depart_cameras',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'department_id', title: __('Department_id')},
                        {field: 'description', title: __('Description')},
                        {field: 'ip', title: __('Ip')},
                        {field: 'brand', title: __('Brand')},
                        {field: 'authcops.cop_name', title: __('Authcops.cop_name')},
                        {field: 'admin.nickname', title: __('Admin.nickname')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});