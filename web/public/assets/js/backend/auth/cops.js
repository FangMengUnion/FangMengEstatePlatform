define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'auth/cops/index' + location.search,
                    add_url: 'auth/cops/add',
                    edit_url: 'auth/cops/edit',
                    del_url: 'auth/cops/del',
                    multi_url: 'auth/cops/multi',
                    table: 'auth_cops',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'cop_name', title: __('Cop_name')},
                        {field: 'stamp_path', title: __('Stamp_path')},
                        {field: 'state', title: __('State')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, 
                        buttons:[
                            {
                                name:'view',
                                text:'部门',
                                title:'部门设置',
                                icon:'fa fa-building',
                                classname:'btn btn-xs btn-success btn-view btn-dialog',
                                url:'auth/cops/department'
                            }
                        ],
                        formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        department: function(){
            var data = [{
                    "time": "2019-08-27",
                    "id": 1,
                    "content": "内容1",
                    "pid": ""
                }, {
                    "time": "2019-08-27",
                    "id": 2,
                    "content": "内容2",
                    "pid": 1
                }, {
                    "time": "2019-08-27",
                    "id": 3,
                    "content": "内容3",
                    "pid": 1
                }, {
                    "time": "2019-08-27",
                    "id": 4,
                    "content": "内容4",
                    "pid": ""
                }, {
                    "time": "2019-08-27",
                    "id": 5,
                    "content": "内容5",
                    "pid": 2
                },
                {
                    "time": "2019-08-27",
                    "id": 6,
                    "content": "内容6",
                    "pid": ""
                },
                {
                    "time": "2019-08-27",
                    "id": 7,
                    "content": "内容7",
                    "pid": 6
                },
            ];

            var table = $("#table");

            table.bootstrapTable({
                data: data,
                idField: 'id',
                dataType: 'jsonp',
                columns: [{
                        field: 'time',
                        title: '时间',
                        width: 140
                    },
                    {
                        field: 'content',
                        title: '主要内容'
                    },
                ],

                //在哪一列展开树形
                treeShowField: 'time',
                //指定父id列
                parentIdField: 'pid',
                onResetView: function(data) {
                    //console.log('load');
                    table.treegrid({
                        initialState: 'collapsed', // 所有节点都折叠
                        // initialState: 'expanded',// 所有节点都展开，默认展开
                        treeColumn: 0,
                        // expanderExpandedClass: 'glyphicon glyphicon-minus',  //图标样式
                        // expanderCollapsedClass: 'glyphicon glyphicon-plus',
                        onChange: function() {
                            table.bootstrapTable('resetWidth');
                        }
                    });
                    //只展开树形的第一级节点
                    //$table.treegrid('getRootNodes').treegrid('expand');

                },
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});