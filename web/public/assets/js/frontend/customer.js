define(['jquery', 'bootstrap', 'frontend', 'form', 'template','table','citypicker','selectpage'], 
function ($, undefined, Frontend, Form, Template,table,citypicker,selectpage) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        customeritemsadd:function(){
           
            $("#btnReport").click(function(){
                console.log('report new win');
            });
        },
        customeritemslist:function(){
            // 查看客户
            $(".customer-item").click(function(){
                Frontend.api.open("customer/customeritemsview?id="+$(this).attr("data-id"),$(this).attr("data-name"),{
                    area:['1000px','600px']
                });
             });
            $(".customer-item").mouseover(function(){
                 $(".customer-item").removeClass("active");
                 $(this).addClass("active");
            }).mouseout(function(){
                $(".customer-item").removeClass("active");
            });
            // 新增客户
            $("#btnNewCustomer").click(function(){
                Frontend.api.open("customer/customeritemsadd",'新增客户',{
                    area:['800px','560px']
                });
            });
        },
        customeritemsview:function(){
            var form=$("form[role=form]");
            Form.events.selectpage(form);
          
            $("#btnFollow").click(function(){
                Frontend.api.open('customer/customerfollowadd?customerid=' + $(this).attr("data-customerid"),'新增跟进',{
                    area:['520px','340px']
                });
            });
            $("#btnNewReport").click(function(){
               $.post('report_items/add',form.serializeArray(),function(ret){
                    if(ret.code==1){
                        Toastr.success(ret.msg);
                        var html=Template('tplReportTableTr',{
                            buildingname:$("#c-building_id_text",form).val(),
                            time:(new Date()).getTime()
                        });
                        $("#tblReportList").append(html);
                    }else
                        Toastr.error(ret.msg);
               });
            });
        },
        customerfollowadd:function(){
            $("#btnSubmit").click(function(){
                console.log('click');
                $.post('customer/customerfollowadd',$("form").serializeArray(),function(){
                    console.log('add result');
                });
            });
        }
    };
    return Controller;
});