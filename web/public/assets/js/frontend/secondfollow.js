define(['jquery', 'bootstrap', 'frontend', 'form', 'template','table','backend','backend/maputils'], 
function ($, undefined, Frontend, Form, Template,Table,Backend,map) {
    var arr=['在售','停售','已售','无效'];
    var labels=['success','primary','info','default'];

    var Controller = {
        
        add:function(){
            Controller.api.bindevent();
        },
        
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    Toastr.success(data);//成功
                    console.log(data,ret);
                    return true;
                    
                }, function(data, ret){
                    console.log(data,ret);
                    return true;
                }, function(success, error){
                    console.log(data,ret);
                    //bindevent的第三个参数为提交前的回调
                    //如果我们需要在表单提交前做一些数据处理，则可以在此方法处理
                    //注意如果我们需要阻止表单，可以在此使用return false;即可
                    //如果我们处理完成需要再次提交表单则可以使用submit提交,如下
                    Form.api.submit(this, success, error);
                    return true;
                });
            }
        }
    };
    
    return Controller;
});