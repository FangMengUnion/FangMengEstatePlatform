# 房盟云台

## 一、介绍

一款开源的中介房源信息管理软件，主要框架基于FastAdmin开发

### 主要功能

- 二手房楼盘管理
- 二手出售房源管理
- 二手出售房源跟进
- 出租房源管理
- 出租房源跟进
- 新房楼盘管理
- 新房客户管理
- 新房报备
- 贷款业务管理
- 评估业务管理
- 支持多用户、多企业使用
- 支持web端/微信小程序端(微信小程序暂未达到开源条件，敬请期待)

## 二、开发设置

### 1. 编译css需要安装

```bash
cnpm install less@2.7.3
```

### 2. 编译scss

```bash
cd web/public/assets/less
lessc frontend.less ../css/frontend.css
```

### 3. 压缩

```bash
cd web
php think min -m all -r css
```

### 4. fastadmin源码改动记录

#### (1) application/common/library/Auth.php

```php
public function register($username, $password, $email = '', $mobile = '', $status,$extend = [])
// 163行改：
  'status'    => $status
```

### (2) 表mf_user 加字段

- applicationdescription 申请说明
- 会员组、会员权限修改内容

### (3) web/application/admin/view/login.html

- css 样式修改
- 添加 63行 用来显示系统名称

### (4) Adminlog.php selectpage加默认参数

```php
    public function selectpage($andor='and')
    {
        return parent::selectpage();
    }
```

### 5. sql记录
导出房源：
```sql
SELECT
    s.id as '编号',
    a.areaName as '区域',
    b.building_name as '小区',
    s.customer_name as '姓名',
    s.shi_count as '室',
    s.ting_count as '厅',
    s.floor as '层',
    s.floor_total as '总层',
    DATE_FORMAT(FROM_UNIXTIME(s.add_time), '%Y-%m-%d %H:%i:%s') as '添加时间',
    DATE_FORMAT(FROM_UNIXTIME(s.update_time), '%Y-%m-%d %H:%i:%s') as '更新时间',
    s.address as '地址',
    s.dong as '栋',
    s.shi as '室',
    s.build_year as '建成年代',
    s.area as '面积',
    s.mobile as '手机',
    s.unit_price as '单价',
    s.total_price as '总价（万）',
    CASE s.state
        WHEN 0 THEN '在售'
        WHEN 1 THEN '停售'
        WHEN 2 THEN '已售'
        WHEN 3 THEN '无效'
        ELSE '未知'
    END as '状态'
FROM
    mf_second_items s
LEFT JOIN
    mf_second_buildings b
ON
    s.building_id = b.id
LEFT JOIN
    mf_area_items a
ON
    s.areaCode = a.areaCode;
```

## 三、部署步骤

以 阿里云 CentOS8新的ECS系统为例，提前准备好已备案的域名

### 1. 安装部署

```bash
# 安装git
yum install git -y

# 安装宝塔
yum install -y wget && wget -O install.sh http://download.bt.cn/install/install_6.0.sh && sh install.sh
# 手工到阿里云安全组设置开启8888、443、80、888端口

# 克隆项目源码
cd /www/wwwroot
git clone https://gitee.com/FangMengUnion/FangMengEstatePlatform
cd /www/wwwroot/FangMengEstatePlatform/web

# 缓存文件夹设置
mkdir runtime
chown www:www /www/wwwroot/FangMengEstatePlatform/web -R

```

### 2. 以下到宝塔后台手工操作

- 按提示安装LNMP
- 新建网站，目录选择/www/wwwroot/FangMengEstatePlatform
- 到thinkphp5.cn下载核心文件，解压后把文件夹上传到 /www/wwwroot/FangMengEstatePlatform/web
- 在站点设置里绑定域名
- 在站点里设置网站目录、运行目录设置为/public
- 在站点设置->伪静态，选择 thinkphp
- 到数据库设置，导入仓库里database/目录下的数据库
- 修改 web/application/database-demp.php 里的数据库账号密码，并把文件名改为database.php
- 建议启用ssl功能

**这时在浏览器可以打开站点了**

### 3. 申请百度地图api

- 到 <http://lbsyun.baidu.com/> 创建浏览器端应用，获取一个AK。
- 到房盟云台后台：插件管理-地图位置(经纬度)选择插件，选择百度地图，并把百度地图KEY替换成自己申请的AK
- 到房盟云台后台：系统配置-字典配置-把baidu_ak值输入刚申请的AK

### 4. 更换logo和站点名称

- logo位置： /www/wwwroot/FangMengEstatePlatform/public/assets/img/logo.png
- favicon.ico 可自行上传更换(public/favicon.ico和assets/img/favicon.ico两个位置要替换)
- 站点名称，在后台： 常规管理-系统配置 设置即可。
- 同样在后台输入备案号。

### 5. 申请发送邮件的邮箱

- 到mail.163.com申请一个邮箱
- 登陆邮箱，在设置-POP3/SMTP/IMAP，开启 POP3/SMTP服务，记录授权密码
- 到系统后台，常规管理-系统设置-邮件配置，选择SMTP
- SMTP服务器 输入：smtp.163.com
- SMTP端口 输入：465
- SMTP用户名输入完整的邮件地址
- SMTP密码输入记录的授权密码
- SMTP验证方式选择SSL
- 发件人邮箱填写申请的邮箱地址

### 5. 其它设置

- 二手房列表排序字段: 在常规管理-系统配置-字典配置-配置分组-secod_sort，输入:add_time 录入时间排序 , update_time 更新时间排序
- 二手房是否显示楼栋: 在常规管理-系统配置-基础管理-二手房列表显示房号 ，选择显示或者不显示
- 私房是否显示楼栋：在常规管理-系统配置-基础管理-私房显示房号 ，选择显示或者不显示
- 二手房录入是否使用填表助手： 在常规管理-系统配置-基础管理-二手房录入助手 ，选择启用或者不启用
- 到后台-插件管理，手工安装插件（部分功能需要安装编辑器插件）

## 四、使用注意事项

- 前台用户自行注册，注册后账号处于锁定状态，需要管理员从后台修改会员账号的状态（从隐藏改为正常）
- 登记房源、楼盘等操作，在后台操作
- 前台用户用来浏览信息
- 后台可以在权限管理-管理员管理处新增账号，按需要分配权限
- 建议部署后把后台 admin2000.php 文件改成一个复杂的文件名

## 五、在线演示(已停用)

- [前台](http://148.70.110.25:82/index/user/login.html)
- [后台](http://148.70.110.25:82/admin2020.php)
- 前台测试账号： admin1 Mftest123456
- 后台测试账号： admin unionadmintest
- 技术讨论QQ群： 106579

## 六、版本更新记录

- v0.1 初始版本
- v1.0 可使用版本
- v1.1 后台房源分级，增加个人私房功能,mf_auth_cops加levels int(11),parent_id int(11), mf_second_items 增加view_level int(11)字段。
- v1.2 二手房界面排序
- v1.3 二手房显示楼栋号、 出租、后台可以控制是否使用二手房录入填表助手
- v1.4 删除官方插件
- v1.5 修复出租跟进，允许设置私房是否显示房号;二手房管理员可以看到私房号码;修复管理员日志列表
